#!/usr/bin/python
#
"""
Helper module/routines for time series consisting of dual nump.arrays (datevals,varvals),
where each datevals element is a datetime.datetime object, and each 
varvals element is a numpy.float. The two arrays must be 1-dimensional and 
have the same length.
The present module contains a few simple/handy routines, which can be used on this 
kind of time series.
"""

#
# TODO:
#  * Maybe? Join time series from different sources (measurements) to single time series, 
#      so that secondary time series can "fill holes" in primary.
#      Do we need some kind of averaging of warnings if they do not match?
#
#  * How to do pre- and post-processing of different modules?
#      Ie how do eg filter-module easy apply use of options set in tsSimple or tsNetcdf?
#      Does this even make sense?

#
# Module history:
#  2019-08-26 bjb 0.0.1: Initial version. Snip methods from filtwrapper.py and nctsValidate.py
#                        Add better unit conversion anf roundto_units
#                        Check for string instance py2 vs py3 (basestring vs str)
#  2019-08-29 bjb 0.0.2: Added methods to check & convert to (and from) fully equidistant
#  2019-09-03 bjb 0.0.3: Decrease amount of output on assert-functions
#                        Add method count_missing
#  2019-09-06 bjb 0.0.4: Add "main" to just show available options and exit.
#                        Options -h and --version are helpful in "script mode".
#  2019-09-09 bjb 0.0.5: Added option --remove_duplicate to control method for duplicate removal
#                         New method 'keep'=='keepall' for duplicates: Keep all (no removal)
#  2019-09-16 bjb 0.0.6: Add method+options to delete part of series (replace with NaNs)
#  2019-10-01 bjb 0.0.7: Added _verbloc() function, Bugfix get_equidistant_piece
#  2019-10-02 bjb 0.0.8: Add apply_options() for easy application of command line options
#                        Add join_series() to copy or fill NaN holes
#  2019-10-03 bjb 0.0.9: Add set_time_range() + command line options to cut/extend time series
#  2019-10-08 bjb 0.0.10: Added callbacks to validate argumens of type "pos int", "nonneg int" and "pos float"
#  2019-10-11 bjb 0.0.11: Serious bugfix on join_series
#  2019-10-14 bjb 0.0.12: Add remove_nonequidistant(), option delete_nonequidistant and related
#  2019-11-05 bjb 0.0.13: Add roundtime to 10sec, 5min, 10min, (1sec=s and 1min=m).
#                          Bugfix for averaging duplicates.
#  2019-11-05 bjb 0.0.14: Add option timestep_large and routine get_separate_piece
#  2019-11-07 bjb 0.0.15: Accept 1, 10, and 100 as prefix to SI-prefix in _unit_in_power_of_base
#  2019-11-07 bjb 0.0.16: Consolidate some computations for roundtime/roundto
#  2019-11-19 bjb 0.0.17: Accept degenerate 1-point "time series" as "consistent" and "monotone"
#  2019-01-30 bjb 0.0.18: Allow time roundto 20,30,60 min
#  2020-01-30 bjb 0.0.19: Add delete_lonely - get rid of part-series consisting of just a single point.
#  2020-01-31 bjb 0.0.20: Speed-up possibility for loop-over equidistant parts, ask for ipiece=-1
#  2022-02-10 bjb 0.0.21: Replace np.where with flatnonzero for speed.
#                          Add "has_missing" (and friends) and insert_data (moved from tsSpline).
#  2022-03-01 bjb 0.0.22: Small bugfix (spurious message) for outliers_to_nan
#  2022-03-01 bjb 0.0.23: Remove check for long in py3
#  2022-03-01 bjb 0.0.24: Add nanjoin_pieces: Join separate pieces with a single nan-element
#                         between each piece (for pretty-plotting)
#  2022-03-01 bjb 0.0.25: Bugfix: outliers_to_nan did not actually delete large values (introduced in 0.0.22)
#  2022-03-22 bjb 0.0.26: Add "select-align" to keep only those those elements, which "fall on" specific seconds-of-day
#  2022-05-10 bjb 0.0.27: Sparse*: Delete periods with many nans. Change structure for lonelies and simple degapping.
#  2022-05-11 bjb 0.0.28: Lonelies-implementation reverted to something like previous for a runtime speedup.
#  2022-05-17 bjb 0.0.29: Deal with degenerate (zero-length) time series in idxbreaks2enddates
#  2022-05-22 bjb 0.0.30: Additional methods for join_series.
#  2022-06-07 bjb 0.0.31: Small bugfix related to delete-period
#  2022-06-10 bjb 0.0.32: Ensure alignment of t0 in default scenario
#                         Use max-length in join_series.
#  2022-06-20 bjb 0.0.33: Add method to initialize a dateval,varval pair?
#  2022-08-04 bjb 0.0.34: Minor update. Add test for arg-is-float in a routine
#
version_info = (0, 0, 34)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

#
# TODO:


import numpy   as np
import datetime
import argparse
import math
import re

import sys
if sys.version_info[0] < 3:
    strtype=basestring
else:
    strtype=str

#
# This is for messing with time series
#
# Two time values are considered identical if difference (dt) is smaller than this.
# TODO: It could be considered to let this be an option.
epsi_time = np.float64(0.0001) # This should be relative to seconds
epsi_var  = np.float64(0.0001) # This should be in meters - similar for elevation

verbose=0

### HARDcodings
# Available round time possibilities:
roundtime_choices = ['us', 'ms', 's', '1sec', '10sec', '20sec', '30sec', 'm', '1min', '2min', '5min', '10min', '15min', '20min', '30min', '60min', 'h', '1hour' ] # 'm'=minute='1min', 'h'='1hour'='60min'
# Methods available for remove_duplicates:
rmdups_choices=['delete','keepfirst','keeplast','average','delete-differs','keep','keepall']


##
# Variables holding information from optional arguments:
time_step=None      # Time step size as specified from command line
roundtime=None      # Round read/loaded time-data to nearest ms, s or even m[inute].
#
rmdups_method=None # What to do with duplicates (when/if remove_duplicates is called)
#
# Possibly delete from and to time:
delete_from_time=None
delete_to_time=None
#
# Possible cut/extend (new time series ends):
set_start_time = None
set_stop_time  = None
# 
# Possibly align-selction:
select_align   = None
#
# Possibly delete values, which do not have "neighbours" in an otherwise piecewise 
# equidistant series, ie. remove pieces of length one.
min_group_size = 1
#
# Possibly delete values which are not equidistantly positioned
delete_nonequidistant = None
time_zero=None      # Time value for computation of noneq times.
timestep_large=None # 
argparse_description="""\
This module implements methods for netcdf time series - primarily for elevations. 
Calling as a script is not really helpful, but use -h to get available options. 
Also you may try \"python -c 'import tsSimple;help(tsSimple)'\" for more info on the module"""

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

# Simple helper function to define local verbosity:
def _verbloc(verb=None):
    if not verb is None:
        return verb
    global verbose
    return verbose

# Simple helper function to ensure that we have dt in a function:
def _dtloc(datevals,dt=None,verb=None):
    verbloc=_verbloc(verb)
    if not dt is None:
        return dt
    dt = get_timestep(datevals,verb=max(0,verbloc-1))
    dt = np.float64(dt)
    return dt

# This module should not be called in script mode, only imported.
# Thus, parser *MUST* be given.
# Note that we will try to deal with being called more than once,
# as there could be an implicit dependency unknown to the caller as eg:
#   filtwrappper->tsSimple->args_define_options()
#   filtwrappper->nctsValidate->tsSimple->args_define_options()
def args_define_options(parser=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    global roundtime_choices
    global roundtime
    parser.add_argument("--timestep","--dt",dest="timestep", metavar="<SECONDS>",
                        type=_check_positive_float,
                        help='specify known time step size in float seconds. '+
                        'Default is to estimate time step size from datevals vector. '+
                        'Used eg to test of equidistance  (t0+N*dt)')
    parser.add_argument("--roundtime",dest="roundtime", metavar="<TIMEUNIT>",
                        choices=roundtime_choices, #type=_checkroundunit,
                        help='round time values to unit on calls to round_time(). '+
                        ' Default is not to round. '+
                        'Allowed values are %s, where m means "minute"'%', '.join(roundtime_choices))
    parser.add_argument("--remove_duplicates",dest="rmdups", metavar="<METHOD>",
                        choices=rmdups_choices, 
                        help='how to deal with time series duplicates (non-unique time steps). '+
                        'Default is to delete duplicates. '+
                        'Allowed values are: %s'%', '.join(rmdups_choices))
    parser.add_argument("--delete_from_time",dest="deletefromtime", metavar="<EPOCHNN>",
                        type=_checkepochstr,
                        help='delete (replace with NaNs) time series data starting at this time')
    parser.add_argument("--delete_to_time",dest="deletetotime", metavar="<EPOCHNN>",
                        type=_checkepochstr,
                        help='delete (replace with NaNs) time series data ending at this time')
    parser.add_argument("--set_start_time",dest="setstarttime", metavar="<EPOCHNN>",
                        type=_checkepochstr,
                        help='enforce this to be the new first time value - cut or pad (with NaNs) if necessary')
    parser.add_argument("--set_stop_time",dest="setstoptime", metavar="<EPOCHNN>",
                        type=_checkepochstr,
                        help='enforce this to be the new last time value - cut or pad (with NaNs) if necessary')
    parser.add_argument("--select_align",dest="selectalign", metavar="<SECONDS>",
                        type=_check_positive_int, default=None,
                        help='Discard all elemenets, where the time doesn not "aligned to" (falls on modulo seconds of) this value. For instance --select_align=600 means keep only those elements, where the minute-of-the-hour is exactly 0, 10, 20, 30, 40, or 50.')
    parser.add_argument("--delete_lonely",dest="deletelonely", 
                        default=False,action='store_true',
                        help='delete lonely points from a piecewise equidistant time series, i.e. points with no neighbors / pieces of length one. Basically, this sets --min_group_size=2')
    parser.add_argument("--min_group_size",dest="mingroupsize",
                        default=1, type=_check_positive_int,
                        help='As delete_lonely, but delete all pieces/groups of less than NUM points. Default is 1, ie do not delete anything.')
    parser.add_argument("--delete_nonequidistant",dest="deletenonequidistant", 
                        default=False,action='store_true',
                        help='delete time series data not aligned to equidistant (t0+N*dt)')
    parser.add_argument("--timezero","--t0",dest="timezero",  metavar="<EPOCHNN>",
                        type=_checkepochstr,
                        help='time value used to define time steps in equidistance computations (t0+N*dt)')
    parser.add_argument("--timestep_large","--dtsep",dest="timestep_large", metavar="<FLOAT>",
                        type=_check_positive_float,
                        help='Value (float) defining a "large time" between to consequtive time steps. This is used to methods, which need to "separate" the time series into pieces')
    #
    # If nobody defined verbose, then I'll do it:
    try:
        parser.add_argument("-v", "--verbose", help="increase output verbosity (allow multiple)",
                            action='count', default=0)
    except:
        # Dont worry. Probably somebody else defined it.
        pass
    #
    _have_defined_options=True
    return parser
# Parse command line options
def args_parse_options(parsed_args):
    global verbose
    verbose=parsed_args.verbose
    global time_step
    if not parsed_args.timestep is None:
        time_step=parsed_args.timestep
        if verbose:
            print(" time step specified to %s seconds"%float(time_step))
    global time_zero
    if not parsed_args.timezero is None:
        time_zero=epochstr2datetime(parsed_args.timezero)
        if verbose:
            print(" time zero specified to "+str(time_zero))
    global roundtime
    if not parsed_args.roundtime is None:
        roundtime=parsed_args.roundtime
        if verbose:
            print(" rounding time values to nearest "+roundtime)
    if not parsed_args.rmdups is None:
        global rmdups_method
        rmdups_method=parsed_args.rmdups
        if verbose:
            print(" duplicates treated by "+rmdups_method)
    ##
    # Options to set-to-nan a particular part of the series:
    global delete_from_time
    delete_from_time=parsed_args.deletefromtime
    if verbose and not delete_from_time is None:
        print(" will delete time series starting at %s"%str(epochstr2datetime(delete_from_time)))
    global delete_to_time
    delete_to_time=parsed_args.deletetotime
    if verbose and not delete_to_time is None:
        print(" will delete time series ending at %s"%str(epochstr2datetime(delete_to_time)))
    # Sanity check if both are given:
    if (not delete_from_time is None) and (not delete_to_time is None):
        dstart=epochstr2datetime(delete_from_time)
        dstop=epochstr2datetime(delete_to_time)
        if dstop < dstart:
            raise AssertionError("Options delete_to_time (%s) cannot be before delete_from_time (%s)"%(dstop,dstart,))
    #
    global min_group_size
    if parsed_args.deletelonely:
        min_group_size=2
    if parsed_args.mingroupsize>1:
        min_group_size=parsed_args.mingroupsize
    if verbose and min_group_size>1:
        print(" will delete time series pieces of length less than %d (aka small groups without neighbors)"%min_group_size)
    #
    global delete_nonequidistant 
    delete_nonequidistant = parsed_args.deletenonequidistant
    if verbose and delete_nonequidistant:
        print(" will delete time steps which appear to be on-equidistant")
    ##
    # Options to cut/extend series (force new start/stop time)
    global set_start_time
    set_start_time=parsed_args.setstarttime
    if verbose and not set_start_time is None:
        print(" enforcing time series start to be at %s"%str(epochstr2datetime(set_start_time)))
    global set_stop_time
    set_stop_time=parsed_args.setstoptime
    if verbose and not set_stop_time is None:
        print(" enforcing time series stop to be at %s"%str(epochstr2datetime(set_stop_time)))
    # Sanity check if both are given:
    if (not set_start_time is None) and (not set_stop_time is None):
        dstart=epochstr2datetime(set_start_time)
        dstop=epochstr2datetime(set_stop_time)
        if dstop < dstart:
            raise AssertionError("Options set_stop_time (%s) cannot be before set_start_time (%s)"%(dstop,dstart,))
    # Time alignment selection:
    global select_align
    select_align = parsed_args.selectalign
    if verbose and not select_align is None:
        print(" Will discard time series elements not aligned to midnight modulo {} seconds".format(select_align))
    # Possibly define what is a "large time step"
    global timestep_large
    timestep_large = parsed_args.timestep_large
    if verbose and not timestep_large is None:
        print(" time series 'pieces' are defined by dt>=%s"%str(timestep_large))
    #
    return
#
# Callback to test if an epochstr-like option is valid, eg replace_{from,to}_time, set_{start,stop}_time
def _checkepochstr(epochstr):
    # This method will carp if the input is not valid epoch8-epoch14.
    # The return value (a datetime object) is not used here.
    try:
        epochstr2datetime(str(epochstr))
    except:
        raise argparse.ArgumentTypeError("%s is an invalid epoch-string" % str(epochstr))
    return epochstr

#
# Callback to verify that an argument is positive integer (natural number)-
# This is to be used with argparse for any N etc:
def _check_positive_int(val):
    try:
        ival=int(val)
    except:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value (not int)" % str(val))
    if ival <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value (not pos)" % str(val))
    return ival
# Alternative name for the above:
def _check_natural(val):
    return check_positive_int(val)

#
# Callback to verify that an argument is non-negative integer (natural number or zero)
# This is to be used with argparse for any N etc:
def _check_nonnegative_int(val):
    try:
        ival=int(val)
    except:
        raise argparse.ArgumentTypeError("%s is an invalid nonnegative int value (not int)" % str(val))
    if ival < 0:
        raise argparse.ArgumentTypeError("%s is an invalid nonnegative int value (is negative)" % str(val))
    return ival

#
# Callback to verify that an argument is a positive floating point number
def _check_positive_float(val):
    try:
        fval=np.float64(val)
    except:
        raise argparse.ArgumentTypeError("%s is an invalid positive float value (not float)" % str(val))
    if fval <= 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive float value (not pos)" % str(val))
    return fval
def _check_nonnegative_float(val):
    try:
        fval=np.float64(val)
    except:
        raise argparse.ArgumentTypeError("%s is an invalid nonnegative float value (not float)" % str(val))
    if fval < 0:
        raise argparse.ArgumentTypeError("%s is an invalid nonnegative float value (is negative)" % str(val))
    return fval



def _check_1comma(opt):
    """
    Assert that argument has exactly one comma
    """
    ncommas=opt.count(',')
    if ncommas==0:
        raise argparse.ArgumentTypeError("Comma missing in option '{}'".format(opt))
    if ncommas>1:
        raise argparse.ArgumentTypeError("Too many commas in option. Expected 1 but got {} in '{}'".format(ncommas,opt))
    return opt

def _check_1dash(opt):
    """
    Assert that argument has exactly one dash
    """
    ncommas=opt.count('-')
    if ncommas==0:
        raise argparse.ArgumentTypeError("Dash missing in option '{}'".format(opt))
    if ncommas>1:
        raise argparse.ArgumentTypeError("Too many dashes in option. Expected 1 but got {} in '{}'".format(ncommas,opt))
    return opt

def _check_int_comma_float(opt):
    """
    Check that argument is a string with comma-separated INT,FLOAT
    """
    opt=_check_1comma(opt)
    #opt=str(opt) # We may assume that the option is already a string
    nrep,vval=opt.split(',')
    try:
        nrep=int(nrep)
    except:
        raise argparse.ArgumentTypeError("Value before comma ('{}') does not evaluate to int in: '{}'".format(nrep,opt))
    try:
        vval=np.float64(vval)
    except:
        raise argparse.ArgumentTypeError("Value after comma ('{}') does not evaluate to float in: '{}'".format(vval,opt))
    return opt

def _check_int_comma_int(opt):
    """
    Check that argument is a string with comma-separated INT,INT
    """
    opt=_check_1comma(opt)
    #opt=str(opt) # We may assume that the option is already a string
    n1,n2=opt.split(',')
    try:
        n1=int(n1)
    except:
        raise argparse.ArgumentTypeError("Value before comma ('{}') does not evaluate to int in: '{}'".format(n1,opt))
    try:
        n2=int(n2)
    except:
        raise argparse.ArgumentTypeError("Value after comma ('{}') does not evaluate to int in: '{}'".format(n2,opt))
    return opt

def _check_float_comma_float(opt):
    """
    Check that argument is a string with comma-separated FLOAT,FLOAT
    """
    opt=_check_1comma(opt)
    #opt=str(opt) # We may assume that the option is already a string
    vval1,vval2=opt.split(',')
    try:
        vval1=np.float64(vval2)
    except:
        raise argparse.ArgumentTypeError("Value before comma ('{}') does not evaluate to float in: '{}'".format(vval,opt))
    try:
        vval2=np.float64(vval2)
    except:
        raise argparse.ArgumentTypeError("Value after comma ('{}') does not evaluate to float in: '{}'".format(vval,opt))
    return opt

def _check_epstr_dash_epstr(opt):
    """
    Check that argument is a string with comma-separated EPOCH,EPOCH
    """
    opt=_check_1dash(opt)
    ep1,ep2=opt.split('-')
    for epstr in [ep1, ep2]:
        _checkepochstr(epstr)
    return opt

# This just returns an "blank" datevals,varvals pair with no data initialization.
def empty(length,verb=None):
    datevals_none=np.empty(shape=(length,),dtype=datetime.datetime)
    varvals_none=np.empty(shape=(length,),dtype=float)
    return datevals_none,varvals_none

# Just test if a time series as a numpy.array pair seems "consistent"
# The varvals is optional. If it is missing, then we will only make a few test on the time array.
def is_consistent(datevals,varvals=None,verb=None):
    verbloc=_verbloc(verb)
    assert_datevals(datevals)
    if not is_datevals(datevals):
        return False
    if not is_monotone(datevals,verb=verbloc):
        print("Inconsistent time series: Datevals is not monotone, ie. some time steps are negative")
        return False
    #
    # If we also have varvals (elevations), then we make some of the same tests.
    if not varvals is None:
        if not is_varvals(varvals):
            return False
        if len(datevals) != len(varvals):
            print("Inconsistent time series: datevals[%d] and varvals[%d] have differing lengths"%(len(datevals),len(varvals),) )
            return False
    # All done - so it presumably looks OK
    return True
# Assert-version of is_consistent
def assert_consistent(datevals,varvals=None):
    if not is_consistent(datevals,varvals,verb=0):
        raise AssertionError("Not compliant time series")
    return


# Round input datevals to given accuracy ('10min', '5min', '1min', 'm', '10sec', '1sec', 's', 'ms' or 'us').
# If roundto is not given, then option --roundtime is used.
# If both are missing, then series is not rounded.
# Argument must be an array/list of datetime.datetime objects, as per
# given from the netCDF4 module.
# The vector is rounded in place, ie. the datevals argument is modified.
# In any case, the datevals (pointer to, really) vector is also returned 
# upon success.
# If roundto=False is passed, then it will NOT round no matter the command-line options.
def round_dates(datevals,roundto=None,verb=None):
    verbloc=verb
    if verb is None:
        verbloc=verbose
    if roundto is None:
        global roundtime
        roundto = roundtime
    if not roundto:
        if verbloc:
            print("No rounding unit set, no rounding performed")
        return datevals
    #
    # Convert integer (seconds) to something we know:
    if isinstance( roundto, int):
        if roundto in [1, 10, 20, 30]:
            roundto=str(roundto)+'sec'
        elif roundto in [1*60, 2*60, 5*60, 10*60, 15*60]:
            roundto=str(roundto/60)+'min'
        else:
            raise argparse.ArgumentTypeError("roundto as integer seconds is allowed only for a few specific values, not including '%s'" %
                                             str(roundto))
    #
    # Check that the roundto is set to something, which we understand:
    if not isinstance( roundto, strtype):
        raise argparse.ArgumentTypeError("%s is not a valid time unit for rounding (not a string)" % str(roundto))
    if not roundto in roundtime_choices:
        raise argparse.ArgumentTypeError("%s is not a valid time unit for rounding (unknown unit)" % roundto)
    #
    if roundto=='us':
        # micros is the basic resolution of datatime objects, 
        # so there is nothing to do in this case
        pass
    elif roundto=='ms':
        for idate in range(len(datevals)):
            # Procedure to round to nearest ms:
            # A: Add 0.5 ms (500us)
            # B: Round down to nearest ms but substracting micros modulo 1000 
            datevals[idate] = datevals[idate] + datetime.timedelta(microseconds=500)
            micromod=datevals[idate].microsecond % 1000
            datevals[idate] = datevals[idate] - datetime.timedelta(microseconds=micromod)
    elif roundto=='s' or roundto=='1sec' or roundto=='10sec' or roundto=='20sec' or roundto=='30sec':
        if roundto=='s':
            nsecs=1
        else:
            nsecs=int(roundto[:-3]) # Everything excepty last 3 characters 'sec'
        for idate in range(len(datevals)):
            # Procedure to round nearest N secs:
            # A: Add N/2 s = N*500ms
            # B: Find and store seconds%N
            # C: Round down by forcing us components to zero:
            # D: Subtract stored second-modulo
            datevals[idate] = datevals[idate] + datetime.timedelta(microseconds=nsecs*500)
            secondmod     = datevals[idate].second % nsecs
            datevals[idate] = datevals[idate].replace(microsecond=0) - datetime.timedelta(seconds=secondmod)
    elif roundto=='m'     or roundto=='1min'  or roundto=='2min'  or roundto=='5min'  or  \
         roundto=='10min' or roundto=='15min' or roundto=='20min' or roundto=='30min' or  \
         roundto=='60min' or roundto=='h'     or roundto=='1hour':
        if   roundto=='m':
            nmins=1
        elif roundto=='h' or roundto=='1hour':
            nmins=60
        else:
            nmins=int(roundto[:-3]) # Everything except last 3 characters 'min'
        for idate in range(len(datevals)):
            # Procedure to round nearest N min:
            #date_orig=datevals[idate] # debug/testing output line
            # A: Add N/2 min = N*30secs
            # B: Find and store min%N
            # C: Round down by forcing s and us components to zero
            # D: Subtract stored minute-modulo
            datevals[idate] = datevals[idate] + datetime.timedelta(seconds=nmins*30)
            minutemod       = datevals[idate].minute % nmins
            datevals[idate] = datevals[idate].replace(second=0,microsecond=0) - datetime.timedelta(minutes=minutemod)
            #if date_orig!=datevals[idate]:
            #    print(" rounding %s to %s"%(str(date_orig),str(datevals[idate]))) # debug/testing output line
    else:
        raise AssertionError("Unexpected unit for time rounding: '%s'"%str(roundto))
    return datevals

# Helper roputine - just check that the argument is valid for datevals:
def is_datevals(datevals,verb=None):
    #  Note: local verb/verbloc not used (yet) in this routine. Included for completeness.
    # Simple sanity check on input:
    if not isinstance( datevals, np.ndarray ):
        print("Inconsistent time series: datevals is not a numpy array: %s"%type(datevals))
        return False
    # It must be a single dimension, which we interpret to be time.
    tshape=datevals.shape
    if len(tshape) != 1:
        print("Inconsistent time series: expected datevals to be 1D but got %dD array"%len(tshape))
        return False
    for ii in range(len(datevals)):
        if not isinstance( datevals[ii], datetime.datetime ):
            print("Inconsistent element in time series: expected datetime but got datevals[%d]=%s"%(ii,type(datevals[ii])))
            return False
    return True

# Assert version of is_datevals
def assert_datevals(datevals):
    if not is_datevals(datevals,verb=0):
        raise AssertionError("Not compliant time series")
    return

# Helper routine - just check that the argument is valid for datevals:
def is_varvals(varvals,verb=None):
    #  Note: local verb/verbloc not used (yet) in this routine. Included for completeness.
    # Simple sanity check on input:
    if not isinstance( varvals, np.ndarray ):
        print("Inconsistent value series: varvals is not a numpy array: %s"%type(datevals))
        return False
    # It must be a single dimension, which we interpret to be time.
    vshape=varvals.shape
    if len(vshape) != 1:
        print("Inconsistent value series: expected varvals to be 1D but got %dD array"%len(vshape))
        return False
    for ii in range(len(varvals)):
        #PEB:        if isinstance( varvals[ii], np.float )   or \
        if                                          \
           isinstance( varvals[ii], np.float32 ) or \
           isinstance( varvals[ii], np.float64 ) :
            pass
        else:
            print("Inconsistent element in value series: expected numpy.float but got varvals[%d]=%s"%(ii,type(varvals[ii])))
            return False
    return True

# Assert version of is_varvals
def assert_varvals(varvals):
    if not is_varvals(varvals,verb=0):
        raise AssertionError("Not compliant value series")
    return

# Estimate time step from vector of datetime.datetime objects
# Returns either just the best-guess for time step (wantvector=False)
# or a 3-element vector (wantvector=True):
#  dt_median (best guess), dt_min and dt_max.
def get_timestep(datevals,roundto=None,dt=None,wantvector=False,verb=None):
    verbloc=_verbloc(verb)
    # 
    # This is just to be able to make quick returns and 
    # ease the calling procedure:
    if (not wantvector) and (not dt is None):
        return dt
    # 
    # Check if we can just return a value known from command line:
    global time_step # This is a command line option - unrelated to datevals(!)
    if (not wantvector) and (not time_step is None):
        if verbloc:
            print("get_timestep: Returning time step from command line option "+str(time_step))
        return time_step
    #
    # If we get here, then we will have to parse the datevals vector (the series time values)
    assert_datevals(datevals)
    if len(datevals)<2:
        print("Cannot find timestep unless there is at least two time values")
        raise AssertionError("Only single time step. Cannot compute DT.")
    #
    # Possibly round datevals:
    round_dates(datevals,roundto=roundto,verb=0)
    #
    # Series of time steps in seconds (numpy.float64):
    dt_secs = timedeltas_totalseconds(datevals)
    #
    # We will assume that most frequent value is at least close to
    # the true time step:
    dt_guess = np.median(dt_secs)
    dt_min   = np.min(dt_secs)
    dt_max   = np.max(dt_secs)
    if (verbloc   and not wantvector) or verbloc>3:
        print("  time series dt median ="+str(dt_guess))
    if (verbloc>1 and not wantvector) or verbloc>3:
        print("  time series dt min    ="+str(dt_min))
        print("  time series dt max    ="+str(dt_max))
    if not time_step is None:
        if verbloc>2:
            print("  time series dt        ="+str(time_step)+"  set from command line option")
        dt_guess=float(time_step)
    #
    # Return the found value:
    if wantvector:
        return dt_guess, dt_min, dt_max
    else:
        return dt_guess

# Determine if a time series is monotone increasing
# (varvals are not actually used)
def is_monotone(datevals,varvals=None,verb=None):
    verbloc=_verbloc(verb)
    if datevals.size==1:
        if verbloc>1:
            print("  time series is degenerate (one point), so in principle monotone")
        return True
    dt_med,dt_min,dt_max=get_timestep(datevals,wantvector=True,verb=verbloc)
    ismon=True
    addstr=""
    if dt_min<0:
        ismon=False
        addstr=" not"
    if verbloc>1:
        print("  time series is%s monotone"%addstr)
    return ismon
# Assert version of is_monotone (varvals are not actually used)
def assert_monotone(datevals,varvals=None):
    if not is_monotone(datevals,verb=0):
        raise AssertionError("Not monotone time series")
    return

# Determine if a time series is equidistant (ie all time steps are the same size).
# (varvals are not actually used)
def is_equidistant(datevals,varvals=None,dt=None,verb=None):
    verbloc=_verbloc(verb)
    dt_med,dt_min,dt_max=get_timestep(datevals,wantvector=True,verb=verbloc)
    iseqv=True
    addstr=""
    if not dt is None and dt_med != dt:
        iseqv=False
        addstr=" not"
    elif dt_min != dt_max:
        iseqv=False
        addstr=" not"
    if verbloc>1:
        print("  time series is%s equidistant"%addstr)
    return iseqv
# Assert version of is_equidistant 
def assert_equidistant(datevals,varvals=None):
    if not is_equidistant(datevals,varvals=varvals,verb=0):
        raise AssertionError("Not equidistant time series")
    return



# TODO: "collapse_nans" [c|sh]ould go here.

# join_series:
# Amend one time series with values from another.
# Join/merge two time series - Each time series is specified as a datevals,varvals pair.
# The two series MUST have the same time step size (dt), and the time values
# must coinside, i.e. not be staggered.
# In general, this copies data from series2 into the "main" series, but without 
# destroying/modifying the original data, ie. a new object is returned.
# The mandatory argument "method" determines the mode of operation of the routine:
#  joinnans/unan: NaN values of series2 are copied to series1, ie the resulting series 
#            nan-values will be the UNION of the nans of the incoming series. 
#  fixnans/inan:  Copy data from series2 into nan-holes of the original series.
#            The resulting series will have nan-values only where both the 
#            incoming series have nan-values - ie the INTERSECTION of the 
#            nan values of the incoming series.
#  In any case no non-nan values of the original series are modified
#  
join_method_choices=['joinnans', 'nanunion',       # Copy NaNs from secondary to primary time series
                     'fixnans', 'nanintersection', # Fill primary NaNs with secondary series data
                     'diffnans',                   # Non-nan values from series 2, where #1 has nans
                     'differ',                     # Values from series 2, which differ from same time in #1
                 ]
def join_series(datevals,varvals,datevals2,varvals2,method,verb=None):
    verbloc=_verbloc(verb)
    # Sanity checks:
    assert_consistent(datevals=datevals, varvals=varvals)
    assert_consistent(datevals=datevals2,varvals=varvals2)
    if datevals2.size>1:
        dt  = get_timestep(datevals, verb=0)
        dt2 = get_timestep(datevals2,verb=0)
        if dt != dt2:
            raise AssertionError("Cannot join series with differing time steps. %s=dt!=dt2=%s"%(str(dt),str(dt2)))
    #
    if not method in join_method_choices:
        raise AssertionError("Illegal method=%s. Allowable methods include: %s"%(method,', '.join(join_method_choices),))
    if verbloc:
        print("Joining two series using method=%s"%method)
    #
    # Start by finding total range min(start),max(end) and adjust as necessary.
    # It is cruxial to do this first.
    date0=min(datevals[0] ,datevals2[0])
    dateN=max(datevals[-1],datevals2[-1])
    # Set range ends. If they already match, the original is just returned:
    datevals,varvals  =set_time_range(datevals, varvals, starttime=date0,stoptime=dateN,verb=verbloc)
    datevals2,varvals2=set_time_range(datevals2,varvals2,starttime=date0,stoptime=dateN,verb=verbloc)
    #
    # Modify the primary series to be fully equidistant.
    # If it is already fully equidistant, then we may get a ref back - and in that case we should 
    # make a copy instead, such that we do not later on modify the original data.
    if is_equidistant(datevals,verb=0):
        datevals=datevals.copy()
        varvals =varvals.copy()
    else:
        datevals,varvals=piecewise_to_fully_equidistant(datevals=datevals,varvals=varvals,dt=dt)
    #
    # For the secondary, will not actually modify the data, so we do not have to 
    # keep track of whether result is a full copy or just a new ref to the same data.
    # Make secondary series equidistant as well
    # (this may result in just the same arrays given back, but that is not a problem)
    datevals2,varvals2=piecewise_to_fully_equidistant(datevals=datevals2,varvals=varvals2,dt=dt)
    # 
    # Sanity check.
    # At this point, the two datvals arrays should be identical, i.e. with the same 
    # size and the same content. Otherwise something is wrong with the above logic:
    if datevals.size!=datevals2.size:
        raise AssertionError("Something is wrong with the size of the dateval arrays (%d != %d). Debug necessary."%(datevals.size,datevals2.size,))
    # So the size is OK. Compute the difference, and count for how many elements that difference is NOT zero
    ndiffers=np.sum((datevals-datevals2)!=datetime.timedelta(0))
    if ndiffers != 0:
        print("Something is wrong with the content the dateval arrays (they differ at %s elements)."%str(ndiffers))
        print("This could mean that the time series are staggered (which is not allowed). Otherwise something is wrong with the code.")
        raise AssertionError("Something is wrong with the content the dateval arrays (they differ). Debug may be necessary.")
    #
    # Preample now done: The time series are in a single compatible format.
    # At this point we can start copying stuff around.
    # If we are verbose, then make some accounting to write stuff later.
    if verbloc:
        nnans1before=count_missing(varvals, verb=0)
        nnans2before=count_missing(varvals2,verb=0)
    if   method=='joinnans' or method=='nanunion':
        # Use secondary time series NaNs as "badness" in the primary:
        idx2cpy=np.flatnonzero(np.isnan(varvals2))
        varvals[idx2cpy]=np.nan
    elif method== 'fixnans' or method=='nanintersection':
        # Copy secondary values in whereever the primary has NaN:
        idx2cpy=np.flatnonzero(np.isnan(varvals))
        varvals[idx2cpy]=varvals2[idx2cpy]
    elif method=='diffnans':
        # We pretty much just want NaNs back, with values in select places.
        # Create alternative return array:
        varsback=np.empty_like(varvals,dtype=varvals.dtype)
        varsback.fill(np.float64('NaN'))
        # Now take the #2 values for all #1 nans (as per 'fixnans'):
        idx2cpy=np.flatnonzero(np.isnan(varvals))
        varsback[idx2cpy]=varvals2[idx2cpy]
        # Let varvals be the updated array:
        varvals=varsback
    elif method=='differ':
        # As per diffnans:
        varsback=np.empty_like(varvals,dtype=varvals.dtype)
        varsback.fill(np.float64('NaN'))
        # Now find all the differs. This will include any index where either element is NaN,
        # so some NaN values will be copied onto Nans, but that is not a problem.
        # Beware that values in array#1 may be overwrtten by nans from #2.
        idx2cpy=np.flatnonzero(varvals!=varvals2)
        varsback[idx2cpy]=varvals2[idx2cpy]
        # Let varvals be the updated array:
        varvals=varsback
    #
    # Possibly make more accounting and write result:
    if verbloc:
        nnans1after=count_missing(varvals, verb=0)
        ncopied=idx2cpy.size
        if   nnans1after==nnans1before:
            print(" copying %d elements from secondary array resulted in no change in #nans. There are still %d nans"%(ncopied,nnans1after))
        elif nnans1after>nnans1before:
            # This *must* be for nanunion.
            print(" copying %d nans from secondary array resulted in %d new nans in varvals (increasing from %d to %d)"%
                  (ncopied,nnans1after-nnans1before,nnans1before,nnans1after))
        else:
            # This means fewer nans, ie nanintersection
            print(" copying %d elements from secondary array resulted in %d fewer nans in varvals (decreased from %d to %d)"%
                  (ncopied,nnans1before-nnans1after,nnans1before,nnans1after))
    #
    # All done: Return the joined time series. Note that the datevals may have changed,
    # as they are now converted to fully equidistant.
    return datevals,varvals


# Very related to "join_series", this routine copies data from src to target time series 
# whereever the two series have coinsiding times and the source have actual (non-NaN) data.
def insert_data(datevals_src,varvals_src,
                datevals_tgt,varvals_tgt, 
                verb=None):
   verbloc=_verbloc(verb)
   # TODO: Make some assertion tests?
   if verbloc>2:
      print(" Asserting that source series is monotone")
   assert_monotone(datevals_src,varvals_src)
   if verbloc>2:
      print(" Asserting that target series is monotone")
   assert_monotone(datevals_tgt,varvals_tgt)
   #
   # If we need to do actual work, then make a copy, so we do not clubber the original
   datevals_rein = datevals_tgt.copy()
   varvals_rein  = varvals_tgt.copy()
   #
   # Keep track of how many entries we copy over:
   ncopied=0
   #
   # We need to run over each time series in full length:
   nsrc=datevals_src.shape[0]
   ntgt=datevals_tgt.shape[0]
   isrc=0
   itgt=0
   while( isrc<nsrc and itgt<ntgt):
      # Run through both time series, always stepping the one, which is behind.
      # This will assume that a runding has been made so that datevals may evaluate as equal:
      if   datevals_src[isrc] == datevals_tgt[itgt]:
         #print "EQUAL",datevals_src[isrc],datevals_tgt[itgt]
         if not np.isnan(varvals_src[isrc]):
            varvals_rein[itgt]=varvals_src[isrc]
            ncopied += 1
         isrc += 1
         itgt += 1
      elif datevals_src[isrc] <  datevals_tgt[itgt]:
         #print "PISRC",datevals_src[isrc],datevals_tgt[itgt]
         isrc += 1
      else: # datevals_src[isrc] >  datevals_tgt[itgt]:
         #print "PITGT",datevals_src[isrc],datevals_tgt[itgt]
         itgt += 1
   if verbloc>0:
      if ncopied>0:
         print(" Inserted %d values from original time series into target series"%ncopied)
      else:
         print(" No data were re-inserted from original time series into spline approximation")
   #
   return datevals_rein,varvals_rein


# This method simply counts the number of missing-value elements in the varvals array.
# The datevals array is allowed but ignored - just to streamline 
# calling different methods.
def count_missing(varvals,datevals=None,fillval=None,verb=None):
    verbloc=_verbloc(verb)
    nmiss=None
    if fillval is None or np.isnan(fillval):
        try:
            nmiss=np.count_nonzero(np.isnan(varvals))
        except:
            nmiss=np.count_nonzero(np.isnan(np.asarray(varvals,dtype=type(varvals[0]))))
    else:
        nmiss=np.count_nonzero(varvals==fillval)
    # If we get here, then there is stuff to delete:
    if verbloc:
        if fillval is None or np.isnan(fillval):
            misstr="nan"
        else:
            misstr=str(fillval)
        print("  data series has %d missing (%s) values"%(nmiss,misstr))
    return nmiss

# Alternate name for count_missing.
# datavals here are just for show - they are not used or modified.
def count_nans(datevals,varvals,fillval=None,verb=None):
    return count_missing(varvals=varvals,fillval=fillval,verb=verb)

#
# If we do not need the count, but just a true/false for a missing/nan value,
# then we may use "in"
def has_missing(varvals,datevals=None,fillval=None,verb=None):
    verbloc=_verbloc(verb)
    hasmiss=False
    if fillval is None or np.isnan(fillval):
        try:
            if True in np.isnan(varvals):
                hasmiss=True
        except:
            if True in np.isnan(np.asarray(varvals,dtype=type(varvals[0]))):
                hasmiss=True
    else:
        if fillval in varvals:
            hasmiss=True
    # If we get here, then there is stuff to delete:
    if verbloc:
        if fillval is None or np.isnan(fillval):
            misstr="nan"
        else:
            misstr=str(fillval)
        if hasmiss:
            hasstr="some"
        else:
            hasstr="no"
        print("  data series has [{}] missing (%s) values"%(hasstr,misstr))
    return hasmiss

# Alternate name for has_missing.
# datavals here are just for show - they are not used or modified.
def has_nans(datevals,varvals,fillval=None,verb=None):
    return has_missing(varvals=varvals,fillval=fillval,verb=verb)

# Assert-version for no missing variables in time series (varvals ONLY):
def assert_no_missing(varvals,fillval=None):
    if has_missing(varvals,fillval=fillval,verb=0):
        global verbose
        if verbose:
            # This is justfor possible print/log:
            count_missing(varvals,fillval=fillval)
        raise AssertionError("Time series (varvals) contains missing values")
    return

# Return an array where the elements are true if the varval is
# missing/nan and false if the value is "ok".
# This is sometimes handy, eg for computing average of non-missing etc.
def is_missing(varvals,datevals=None,fillval=None,verb=None):
    verbloc=verb
    if verb is None:
        verbloc=verbose
    if fillval is None or np.isnan(fillval):
        try:
            is_nan=np.isnan(varvals)
        except:
            is_nan=np.isnan(np.asarray(varvals,dtype=type(varvals[0])))
    else:
        is_nan=(varvals==fillval)
    return is_nan

# Get the actual indices of all missing values in an array.
# This will (normally) be a much shorter array than the full.
# Beware, that it is in general not simple to "invert" this array 
# to get all the non-missing value indices.
# Use is_missing to do the heavy lifting
def idx_missing(varvals,datevals=None,fillval=None,verb=None):
    # First get a full array of bools for missing vals:
    is_nan=is_missing(varvals=varvals,datevals=datevals,fillval=fillval,verb=verb)
    # Then get the indices (where) of the true values:
    return np.flatnonzero(is_nan)

# Get the actual indices of all NON-missing values in an array.
def idx_notmissing(varvals,datevals=None,fillval=None,verb=None):
    # First get a full array of bools for missing vals:
    is_notnan=~is_missing(varvals=varvals,datevals=datevals,fillval=fillval,verb=verb)
    # Then get the indices (where) of the true values:
    return np.flatnonzero(is_notnan)

# Remove time series elements where the variable is NaN
# Subroutine will return a copy of the shortened datevals,varvals.
# If the time series is equidistant, then this can be seen as the 
# inverse of piecewise_to_fully_equidistant
def remove_nans(datevals,varvals,fillval=None,verb=None):
    verbloc=_verbloc(verb)
    if fillval is None or np.isnan(fillval):
        try:
            idx2del=np.flatnonzero(np.isnan(varvals))
        except:
            idx2del=np.flatnonzero(np.isnan(np.asarray(varvals,dtype=type(varvals[0]))))
    else:
        idx2del=np.flatnonzero(varvals==fillval)
    if len(idx2del)==0:
        if verbloc:
            print("No missing-/nan-values to delete")
        # Dont make a copy. Just send back the original (pointers)
        return datevals,varvals
    # If we get here, then there is stuff to delete:
    if verbloc:
        print(" Removing %d NaN-element(s) from time series"%len(idx2del))
    return np.delete(datevals,idx2del),np.delete(varvals,idx2del)
#
# Remove_missing is just the same as remove_nans
def remove_missing(datevals,varvals,fillval=None,verb=None):
    return remove_nans(datevals,varvals,fillval=fillval,verb=verb)
#
# This is yet another name for remove_nans:
def fully_to_piecewise_equidistant(datevals,varvals,fillval=None,verb=None):
    return remove_nans(datevals,varvals,fillval=fillval,verb=verb)

# Remove duplicate data in time series, 
# ie elements, where both time and elev values match.
# It is assumed that the time series is already sorted.
# The optional third argument specifies what to do with duplicates times, 
# where the variable (elevation) value is not identical.
#  duplicates: 'delete', 'keepfirst', 'average'
# It is assumed that there are no missing_value elements in the array.
def remove_duplicates(datevals,varvals,epsitime=None, epsivar=None, duplicates=None,verb=None):
    if verb is None:
        verbloc=verbose
    else:
        verbloc=verb
    assert_consistent(datevals,varvals)
    # Get information on time series to see how to proceed:
    dt_med,dt_min,dt_max = get_timestep(datevals,wantvector=True)
    #
    if epsitime is None:
        global epsi_time
        epsitime=epsi_time
    if epsivar is None:
        global epsi_var
        epsivar=epsi_var
    #
    if duplicates is None:
        duplicates=rmdups_method
    if duplicates is None:
        duplicates='delete'
    global rmdups_choices
    if not duplicates in rmdups_choices:
        raise argparse.ArgumentTypeError("%s is not a valid method for duplicate time steps (unknown method)" % str(duplicates))
    #
    if duplicates in ['keep', 'keepall', 'noremove', ]:
        # Dont actually remove duplicates:
        if verbloc:
            print("Not actually removing duplicates (method %s)"%duplicates)
        return datevals,varvals
    #
    # If dt_min=dt_max>0, then we are already OK with nothing to do:
    if dt_min > epsitime:
        if verbloc>1:
            print("Time series has no duplicate times - no duplicates to remove")
        return datevals,varvals
    # 
    dt_secs = timedeltas_totalseconds(datevals)
    # 
    # These are all the indices, where the NEXT datetime is "too close".
    # Really, we will keep these (possibly with modified varvalues) and
    # delete the NEXT (i+1)
    #idx2check=np.asarray(np.where(dt_secs<=epsi_time),dtype=np.int)
    # For some reason, np.where returns a one-element tuple, where the element is a numpy array.
    # We want to move on with the array - not the enclosing tuple, thus the "[0]":
    idx_first=np.flatnonzero(dt_secs<=epsitime)
    #
    # These are the indices of the other half of the duplicates (i+1, or NEXT):
    idx_last = idx_first+1
    #
    # These are the combo of idx_first and idx_last, ie all those elements, which are on the 
    # same time step with either a previous or a next element.
    # Beware, that idx_all *could* percievably be all elements in the time series, as in (t0,t0,t0,t1,t1,t1,t2,t2,t2)
    idx_all  = np.sort(np.unique( np.concatenate((idx_first,idx_last),) ))
    #
    # Explanation of idx_first and idx_last:
    #  * idx_first are all those elements, where the *next* element is on the same time value
    #  * idx_last  are all those elements, where the *previous* element is on the same time value
    #  * idx_all   are all those elements, where either the previous or the next element is on the same time value
    # So, if elements #i, #i+1, and #i+2 all are at t=ti, then i and i+1 are in idx_first, 
    # while i+1 and i+2 are in idx_last
    #
    if   duplicates == 'delete':
        # This is the easiest. Just delete both/all duplicate values.
        #
        # We need to "uniqify" total index array, as there could be tripplets (or whatever)
        # so some indixecs in idx_plus could already be in idx_last:
        idx2del = np.unique( np.concatenate((idx_first,idx_last),) )
        # There is no need to do anything for the elevation values, as none of the duplicates are kept around.
        #
    elif duplicates == 'keepfirst':
        # TODO: This likely does not work if there are more than two vals joined together,
        idx2del = idx_last
    elif duplicates == 'keeplast':
        # TODO: This likely does not work if there are more than two vals joined together,
        idx2del = idx_first
    elif duplicates == 'average':
        # We _know_ that we will update the first element in each group, and delete the rest/last.
        idx2del = idx_last
        # But in this case, we need to average over the ones that have "identical" identities.
        # We will store the averaged values in the first element of each "non-unique" series,
        # ie those idx_first, which are not in idx_last (and so, which are not going to be 
        # deleted further down).
        t_secs  = timesince_totalseconds(datevals)
        #
        i0=idx_all[0]
        varsum=varvals[i0]
        nvals=1
        for iidx in idx_all[1:]:
            if t_secs[iidx]-t_secs[i0]<=epsitime:
                # Still the same time step (time value):
                varsum += varvals[iidx]
                nvals  += 1
                if verbloc:
                    print("  averaging-away duplicate step #%d at %s"%(iidx,str(datevals[iidx]),))
            else:
                # This is the next time value.
                # Store what we have for the previous:
                varvals[i0]=varsum/nvals
                # Initiate new average:
                i0=iidx
                varsum=varvals[i0]
                nvals=1
        # Store for the final sub-series:
        varvals[i0]=varsum/nvals
    elif duplicates == 'delete-differs':
        # If all varvalues in a duplicate-group are close/identical, then keep the values,
        # but consolidate to a single time series value.
        # All other duplicates are deleted.
        #
        # A priori we do not know which indixes to delete, so we will start with "none" and 
        # build up an array with the indixes-to-delete:
        idx2del = np.empty(shape=(0,),dtype=np.int32)
        iidx=0
        while( iidx < len(idx_first)-1):
            idx=idx_first[iidx]
            idx_main=idx
            epsi=epsivar
            if np.abs(varvals[idx])>0.00001:
                epsi=epsivar*np.abs(varvals[idx_main])
            differs=False
            while( iidx < len(idx_first) and idx_first[iidx] == idx ):
                if ( abs( varvals[idx_main]-varvals[idx+1] ) > epsi):
                    if verbloc:
                        print("  deleting entire differing duplicate step at %s"%(str(datevals[idx+1]),))
                    # We can assume that the vars differ.
                    differs=True
                idx    += 1
                iidx   += 1
            #
            # If any elements in the duplets differ from the first, then remove all in the group:
            if differs:
                idx2del = np.append(idx2del,range(idx_main,idx+1),axis=0)
    #
    # Actual deletion is here:
    if len(idx2del)>0:
        if verbloc>0:
            print(" Removing %d duplicate elements from time series"%len(idx2del))
        datevals=np.delete(datevals,idx2del)
        varvals =np.delete(varvals,idx2del)
    # 
    # For "delete-difers, we still need to consolidate duplicates into single values
    if duplicates == 'delete-differs':
        datevals,varvals=remove_duplicates(datevals,varvals,epsitime=epsitime, epsivar=epsivar, duplicates='average',verb=verbloc)
    #
    return datevals,varvals

def outliers_to_nan(datevals,varvals,varmin=None,varmax=None,verb=None):
    """
    Remove data values outside [varmin:varmax] specification.
    In reality, remove everything below min and everything above max, 
    so OK to specify only min or only max.
    Values outside the range will be replaced by NaNs, ie not actually removed.
    """
    verbloc=verb
    if verb is None:
        verbloc=verbose
    #
    if varmin is None:
        minlocs=np.zeros(shape=(0,),dtype=np.int32)
    else:
        try:
            tmp=np.float64(varmin)
        except:
            assert False,"Argument varmin='{}' is not a number".format(varmin)
        minlocs=np.flatnonzero(varvals<varmin)
        if verbloc:
            for ii in minlocs:
                print(" Eliminating elev[{}]=elev[{}]={} [low limit exceeded]".format(ii,datevals[ii].strftime("%Y:%m:%d %H:%M"),varvals[ii]))
    #
    if varmax is None:
        maxlocs=np.zeros(shape=(0,),dtype=np.int32)
    else:
        try:
            tmp=np.float64(varmax)
        except:
            assert False,"Argument varmax='{}' is not a number".format(varmax)
        maxlocs=np.flatnonzero(varvals>varmax)
        if verbloc:
            for ii in maxlocs:
                print(" Eliminating elev[{}]=elev[{}]={} [high limit exceeded]".format(ii,datevals[ii].strftime("%Y:%m:%d %H:%M"),varvals[ii]))
    # Actually set to NaN values
    locs=np.concatenate((minlocs,maxlocs),axis=0)
    varvals[locs]=np.nan
    #
    return datevals,varvals

def value_to_nan(datevals,varvals,delvalue,verb=None):
    """
    Remove data values which exactly matches a delete-value specified.
    Values are replaced by NaNs, ie not actually removed.
    """
    verbloc=verb
    if verb is None:
        verbloc=verbose
    # Write what we will do:
    if verbloc:
        for ii in np.flatnonzero(varvals==delvalue):
                print(" Eliminating elev[{}]=elev[{}]={} [matches delete value {}]".format(ii,datevals[ii].strftime("%Y:%m:%d %H:%M"),varvals[ii],delvalue))
    # Actually remove:
    varvals[varvals==delvalue]=np.nan
    #
    return datevals,varvals

# Seek and delete (set value to nan) when a given value (repval) is repeated
def repeatvalues_to_nan(datevals,varvals,nrepeats,repval=None,verb=None):
    """
    Arguments:
      datevals,varvals: tsSeries <date,value> numpy ndarray pair.
      nrepeats: Treat sub-series with this many or more repeated values
      repval: Value that must not be repeated (optional). If not given, then *any* value.

    Performs the following filtering:
      Seek and set-to-nan when a given (or any) value (repval) is repeated nrepeats or more times.
    """
    verbloc=verb
    if verb is None:
        verbloc=verbose
    #
    # Create a copy, so we do not destroy the original.
    # The copy will be returned - unless there is no change, in which case the original may be returned.
    rvals = varvals.copy()
    #
    # Sanity checks:
    nrepeats=int(nrepeats)
    assert nrepeats>0,"nrepeats must be a positive integer"
    #
    # If nrepeats is unity, then all single-values are repeats:
    if nrepeats==1:
        rvals.fill(np.float64('NaN'))
        if verbloc:
            print(" Eliminating ALL observations from [{}]='{}' to [{}]='{}' [nrepeats==1]"% \
                  (i1,datevals[i1].strftime("%Y:%m:%d %H:%M"),i1+i2-1,datevals[i1+i2-1].strftime("%Y:%m:%d %H:%M")))
        return datevals,rvals
    #
    # From here we deal only with nrepeats > 1:
    #
    #  Seek and delete repeated value over nrepeats or more steps
    #  Initialize counter, indices and "old value".
    nchanged = 0
    i0     = 0 # index/start of subseries
    nr     = 1 # number of repeats in subseries
    val1   = rvals[i0]
    for i in range(1,len(rvals)):
        if rvals[i] == val1 and (repval is None or val1 == repval):
            nr = nr + 1
        else:
            # values at i0 and i differs:
            if nr >= nrepeats:
                # Set this subseries to NaN
                rvals[i0:i0+nr] = np.float64('NaN') # nan
                # Increment count of changes:
                nchanged += nr
                # Optionally, write that we did it:
                if verbloc:
                    print(" Eliminating {} obs from [{}]='{}' to [{}]='{}' [stuck value at obs={}]".\
                          format(nr,i0,datevals[i0].strftime("%Y:%m:%d %H:%M"),i0+nr-1,datevals[i0+nr-1].strftime("%Y:%m:%d %H:%M"),val1))
            # Reset subseries counters
            i0 = i
            nr = 1
            val1 = rvals[i0]
    #
    # catch up if we ended the time series on fixed values:
    if nr >= nrepeats:
        # Set this subseries to NaN
        rvals[i0:i0+nr] = np.float64('NaN') # nan
        # Increment count of changes:
        nchanged += nr
        # Optionally, write that we did it:
        if verbloc:
            print(" Eliminating {} obs from [{}]='{}' to [{}]='{}' [stuck value at obs={}]".\
                  format(nr,i0,datevals[i0].strftime("%Y:%m:%d %H:%M"),i0+nr-1,datevals[i0+nr-1].strftime("%Y:%m:%d %H:%M"),val1))

    if nchanged > 0:
        if verbloc>0:
            print( '   Deleted %d repeated values'%nchanged)
        return datevals,rvals
    # else
    if verbloc>1:
        print( '   Deleted no repeated values')
    return datevals,varvals

# Seek and delete (set value to nan) when nrepeats elements only
# have nunique different values 
# TODO:
#   In general, look ahead nrepeats from each i and count np.unique 
#   number of unique values.
#   If > threshold, then go i+1.
#   Otherwise, then keep increasing interval length while we keep 
#    finding already-found values.
def flapvalues_to_nan(datevals,varvals,nrepeats,nunique=1,verb=None):
    """
    Arguments:
      datevals,varvals: tsSeries <date,value> numpy ndarray pair.
      nrepeats: Treat sub-series with this many or more repeated values
      nunique: If less than this number of unique values in subseries, 
         then the subseries is considered flapping, ans will be set to NaN.

    Performs the following filtering:
      Seek and set-to-nan when a series seems to be flapping between a few unique values.
    """
    verbloc=verb
    if verb is None:
        verbloc=verbose
    #
    # Create a copy, so we do not destroy the original.
    # The copy will be returned - unless there is no change, in which case the original may be returned.
    rvals = varvals.copy()
    # Counter for how many values we nuked.
    ndel  = 0
    #
    # Sanity checks:
    nrepeats=int(nrepeats)
    assert nrepeats>1,"nrepeats must be a positive integer greater than one"
    assert nunique>=1,"nunique must be a positive integer"
    #
    ntot=varvals.size;
    #
    if verb>3:
        print(" Looking for flapping from '{}' to '{}'. nunique={}, nrepeat={}".format(datevals[0],datevals[-1],nunique,nrepeats))
    #
    # Go over the series, starting at the begining (i=0) and always looking forward nrepeats values
    i0=0
    while i0<ntot-nrepeats+1:
        if verb>3:
            print(" Looking for flapping starting at {}".format(datevals[i0]))
        nlen=nrepeats
        iloc=i0+nlen-1 # Last element in local lookup/slice
        # This is the largest interval size we can have (starting at i0) before we run out of time series
        nmax=ntot-i0
        if nlen>nmax:
            raise AssertionError("Kont Koding break-out part")
            pass
            # Really, break/stop - but first check that we are not in an extension-loop?
        vuniq=np.unique(rvals[i0:i0+nlen])
        nuniq=vuniq.size
        #
        # Deal with *not* flapping first:
        if nuniq > nunique:
            # So, present initial-slice is not flapping.
            # We can shift at least one index to the right.
            # But in fact, we can at most remove one uniq value
            # per shift to the right, so we may shift 
            i0 += (nuniq-nunique)
            # and there should still be at least nunique values in present slice.
            continue
        if verb>1:
            print(" Detected flapping starting at {} values:{}".format(datevals[i0],vuniq))
        #
        # We should be careful about unique (non-flapping) values.
        # If there is a series of constant/stuck values, then we could risk to include 
        # extra data if we run with nunique>1.
        # If the number of unique values decrease if we omit the first point, 
        # then the first point (present i0) is probably not *really* part of the 
        # flapping-series. In such a case, just increment i0 and reiterate loop.
        if ( np.unique(rvals[i0+1:i0+nlen]).size < nuniq ):
            if verb>1:
                print("  Flapping detected from {}, but first point does not appear to be part of flapping".format(datevals[i0]))
        # 
        #
        # If we get here, then there is flapping. We will try to add one extra 
        # element at a time to see if that also can be included in the flapping.
        # We will implement various outs as we go along.
        for nlentry in range(nlen+1,nmax+1):
            iloc=i0+nlentry-1
            vtry=varvals[iloc]
            if verbloc>2:
                print("  check also {} at time {}".format(vtry,datevals[iloc]))
            if vtry in vuniq:
                # This is one of the flap-values, so try to increase by one more
                if verbloc>2:
                    print("   also flapping:  {} at time {} (nlen={})".format(vtry,datevals[iloc],nlen))
                nlen=nlentry
                continue
            if nuniq < nunique:
                # So we can add one extra uniq flapping-value.
                vuniq=np.sort(np.append(vuniq,vtry))
                nuniq=vuniq.size
                if verbloc>2:
                    print("   also flapping:  {} at time {} (adding extra uniq value, nlen={})".format(vtry,datevals[iloc],nlen))
                    print("     nuniq={}, vuniq={}".format(nuniq,vuniq))
                continue
            # If we get here, then this is the end of the present flapping thing.
            break
        # Like at begining, we need to check if reducing the length by 1 (n) 
        # also reduces the number of uniq values by 1 (n).
        for iback in range(nuniq):
            if np.unique(rvals[i0+1:i0+nlen-1]).size < nuniq:
                iloc=i0+nlen-2
                if verb>2:
                    print("   Last point {}, val={}does not appear to be part of flapping".format(datevals[iloc],varvals[iloc]))
                # The last element is not really part of the flapping.
                nlen=nlen-1
                # Try again:
                continue
        # Make sure that the found section is actually long enough.
        # (The reduction above could just send it back under the limit):
        if nlen<nrepeats:
            # Skip to next offset. 
            # This is a special situation, so just +1 and continue
            # (presumably +nlen would work too).
            
            i0+=1
        # Actually apply found nlen to update/eliminate flapping values
        if verbloc:
            print(" Eliminating {} flapping values {} from {} to {}".format(nlen,vuniq,datevals[i0],datevals[i0+nlen-1]))
        rvals[i0:i0+nlen]=np.float64('NaN')
        ndel += nlen
        #
        # Update i0 for next outer-loop iteration
        i0+=nlen
    if ndel>0:
        if verbloc:
            print(" Totally removed {} flapping values".format(ndel))
        return datevals,rvals
    if verbloc:
        print(" No flapping detected, no values removed")
    return datevals,varvals


# Seek and delete (set value to nan) when nlength elements have 
# nmissing or more missing value elements.
def sparsevalues_to_nan(datevals,varvals,nmissing,nlength,dt=None,verb=None):
    """
    Arguments:
      datevals,varvals: tsSeries <date,value> numpy ndarray pair.
      nmissing: Treat sub-series with this many or more missing values
      nlength: Look in this long subarray (window) at a time
    """
    verbloc=_verbloc(verb)
    # Make sure that we have dt:
    dt=_dtloc(datevals,dt=dt,verb=verbloc)
    #
    if verbloc:
        print("Deleting periods with {} or more missing values in {} or less elements".format(nmissing,nlength))
    #
    # Some time series may have rather large holes, and we do not want to scan those.
    # So, clip into bits/subseries, when there are nlength (or more) missing-values in a single straight.
    dtsep=(nmissing+1)*dt
    #dtsep=nlength*dt
    idxbreaks = get_separate_piece(datevals,varvals,dt=dt,dtsep=dtsep,want_breaks=True,verb=verbloc)
    npieces   = idxbreaks.size - 1
    # Create lists to be catenated at the end:
    dvalslist=[]
    vvalslist=[]
    # Go over the sub-series (time series pieces) on a per-piece basis, possibly adding missing as we go along
    for ip in range(npieces):
        i0=idxbreaks[ip]
        iN=idxbreaks[ip+1]
        dvals,vvals=sparsevalues_to_nan_ip(datevals[i0:iN],varvals[i0:iN],nmissing,nlength,dt=dt,verb=verbloc)
        vvalslist.append(vvals)
        dvalslist.append(dvals)
    # Catenate the lists of datevals,varvals-subseries
    # Any added nan-values have been removed by the *_ip-sub
    datevals_ret=np.concatenate(dvalslist)
    varvals_ret =np.concatenate(vvalslist)
    return datevals_ret,varvals_ret

# Deal with periods with many missing values in a single subseries.
# This is mostly an internal sub, callable from sparsevalues_to_nan
# TODO: This sub can probably be vectorized, if it is needed for performance
def sparsevalues_to_nan_ip(datevals,varvals,nmissing,nlength,dt,verb=None):
    subnam="sparsevalues_to_nan_ip"
    verbloc=_verbloc(verb)
    verbnxt=max(0,verbloc-1)
    if verbloc>2:
        print("{}: Processing subseries '{}' to '{}'".format(subnam,datevals[0],datevals[-1]))
    # Transform this subseries to "equidistant format", adding missng/nan values
    # where applicable.
    datevals_eq,varvals_eq=piecewise_to_fully_equidistant(datevals=datevals,varvals=varvals,dt=dt,verb=0)
    #
    # We will go over the (sub)series in a sliding-windows way, looking for 
    # windows with more than nmissing nan/missing values.
    iplen=datevals_eq.size
    nmiss=count_missing(varvals_eq,verb=0)
    if verbloc>2:
        print("{}:   missing {} of {} elements".format(subnam,nmiss,iplen))
    #
    # If the local series piece is too short to actually contain an nlength window, 
    # then we may look if there is actually enough OK/good/notmissing values to keep it.
    # Otherwise, we know that it is really padded with baddies.
    if iplen<nlength:
        ngood_got=iplen-nmiss
        ngood_need=nlength-nmissing
        if ngood_got<ngood_need:
            ndel=ngood_got
            nnan=nmiss
            nlen=iplen
            print(" Eliminating elev[{} to {}] (eliminating {} in subseries missing {} of {} elements): {}".format(datevals_eq[0].strftime("%Y-%m-%d %H:%M"),datevals_eq[-1].strftime("%Y-%m-%d %H:%M"),ndel,nnan,nlen,varvals_eq[:]))
            datevals_none=np.empty(shape=(0,),dtype=datetime.datetime)
            varvals_none=np.empty(shape=(0,),dtype=float)
            return datevals_none,varvals_none
    #
    # Quick return of the original unmodified arrays if not enough missing to actually trigger any window:
    if nmiss<nmissing:
        return datevals,varvals
    # Otherwise go over the possible windows with missing-values.
    # *Can*we*do*this*vectorized?:
    #  First convert minssing-values (nans) to True-values, value 1, and others to zero.
    valsnan=np.isnan(varvals_eq)
    #  Then accumulate, ie return tmp-array will contain number of missing-values up til this index:
    nancumsum = np.cumsum(valsnan)
    #  We need to prepend a single zero for computational reasons.
    #  For instance, if the first element is nan/missing, then we need to subtract a zero (from idx=-1) 
    #  in order for the difference to be meaningful.
    nancumsum = np.insert(nancumsum,0,1,axis=0)
    #  Use different to count the windowed version, see eg
    #   https://newbedev.com/how-to-calculate-rolling-moving-average-using-numpy-scipy
    nanwinsum = nancumsum[nlength:]-nancumsum[:-nlength]
    #
    # Whereever we have more than the limit 8nmissing) missing-values within the window,
    # we will add a start-stop index pair to be turned to missing later.
    # The index will be set from the first missing to the last missing withing that window.
    # Note that the "blanking-windows" will normally have quite a bit of overlap, 
    # but that is less critical, as they will likely not be in effect often.
    # (So speed is on finding and not applying the culprits).
    idxwhigh=np.where(nanwinsum>=nmissing)[0]
    # 
    # If we do not have any windows with a large number of missing values, then we can
    # return the original unmidified time series:
    if idxwhigh.size==0:
        return datevals,varvals
    #
    # Otherwise we will continue and eventually modify the time series.
    #
    # Initial list of start-stop index pairs:
    blankouts1=[]
    # append all windows with high count of nan/missing value elements
    for iw0 in idxwhigh:
        iwN=min(iw0+nlength-1,iplen)
        # These are the nan/missing indices within the present window 
        # (we know that there are nmissing or more of them)
        idxnanswin=iw0+np.flatnonzero(valsnan[iw0:iwN+1])
        # First and last index represent the range we must blank:
        idxnan0=idxnanswin[0]
        idxnanN=idxnanswin[-1]
        blankouts1.append([idxnan0,idxnanN])
    # 
    # Consolidate the blankouts list to secondary list.
    blankouts2=[]
    for blpair in blankouts1:
        if len(blankouts2)>0 and blpair[0]<blankouts2[-1][1]:
            # Consolidate this range with the previous, by just pushing the end of the window:
            blankouts2[-1][1]=blpair[1]
        else:
            blankouts2.append(blpair)
    #
    for blpair in blankouts2:
        nlen=blpair[1]-blpair[0]+1
        nnan=np.count_nonzero(valsnan[blpair[0]:blpair[1]+1])
        ndel=nlen-nnan
        # Note: If nlen==nnan, then all the blanking is already nans, so the efect will be void.
        if ndel>0:
            #if verbloc:
            #    print(" Eliminating elev[{} to {}] (eliminating {} in window missing {} of {} elements): {}".format(datevals_eq[blpair[0]].strftime("%Y-%m-%d %H:%M"),datevals_eq[blpair[1]].strftime("%Y-%m-%d %H:%M"),ndel,nnan,nlen,varvals_eq[blpair[0]:blpair[1]+1]))
            if verbloc:
                for ibl in range(blpair[0],blpair[1]+1):
                    if not np.isnan(varvals_eq[ibl]):
                         print(" Eliminating elev[%d]=elev[%s]=%.3f [got %d nans of %d vals in %s to %s]]"%(ibl,datevals_eq[ibl].strftime("%Y-%m-%d %H:%M"),varvals_eq[ibl],nnan,nlen,datevals_eq[blpair[0]].strftime("%Y-%m-%d %H:%M"),datevals_eq[blpair[1]].strftime("%Y-%m-%d %H:%M")))
            # Actually blank the values - in the equidistant value series:
            varvals_eq[blpair[0]:blpair[1]+1]=np.float64('NaN')
    #
    # Now consolidate series, removing the nan-values again:
    return remove_nans(datevals=datevals_eq,varvals=varvals_eq,verb=0)


# Seek and delete (set value to nan) when nlength elements have 
# nmissing or more missing value elements.
def lonelies_to_nan(datevals,varvals,nlonely,nneigh,dt=None,verb=None):
    """
    Arguments:
      datevals,varvals: tsSeries <date,value> numpy ndarray pair.
      nlonely: Number lonelies in saparated group
      nneigh:  Number of missing neighbors delimiting group.
    """
    # Use a helper routine to do the hard work
    #return _gaplonlies_fill_simple(datevals,varvals,ngroup=nlonely,nneigh=nneigh,filltype='lonelies',dt=dt,verb=verb)
    return _lonelies2nan(datevals,varvals,ngroup=nlonely,nneigh=nneigh,dt=dt,verb=verb)

# Actual work-horse of lonelies_to_nan - as an alternative to calling _gaplonlies_fill_simple(filltype='lonelies')
# This version divides the timeseries into pieces based on dtsep=dt*(nneigh+0.5), and then simply 
# looks at the length of each piece.
def _lonelies2nan(datevals,varvals,ngroup,nneigh,dt=None,verb=None):
    """
    Arguments:
      datevals,varvals: tsSeries <date,value> numpy ndarray pair.
      nlonely: Number lonelies in saparated group
      nneigh:  Number of missing neighbors delimiting group.
    """
    subnam='_lonelies2nan'
    verbloc=_verbloc(verb)
    verbnxt=max(0,verbloc-1)
    # Make sure that we have dt:
    dt=_dtloc(datevals,dt=dt,verb=verbnxt)
    #
    # Sanity checks:
    ngroup=int(ngroup)
    nneigh=int(nneigh)
    assert ngroup>0, "Argument ngroup must be positive integer, not '{}'".format(ngroup)
    assert nneigh>0, "Argument nneigh must be positive integer, not '{}'".format(nneigh)
    #
    # Divide by length of neighbor nans:
    dtsep=(nneigh+0.5)*dt
    #
    # This is the length of the group we look for
    # ie time between first and last element in group.
    # For ngroup=1 it will be zero, as there is just one point (no time difference)
    dtgroup=(ngroup-1)*dt
    #
    idxbreaks = get_separate_piece(datevals,varvals,dt=dt,dtsep=dtsep,want_breaks=True,verb=0)
    npieces   = idxbreaks.size - 1
    # Create lists to be catenated at the end:
    dvalslist=[]
    vvalslist=[]
    deldates=[]
    # Go over the sub-series (time series pieces) on a per-piece basis, possibly adding missing as we go along
    # Each piece is either returned unmodified - or replaced by an empty piece (ie converted to nan),
    # all new pieces are added to the updated lists
    for ip in range(npieces):
        i0=idxbreaks[ip]
        iN=idxbreaks[ip+1]
        # We will look at these subseries/pieces:
        dvals=datevals[i0:iN]
        vvals=varvals[i0:iN]
        #print("TEST SUBSERIES[nlen={}] {} - {}".format(iN-i0,dvals[0],dvals[-1]))
        # The most common will likely be a subseries which is larger than what we look for,
        # so we will deal with that first:
        if iN-i0>ngroup:
            # Too many points in subseries to be a lonely-group.
            dvalslist.append(dvals)
            vvalslist.append(vvals)
            continue
        #
        # Otherwise, we will look at the length (in time) of the subseries.
        # The number of (non-missing) points in the group will be eg dvals.size,
        # but it could actually contain some nan/missing values, which must be 
        # counted as part of the group.
        # If there are 3 non-nan values/elements in the subseries/piece, then normally the length will be 2*dt.
        # But the 3 points could hide one or more nan-values, so we need to look at the actual length of the piece.
        # This is the length of the present time series piece (subseries):
        dtg=(dvals[-1]-dvals[0]).total_seconds()
        # And we should compare to dtgroup.
        # As both are floats, we will make the comparison with a little bit of fuzz,
        #  THIS IS WRONG: abs(dtg-dtgroup)<0.1*dt
        if dtg-dtgroup<0.1*dt:
            # In this case we will eliminate all the (non-nan) points in the present group.
            for ii in range(dvals.size):
                deldates.append(dvals[ii])
                if verbloc:
                    print(" Eliminating elev[{}]=elev[{}]={} [lonely {}-{}-{}]".format(ii+i0,dvals[ii].strftime("%Y:%m:%d %H:%M"),vvals[ii],nneigh,ngroup,nneigh))
            dvalslist.append( np.empty(shape=(0,),dtype=datetime.datetime) )
            vvalslist.append( np.empty(shape=(0,),dtype=float) )
        else:
            # Group length does not match.
            dvalslist.append(dvals)
            vvalslist.append(vvals)
    #
    # If there were no modifications, then we can return the original arrays
    # (plus an empty list of modifications).
    # Otherwise we will need to catenate the subseries
    if len(deldates)>0:
        datevals =np.concatenate(dvalslist)
        varvals  =np.concatenate(vvalslist)
    return datevals,varvals,deldates
 
# Seek and fill small gaps (missing elements), where there are 
# neighbors with defined (not nan/missing) values. 
# Fill-values are simply the averages of the neighbors.
# (TODO: We could use a linear trend or something, but it would likely not change much, 
#  this routine is just to fill with *something* which is not totally unreasonable.
#  To get better values use eg spline approximations.
def fill_gaps_simple(datevals,varvals,ngap,nneigh,dt=None,verb=None):
    """
    Arguments:
      datevals,varvals: tsSeries <date,value> numpy ndarray pair.
      nlonely: Number lonelies in saparated group
      nneigh:  Number of missing neighbors delimiting group.
    Returns:
      datevals,varvals: updated time series
      moddates: Array with the datetimes, which where modified.
    """
    # Use a helper routine to do the hard work
    return _gaplonlies_fill_simple(datevals,varvals,ngroup=ngap,nneigh=nneigh,filltype='degap',dt=dt,verb=verb)
   
#
# Common work routine for filling small gaps and removing lonelies.
# All the stuff with stensils etc are here.
def _gaplonlies_fill_simple(datevals,varvals,ngroup,nneigh,filltype,dt=None,verb=None):
    subnam='_gaplonlies_fill_simple'
    verbloc=_verbloc(verb)
    verbnxt=max(0,verbloc-1)
    # Make sure that we have dt:
    dt=_dtloc(datevals,dt=dt,verb=verbloc)
    #
    # Sanity checks:
    assert (filltype=='lonelies' or filltype=='degap' ),"Unexpected argument filltype='{}'. Use 'lonelies' or 'degap'".format(filltype)
    ngroup=int(ngroup)
    nneigh=int(nneigh)
    assert ngroup>0, "Argument ngroup must be positive integer, not '{}'".format(ngroup)
    assert nneigh>0, "Argument nneigh must be positive integer, not '{}'".format(nneigh)
    #
    # This is for degab: Look for nan groups in non-nan neighbors
    nanstencil=np.concatenate((np.zeros(shape=(nneigh,),dtype=bool),np.ones(shape=(ngroup,),dtype=bool),np.zeros(shape=(nneigh,),dtype=bool)),axis=0)

    if filltype=='degap':
        fillmidx=np.flatnonzero(nanstencil)            # Fill where mask has nans. Indices (relative to mask), which will be updated in case of a hit.
        fmeanidx=np.flatnonzero(True - nanstencil)     # Filling value (mean) is based on entries, where the is NO nan
        if verbloc:
            print("Fill {}-wide groups of nan/missing with {}-wide non-missing neighbors".format(ngroup,nneigh))
    elif filltype=='lonelies':
        # For lonelies, the stencil is basically the opposite as for degap. Look for non-nan groups in nan neighbors
        nanstencil=True - nanstencil
        fillmidx=np.flatnonzero(True - nanstencil)  # Fill where mask (the updated one!) has not-nans
        # For lonelies, we always fill with NaN, and we do not need fmeanidx
        vuse=np.float64('NaN')
        if verbloc:
            print("Deleting {}-wide groups of values with {}-wide nan/missing neighbors".format(ngroup,nneigh))
    #
    if verbloc>3:
        print("{}[{}] nanstencil={}".format(subnam,filltype,nanstencil))
    # 
    # Common code for degap/lonelies:
    datevals_eq,varvals_eq=piecewise_to_fully_equidistant(datevals=datevals,varvals=varvals,dt=dt,verb=0)
    varvals_eq_nan= is_missing(varvals_eq,verb=0) # (ts.is_missing(varvals_eq,verb=0)).astype('int')
    # This list will be filled with the datetime of the elements we modify.
    # It will returned alongside updated datevals,varvals
    dateprefilled=[]
    #
    for itid in range(datevals_eq.size-nanstencil.size):
        sl=slice(itid,itid+nanstencil.size)
        vv=varvals_eq_nan[sl]
        #if verbloc>4:
        #    print("TEST1[{}] {}  {}:={}  X {}".format(filltype,datevals_eq[itid].strftime("%Y-%m-%d %H:%M"),vv,np.all(vv==nanstencil),varvals_eq[sl]))
        if np.all(vv==nanstencil):
            #print("TEST2[{}]  {} = {}  X {}".format(filltype,vv,nanstencil,varvals_eq[sl]))
            if filltype=='degap':
                # The value to insert will be the average of the non-nan values within the mask/stencil
                vuse=np.mean(varvals_eq[itid+fmeanidx])
            # For 'lonelies', the fill is nan, and it is already set above.
            #
            # This is an array of the indixes, which we must change/update:
            chsl=itid+fillmidx
            # Append dates to list that we have changed:
            datstrlst=[] # This is just for output, and to avoid a second loop
            for dat in datevals_eq[chsl]:
                dateprefilled.append(dat)
                # The following line is just to format log-line
                datstrlst.append(dat.strftime("%Y:%m:%d %H:%M"))
            if verbloc>1:
                if filltype=='degap':
                    print(" Degapping elev={:.4f} at [{}]: local {}".format(vuse,' , '.join(datstrlst),varvals_eq[sl]))
                if filltype=='lonelies':
                    for itid in chsl:
                        print(" Eliminating elev[{}]={} [lonely in: {}]".format(datevals_eq[itid].strftime("%Y-%m-%d %H:%M"),varvals_eq[itid],varvals_eq[sl]))
            # Actually, change the values:
            varvals_eq[chsl]=vuse
            # 
    if len(dateprefilled)>0:
        # There were changes, so we must remove the nans
        # (otherwise we simply return the unmodified originals)
        datevals,varvals=remove_nans(datevals_eq,varvals_eq,verb=0)
    return datevals,varvals,dateprefilled

# Helper routine to get the seconds (np.float64) between each consequtive time step 
# in a time series (datevals numpy.ndarray with elements of type datetime.datetime).
def timedeltas_totalseconds(datevals):
    # Make sure that we are dealing with a 1D array of datetime-objects:
    assert_datevals(datevals)
    # Series of time steps:
    timedeltas=datevals[1:]-datevals[0:-1]
    #
    # Convert time step to seconds. 
    dt_secs = np.empty(shape=(len(timedeltas),),dtype=np.float64)
    for idelta in range(len(timedeltas)):
        dt_secs[idelta] = np.float64(timedeltas[idelta].total_seconds())
    return dt_secs

# Helper routine to convert array of datevals (datetime.datetime objects) to 
# array of float (64bit) seconds.
# There are a few possibilities for the optional "since" argument,
# which will be used as the epoch (time zero) of the returned values:
#   A: a datetime.datetime object
#   B: A string - will be interpreted as epoch8, epoch10, epoch12, or even epoch14.
#   C: An integer or 64bit float - will be interpreted as unix time (UTC seconds since 1970-01-01)
# If "since" is not given, then datevals[0] will be used.
def timesince_totalseconds(datevals,since=None):
    assert_datevals(datevals)
    if since is None:
        since=datevals[0]
    elif isinstance(since,strtype):
        since=epochstr2datetime(since)
    elif isinstance(since,int) or isinstance(since,float):
        since=unixtime2datetime(uxtime)
    elif sys.version_info[0] < 3 and isinstance(since,long):
        # "long" exists in py2, but not in py3, so we need to be a little careful
        since=unixtime2datetime(uxtime)
    if not isinstance(since,datetime.datetime):
        raise TypeError("Optional argument 'since' should be datetime (or convertable to). Got %s"%str(since))
    timedeltas=datevals[:]-since
    #
    # Convert times to seconds. 
    secs = np.empty(shape=(len(timedeltas),),dtype=np.float64)
    for idelta in range(len(timedeltas)):
        secs[idelta] = np.float64(timedeltas[idelta].total_seconds())
    #
    return secs


# For each element in time series, compute time to nearest neighbor
# element in seconds. 
# This method is very similar to timedeltas_totalseconds, but where the 
# latter is "one-sided" (computing time to next element), the present 
# method is "two-sided", looking both forwards and backward and picking  
# the minimum (nearest) timedelta.
# Return value is array of floats (seconds).
def seconds2nearestneighbor(datevals):
    # dt_secs is the actual delta-t's for each element, size N-1.
    dt_secs = timedeltas_totalseconds(datevals)
    # Minimum distance to neighbor
    # First allocate for all elements, size is N
    dt_nearest=np.empty(shape=(datevals.size,),dtype=np.float64)
    # Set values for the "inner" elements, size is N-2
    # This is an element-wise minimum between the two arrays:
    dt_nearest[1:-1]= np.minimum(dt_secs[1:],dt_secs[:-1])
    # Pad with first and last dt_secs value, which is the distance for first and last element:
    dt_nearest[0]   = dt_secs[0]
    dt_nearest[-1]  = dt_secs[-1]
    # 
    return dt_nearest

# Estimate slope - Deal with time series in several pieces, and treat
# each piece separately, handling the end points with forward/backward 
# Euler approximations and other points with "central'ish" aproximation.
def slope(datevals,varvals,dt=None,dtsep=None,verb=None):
    verbloc=_verbloc(verb)
    #
    idxbreaks = get_separate_piece(datevals,varvals,dt=dt,dtsep=dtsep,want_breaks=True,verb=verbloc)
    npieces   = idxbreaks.size - 1
    # Figure out the slopes on a per-piece basis, and append results as we go along
    slps=[]
    for ip in range(npieces):
        i0=idxbreaks[ip]
        iN=idxbreaks[ip+1]
        slps.append(slope_ip(datevals[i0:iN],varvals[i0:iN]))
    slp=np.concatenate(slps)
    return slp

# Compute an estimate of the local slope - at least a first-order approximation.
# This is the work-horse that does the actual computation.
# This version works only on a single time series piece.
def slope_ip(datevals,varvals):
    # This method will fail, if there is not at least two data points
    # (in this subseries).
    # If that is the case, then simply return zero (the alternative being None)
    nn=datevals.size
    if nn<=1:
        return np.zeros(shape=(nn,),dtype=np.float64)
    #
    # dt_secs is the actual delta-t's for each element, size N-1.
    dt_secs = timedeltas_totalseconds(datevals)
    # Compute difference y2-y0 for estiamation of f'(x1)
    # This is size N-2
    dvals=varvals[2:]-varvals[:-2]
    # This is the "double dt" centred on xi - size N-2
    dt2=dt_secs[1:]+dt_secs[:-1]
    # Create a target array for the slopes. Size N.
    slp=np.empty(shape=(datevals.size,),dtype=np.float64)
    # Set values for the "inner" elements, size is N-2.
    # This is a "near-centred" approach, so normally O(dt**2),
    # but for non-equidistant parts O(dt).
    # Use elementwise division
    slp[1:-1]= np.divide(dvals,dt2)
    # Pad with forward and backward first-order difference for first and last element:
    slp[0]   = (varvals[ 1]-varvals[ 0])/dt_secs[0]
    slp[-1]  = (varvals[-1]-varvals[-2])/dt_secs[-1]
    # 
    return slp

# Estimate curvature in time series
# Deal with time series in several pieces, and treat
# each piece separately, handling the end points by copying neighbor curvature,
# and other points with "central'ish" aproximation.
def curvi(datevals,varvals,dt=None,dtsep=None,verb=None):
    verbloc=_verbloc(verb)
    #
    idxbreaks = get_separate_piece(datevals,varvals,dt=dt,dtsep=dtsep,want_breaks=True,verb=verbloc)
    npieces   = idxbreaks.size - 1
    # Figure out the slopes on a per-piece basis, and append results as we go along
    curvs=[]
    for ip in range(npieces):
        i0=idxbreaks[ip]
        iN=idxbreaks[ip+1]
        curvs.append(curvi_ip(datevals[i0:iN],varvals[i0:iN]))
    curv=np.concatenate(curvs)
    return curv

# Compute an estimate of the local curvature - at least a first-order approximation.
# This is for a single time series piece.
# Theory:
#  We want to estimate f''(t0) based on [t0, t1, t2] and [f(t0), f(t1), f(t2)]=[f0,f1,f2]. 
# Using a Taylor expansion around t=t1 (the central point), we write the f(t) as:
#   f(t) = f(t1) + f'(t1)/1! * (t-t1) + f''(t1)/2! * (t-t1)^2 + ...
# Insert t=t0 and t=t2 [and define dti=t{i+1}-ti] to get
#  A:  f0 = f1 + f'1 * (t0-t1) + (1/2)*f''1 * (t0-t1)^2 + ...  # note that t0-t1<0
#         = f1 + f'1 * (-dt0)  + (1/2)*f''1 * (dt0)^2   + ...
#  B:  f2 = f1 + f'1 * (t2-t1) + (1/2)*f''1 * (t2-t1)^2 + ...  # note that t2-t1>0
#         = f1 + f'1 * (dt1)   + (1/2)*f''1 * (dt1)^2   + ...
# Compute A*dt1+B*dt0:
#   dt1*f0+dt0*f2 = (dt0+dt1)*f1 + f'1*(-dt0*dt1+dt1*dt0) + (1/2) * f''1 * { dt0^2 * dt1 + dt1^2 * dt0 } + ...
#                 = (dt0+dt1)*f1 +                          (1/2) * f''1 * dt0 * dt1 { dt0 + dt1 } + .. ..
# =>
#   f''1 = { dt0*f2-[dt0+dt1]*f1+dt1*f0 } / { (1/2) [dt0+dt1] * dt0 * dt1 } + ...
#
def curvi_ip(datevals,varvals):
    # This method will fail, if there is not at least _three_ data points
    # (in this subseries).
    # If that is the case, then simply return zero (the alternative being None)
    nn=datevals.size
    if nn<=2:
        return np.zeros(shape=(nn,),dtype=np.float64)
    # dt_secs is the actual delta-t's for each element, size N-1.
    dt_secs = timedeltas_totalseconds(datevals)
    # Compute Taylor approximation for f''(x1) using y0,y1,y2
    # based on "central" differences. 
    # As the series may not be equidistant, the result is rather complicated.
    # Note, however, that if/where the series is equidistant, the result
    # degenerates to the well-known form:
    #   (y-1 -2y0 + y1)/dt**2 
    # 
    dt2=dt_secs[1:]+dt_secs[:-1] # N-2 of these
    # Find numerator/denominator
    numer = dt_secs[0:-1]*varvals[2:]-dt2[:]*varvals[1:-1]+dt_secs[1:]*varvals[:-2]
    denom = 0.5*dt2*dt_secs[0:-1]*dt_secs[1:]
    #
    # Create a target array for the curvature. Size N. - TODO Use empty and fill ends.
    curv=np.zeros(shape=(datevals.size,),dtype=np.float64)
    # Set values for the "inner" elements, size is N-2.
    # This is a "near-centred" approach, so normally O(dt**2),
    # but for non-equidistant parts O(dt).
    # Use elementwise division
    curv[1:-1]= np.divide(numer,denom)
    # 
    # For first and last element, we can either make separate approcimations arund eg t0,
    # or we can exploit that we are essentially fitting a second-order poly the the exact 
    # same points as for the first [last] interior element, so we will get the same 
    # polynomial. And as the second derivative (of a second-order poly) is constant,
    # we are essentially just getting the exact same values as for the first "interior" 
    # element.
    # Here is the result for direct expansion around t0:
    #curv[0]=(dt_secs[1]*varvals[0]-dt2[0]*varvals[1]+dt_secs[0]*varvals[2])/(0.5*dt_secs[0]*dt_secs[1]*dt2[0])
    # It happens to be the same as just doing:
    curv[0]   = curv[1]
    curv[-1]  = curv[-2]
    return curv


#time2datetime:
#  Returning either based on type of input
# Retrieve a time in some format, and return a datetime.datetime object correspondig to that format.
# Accepted inputs:
#  A: datetime.datetime object: Simply returned unmodified
#  B: string: Assume it is an epochstr, ie an epoch{8,12,12,14}
#  C: int: Assume that it is a unix timestamp.
def time2datetime(tim,verb=None):
    verbloc=_verbloc(verb)
    dateobj=None
    if  isinstance(tim, datetime.datetime ):
        if verbloc>2:
            print("time2datetime: Arg is already a datetime object. Returning "+str(tim))
        dateobj=tim
    elif isinstance( tim, strtype):
        if verbloc>2:
            print("time2datetime: Assuming arg is an epoch-string epoch12 and friends "+str(tim))
        dateobj=epochstr2datetime(tim,verb=verbloc)
    else:
        if verbloc>2:
            print("time2datetime: Assuming arg is a unix-time (seconds since 1970-01-01) "+str(tim))
        dateobj=unixtime2datetime(tim,verb=verbloc)
    # All done
    return dateobj

# Convert a known epoch-like string to an equivalent datetime object.
# Input epoch-like string (epoch8, epoch10, epoch12) and return datetime.datetime object.
# The returned datetime object is NOT timezone aware (tzinfo=None)
# For convenience, it is allowed to pass a datetime.datetime object as argument.
# In this case, the argument is just passed straight back.
def epochstr2datetime(epochstr,verb=None):
    verbloc=_verbloc(verb)
    if isinstance( epochstr, datetime.datetime ):
        # It is already the right type.
        if verbloc>2:
            print("epochstr2datetime: Arg is already a datetime object. Returning "+str(epochstr))
        return epochstr
    if isinstance( epochstr, int ):
        # Make sure that we work with a string.
        epochstr=str(epochstr)
    if not isinstance( epochstr, strtype):
        raise TypeError("%s is not a valid epoch str (not a string)" % str(epochstr))
    epochlen=len(epochstr)
    if epochlen!=8 and epochlen!=10 and epochlen!=12 and epochlen!=14:
        raise ValueError("%s is not a valid epoch str (unexpected length=%d)" %(epochstr,epochlen))
    hour   = 0
    minute = 0
    second = 0
    try:
        year   = int(epochstr[0:4])
        month  = int(epochstr[4:6])
        day    = int(epochstr[6:8])
        if epochlen>=10:
            hour   = int(epochstr[8:10])
        if epochlen>=12:
            minute = int(epochstr[10:12])
        if epochlen>=14:
            second = int(epochstr[12:14])
    except:
        raise ValueError("%s is not a valid epoch str (does not look like integer)" % epochstr)
    try:
        dateobj = datetime.datetime(year=year, month=month, day=day, hour=hour, minute=minute, second=second)
    except:
        raise argparse.ArgumentTypeError("%s is not a valid epoch str (failed to convert to datetime)" % epochstr)
    return dateobj

# Convert a unix time "seconds since 1970-01-01" to an equivalent datetime object
# Input integer (or float/np.float64) seconds and return datetime.datetime object.
# The returned datetime objet is NOT timezone aware (tzinfo=None)
# For convenience, it is allowed to pass a datetime.datetime object as argument.
# In this case, the argument is just passed straight back.
def unixtime2datetime(uxtime,verb=None):
    verbloc=_verbloc(verb)
    if isinstance( uxtime, datetime.datetime ):
        # It is already the right type.
        if verbloc>2:
            print("unixtime2datetime: Arg is already a datetime object. Returning "+str(uxtime))
        return uxtime
    # Really, utcfromtimestamp needs a float 64bit) as input.
    # So, we will try to convert to float right away:'
    try:
        uxtime=float(uxtime)
    except:
        raise TypeError("%s is not a valid unix time (cannot convert to float64)" % str(uxtime))
    return datetime.datetime.utcfromtimestamp(uxtime)


# Check if a datetime series (datevals) is "piecewise equidistant", ie 
# if each element (datetime objects) in the series is N*dt removed from the 
# first element, and dt is a constant.
# Dt should either be given as an argument (in seconds) or it will be
# guestimated from the time series using the median (the get_timestep method).
# Note that for very large time intervals (greater than 270 years on most platforms) 
# the used method will lose microsecond accuracy (see datetime docs), 
# but for elevation time series, it should be very accurate.
# The epsi/accuracy used will be determined based on the total length of the 
# time series - and the time step size.
# The optional varvals argument is only used to test if the time series is consistent.
# Optional t0: The time value for which t0+N*dt is compared
# Optional wantvector: Return vector with True/False for each timestep whether it is 
#   on equidistant or not.
def is_piecewise_equidistant(datevals,varvals=None,dt=None,t0=None,wantvector=False,verb=None):
    verbloc=_verbloc(verb)
    #
    # Time series must be consistent in order for this to work:
    if not is_consistent(datevals,verb=0):
        return False
    if dt is None:
        dt = get_timestep(datevals,verb=0)
    if dt<0.1:
        print("WARNING: Accuracy may be reduced for very low time step values")
        # TODO: In this case we may want to use RELATIVE comparisons as well.
    dt=np.float64(dt)
    #
    # This is the time value to compare with:
    if t0 is None:
        # No functional argument, try command line option (may still be None)
        global time_zero
        t0=time_zero
    if t0 is None:
        # Ensure that we get an "aligned" point, ie if the first value happens to be 
        # a non-equidistant point, then do not select that.
        # So we truncate down to day
        t0=datevals[0].replace(hour=0,minute=0, second=0, microsecond=0)
    #
    # Really, we do not need to create a vector - nor to examine all time steps - unless we 
    # should return a vector.
    # For coding convenience, however, we make the infrastructure ready in any case
    # So far we found no nonequidistant points.
    num_nonequidistants=0
    step_is_ontarget=np.ones(shape=datevals.shape,dtype=bool)
    #
    # Figure out what precision we can expect on .total_seconds()
    # Float64 is precision ~16 digits, so can hold up to 10**10 seconds
    #  before loosing us accuracy.
    range_secs=np.float64((datevals[-1]-datevals[0]).total_seconds())
    range_years=range_secs/np.float64(365*24*3600)
    #
    # If the time series covers an extremely long period (>270 years), we will 
    # only compare relative to time step (and hope that the time step is at least 
    # a few seconds
    compare_relative=False
    if range_years>270 or dt<0.1:
        compare_relative=True
    #
    if compare_relative:
        # Compare relative to dt
        for idate in range(len(datevals)):
            # Time since time series start in seconds:
            deltasecs=np.float64((datevals[idate]-t0).total_seconds())
            # Find the  ditto measured in dt
            deltadt  =deltasecs/dt
            #  find nearest N*dt and subtract that from the actual
            deviation=np.mod(deltadt,np.float64(1))
            if ( deviation > 10**-4 ):
                step_is_ontarget[idate]=False
                num_nonequidistants += 1
                if verbloc>2 or not wantvector:
                    print("   timevals[%d]='%s' is non-equidistant pnt"%(idate,str(datevals[idate])))
                if not wantvector:
                    # No need to keep checking
                    break
    else:
        # Compare in absolute seconds - down to 10 us.
        for idate in range(len(datevals)):
            # Time since time series start in seconds: 
            deltasecs=np.float64((datevals[idate]-t0).total_seconds())
            # Number of dts this compares to:
            deviation = np.mod(deltasecs,dt)
            if ( deviation > 10**-4 ):
                step_is_ontarget[idate]=False
                num_nonequidistants += 1
                if verbloc>2 or not wantvector:
                    print("   timevals[%d]='%s' is non-equidistant pnt"%(idate,str(datevals[idate])))
                if not wantvector:
                    # No need to keep checking
                    break
    # Possibly log stuff:
    if verbloc:
        if num_nonequidistants>0:
            if compare_relative:
                print("At least one time step is positioned more than 10^-4 * dt wrong")
                print(" time series is not piecewise equidistant")
            else:
                print("At least one time step is positioned more than 10^-4 s wrong")
                print(" time series is not piecewise equidistant")
        else:
            print("Time series is piecewise equidistant")
    if wantvector:
        return step_is_ontarget
    elif num_nonequidistants>0:
        return False
    else:
        return True


# Assert version of is_piecewise_equidistant
def assert_piecewise_equidistant(datevals,varvals=None,dt=None):
    if not is_piecewise_equidistant(datevals,varvals=varvals,dt=dt,verb=0):
        raise AssertionError("Time series is not piecewise equidistant")

# TODO/BJB: 
#   This routine should work with timestep_large - or something similar - to define "lonely"
#   points, ie define pieces not just as "have to be equidistant with time step dt".
# Remove all values in the time series, which do not have a neighbor at a distance dt.
# Aka remove all "pieces" of length unity.
def remove_lonely(datevals,varvals,mingroupsize=None,dt=None,verb=None):
    verbloc=_verbloc(verb)
    # The time series must be piecewise equidistant for this to work.
    if not is_piecewise_equidistant(datevals,varvals,verb=0):
        print("  time series is not (piecewise) equidistant - cannot delete lonely points")
    #
    # Deal with option:
    if mingroupsize is None:
        global min_group_size
        mingroupsize=min_group_size
    if mingroupsize < 2:
        if verbloc:
            print(" No need to remove lonlies with mingroupsize=%d"%mingroupsize)
        return datevals,varvals
    #
    if dt is None:
        dt = get_timestep(datevals,verb=0)
    # It is an advantage, if we make the series piecewise equidistant *here*
    datevals_loc,varvals_loc=remove_nans(datevals,varvals,verb=0)
    #
    # Get number of pieces, and then (if more han one piece) loop over each to see 
    # if it should be skipped.
    #npieces=get_equidistant_piece(datevals,varvals,dt=dt,verb=verbloc)
    datevals_pieces,varvals_pieces=get_equidistant_piece(datevals,varvals,dt=dt,ipiece=-1,verb=verbloc)
    npieces=len(datevals_pieces)
    #
    # Keep track of how many points and pieces we eliminate
    ndel_pts=0
    ndel_pieces=0
    # 
    # These will catenate to final time series (without the small groups):
    datevals_cat=np.empty(shape=(0,),dtype=datetime.datetime)
    varvals_cat=np.empty(shape=(0,),dtype=float)
    datevals2cat=[]
    varvals2cat=[]
    for ipiece in range(npieces):
        #  Slow way:
        #datevals_i,varvals_i=get_equidistant_piece(datevals,varvals,ipiece=ipiece,dt=dt,verb=0)
        #  Fast way:
        datevals_i=datevals_pieces[ipiece]
        varvals_i=varvals_pieces[ipiece]
        print(" Piece #%d is len %d"%(ipiece,varvals_i.shape[0]))
        if varvals_i.shape[0]<mingroupsize:
            ndel_pts    += varvals_i.shape[0]
            ndel_pieces += 1
            if verbloc:
                print(" Deleting equidistant piece #%d (consists of %d points)"%(ipiece,varvals_i.shape[0]))
        else:
            # It is long enough, so add it to the time series
            datevals2cat.append(datevals_i)
            varvals2cat.append(varvals_i)
    #
    # So we are ready.
    # If nothing was deleted
    if ndel_pieces==0:
        if verbloc:
            print("  No small groups (lonely points) were deleted)")
        # Just return the original unmodified points
        return datevals,varvals
    # 
    # Something was deleted:
    if verbloc:
        print("  Deleted %d lonely point(s) in %d separate group(s)"%(ndel_pts,ndel_pieces))
    datevals_cat=np.concatenate( datevals2cat, axis=0)
    varvals_cat =np.concatenate(  varvals2cat, axis=0)
    return datevals_cat,varvals_cat

#
# Remove all values in the time series, which do not match a specified 
# equidistant pattern, ie where t!=N*dt+t0.
def remove_nonequidistant(datevals,varvals,t0=None,dt=None,verb=None):
    verbloc=_verbloc(verb)
    # If the timeseries is (piecewise) equidistant, then the following array
    # will just be all True. Otherwise, there will be false values for the 
    # problematic times.
    tim_is_ontarget =is_piecewise_equidistant(datevals,varvals,dt=dt,t0=t0,wantvector=True,verb=verbloc)
    idx2del=np.flatnonzero(~ tim_is_ontarget)
    num2del=idx2del.size
    if num2del == 0:
        if verbloc:
            print(" Time series is already piecewise equidistant - nothing to delete")
        return datevals,varvals
    else:
        if verbloc:
            print(" Deleting %d time steps, which are not on equidistant times"%num2del)
        if verbloc>2:
            print("  Time steps to del: "+str(idx2del))
        return np.delete(datevals,idx2del),np.delete(varvals,idx2del)

# Convert a piecewise equidistant time series to a fully equidistant time 
# series by adding additional time steps. The elevation value (varvals element)
# of the added time steps will be set to the given fillval - or to np.nan if
# no other value is given.
# If a varvals vector (np.ndarray) is not given, then the datevals will just 
# be extended without chaning any varvals elements.
def piecewise_to_fully_equidistant(datevals,varvals=None,fillval=np.float64("NaN"),dt=None,verb=None):
    verbloc=_verbloc(verb)
    verbnxt=max(0,verbloc-1)
    #
    if datevals.size<2 or is_equidistant(datevals,verb=verbnxt):
        # It is already equidistant (or it is just a single value), Fast return:
        if varvals is None:
            return datevals
        else:
            return datevals,varvals
    #
    if dt is None:
        dt = get_timestep(datevals,verb=verbnxt)
    #
    dt     =np.float64(dt)
    fillval=np.float64(fillval)
    #
    # Time series must be piecewise equidistant in order for this to work:
    assert_piecewise_equidistant(datevals,varvals=varvals,dt=dt)
    #
    # Also, we must get rid of any repeat-time steps.
    datevals,varvals=remove_duplicates(datevals,varvals,duplicates='delete',verb=verbnxt)
    #
    datevals_new = np.arange(datevals[0], 
                             datevals[-1]+datetime.timedelta(seconds=0.5*dt), 
                             datetime.timedelta(seconds=dt)).astype(datetime.datetime)
    # Number of time steps - of original and filled time series:
    nold=datevals.size     #len(datevals)
    nnew=datevals_new.size #len(datevals_new)
    if verbloc:
        print(" Adding %d time steps to make time series fully equidistant"%(nnew-nold,))
        #print("   existing: %s -- %s"%(str(datevals[0]),str(datevals[-1])))
        #print("   filled:   %s -- %s"%(str(datevals_new[0]),str(datevals_new[-1])))
    if varvals is None:
        # If there are no varvals to consider, then we are already done:
        return datevals_new
    varvals_new=np.empty(shape=(nnew,),dtype=varvals.dtype)
    # Prefill with NaNs:
    #varvals_new.fill(np.float64('NaN'))
    #
    # Fill the new varvals array:
    iold=0
    ncopied=0
    for inew in range(nnew):
        if iold>=nold:
            raise AssertionError("BUG: iold out of range")
        tdiff=(datevals_new[inew]-datevals[iold]).total_seconds()
        if np.abs( tdiff ) < 0.00001: # 10 us will be acceptable
            # This step matches and old one - copy the elevation over:
            varvals_new[inew]=varvals[iold]
            # Increment to next old element
            iold += 1
        elif datevals_new[inew]<datevals[iold]:
            # This is an inserted element:
            varvals_new[inew]=fillval
        else:
            # In special cases, the new-array may be getting ahead
            # It can happen eg when the input array has multiple elements with identical time step value.
            # To avoid this, we first remove duplicates above.
            raise AssertionError("BUG: inew is getting ahead - debugging will be needed")
    # We should be at end-of-array for both inew and iold:
    if iold != nold:
        raise AssertionError("Somehow we did not copy all varvals over (iold=%d, nold=%d). Debugging needed"%(iold,nold))
    # All done.
    return datevals_new,varvals_new

# Join pieces of series by single-nan-element in between.
# In particular, get all "pieces" (separated by timestep_large, default to 1.5*dt), and 
# insert a single NaN element (dt after each piece-end) then concatenate the entirety
def nanjoin_pieces(datevals,varvals,fillval=np.float64("NaN"),dt=None,dtsep=None,verb=None):
    verbloc=_verbloc(verb)
    #
    # Time series must be consistent in order for this to work:
    if not is_consistent(datevals,verb=0):
        return False
    if dt is None:
        dt = get_timestep(datevals,verb=0)
    #
    # dtsep can just be passed to "get_separate_piece", which will default appropriately.
    idxbreaks = get_separate_piece(datevals,varvals,dt=dt,dtsep=dtsep,want_breaks=True,verb=verbloc)
    #
    # Get the subseries (dates and vals), where we will insert a single NaN in between each
    datevalsSubs=idxbreaks2subseries(idxbreaks,datevals)
    varvalsSubs =idxbreaks2subseries(idxbreaks,varvals)
    nsubs=len(datevalsSubs)
    # Create lists to be catenated at the end:
    dvalslist=[datevalsSubs[0],]
    vvalslist=[ varvalsSubs[0],]
    for ip in range(1,nsubs):
        # Get the end-time of previous piece, and add 1*dt:
        tid=datevalsSubs[ip][-1]+datetime.timedelta(seconds=dt)
        dvalslist.append(np.asarray([tid,]))
        vvalslist.append(np.asarray([np.float64('NaN'),]))
        # And append the next piece of timeseries
        dvalslist.append(datevalsSubs[ip])
        vvalslist.append( varvalsSubs[ip])
    #
    # Catenate everything - including the added NaN-values
    datevals_wnan=np.concatenate(dvalslist)
    varvals_wnan =np.concatenate(vvalslist)
    return datevals_wnan,varvals_wnan

# Find the pieces in a "piecewise equidistant" time series.
# Returns either the number of such pieces, of - if iseries is specified - 
# the datevals,varvals of the i'th such series
# Beware that this routine does not check for missing values/nans,
# so it is NOT the same as getting the non-nan pieces(!)
def get_equidistant_piece(datevals,varvals=None,dt=None,ipiece=None,verb=None):
    verbloc=_verbloc(verb)
    assert_piecewise_equidistant(datevals,varvals)
    #
    # In order to find the pieces, we need to get rid of nans:
    if count_nans(datevals,varvals,verb=0):
        datevals_nonan,varvals_nonan=remove_nans(datevals,varvals,verb=0)
    else:
       datevals_nonan=datevals
       varvals_nonan=varvals
    #
    # Make sure we have the timestep:
    if dt is None:
        dt=get_timestep(datevals,verb=0)
        # Really we should call get_timestep, but we already have the timedeltas, 
        # so we'll go straight to the median:
        #dt=np.median(dt_secs)
    #
    # Use an even more generic routine for the hard lifting:
    return get_separate_piece(datevals=datevals_nonan,varvals=varvals_nonan,
                              dt=dt,dtsep=1.5*dt,
                              ipiece=ipiece,verb=verbloc)

#
# Divide time series into "separate" pieces, where two pieces are separated by a
# period with no data points, and the no-data-period is of at least a particular length.
# This method should be used for non-equidistant time series.
# dtsep is the minimum time (time step) separating two pieces.
#
# Returns either the number of such pieces, of - if iseries is specified - 
# the datevals,varvals of the i'th such series
# Beware that this routine does not check for missing values/nans,
# so it is NOT the same as getting the non-nan pieces(!)
#
# With want_breaks, then return the "end-point indices", which divide the series,
# ie [0, i1, ..., nlen]
# where each pair slice(0,i1) defines a subseries.
#
# With want_slice, then return a slice object rather than the sliced data
# (only for ipiece set).
def get_separate_piece(datevals,varvals,dt=None,dtsep=None,want_breaks=False,ipiece=None,want_slice=False,verb=None):
    verbloc=_verbloc(verb)
    # The key is to find the indices, where the time to next time step is 
    # larger than whatever is decided for dtmax (eg 1.5*dt for finding equidistant pieces)
    dt_secs = timedeltas_totalseconds(datevals)
    #
    # Indices of time deltas > 1.5*dt.
    if dtsep is None:
        # Use global (command line option) if available:
        global timestep_large
        dtsep=timestep_large
    if dtsep is None:
        # Use something like the time step (as per equidistant);
        if dt is None:
            dt=np.median(dt_secs)
        dtsep=1.5*dt
    #
    # These compare to indices in the datevals array, where the time-to-next-element 
    # is larger than expected. For an equidistant time series, this array will be empty.
    idxs=np.flatnonzero(dt_secs>=dtsep)
    #
    npieces=len(idxs)+1
    #
    # Idxs contain indices for time_i, where the _next_ dt is large.
    # Essentially, that is the end-index of each piece.
    # Adding +1, we get a list of the start of the _next_ piece:
    idxs += 1
    # Prepend the start of the first piece (0) and 
    # append the end of the last piece (-1, or N+1):
    idxs = np.concatenate( (np.asarray([0,],dtype=np.int32),
                            idxs,
                            np.asarray([datevals.size,],dtype=np.int32)),axis=0 )
    # Now idxs[i]:idxs[i+1] is exactly the range of piece #i.
    if ipiece is None:
        # Special case: Just count the number of pieces:
        if verbloc:
            if not dt is None and np.abs(dtsep-1.5*dt)<0.001*dt:
                print("Time series consists of %d equidistant pieces"%npieces)
            else:
                print("Time series consists of %d pieces"%npieces)
        if want_breaks:
            return idxs
        return npieces
    if ipiece<0:
        # Special case: Just give me all the pieces in lists
        datevals_pieces=[]
        varvals_pieces=[]
        for ipc in range(npieces):
            #print "TEST ",ipc
            istart=idxs[ipc  ]
            istop =idxs[ipc+1]
            datevals_pieces.append(datevals[istart:istop])
            varvals_pieces.append(varvals[istart:istop])
        return datevals_pieces,varvals_pieces
    #
    # If we get here, then the caller wants a particular piece returned
    if ipiece>=npieces:
        raise AssertionError("OUT-OF-RANGE: Asked for piece idx=%d (zero-based), but time series only has %d equidistant pieces"%(ipiece,npieces))
    #
    # Return the piece as a slice object or slice of the data
    istart=idxs[ipiece  ]
    istop =idxs[ipiece+1]
    if verbloc>1:
        if not dt is None and np.abs(dtsep-1.5*dt)<0.001*dt:
            typ="equidistant_piece"
        else:
            typ="piece"
        print("  %s[#%d]=['%s' - '%s']"%
              (typ,ipiece,str(datevals[istart]),str(datevals[istop-1])))
    #
    slc=slice(istart,istop)
    if want_slice:
        return slc
    if varvals is None:
        return datevals[slc]
    return datevals[slc],varvals[slc]

# Info:
#    idxbreaks: np array with start indices of subseries ending with NLEN,
#        so subseries i (zero-indexed) can be sliced as [idxbreaks[i]:idxbreaks[i+1]].
#    enddates: array of shape (#subseries,2), where first and last date is given for each subseries/piece
#
# This routine returns start&stop dates for each subseries defined by idxbreaks.
# It is purely book-keeping.
def idxbreaks2enddates(idxbreaks,datevals):
    for arg in (idxbreaks,datevals):
        assert isinstance(arg,np.ndarray),'Args must be ndarray, not {}'.format(type(arg))
        assert len(arg.shape)==1,'Args must be 1D arrays, not shape={}'.format(arg.shape)
    assert idxbreaks[0]==0,'First element in idxbreaks must be zero, not true for idxbreaks={}'.format(idxbreaks)
    assert idxbreaks[-1]==datevals.size,'Last element in idxbreaks must be timeseris len ({}), not true for idxbreaks={}'.format(datevals.size,idxbreaks)
    # Note that for degenerate timeseries, we may be called with an empty datevals, in which case idxbreaks is [0 0].
    if datevals.size==0:
        #print("Degenerate (zero-length) time series detected")
        return datevals
    # Create an array with room for two ends of each subseries/piece
    npieces=idxbreaks.size-1
    enddates=np.empty(shape=(npieces,2),dtype=datetime.datetime)
    for ip in range(npieces):
        i0=idxbreaks[ip]
        iN=idxbreaks[ip+1]-1
        enddates[ip,0]=datevals[i0]
        enddates[ip,1]=datevals[iN]
    return enddates

def idxbreaks2subseries(idxbreaks,vals):
    for arg in (idxbreaks,vals):
        assert isinstance(arg,np.ndarray),'Args must be ndarray, not {}'.format(type(arg))
        assert len(arg.shape)==1,'Args must be 1D arrays, not shape={}'.format(arg.shape)
    assert idxbreaks[0]==0,'First element in idxbreaks must be zero, not true for idxbreaks={}'.format(idxbreaks)
    assert idxbreaks[-1]==vals.size,'Last element in idxbreaks must be time series len ({}), not true for idxbreaks={}'.format(vals.size,idxbreaks)
    # Create a list with one element per subarray.
    npieces=idxbreaks.size-1
    subseries=[]
    for ip in range(npieces):
        subseries.append(vals[idxbreaks[ip]:idxbreaks[ip+1]].copy())
    return subseries

# Helper routine to see if two ndarrays are identical.
# Rather than compare one element at a time, subtract and compute sum-abs
# This should work for arrays of eg ints and floats, 
# but eg dtype=datetime.datetime requires special handling.
def _ndarrays_identical(arr1,arr2):
    if arr1.shape != arr2.shape:
        return False
    #
    if arr1.size==0:
        # Two empty arrays. Assume identical
        return True
    #
    foundtype=None
    if isinstance(arr1.flat[0],datetime.datetime) and \
       isinstance(arr2.flat[0],datetime.datetime):
        foundtype='datetime'
    else:
        try:
            np.float64(arr1.flat[0])
            np.float64(arr2.flat[0])
            foundtype='numeric'
        except:
            pass
    # If they are not both numeric and not both datetime, then we dont know what to do
    assert not foundtype is None,"_ndarrays_identical: Arrays must be both numeric or both datetime, not type(arr1[0])={} and type(arr2[0])={}: DEBUG={}".format(type(arr1[0]),type(arr2[0]),arr1)
    #    
    # Shape matches so we can compute sum-abs-diff:
    if foundtype=='numeric':
        asum=np.sum(np.abs(arr1-arr2))
    elif foundtype=='datetime':
        asum=np.sum((arr1-arr2)!=datetime.timedelta(0))
    # If the arrays are truly identical, then the difference will be zero.
    # (The abs-part is just to avoid nonzero differences adding up to zero).
    if asum==0:
        return True
    return False

# We can do a pre-compile for large arrays, but we work on small arrays
# and so prioritize portability over speed (in this particular case)
# For arrays with non-number (eg datetime) elements, it is not straight-forward to 
# do sum(abs()), so we must compare the elements instead.
# Really, this aught to work for most ndarrays, even with slightly "weird" element types
# Presently, we can use _ndarrays_identical also for datetime arrays (bjb 2022-02-08)
#@nb.jit(nopython=True)
def _nd_datetime_arrays_identical(arr1,arr2):
    return _ndarrays_identical(arr1,arr2)
    if arr1.shape != arr2.shape:
        return False
    # See eg https://stackoverflow.com/questions/43973991/check-if-two-numpy-arrays-are-identical
    for ai, bi in zip(arr1.flat, arr2.flat):
        if ai != bi:
            return False
    return True


#
# Convert varvals (elevation) series from one unit to another, eg from mm or cm to m,
# combining the ratio (power) of src and tgt units relative to the base SI-unit [m].
# Conversion is done in-place, but a reference to the result is returned as well.
def change_unit(varvals,srcunit,tgtunit):
    # Sanity checks:
    if not isinstance( varvals, np.ndarray ):
        raise AssertionError("Inconsistent data series: varvals is not a numpy array: %s"%type(datevals))
    if not isinstance( srcunit, strtype ):
        raise AssertionError("Inconsistent data series: srcunit str: %s"%type(srcunit))
    if not isinstance( tgtunit, strtype ):
        raise AssertionError("Inconsistent data series: tgtunit str: %s"%type(tgtunit))
    #
    # Fast return on no-conversion 
    if srcunit == tgtunit:
        if verbose>1:
            print(" No conversion needed from '%s' to '%s'"%(srcunit,tgtunit))
        return varvals
    srcscale,srcpow=_unit_in_power_of_base(srcunit)
    tgtscale,tgtpow=_unit_in_power_of_base(tgtunit)
    if srcscale != tgtscale:
        raise AssertionError("Cannot convert from '%s' (%s-scale) to '%s' (%s-scale)"%(srcunit,srcscale,tgtunit,tgtscale))
    convpow=srcpow-tgtpow
    if convpow<0:
        # numpy.power does not accept negative integers as power, 
        # so we convert to 64bit float 
        convpow=np.float64(convpow)
    convfac=np.power(10,convpow)
    if verbose:
        print(" Conversion from '%s' to '%s' is x10^%s = x%s"%(srcunit,tgtunit,str(convpow),str(convfac)))
    # If convfac is not the same type as varvals, then convert convfac, 
    # to avoid possible casting from 32bit -> 64 bit -> 32 bit.
    if type(convfac) != varvals.dtype:
        convfac = varvals.dtype.type(convfac)
    # Actually do conversion. This can now be done without any typecasting.
    varvals[:] = varvals[:] * convfac
    return varvals

# Round varvals (time series *values*, eg elevations, not datetimes) to some
# specified precision - in terms of units.
# For instance, round srcunit="m" roundto="cm" will round to 2 decimal places.
# Reference to the rounded array is returned, ie it is NOT done in-place.
def roundto_unit(varvals,srcunit,roundto):
    # Sanity checks:
    if not isinstance( varvals, np.ndarray ):
        raise AssertionError("Inconsistent data series: varvals is not a numpy array: %s"%type(datevals))
    if not isinstance( srcunit, strtype ):
        raise AssertionError("Inconsistent data series: srcunit str: %s"%type(srcunit))
    if not isinstance( roundto, strtype ):
        raise AssertionError("Inconsistent data series: roundto str: %s"%type(roundto))
    srcscale,srcpow=_unit_in_power_of_base(srcunit)
    rndscale,rndpow=_unit_in_power_of_base(roundto)
    if srcscale != rndscale:
        raise AssertionError("Cannot round unit '%s' (%s-scale) to nearest '%s' (%s-scale)"%(srcunit,srcscale,roundto,rndscale))
    # This is the number of decades from srcunits to rundto-units.
    roundpow=srcpow-rndpow
    roundint=np.int32(np.around(roundpow,decimals=0)) 
    # In special cases, it could be non-int (rounding m to nearest inch).
    if roundpow-roundint>0.01:
        roundint +=1 # Round up - keeping extra digit - in this case
    if verbose:
        print("  rounding '%s' to '%s' keeping %d decimals"%(srcunit,roundto,roundint))
    return np.around(varvals,decimals=roundint)

# This is a helper routine for converting or rounding units.
# It returns the kind/scale of the unit (eg "length") and 
# the given unit (eg "mm") in powers of ten of the base SI-unit (eg "m").
# Parsing "mm" the return value would be ("length",-3), as 1 mm = 10^-3 m
# It is accepted to prefix the unit by "100", "10" (and for consitency with "1")
# to be able to compute for eg 10um = 10^-5 m, so that we can cover 
# all orders-of-magnitude within the "SI-range".
def _unit_in_power_of_base(unit):
    # Sanity check on input:
    if not isinstance(unit,strtype):
        raise AssertionError("Illegal argument: Expected str but got %s"%type(unit))
    if len(unit)==0:
        raise AssertionError("Illegal argument: Called with empty string")
    #
    # Match/search for ending letters in string
    match=re.search('[a-zA-Z]+$',unit) # Alphabetic letters at end-of-string, returns re match object
    if not match:
        # Match object is None (ie no match)
        raise AssertionError("Illegal argument: String does not end with alphabetic letter(s): '%s'"%unit)
    #.group(0)
    if match.start(0)>0:
        # We have some kind of specified value attached to the unit:
        value=unit[:match.start(0)]
        try:
            val=float(value)
        except:
            raise AssertionError("Illegal argument: Unit is prepended with a value, which does not convert to float: '%s' in %s''"%(value,unit))
        if val<=0:
            raise AssertionError("Illegal argument: Unit is prepended with a non-positive value: '%s' in %s''"%(value,unit))
        logval=np.log10(val)
        unit  =unit[match.start(0):]
    else:
        logval=0.0
    # Logval should just be added to power at end-of script
    #
    scale=None
    power=None
    # Here we can define funky units if we want.
    #   ... inch, lightyear, parsec, (yard, mile etc are not well-defined)
    if len(unit)>3:
        if unit == "inch":
            scale="length"
            # 1 inch = 0.0254 m = 10^log10(0.0254) = 10^-1.5951662833800619
            power=-1.5951662833800619
            scale,power=_unit_in_power_of_base('0.0254m') # Same thing, but maybe easier
    # From here on, we will assume that the argument is an SI-unit with a possible prefix.
    if scale is None:
        if (len(unit)==1 or len(unit)==2) and unit[-1] == "m":
            scale="length"
            prefix=unit[:-1]
        else:
            raise AssertionError("Unknown unit %s - additional implementation may be needed"%str(unit))
    if power is None:
        # Here is a dict with commonly used SI-prefixes. We will assume that we need one of these:
        _prefix = {'y': -24,  # yocto
                   'z': -21,  # zepto
                   'a': -18,  # atto
                   'f': -15,  # femto
                   'p': -12,  # pico
                   'n': -9,   # nano
                   'u': -6,   # micro
                   'm': -3,   # milli
                   'c': -2,   # centi
                   'd': -1,   # deci
                   '' : 0,    #  no prefix
                   'k': 3,    # kilo
                   'M': 6,    # mega
                   'G': 9,    # giga
                   'T': 12,   # tera
                   'P': 15,   # peta
                   'E': 18,   # exa
                   'Z': 21,   # zetta
                   'Y': 24,   # yotta
               }
        if not prefix in _prefix:
            raise AssertionError("Unexpected prefix in unit %s (parsed prefix to be '%s', but I cant understand that)"%(unit,prefix,))
        power=_prefix[prefix]
    return scale,power+logval


##
# Find mean (average) of timeseries values excluding any possible NaN values
def mean(datevals,varvals,fillval=None,verb=None):
    verbloc=_verbloc(verb)
    # We will accept that datevals may be None here:
    if not datevals is None:
        assert_consistent(datevals=datevals,varvals=varvals)
    else:
        assert_varvals(varvals=varvals)
    #
    # Beware that the time series (values) could contain NaN values,
    if count_missing(varvals=varvals,fillval=fillval,verb=0) == 0:
        avg=varvals.mean()
    else:
        idx_nan = is_missing(varvals=varvals,datevals=datevals,fillval=fillval,verb=verbloc)
        avg=(varvals[~idx_nan]).mean()
        #if fillval is None or  np.isnan(fillval):
        #    idx_notmissing= ~np.isnan(varvals)
        #else:
        #    idx_notmissing= (varvals != float(fillval) )
        ## We need to remove a particular value from the series.
        #avg=(varvals[idx_notmissing]).mean()
    if verbloc>1:
        print(" average is %s"%str(avg))
    return avg

##
# Figure out if a time (datetime or epochstr is in a range defined by two  
# other such times (fromtime and totime). 
# These arguments may be given either as epochNN (string or ints) 
# or as datetime.datetime objects.
# 
# Either fromtime _or_ totime can be missing, which means get
# range from start or to end of series.
# If include_ends is set to false, then the > and < will used rather than >= and <=.
def time_in_range(testtime,fromtime=None,totime=None,include_ends=True,verb=None):
    verbloc=_verbloc(verb)
    #
    dtest=time2datetime(testtime)
    #  Must have fromtime and totime, otherwise nothing to do!
    # But we will initiate with min/max for datetime
    dstart=datetime.datetime.min
    dstop =datetime.datetime.max
    if not fromtime is None:
        dstart=time2datetime(fromtime)
    if not totime is None:
        dstop=time2datetime(totime)
    assert dstop>=dstart,"get_time_range: Cannot have fromtime={}>{}=totime".format(fromtime,totime)
    #
    inrange=False
    if include_ends:
        inrange=(dstart<=dtest and dtest<=dstop)
    else:
        inrange=(dstart< dtest and dtest< dstop)
    if verbloc>1:
        if include_ends:
            rangstr="[ '{}' - '{}' ]".format(fromtime,totime)
        else:
            rangstr="] '{}' - '{}' [".format(fromtime,totime)
        if inrange:
            print(" {} is in range {}".format(testtime,rangstr))
        else:
            print(" {} not in range {}".format(testtime,rangstr))
    return inrange

##
# Figure out if two time-ranges overlap. 
# Each time range is defined by two datetime.datetime or epochstr values.
# Missing ends are set to min/max of datetime.
# 
# If include_ends is set to false, then the start of one must not match the end of the other
def time_ranges_overlap(fromtime1=None,totime1=None,fromtime2=None,totime2=None,include_ends=True,verb=None):
    verbloc=_verbloc(verb)
    #
    #  Must have fromtime and totime, otherwise nothing to do!
    # But we will initiate with min/max for datetime
    dstart1=datetime.datetime.min
    dstop1 =datetime.datetime.max
    dstart2=datetime.datetime.min
    dstop2 =datetime.datetime.max
    if not fromtime1 is None:
        dstart1=time2datetime(fromtime1)
    if not totime1 is None:
        dstop1=time2datetime(totime1)
    if not fromtime2 is None:
        dstart2=time2datetime(fromtime2)
    if not totime2 is None:
        dstop2=time2datetime(totime2)
    assert dstop1>=dstart1,"time_ranges_overlap: Cannot have fromtime1={}>{}=totime1".format(fromtime1,totime1)
    assert dstop2>=dstart2,"time_ranges_overlap: Cannot have fromtime2={}>{}=totime2".format(fromtime2,totime2)
    #
    dstart=max(dstart1,dstart2)
    dstop =min(dstop1, dstop2)
    overlaps=False
    if include_ends:
        overlaps=(dstart<=dstop)
    else:
        overlaps=(dstart<dstop)
    print("TTEST",dstart,dstop,include_ends,overlaps)
    if verbloc>1:
        if include_ends:
            rangstr1="['{}' - '{}']".format(fromtime1,totime1)
            rangstr2="['{}' - '{}']".format(fromtime2,totime2)
        else:
            rangstr1="]'{}' - '{}'[".format(fromtime1,totime1)
            rangstr2="]'{}' - '{}'[".format(fromtime2,totime2)
        if overlaps:
            print(" invervals overlap: {} and {}".format(rangstr1,rangstr2))
        else:
            print(" invervals doent overlap: {} and {}".format(rangstr1,rangstr2))
    return overlaps

##
# Return part of the time series given by from_time and to_time.
# The arguments "fromtime" and "totime" may be given either as epochNN (string or ints) 
# or as datetime.datetime objects.
# The original datevals/varvals are not changed.
# Either fromtime _or_ totime can be missing, which means get
# range from start or to end of series.
# If include_ends is set to false, then the > and < will used rather than >= and <=.
def get_time_range(datevals,varvals,fromtime=None,totime=None,include_ends=True,verb=None):
    verbloc=_verbloc(verb)
    # Sanity checks:
    #  We will NOT accept that datevals may be None here:
    assert_consistent(datevals=datevals,varvals=varvals)
    #  Must have fromtime and totime, otherwise nothing to do!
    # But we will initiate with min/max for datetime
    dstart=datetime.datetime.min
    dstop =datetime.datetime.max
    if not fromtime is None:
        dstart=time2datetime(fromtime)
    if not totime is None:
        dstop=time2datetime(totime)
    assert dstop>=dstart,"get_time_range: Cannot have fromtime={}>{}=totime".format(fromtime,totime)
    #
    if verbloc:
        if dstart is None:
            strstart="-"
        else:
            strstart=str(dstart)
        if dstop is None:
            strstop="-"
        else:
            strstop=str(dstop)
        print("  Will copy time series from %s to %s"%(strstart,strstop,))
    #
    # This gives all the matching indixes in the range, but really, it still is a *range*
    # so we could create a i0:iN+1 or a slice operation if we wanted.
    if include_ends:
        idxs=np.flatnonzero(np.logical_and(datevals>=dstart,datevals<=dstop))
    else:
        idxs=np.flatnonzero(np.logical_and(datevals>dstart,datevals<dstop))
    dats=datevals[idxs].copy()
    vals=varvals[idxs].copy()
    ndats=idxs.size
    if verbloc:
        print(" Getting {} values from time series".format(ndats))
    return dats,vals

##
# Delete (replace with NaN) a part of the time series given by from_time and to_time.
# The arguments "fromtime" and "totime" may be given either as epochNN (string or ints) 
# or as datetime.datetime objects.
def delete_time_range(datevals,varvals,fromtime=None,totime=None,fillval=None,verb=None):
    verbloc=_verbloc(verb)
    # We will NOT accept that datevals may be None here:
    assert_consistent(datevals=datevals,varvals=varvals)
    #
    # Figure out time range:
    global delete_from_time
    global delete_to_time
    dstart=None
    dstop=None
    if (not fromtime is None) or (not totime is None):
        # In this case we will rely only on the routine arguments, 
        # ie the command line options (globals) will not be used
        if not fromtime is None:
            dstart=time2datetime(fromtime)
        if not totime is None:
            dstop=time2datetime(totime)
    elif (not delete_from_time is None) or (not delete_to_time is None):
        # In this case we will rely only on the command line options (globals),
        # as there are no function arguments overriding them
        if not delete_from_time is None:
            dstart=epochstr2datetime(delete_from_time)
        if not delete_to_time is None:
            dstop=epochstr2datetime(delete_to_time)
    else:
        # No delete-range set.
        if verbloc:
            print("No time range given - not deleting time range")
        return datevals,varvals
    # Sanity check:
    if (not dstart is None) and (not dstop is None):
        if dstop < dstart:
            raise AssertionError("In delete-range, totime (%s) cannot be before fromtime (%s)"%(str(dstop),str(dstart),))
    if (dstart is None) and (dstop is None):
        # This should not happen, as we should have hit the return above:
        raise AssertionError("In delete-range, somehow both fromtime and totime is None (and we missed the early return) - go debug")
    # What to fill in the deleted varvals:
    if fillval is None:
        fillval=np.nan
    #
    if verbloc:
        if dstart is None:
            strstart="-"
        else:
            strstart=str(dstart)
        if dstop is None:
            strstop="-"
        else:
            strstop=str(dstop)
        print("Will delete time series from %s to %s"%(strstart,strstop,))
    #
    #
    # This gives all the matching indixes in the range, but really, it still is a *range*
    # so we could create a i0:iN+1 or a slice operation if we wanted.
    idxs=np.flatnonzero(np.logical_and(datevals>=dstart,datevals<=dstop))
    ndats=idxs.size
    if ndats==0:
        if verbloc:
            print(" No values actually deleted (no values in given time range). Returning original series")
        return datevals,varvals
    # Otherwise (nonzero number of time series elements to modify):
    varvals_deleted=varvals.copy()
    varvals_deleted[idxs]=fillval
    if verbloc>1:
        print(" Deleted %d values from time series"%ndats)
    return datevals,varvals_deleted

# set_time_range:
# Ensure that a timeseries starts at tstart and/or ends at tstop.
# This is a helper-routine, which may come in handy at various times.
# Note that the the routine may return a version of the time series which is equidistant, 
# using NaNs where necessary.
# This routine may shorten or extend the time series as necessary.
# The returned (datevals,varvals) may be a new copy, or - if no changes are necessary - a 
# (copy of the) pointer to the incoming arguments.
def set_time_range(datevals,varvals,starttime=None,stoptime=None,fillval=None,verb=None):
    verbloc=_verbloc(verb)
    if fillval is None:
        fillval=np.nan
    #
    # Figure out time range:
    global set_start_time
    global set_stop_time
    dstart=None
    dstop=None
    if (not starttime is None) or (not stoptime is None):
        # In this case we will rely only on the routine arguments, 
        # ie the command line options (globals) will not be used
        if not starttime is None:
            dstart=time2datetime(starttime)
        if not stoptime is None:
            dstop=time2datetime(stoptime)
    elif (not set_start_time is None) or (not set_stop_time is None):
        # In this case we will rely only on the command line options (globals),
        # as there are no function arguments overriding them
        if not set_start_time is None:
            dstart=epochstr2datetime(set_start_time)
        if not set_stop_time is None:
            dstop=epochstr2datetime(set_stop_time)
    else:
        # No delete-range set.
        if verbloc:
            print("set-range: No time range given - not deleting time range")
        return datevals,varvals
    # Sanity check:
    if (not dstart is None) and (not dstop is None):
        if dstop < dstart:
            raise AssertionError("In set-range, stoptime (%s) cannot be before starttime (%s)"%(str(dstop),str(dstart),))
    if (dstart is None) and (dstop is None):
        # This should not happen, as we should have hit the return above:
        raise AssertionError("In set-range, somehow both starttime and stoptime is None (and we missed the early return) - go debug")
    #
    # Sanity checks on input:
    assert_consistent(datevals,varvals)
    #
    # Check if the range is already OK. That may happen quite frequently.
    if ( dstart is None or dstart==datevals[0] ) and ( dstop is None or dstop==datevals[-1] ):
        if verbloc:
            print("set-range: Time range already OK, returning original time series")
        return datevals,varvals
    # If we continue, then the series WILL be changed, but we will ensure to create 
    # a new data object, to not ruin the original (the arguments)
    idx2truncate=None
    # Maybe the series already has the right start and stop?
    # (This could happen more often than not).
    if (not dstart is None) and (datevals[0] < dstart):
        # Remove values before starttime:
        idx2truncate=np.flatnonzero(datevals<dstart)
    if (not dstop is None) and (datevals[-1] > dstop):
        # Remove values after stoptime:
        idxs=np.flatnonzero(datevals>dstop)
        if idx2truncate is None:
            idx2truncate=idxs
        else:
            idx2truncate=np.concatenate((idx2truncate,idxs),axis=0)
    if not idx2truncate is None:
        # Actually truncating.
        if verbloc:
            print("set-range: Truncating %d elements from time series end(s)"%idx2truncate.size)
        datevals=np.delete(datevals,idx2truncate)
        varvals =np.delete( varvals,idx2truncate)
    # 
    # By default, we will add a zero-size array at ends
    datestart  = np.empty(shape=(0,),dtype=datetime.datetime)
    datestop   = np.empty(shape=(0,),dtype=datetime.datetime)
    valstart   = np.empty(shape=(0,),dtype=float)
    valstop    = np.empty(shape=(0,),dtype=float)
    needadjust = False
    if (not dstart is None) and (datevals[0] > dstart):
        # start-justification necessary
        if verbloc:
            print("set-range: Adding early start")
        # TODO: Possibly round down to "equidistant time step"!
        datestart  = np.asarray([dstart, ])
        valstart   = np.asarray([fillval,])
        needadjust = True
    if (not dstop is None) and (datevals[-1] < dstop):
        # stop-justification necessary
        if verbloc:
            print("set-range: Adding late stop")
        # TODO: Possibly round up to "equidistant time step"!
        datestop   = np.asarray([dstop,  ])
        valstop    = np.asarray([fillval,])
        needadjust = True
    if needadjust:
        datevals=np.concatenate( ( datestart,  datevals, datestop),axis=0 )
        varvals =np.concatenate( ( valstart,   varvals,  valstop) ,axis=0 )
    #
    # Should be all done now.
    return datevals,varvals

##
# Create a datevals series based on start,stop,delta, possibly with 
# a rounding and timezero effect.
def create_time_range(timestart, timestop, dt=None, roundto=None, alignto=None, verb=None):
    verbloc=_verbloc(verb)
    # Sanity checks:
    if not isinstance( timestart, datetime.datetime ) or not isinstance( timestop, datetime.datetime ):
        raise AssertionError("Need two datetime.datetime arguments. Got '%s', '%s'"%(type(timestart),type(timestop)))
    if timestop <= timestart:
        raise AssertionError("Need two timestart<timestop, but got '%s', '%s'"%(str(timestart),str(timestop)))
    ###
    # Apply defaults:
    if dt is None:
        # Try if we can get it from command line options:
        global time_step
        dt = time_step
    #
    if roundto is None:
        global roundtime
        roundto = roundtime
    if alignto is None:
        global select_align
        alignto=select_align
    #
    # Possibly align start time (end time follows as used in range):
    if not alignto is None:
        # We want to make sure that the target time values are "nicely aligned" 
        #to the "wall clock ie (normally) that all "on-the-hour" values are included.
        # For instance, output_dt=600 should result in output on minute values 
        # 00, 10, 20, 30, 40, and 50, not eg 05, 15, 25, 35, 45, and 55.
        # We want to start at the first *aligned* time step *at_or_after* the first 
        # (sub)series time step.
        # Note that this will *ALWAYS* "round" output times (as they are forced-aligned).
        tid0=timestart
        prev_midnight= datetime.datetime(tid0.year, tid0.month, tid0.day)
        dt_after_midnight=((tid0-prev_midnight).total_seconds())/dt
        starts_after_midnight=math.ceil(dt_after_midnight-0.0000001)*dt
        timestart=prev_midnight+datetime.timedelta(seconds=starts_after_midnight)
        if verbloc and tid0 != timestart:
            print("  Aligning output time steps to midnight")
    #
    # Possibly round times (start+stop)
    if not roundto is None:
        timestart = round_dates(np.asarray([timestart,]), roundto=roundto,verb=verbloc)[0]
        timestop  = round_dates(np.asarray([timestop, ]), roundto=roundto,verb=verbloc)[0]
    #
    # Find total length of time span in seconds and in #dts:
    range_seconds = (timestop - timestart).total_seconds()
    ndt           = int(np.floor( (range_seconds+0.5*dt)/dt ))
    if verbloc:
        print(" Creating time series (datetime array) from '%s' to '%s' with dt=%s (%d elements)"%(str(timestart),str(timestop),str(dt),ndt+1))
    #print "KURT1",str(timestart),str(timestop)
    #print "KURT2",dt,range_seconds ,ndt
    # There are ndt time steps, so ndt+1 time values (0...ndt)
    datevals_list = [timestart + datetime.timedelta(seconds=x*dt) for x in range(ndt+1)]
    datevals=np.asarray(datevals_list,dtype=datetime.datetime)
    #print "KURT3",str(datevals[0]),str(datevals[1]),'...',str(datevals[-2]),str(datevals[-1])
    #print "KURT4",datevals
    return datevals

def keep_only_aligned(datevals,varvals, alignto=None, verb=None):
    verbloc=_verbloc(verb)
    #
    if alignto is None:
        global select_align
        alignto=select_align
    if alignto is None:
        # Nothing to do / not set:
        if verbloc:
            print("keep_only_aligned: Alignment not set - not deleting time series elements")
        return datevals,varvals
    #
    # Get an "aligned" equidistant time series - just the datevals:
    output_datevals=create_time_range(timestart=datevals[0],timestop=datevals[-1],dt=alignto,alignto=alignto,verb=verbloc)
    # 
    # Create an output/target vector for the values:
    output_varvals=np.empty(shape=output_datevals.shape,dtype=np.float64)
    output_varvals.fill(np.float64('NaN'))
    #
    # Now copy over the values, where the dates match:
    datevals,varvals= insert_data(datevals_src=datevals,       varvals_src=varvals,
                                  datevals_tgt=output_datevals,varvals_tgt=output_varvals,
                                  verb=verbloc)
    # Get rid of any left-over NaNs (from the equidistant-part):
    datevals,varvals=remove_nans(datevals,varvals,verb=verbloc)
    #
    # All done:
    return datevals,varvals

##
# Apply set options to time series.
# This method can be called by other scripts, which import this module, 
# such that they avoid to actually "know about" the options of the present module.
def apply_options(datevals,varvals):
    #
    global roundtime
    if not roundtime is None:
        datevals=round_dates(datevals)
    #
    global rmdups_method
    if not rmdups_method is None:
        datevals,varvals=remove_duplicates(datevals,varvals)
    #
    global select_align
    if not select_align is None:
        datevals,varvals=keep_only_aligned(datevals,varvals,alignto=select_align)
    #
    global delete_from_time
    global delete_to_time
    if (not delete_from_time is None) or (not delete_to_time is None):
        datevals,varvals=delete_time_range(datevals,varvals)
    #
    global set_start_time
    global set_stop_time
    if (not set_start_time is None) or (not set_stop_time is None):
        datevals,varvals=set_time_range(datevals,varvals)
    #
    global delete_nonequidistant
    if delete_nonequidistant:
        datevals,varvals=remove_nonequidistant(datevals,varvals)
    #
    global min_group_size
    if min_group_size>1:
        datevals,varvals=remove_lonely(datevals,varvals,mingroupsize=min_group_size)
    #
    # All done
    return datevals,varvals

##
# Default behaviour is to just show options (as per help)
def main():
    parser=args_define_options()
    args=parser.parse_args()
    args_parse_options(args)
    parser.error('Call as script with -h or --version to get info about module.')

if __name__ == '__main__':
    main()
