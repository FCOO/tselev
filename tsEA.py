#!/usr/bin/python
#
"""
Read a stack of netcdf files and write files with 
different max/min/avg/median and percentiles in 
an Ensamble Average way.
Each netcdf file (input and output both) must/will contain
just a single time series elev[time].
Time axes must be identical in all files.

The naming of the present script is equivalent to "ncea" 
(from NCO by Zender), but it will only work on tsNetcdf files,
we will have more/different "averagers", and we will perform 
more than a single averager operation at a time (maybe not?)
"""

#
# Module history:
#  2022-06-14 bjb 0.0.0: Initial version. Simple read input files to numpy array
version_info = (0, 0, 0)
version      = '.'.join(str(c) for c in version_info)
__version__  = version


#
# For now, only use as a script, not include as a module.
# At some point, it may be feasible to use as module to use with other scripts/modules,
# but for now that has not been considered.
if __name__ != '__main__':
    raise AssertionError("tsEA.py should not be imported - only run as script")

#
# Locally implemented modules
import tsSimple as ts
import tsNetcdf as tsnc

# Other imports
import numpy   as np
import datetime
import argparse


# Global variables for this module/method, including defaults
#  Ensemble Averager options:
averagerName=None


argparse_description="""Perform Ensemble Averaging on a number of (input) time series. 
Input (source) data and output (target) data in local tsElev-compliant NetCDF files.
Output file name can be a file name mould - '%%y' may evaluate to averager name to 
compute several averagers in one call.
"""

# TODO: comma-separated averager names, with an output-file name mould

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

##
# This is to keep track of what positional arguments are defined.


# Define possible named command line options
def args_define_options(parser=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    # ============= parameters to select averager type
    parser.add_argument('--averager', '-y', dest="averagerName", 
                        metavar="<NAME>", #type=string,
                        action="store", default=None,
                        required=True, type=_check_commasep_yopr,
                        help="Select averager to use in this operation. min, max, avg (or mean), median, qNN (for percentile NN). "+\
                        "Use comma-separated list to create several output files (output file name must then contain '%%y'). "+\
                        "There is no default.")
    #
    _have_defined_options=True
    # Let other used modules define their options if we are in MAIN/SCRIPT mode.
    # In the present case, we do not want any such options??
    if __name__ == '__main__':
        # Include tsnc options:
        parser = tsnc.args_define_options(parser,recurse=True)
    return parser

# Companion parser for args_define_options
# This just copies arguments to module-scoped variables
def args_parse_options(parsed_args):
    global verbose 
    verbose       = parsed_args.verbose # tsnc.verbose
    # Now deal with our own options:
    global averagerName
    averagerName=parsed_args.averagerName
    # TODO: Sanity check on averager name
    #
    if __name__ == '__main__':
        tsnc.args_parse_options(parsed_args)
    #
    return



# Define positional arguments.
# In this case we can have any number of input files - and in some/most cases 
# also an output file name.



# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC>  <NCTGT>
def args_define_positional(parser=None):
    if __name__ != '__main__':
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    # Define a two mandatory arguments here: IN OUT
    parser = tsnc.args_define_positional_Nplus1(parser)
    #parser = tsnc.args_define_positional_two(parser)
    return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
    tsnc._do_parse_positional(parsed_args)
    return

#
# Validate -y/--averager comma-separated list.
def _check_commasep_yopr(commalist):
    for yopr in commalist.split(","):
        _check_one_yopr(yopr)
    return commalist

# This is to check a single averager/y-operator
def _check_one_yopr(yopr):
    if len(yopr)<1:
        raise argparse.ArgumentTypeError("Unsupported empty averager/operator (dont give comma-comma)")
    if yopr in ('max','min','mean','avg','median'):
        pass
    elif yopr[0]=='q':
        try:
            qval=int(yopr[1:])
        except:
            raise argparse.ArgumentTypeError("Unsupported averager/operator: {} looks like a percentlile, but '{}' does not seem tp be an integer".format(str(yopr),yopr[1:]))
        if qval<0 or qval>100:
            raise argparse.ArgumentTypeError("Unsupported averager/operator: {} percentlile ({}) out of range [0:100]".format(str(yopr),qval))
    else:
        raise argparse.ArgumentTypeError("%s is an unsupported averager/operator" % str(yopr))
    return




def main():
    # Deal with options:
    parser=args_define_options()
    parser=args_define_positional(parser)
    global args
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Sanitizing of arguments is primarily done by the argparser.
    #
    # Sanity check:
    yoprs=(args.averagerName).split(",")
    nctgt=args.nctgt
    if len(yoprs)>1:
        print("nctgt={}".format(nctgt))
        # In this case the output file name must contain a '%y' descriptor for multiple output files
        assert '%y' in nctgt,"Multiple averagers specified, but output file name '{}' does not contain replace-string '%y'".format(nctgt)
    #
    verbose=ts._verbloc()
    #
    # Local shorthand to list of input files:
    ncsrcs=args.ncsrc
    nsrcs=len(ncsrcs)
    print("Will read {} netcdf files with data".format(nsrcs))
    #
    # Read first data file to get metadata:
    datevals0,varvals0,longname0=tsnc.ncfil_load(filename=ncsrcs[0])
    ntim=datevals0.size
    #
    # Allocate a single big array for all the varvals (over all the files):
    vvarvals=np.empty(shape=(ntim,nsrcs),dtype=type(varvals0[0]))
    # Copy data for first file:
    vvarvals[:,0]=varvals0[:]
    #
    # Read remaining input files one at a time:
    for ifil in range(1,nsrcs):
        datevalsi,varvalsi,longnamei=tsnc.ncfil_load(filename=ncsrcs[ifil])
        # Sanity check - just make sure that the length matches.
        # In principle, we could test also the actual time values, but that could be expensive
        #print(abs(datevalsi-datevals0))
        assert datevalsi.size == ntim, \
            "File length discrepancy. First file has {} time steps, while there are {} in {}".format(ntim,datevalsi.size,ncsrcs[ifil])
        vvarvals[:,ifil]=varvalsi[:]
    #
    for yop in yoprs:
        vals=None
        if   yop=='min':
            vals=np.amin(vvarvals,axis=1,keepdims=False)
        elif yop=='max':
            vals=np.amax(vvarvals,axis=1,keepdims=False)
        elif yop=='mean' or yop=='avg':
            vals=np.mean(vvarvals,axis=1,keepdims=False)
        elif yop=='median':
            vals=np.median(vvarvals,axis=1,keepdims=False)
        elif yop[0]=='q':
            qval=int(yop[1:])
            vals=np.percentile(vvarvals,q=qval,axis=1,keepdims=False)
        else:
            raise AssertionError("Unexpected value for averager name '{}'".format(yop))
        # Ensure that the representation is same as fo input varvals
        #vals=vals.astype(dtype=type(varvals0[0]))
        # Now let tsNetcdf save the file:
        fnam=nctgt.replace('%y',yop)
        tsnc.ncfil_save(datevals=datevals0,varvals=vals,filename=fnam)
    # All done
    exit(0)


if __name__ == '__main__':
    main()
