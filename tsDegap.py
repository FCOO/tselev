#!/usr/bin/python
#
"""
Try to remove "gaps" (periods with missing values) in a time series. 
Only gaps up to a specified length will be treated.
A polynomial will be fitted through the data values before and after the gap, 
and the polynomial will be used to estimate data values in the gap.
"""

# 
# Module history:
#  2019-10-03 bjb 0.0.1: Initial work
#  2019-10-08 bjb 0.0.2: Add option for higher-order derivative for smoothness detection
#  2019-10-11 bjb 0.0.3: Significant changes in how gaps are filled - selection of order etc
#  2020-01-31 bjb 0.0.4: Add B-spline-based method for larger gaps.
#  2020-03-23 bjb 0.0.5: Implement harmonic/M2 based method for larger gaps.
#  2020-03-26 bjb 0.0.6: Improvements to fitting-windows for gapmethod=harmonic
#  2020-03-26 bjb 0.0.7: Bugfix gapmethod=harmonic
#  2020-03-26 bjb 0.0.8: Bugfix gapmethod=harmonic - add multiple start points to curvefit
#  2020-03-26 bjb 0.0.9: Better error-estimate (log feature only) for gapmethod=harmonic

version_info = (0, 0, 9)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

#
# The present file will implement a few new routines/methods, and rely on
# tsSimple and tsNetcdf to do the heavy lifting.

#
# This file may be used as a module (imported) or as a script.

import numpy as np
import datetime
from scipy import interpolate # used for gap_method=spline
# Locally implemented modules
import tsSimple as ts

# We do not need access to netcdf unless called as a script:
if __name__ == '__main__':
    import tsNetcdf as tsnc

# Possible file to store just the interpolating polynomial values 
# This may be feasible for debugging, and for trying out various methods (orders, window sizes etc):
ncfit = None

# There are a few methods to use for degapping.
# The primary is to fit a polynomial loosely through points at both ends.
# The secondary is to use a B-spline with some internal knots. This will be 
# less accurate for smaller gaps, but could give some decent over-all 
# low-frequency statistics for large gaps.
gap_method        = 'polynomial'
gap_poly_methods  =['polynomial','spline']
gap_method_choices=['polynomial','spline','harmonic']

# Global variables for this module/method, including defaults
gap_maxwidth   = None # Max window/gap width to fill - in seconds
gap_maxnpts    = None # Alternative to width - specify gap in #missing points.
#
poly_order     = None # Order of poly to fit with (will be a max value!)
poly_minorder  = None
#
#
fit_width     = None # How wide a window to use (maybe we will need both absolute and relative settings?) TBD
#                     #  Better: Use either a fixed width (eg 6h=21600s) or a gap+sides (sides=1h, 2h, ..?)
fit_sidewidth = None # Alternative to fit_width give how much time on each side of the gap should be used for 
#                     # interpolation. Actual width would then be gapwidth+2*sidewidth
fit_nsidepts  = None # Alternative to width: Give number of points on each side. (Must be positive)
#
# Option to allow shifting of the interpolation window back and forth to
# achieve better interpolation results. A low integer 0-2 is suggested.
poly_nshift    = 0
# 
# Option on which higher-order derivative to use for smoothness estimation:
poly_minimizer        = 'f2'
poly_minimizer_choices=['f2','f3','f4']
#
# What order spline to use for bspline method
spline_order   = None # Should default to something reasonable?
#
# This is a selector of what the OPTINAL output stores:
#   'full': (the default) store poly values in entire interpolating window (some values may be overwritten).
#   'gap':  Only store values "in the gap".
poly_optoutput='full'
poly_optoutput_choices=['full','gap']
#
# 
argparse_description="Fill gaps in a time series using polyfit of values before and after the gap."



# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

# Define possible named command line options
def args_define_options(parser=None, ):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    # This module cannot be called as script (yet), but we will add the 
    # magic needed if someone opts to implement it later:
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    #
    global gap_method_choices
    global gap_method
    parser.add_argument("--gapmethod",dest="gapmethod", metavar="<METHOD>",
                        default=gap_method,
                        choices=gap_method_choices, #type=_checkroundunit,
                        help='select which type of method is used for degapping.'+
                        ' Default is %s. '%gap_method +
                        'Allowed values are %s'%', '.join(gap_method_choices))
    #
    # Optional arguments (aka "options"):
    needgapopts=False
    needwidthopts=False
    needpolyopts=False # This depends on the type of 
    if __name__ == '__main__':
        needgapopts=True
        needsideopts=True
    #
    gapwidthgroup = parser.add_mutually_exclusive_group(required=needgapopts)
    global gap_maxwidth
    gapwidthgroup.add_argument("--gapmaxwidth",dest="gapmaxwidth", 
                               type=ts._check_positive_float, metavar="<MAXWIDTH>",
                               default=gap_maxwidth,
                               help='maximum width of gaps to fill.'+
                               'Width is computed as time between values before and after gap. '+
                               'Thus, smallest gap is 2*dt')
    global gap_maxnpts
    gapwidthgroup.add_argument("--gapmaxnpts",dest="gapmaxnpts", 
                               type=ts._check_positive_int, metavar="<NMAX>",
                               default=gap_maxnpts,
                               help='maximum number of points in gaps to fill. '+
                               'Only points IN the gap are counted. '+
                               'Thus, smallest gap is 1 (one) point.')
    #
    global poly_order
    parser.add_argument("--gappolyorder",dest="gappolyorder", required=False, # Only required for *some* fit types
                        type=ts._check_positive_int, metavar="<ORDER>",
                        default=poly_order,
                        help='(max) order of polynomials to use for fitting. Lower-order polys may also be tried for properties in each gap')
    #
    global poly_minorder
    parser.add_argument("--gappolyminorder",dest="gappolyminorder", required=False,
                        type=ts._check_positive_int, metavar="<ORDER>",
                        default=poly_minorder,
                        help='minimum order of polynomials to use for fitting. This may be used with polyorder to fix the actual order used. YMMV')
    #
    widthgroup = parser.add_mutually_exclusive_group(required=needwidthopts)
    global fit_width
    widthgroup.add_argument("--gapfitwidth","--gappolywidth",dest="gapfitwidth",
                            type=ts._check_positive_float, metavar="<WIDTH>",
                            default=fit_width,
                            help='width of fitting windwow (in seconds) for each gap. '+
                            'Note that as the gap widths (may) differ, the number of values available for fitting may also vary..')
    global fit_sidewidth
    widthgroup.add_argument("--gapfitsidewidth","--gappolysidewidth",dest="gapfitsidewidth",
                            type=ts._check_positive_float, metavar="<WIDTH>",
                            default=fit_sidewidth,
                            help='width of data-window to use before and after each gap. '+
                            'With this option, the number of values available for fitting should be constant, ie not vary from one gap the next')
    global fit_nsidepts
    widthgroup.add_argument("--gapfitnsidepts","--gappolynsidepts",dest="gapfitnsidepts",
                            type=ts._check_positive_int, metavar="<n>",
                            default=fit_nsidepts,
                            help='number of points to use for fitting data-window before and after each gap. '+
                            'Width this option, the number of values available for fitting should be constant, ie not vary from one gap the next')
    #
    global poly_nshift
    parser.add_argument("--gappolynshift",dest="gappolynshift", required=False,
                        type=ts._check_nonnegative_int, metavar="<IMAX>",
                        default=poly_nshift,
                        help='allow the shifting of the polynomial interpolation window by a number of points back and forth. '+
                        'It is suggested to use a small integer in the range 0-2. '+
                        'Default is 0 - not to shift.')
    #
    global poly_minimizer_choices
    global poly_minimizer
    parser.add_argument("--gappolyminimizer",dest="gappolyminimizer", metavar="<NAME>",
                        choices=poly_minimizer_choices, #type=_checkroundunit,
                        help='select which higher-order derivative will be used to estimate '+
                        'the smoothnes of an interpolating polynomial (for gapmethod=polynomial).'+
                        ' Default is %s. '%poly_minimizer +
                        'Allowed values are %s'%', '.join(poly_minimizer_choices))
    #
    global spline_order
    parser.add_argument("--gapsplineorder",dest="gapsplineorder", 
                        type=ts._check_positive_int, metavar="<ORDER>",
                        default=poly_order,
                        help='order B-spline through gap (for gapmethod=spline)')
    # 
    global poly_optoutput
    global poly_optoutput_choices
    parser.add_argument("--gapfitoptout","--gappolyoptout",dest="gappolyoptout", metavar="<choice>",
                        default=poly_optoutput,
                        choices=poly_optoutput_choices, #type=_checkroundunit,
                        help='select whether optional output (third positional argument) '+
                        'contains data from full interpolation window or just from the gap. '+
                        'For the full window, some values may be overwritten '+
                        '(but none of these are actually used to fill the gaps) '+
                        'CAVEAT: may not work - YMMV. '
                        'Default is %s. '%poly_optoutput +
                        'Allowed values are %s'%', '.join(poly_optoutput_choices))
    _have_defined_options=True
    # Let other used modules define their options if we are in MAIN/SCRIPT mode:
    if __name__ == '__main__':
        parser = ts.args_define_options(parser)
        parser = tsnc.args_define_options(parser)
    return parser
#
# Companion parser for args_define_options
def args_parse_options(parsed_args):
    global verbose 
    verbose       = parsed_args.verbose # tsnc.verbose
    # Now deal with our own options:
    global gap_method
    gap_method = parsed_args.gapmethod
    if verbose:
        print(" setting gapmethod=%s"%gap_method)
    #
    global gap_maxwidth
    gap_maxwidth = parsed_args.gapmaxwidth
    if verbose and not gap_maxwidth is None:
        print(" setting gapmaxwidth=%s"%gap_maxwidth)
    #
    global gap_maxnpts
    gap_maxnpts = parsed_args.gapmaxnpts
    if verbose and not gap_maxnpts is None:
        print(" setting gapmaxnpts=%s"%gap_maxnpts)
    #
    global poly_order
    poly_order = parsed_args.gappolyorder
    if verbose and not poly_order is None:
        print(" setting gappolyorder=%s"%poly_order)
    #
    global poly_minorder
    poly_minorder = parsed_args.gappolyminorder
    if verbose and not poly_minorder is None:
        print(" setting gappolyminorder=%s"%poly_minorder)
    #
    global fit_width
    fit_width = parsed_args.gapfitwidth
    if verbose and not fit_width is None:
        print(" setting gapfitwidth=%s"%fit_width)
    global fit_sidewidth
    fit_sidewidth = parsed_args.gapfitsidewidth
    if verbose and not fit_sidewidth is None:
        print(" setting gapfitsidewidth=%s"%fit_sidewidth)
    global fit_nsidepts
    fit_nsidepts = parsed_args.gapfitnsidepts
    if verbose and not fit_nsidepts is None:
        print(" setting gapfitnsidepts=%s"%fit_nsidepts)
    #
    global poly_nshift
    poly_nshift = parsed_args.gappolynshift
    if verbose>2 or (verbose and not parsed_args.gappolynshift is None):
        print(" setting gappolynshift=%s"%poly_nshift)
    #
    global poly_minimizer
    if not parsed_args.gappolyminimizer is None:
        poly_minimizer = parsed_args.gappolyminimizer
    if verbose>2 or (verbose and not parsed_args.gappolyminimizer is None):
        print(" setting gappolyminimizer=%s"%poly_minimizer)
    #
    global spline_order
    if not parsed_args.gapsplineorder is None:
        spline_order = parsed_args.gapsplineorder
    if verbose and not spline_order is None:
        print(" setting gapsplineorder=%s"%spline_order)
    #
    global poly_optoutput
    poly_optoutput = parsed_args.gappolyoptout
    if __name__ == '__main__' and verbose and parsed_args.ncfit:
        print(" optional (third positional) argument will store fitted values for '%s' range"%poly_optoutput)
    #
    # Let imported modules deal with their own options:
    if __name__ == '__main__':
        ts.args_parse_options(parsed_args)
        tsnc.args_parse_options(parsed_args)
    return

# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC> <NCSRC2> <NCTGT>
def args_define_positional(parser=None):
    if __name__ != '__main__':
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    # Define two mandatory arguments here. IN OUT:
    parser = tsnc.args_define_positional_two(parser)
    # Add a special optional positional argument, which - if given - will 
    # be an output file with just the polyfit data - over each window
    parser.add_argument('ncfit', action="store",metavar="<POLYNC>",
                        help='output netcdf file - with only the interpolating polynomials at the time series points',nargs='?')
    return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
    if __name__ != '__main__':
        return
    tsnc.args_parse_positional(parsed_args)
    global ncfit
    ncfit = parsed_args.ncfit
    return

# Get the [start stop]-indices of any time series gaps (periods with NaNs).
# Return values are two numpy ndarrays with, respectively, the first and last 
# index of each gap (nan period) in the time series.
# If igap is given and negative, then the number of gaps (ngaps) is returned.
# If igap is given and non-negative, then it must be and integer in [0:ngaps-1],
# and then just the first and last index of that particular gap is returned.
# It is imperative that the time series is equidistant on call: otherwise 
# the indices are meaningless.
def get_gaps_startstopidx(datevals,varvals,igap=None,verb=None):
    verbloc=ts._verbloc(verb)
    ts.assert_consistent(datevals,varvals)
    # 
    # We can only find gap indices in a series which is fully equidistant, 
    # otherwise the idxs are not well-defined.
    if not ts.is_equidistant(datevals,varvals,verb=0):
        # This is just to spit out additional info:
        ts.is_equidistant(datevals,varvals)
        # Carp and crash
        raise AssertionError("Gap indices are not meaningfull unless time series is already fully equidistant.\n"+
                             "Call piecewise_to_fully_equidistant before get_gap_startstop")
    #
    # Get a list of indices for each nan in the time series
    idxs_gaps=ts.idx_missing(datevals=datevals,varvals=varvals)
    nnans=idxs_gaps.size
    # If nnans==0, then we are already done - there are no gaps.
    # In that case, break-out, as continuing may lead to funky errors with empty arrays.
    if nnans==0:
        ngaps=0
        # We still have to treat possible specification of igap:
        if not igap is None:
            if igap < 0:
                return igap
            raise AssertionError("Cannot compute gap start/stop for (zero-based) igap=%s. There are no gaps."%(str(igap),))
        # Return arrays with NO ints:
        return np.empty(shape=(0,),dtype=np.int32),np.empty(shape=(0,),dtype=np.int32)
    #
    # Get start/stop for each gap. Possibly also number of gaps etc.
    # This is much like the ts.get_equidistant_piece except that we want the gaps rather than the pieces.
    # gaps_diff[i] is the number of dt's between nan[i] and nan[i+1].
    # So if gaps_diff[i]>1, then i is the END of a nan-gap(!)
    gaps_diff=idxs_gaps[1:]-idxs_gaps[:-1]
    # "dividers" is the ENDs of each nan-gap (except the last one, where there are no "next" nan-gap)
    dividers=np.where(gaps_diff!=1)[0]
    # Number of gaps:
    ngaps=1+dividers.size
    #
    # Return just the number of gaps, if that is wanted:
    if not igap is None and igap<0:
        return ngaps
    #
    # Increment (+1) to make dividers the START of each (next) GAP.
    dividers[:] += 1
    #
    # Add 0 and end-of-array as start-of-first and start-of-beyond-last gap.
    # Note that dividers are NOT indices into the time series.
    # Rather, dividers contains indices into the idxs_gaps array, 
    # which again contain the actual indices of the time series. 
    dividers=np.concatenate( (np.asarray([0,]), dividers, np.asarray([idxs_gaps.size,]) ), axis=0)
    #
    # Store start and stop indices of each of the Ngaps gaps:
    gaps_start=np.empty(shape=(ngaps,),dtype=int)
    gaps_stop =np.empty(shape=(ngaps,),dtype=int)
    #
    # Go over the gap-indices and store start&stop.
    for ig in range(ngaps):
        gaps_start[ig] = idxs_gaps[dividers[ig  ]  ]
        gaps_stop[ig]  = idxs_gaps[dividers[ig+1]-1]
    #
    # If caller asked for a single, specific gap, just return that:
    if not igap is None:
        if igap>=ngaps:
            raise AssertionError("Cannot compute gap start/stop for (zero-based) igap=%s. There are only %s gaps."%(str(igap),str(ngaps)))
        return gaps_start[igap],gaps_stop[igap]
    # 
    # Otherwise (no igap specified), return all the first+last indices:
    return gaps_start,gaps_stop

#
# Helper method to get a handle of common arguments for both/all degapping methods:
def join_args_and_opts(datevals,varvals,
                       gapmaxwidth=None,
                       gapmaxnpts=None,
                       polyorder=None,polyminorder=None,
                       fitsidewidth=None,fitnsidepts=None,
                       fitwidth=None,nshift=None,
                       minimizer=None,
                       splineorder=None,
                       verb=None):
    fnam="join_args_and_opts" # Just for log/debug
    verbloc=ts._verbloc(verb)
    ts.assert_consistent(datevals,varvals)
    # We will return a dict with the parsed options/arguments.
    # This will be ALL options, not just the ones appropriate for the 
    # chosen gap_method:
    parsed_opts={}
    #
    # Snipped from polyfill_gaps:
    #<argument parsing>
    global gap_method # Should only be used with __main__
    global gap_maxwidth
    global gap_maxnpts
    global fit_width
    global fit_sidewidth
    global fit_nsidepts
    global poly_order
    global poly_minorder
    global poly_nshift
    global poly_minimizer
    global spline_order
    # Deal with optional arguments (these are more or less the same as the command line options).
    # First figure out if gapwidth is determined by funcitonal arguments or command line options.
    # Default will be to use functional arguments.
    if (gapmaxwidth is None) and (gapmaxnpts is None):
        # No functional width arguments. Use globals / command line options:
        gapmaxwidth=gap_maxwidth
        gapmaxnpts = gap_maxnpts
    if (gapmaxwidth is None) and (gapmaxnpts is None):
        raise AssertionError("Missing maxwidth (or maxnpts) for gaps to interpolate. "+
                             "Either as functional argument or command line option")
    # 
    # We will need the time step size on order to set relative to poly window widths
    dt=ts.get_timestep(datevals=datevals,verb=max(0,verbloc-1))
    parsed_opts['dt']=dt
    #
    # If we do not (yet) have gapmaxwidth, then we do have gapmaxnpts, and so we can 
    # compute max gap from dt and npts:
    if   gapmaxwidth is None:
        # Gap "width" is dt + npts*dt = (1+npts)*dt. Add 0.5*dt for rounding etc.
        gapmaxwidth = (1.5 + gapmaxnpts) * dt
        if verbloc>1:
            print(fnam+": Converting gapmaxnpts=%d to gapmaxwidth=%.1f (dt=%.1fs)"%(gapmaxnpts,gapmaxwidth,dt))
    elif gapmaxnpts is None:
        # Gap "width" is dt + npts*dt ; round back to dt:
        gapmaxnpts=int( np.floor( gapmaxwidth/dt + 0.1 ) )
        if verbloc>1:
            print(fnam+": Converting gapmaxwidth=%.1f to gapmaxnpts=%d (dt=%.1fs)"%(gapmaxwidth,gapmaxnpts,dt))
    # Now these should both be defined:
    parsed_opts['gapmaxwidth']=gapmaxwidth
    parsed_opts['gapmaxnpts'] =gapmaxnpts
    #
    if polyorder is None:
        polyorder=poly_order
    if polyminorder is None:
        polyminorder=poly_minorder
    if splineorder is None:
        splineorder=spline_order
    #
    # polyminorder is only needed for "polynomial" not useful for "spline"
    if __name__ == '__main__' and gap_method == 'polynomial':
        if polyminorder is None:
            polyminorder=0
        #
        if poly_minorder>polyorder:
            raise AssertionError("Unexpectedly, %s=polyminorder>poly(max)order=%s for interpolating polynomial"
                                 %(str(polyminorder),str(polyorder)))
    # 
    if __name__ == '__main__' and gap_method == 'spline':
        if not polyminorder is None:
            raise AssertionError("gap_method=spline does not support specification of polyminorder")
        #
        if (not fitwidth is None) or (not fit_width is None):
            raise AssertionError("gap_method=spline does not support specification of fitwidth")
        #
        if splineorder is None:
            # Try global default:
            splineorder = spline_order
        if splineorder is None:
            raise AssertionError("gap_method=spline needs --gapsplineorder")
        if polyorder is None:
            polyorder=splineorder
    #
    # Polyorder is needed for both/all methods. But spline may default it (see above)
    if (gap_method in gap_poly_methods) and (polyorder is None):
        raise AssertionError("Missing (max) order for interpolating polynomial. "+
                             "Either as functional argument or command line option.\n"+
                             "Hint: --gapmethod={} requires the use of --gappolyorder".format(gap_method))
    # Store poly-orders for output:
    parsed_opts['polyorder']    = polyorder
    parsed_opts['polyminorder'] = polyminorder
    parsed_opts['splineorder']  = splineorder
    #
    # For definition of the "window width" for the interpolating polynomium there are several
    # possibilities, but we will either base on functional arguments OR command line options. 
    # Not a mix.
    #
    num_wargs=0
    for parg in [ fitwidth,  fitsidewidth,  fitnsidepts]:
        if not parg is None:
            num_wargs += 1
    # Deal with the case of no functional arguments (of this kind).
    # In that case we rely fully on command line options:
    if num_wargs == 0:
        fitwidth=fit_width
        fitsidewidth=fit_sidewidth
        fitnsidepts=fit_nsidepts
    #
    # In either case, we can have one and only one specification of window width:
    num_wspecs=0
    if (not fitwidth is None):
        num_wspecs += 1
    if (not fitsidewidth is None):
        num_wspecs += 1
    if (not fitnsidepts is None):
        num_wspecs += 1
    if num_wspecs != 1:
        raise AssertionError("Need one and only one specification of width-parameter: fitnsidepts, fitsidewidth, fitwidth")
    # If we have either of the "side" optsions, then compute the other, so the caller
    # can use either. If fitwidth is set, then these two must remain unset, as the 
    # actual "side-width" will need to be computed on a per-gap basis:
    if not fitsidewidth is None or not fitnsidepts is None:
        # This is very similar to the gapwidth above, except that 
        #   sidewidth = nsidepts*dt [ie not with the +1 as per gap width]
        if fitsidewidth is None:
            # fitnsidepts must exist. Compute from that:
            fitsidewidth = (fitnsidepts + 0.1) * dt
        else:
            #  fitsidewidth exists, but fitnsidepts does not:
            fitnsidepts=int( np.floor( fitsidewidth/dt + 0.1 ) )
    # Store in dict:
    parsed_opts['fitnsidepts'] = fitnsidepts
    parsed_opts['fitsidewidth']= fitsidewidth
    parsed_opts['fitwidth']    = fitwidth     # This is always None for spline (see above)
    #
    if nshift is None:
        nshift = poly_nshift
    if nshift is None:
            raise AssertionError("Somehow is missing poly nshift (even though there should be a default). Go debug.")
    parsed_opts['nshift']    = nshift
    #
    # What minimizer f^(2), f^(3), f^(4) to use for polynomial (not for spline)
    if minimizer is None:
        minimizer=poly_minimizer
    if minimizer is None:
        raise AssertionError("Somehow is missing poly minimizer (even though there should be a default). Go debug.")
    parsed_opts['minimizer']    = minimizer
    # TODO: Special options for gap_method=spline
    if splineorder is None:
        splineorder=spline_order
    parsed_opts['splineorder']  = splineorder
    if __name__ == '__main__' and gap_method == 'polynomial':
        if not splineorder is None:
            raise AssertionError("gap_method=polynomial does not support specification of splineorder")
    
    #</argument parsing>
    return parsed_opts


#
# Go over the entire time series, and interpolate over gaps (up to a max size) wusing local polynomials.
# For each found gap, the method will try polynomials of different order and (if allowed) try to 
# shift the interpolation-window back and forth around the gap (tshift)
def polyfill_gaps(datevals,varvals,
                  gapmaxwidth=None,
                  gapmaxnpts=None,
                  polyorder=None,polyminorder=None,
                  fitwidth=None,fitsidewidth=None,fitnsidepts=None,nshift=None,
                  minimizer=None,
                  verb=None):
    fnam="polyfill_gaps" # Just for log/debug
    verbloc=ts._verbloc(verb)
    #
    parsed_opts=join_args_and_opts(datevals,varvals,
                                   gapmaxwidth=gapmaxwidth,
                                   gapmaxnpts=gapmaxnpts,
                                   polyorder=polyorder,
                                   polyminorder=polyminorder,
                                   fitsidewidth=fitsidewidth,
                                   fitnsidepts=fitnsidepts,
                                   fitwidth=fitwidth,
                                   nshift=nshift,
                                   minimizer=minimizer,
                                   verb=verb)
    #
    # Extract my options:
    gapmaxwidth   = parsed_opts['gapmaxwidth']
    gapmaxnpts    = parsed_opts['gapmaxnpts']
    polyorder     = parsed_opts['polyorder']
    polyminorder  = parsed_opts['polyminorder']
    fitsidewidth  = parsed_opts['fitsidewidth']
    fitnsidepts   = parsed_opts['fitnsidepts']
    fitwidth      = parsed_opts['fitwidth']
    nshift        = parsed_opts['nshift']
    minimizer     = parsed_opts['minimizer']
    #
    # Shorthands:
    maxorder=polyorder
    minorder=polyminorder
    #
    # Extend series to fully equidistant. This fills NaN values in missing data/holes
    # If this is not done, then indices for gaps are meaningless.
    datevals,varvals=ts.piecewise_to_fully_equidistant(datevals=datevals,varvals=varvals)
    #
    # Shorthand for length of time series: number of elements:
    neles = varvals.size
    #
    # Create a copy, which will end up having the filled values.
    # The dates should be the same for original and the "fixed", 
    # so we can just make a second pointer to the same memory:
    datevals_filled=datevals
    # The varvals will be modified extensively, so we will dedicate new memory.
    # We will have 3 different arrays:
    #   varvals_filled: include all the original values + interpolations in thwe gaps
    #   varvals_poly:   No original values, but include polynomial approximations
    #   varvals_gap:    Poly-values, but only in the actual gaps [TODO - could be removed?]
    varvals_filled = varvals.copy() #
    # The last two will start out by nan-values
    varvals_poly=np.empty(shape=varvals.shape,dtype=varvals.dtype)
    varvals_poly[:]=np.nan
    # This one will have JUST the gap values - not the adjacent fitting/training values
    varvals_gap=np.empty(shape=varvals.shape,dtype=varvals.dtype) # TODO: May be unnecessary
    varvals_gap[:]=np.nan
    #
    # First locate all the gaps in the time series, by
    # getting first and last index for each gap in series
    gaps_idxfirst,gaps_idxlast=get_gaps_startstopidx(datevals,varvals)
    #
    if verbloc:
        print(fnam+": Going over %d gaps in time series"%gaps_idxfirst.shape[0])
    #
    num_gaps_tot = gaps_idxfirst.size
    # Counters for simple statistics/logging. Conters will be modified per loop execution
    num_gaps_filled = 0 # Actual #gaps we process and fill
    num_nans_tot    = 0 # Total number of missing-values in series.
    num_nans_filled = 0 # Number of these, which we end filling.
    # Loop over the gaps, and deal with each in turn:
    for ig in range(num_gaps_tot):
        ifirst=gaps_idxfirst[ig]
        ilast =gaps_idxlast[ig]
        num_nans_tot += 1+ilast-ifirst
        if ifirst <= 0:
            if verbloc>1:
                print(fnam+":  skipping gap #%d: not filling nan-segment at start-of-series"%ig)
            continue
        if ilast >= neles-1:
            if verbloc>1:
                print(fnam+":  skipping gap #%d: not filling nan-segment at end-of-series"%ig)
            continue
        if verbloc>1:
            print(fnam+":  examining gap #%d at [%d:%d] (%d pts in gap), ie [%s : %s]"%(ig,ifirst,ilast,ilast-ifirst+1,str(datevals[ifirst]),str(datevals[ilast])))
        # Gap width - in terms of datetime.timedelta: time between last value before gap and first after gap.
        dtgap_delta=datevals[ilast+1]-datevals[ifirst-1]
        # Gap width in seconds:
        dtgap_secs=dtgap_delta.total_seconds()
        # Center of gap - in indices. May be used later
        imidt=(ifirst+ilast)/2.
        if verbloc>2:
            print(fnam+":    gap covers %s seconds between sampled values"%str(dtgap_secs))
        if dtgap_secs>gapmaxwidth:
            if verbloc:
                print(fnam+" skipping gap #%d: too large - covering %s s > specified maxwidth"%(ig,str(dtgap_secs),))
            continue
        #
        # Now we need to find the piece of the time series to use as input for the polyfit
        # This depends on the choise for width: poly -width, -sidewidth vs -nsidepts.
        # (Recall that we already tested, that exactly ONE of these are defined - the others are None).
        # The easiest is to select N points on each side of the gap:
        if   not fitnsidepts is None:
            nside       = fitnsidepts
        elif not fitsidewidth is None:
            nside       = np.int32(np.ceil((1.0+fitsidewidth)/dt))
        elif not fitwidth is None:
            sidwid     = 0.5*(fitwidth-dtgap_secs)
            nside       = np.int32(np.ceil((1.0+sidwid)/dt))
        else:
            raise AssertionError("Out of scope. Go debug.")
        #
        ipoly_first= ifirst-nside
        ipoly_last = ilast +nside
        # Take care of polynomial windows "hanging out" of the time series
        if ipoly_first<0:
            ipoly_first=0
        if ipoly_last>=neles:
            ipoly_last=neles-1
        #
        # Number of samples (including nans) inside the window:
        npolytims  = ipoly_last-ipoly_first+1
        #
        if verbloc>2:
            print(fnam+":    poly covers %d eles (incl gap): nside=%d, ipoly_first=%d, ipoly_last=%d"%(npolytims,nside,ipoly_first,ipoly_last))
        #
        # First find the residual when we go to max poly order.
        # This is used for comparison, so that we can estimate if lower-order polys fit well or not.
        dum,dum,stdres_pmax,nextr_pmax,f2_pmax,f3_pmax,f4_pmax = polyfill_gap(varvals,
                                                                              gapidx_first=ifirst,gapidx_last=ilast,
                                                                              polyidx_first=ipoly_first,polyidx_last=ipoly_last,
                                                                              polyorder=maxorder,verb=0)
        #
        if verbloc>2:
            print(fnam+":    %d-order polynomial fit results in std residual of %.2e"%(maxorder,stdres_pmax))
        #
        # Just set the "best minimizer" to something crazy, so this particular case will not pan out:
        mini_best = 10^12 # * (f2_pmax+f3_pmax+f4_pmax)
        # Best (so far) - the only one checked
        #  TODO: Set option on optimize based on f2, f3, or f4.
        #if   minimizer == 'f2':
        #    mini_best    = f2_pmax
        #elif minimizer == 'f3':
        #    mini_best    = f3_pmax
        #elif minimizer == 'f4':
        #    mini_best    = f4_pmax
        #else:
        #    raise AssertionError("Unexpected minimizer value = '%s'. Go debug."%minimizer)
        # Keep track of "best minimizer" (will be unrealistinc for low orders, so rely on residual)
        # and best residual (in case the higher-order version fail)
        minibest_value=10^12
        minibest_porder=-1
        minibest_ioff=0
        #
        resibest_value=10^12
        resibest_porder=-1
        resibest_ioff=0
        # Look for better (yes, one of these will be the one from above)
        for porder in range(minorder,maxorder+1):
            for ioff in range(-nshift,nshift+1):
                # Keep track of how many (non-nan) data points we haev before and after the gap:
                ipfirst=ipoly_first+ioff
                iplast=ipoly_last+ioff
                if ipfirst<0 or iplast>=varvals.size:
                    if verbloc>2:
                        print(fnam+"  ... skipping porder=%d ioff=%d (window ends hanging out of time series)"%(porder,ioff))
                    # Skip this - go to next
                    continue
                # Check if there are a reduced number of fitting-points (non-nan varvals) 
                # in the pre-gap or post-gap fitting windows.
                # Order in the polyfill (which calls numpy.polyfit) corresponds to polynomial order,
                # ie the used #coeffs is order+1.
                # For instance, to do linear fitting (order=1), we will need AT LEAST one point at EACH end.
                num_pre  = np.sum(~np.isnan(varvals[ipfirst:ifirst]))
                num_post = np.sum(~np.isnan(varvals[ilast+1:iplast+1]))
                if num_pre<porder or num_post<porder or num_pre==0 or num_post==0:
                    if verbloc>2:
                        print(fnam+"  ... skipping porder=%d ioff=%d (reduced npts at ends)"%(porder,ioff))
                    # Skip this - go to next
                    continue
                gapvals,gapvals_extd,stdres,nextr,f2,f3,f4 =  polyfill_gap(varvals,
                                                                           gapidx_first=ifirst,gapidx_last=ilast,
                                                                           polyidx_first=ipfirst,polyidx_last=iplast,
                                                                           polyorder=porder,verb=0)
                # Keep track of best residual so far:
                if stdres<resibest_value:
                    resibest_porder=porder
                    resibest_ioff=ioff
                #
                # If the residaul is "good enough", then also check - and possibly store - based on minimizer
                if stdres>2.*stdres_pmax:
                    if verbloc>2:
                        print(fnam+":     skipping order=%d, offset=%d (too large residual = %.2e)"%(porder,ioff,stdres))
                    continue
                if   minimizer == 'f2':
                    mini_here    = f2_pmax
                elif minimizer == 'f3':
                    mini_here    = f3_pmax
                elif minimizer == 'f4':
                    mini_here    = f4_pmax
                if mini_here<minibest_value:
                    minibest_value=mini_here
                    minibest_porder=porder
                    minibest_ioff=ioff
        #
        # Normally, we want to use the poly based on the minimizer, but sometimes that is not found:
        if minibest_porder>=0:
            porder_best=minibest_porder
            ioff_best  =minibest_ioff
        else:
            porder_best=resibest_porder
            ioff_best  =resibest_ioff
        #
        # Sometimes there is just nothing to do (order will be -1)
        if porder_best<0:
            if verbloc:
                print(fnam+" skipping gap #%d: no matching poly update found"%(ig,))
            # Just skip to next gap - do not make an update
            continue
                
        #
        if verbloc:
            print(fnam+" degapping gap #%d [%s - %s]"%(ig,str(datevals[ifirst]),str(datevals[ilast])))
        if verbloc>1:
            print(fnam+":    selecting polynomial order=%d, ishift=%d for gap #%d"%(porder_best,ioff_best,ig))
        # Recompute the "best" to store in target array(s)
        gapvals,gapvals_extd,stdres,nextr,f2,f3,f4 =  polyfill_gap(varvals,
                                                                   gapidx_first=ifirst,gapidx_last=ilast,
                                                                   polyidx_first=ipoly_first+ioff_best,polyidx_last=ipoly_last+ioff_best,
                                                                   polyorder=porder_best,verb=0)
        #
        # Store in varvals copy (for interpolated gaps values):
        varvals_filled[ifirst:ilast+1]=gapvals
        varvals_poly[ipoly_first+ioff_best:ipoly_last+1+ioff_best]=gapvals_extd
        varvals_gap[ifirst:ilast+1]=gapvals
        #
        # Update statistical counters:
        num_gaps_filled += 1
        num_nans_filled += 1+ilast-ifirst
    #
    # Write simple statistics:
    if verbloc:
        print(fnam+": Filled %d of %d gaps (%d of %d total nan values) in series"%(num_gaps_filled,num_gaps_tot,num_nans_filled,num_nans_tot))
    #
    # Time series all updated.
    # The selector poly_optoutput selects between varvals_{poly,gap}
    global poly_optoutput
    if   poly_optoutput == 'full':
        varvals_opt = varvals_poly
    elif poly_optoutput == 'gap':
        varvals_opt = varvals_gap
    else:
        raise AssertionError("Unexpected value for optout='%s'. Go debug."%str(poly_optoutput))
    #
    return datevals_filled,varvals_filled,varvals_opt
    



# Make a polyfit over a particular gap.
#   Return: 
#     gapvals: Estimated values in the gap
#     gapvals_extd: All the values, which are used for fit+gap. some kind of indexing is needed!
#     stdres:    std of residual values - "goodness of fit"
#     nextr:     number of local extremums (max/min) found in the gap. Values > 1 could indicate a problem
#     f2,f3,f4:  abs-sum of fd estimate of second, third and fourth poly derivative over the gap. 
#                The derivatives can be used as "smoothness indixators", small values indicate smooth.
#     OLD: penalty:   Sum of local |max-min| - normalized with total range of poly found
def polyfill_gap(varvals,gapidx_first,gapidx_last,polyidx_first,polyidx_last,polyorder,verb=None):
    verbloc=ts._verbloc(verb)
    #
    # Default to (max) poly order:
    if polyorder is None:
        polyorder=poly_order
    #poly_times=np.asarray(range(ipoly_last-ipoly_first)
    # This is the total number of points within the poly-range, ie gapplus "side bands" used for estimation:
    npolytims=1+polyidx_last-polyidx_first
    ngaptims =1+gapidx_last -gapidx_first
    #
    # Sanity checks:
    if gapidx_first<=polyidx_first:
        raise AssertionError("Internal error: Refuse to fit gap one-sided (gapidx_first=%d, polyidx_first=%d). Go debug"%(gapidx_first,polyidx_first))
    if gapidx_last>=polyidx_last:
        raise AssertionError("Internal error: Refuse to fit gap one-sided (gapidx_last=%d, polyidx_last=%d). Go debug"%(gapidx_last,polyidx_last))
    # Define x-values in the range [-1:1] to have decent conditioning of the resulting system for polyfit
    dx=2./(npolytims-1)
    xvals=2*np.arange(npolytims)/(npolytims-1.)-1
    vvals=varvals[polyidx_first:polyidx_last+1] # TODO: Find xvals_gap
    gapidx_loc_first=gapidx_first-polyidx_first
    gapidx_loc_last =gapidx_first-polyidx_first+ngaptims-1
    xvals_gap=xvals[gapidx_loc_first:gapidx_loc_last+1]
    #
    # Stupid sanity checks:
    errstr=""
    if   vvals.size!=npolytims:
        errstr="Internal error: Expected vvals.size==%d, but got %d. Go debug"%(npolytims,vvals.size)
    elif xvals.size!=npolytims:
        errstr="Internal error: Expected xvals.size==%d, but got %d. Go debug"%(npolytims,xvals.size)
    elif xvals[0]!=-1:
        errstr="Internal error: Expected xvals[0]==-1, but got %s. Go debug"%str(xvals[0])
    elif xvals[-1]!=1:
        errstr="Internal error: Expected xvals[-1]==1, but got %s. Go debug"%str(xvals[-1])
    if errstr != "":
        print(" Something when really bad in polyfill_gap - debugging may be needed")
        print("  dx=%f"%dx)
        print("  gap[%d]=%d-%d"%(ngaptims,gapidx_first,gapidx_last))
        print("  poly[%d]=%d-%d"%(npolytims,polyidx_first,polyidx_last))
        print("  xvals[%d]=%s"%(xvals.size,str(xvals)))
        print("  vvals[%d]=%s"%(vvals.size,str(vvals)))
        raise AssertionError(errstr)
    #
    # The nan-values should not be part of the estimator/training/fitting set of data:
    # Basically, this is the gap in localized poly-size coordinates/indices
    idx_nans=np.where(np.isnan(vvals))[0]
    if verbloc>2:
        print("polyfill_gap:  localized gap idx: "+str(idx_nans))
    # 
    # For the training (polyfit) we do not use nan data:
    xvals_train=np.delete(xvals,idx_nans)
    vvals_train=np.delete(vvals,idx_nans)
    #
    if verbloc>2:
        print("polyfill_gap:  xvals=",str(xvals_train))
        print("polyfill_gap:  vvals="+str(vvals_train))
    #   NOTE: It does not really help to set rcond to exclude certain singular values,
    #         as the singular values seem to depend only on the time-values (xvals_train) and 
    #         not on the funciton (observation; elevation) values (vvals_train).
    #         Thus, the singluar values are basically the same for all gaps of equal size.
    #   NOTE2: Specifying full=True will give some additional information:
    #         A: "residuals" -> sum-of-squares of residual values at xtrain points.
    #            This can be computed later, if we want.
    #         B: rank: Hopefully, this is always = polyorder+1, unless the system is singular.
    #         C: singular_values: These should be the same for all gaps of the same size (see above)
    #         D: _used_ value of rcond, which defaults to xvals_train.size*(np.finfo(float)).eps, ie len*2.22^-16
    #       So really, we do not need any of the full output. 
    #       We can compute the residuals, and the rest is not important in terms of sorting "good" fits from "bad".
    #poly_weights,residuals, rank, singular_values, rcond = np.polyfit(xvals_train,vvals_train, poly_order, full=True)#, w=wvals_train)rcond=0.01,
    poly_coeffs = np.polyfit(xvals_train,vvals_train, polyorder)
    #
    # This just makes the found coefficients in to a polynomial, which we can evaluate:
    poly_model = np.poly1d(poly_coeffs)
    #
    # Compute polomial data in:
    #   A: training points (this is to compute residual/std)
    #   B: full xrange (to send back extended poly)
    #   C: values in gap (this should be part of the ull xrange as well)
    # Really, B should be the union "A U C"
    poly_train   = poly_model(xvals_train)-vvals_train
    poly_predict = poly_model(xvals)
    poly_gap     = poly_model(xvals_gap)
    #xvals_gap=xvals[gapidx_loc_first:gapidx_loc_last+1]
    #vvals_gap=vvals[gapidx_loc_first:gapidx_loc_last+1] # Note really necessary.
    # Get residuals in the training points:
    residual = np.std(poly_train)
    #
    # In principle, we can find analytical derivative of the poly1d.
    p1=np.polyder(poly_model,m=1)
    p2=np.polyder(poly_model,m=2)
    p3=np.polyder(poly_model,m=3)
    p4=np.polyder(poly_model,m=4)
    #
    # We will compute the abs-sum of higher derivaties f''=f^(2), f^(3) and f^(4) 
    # to give an indication of the "smoothness" of the solution within the gap.
    # Also, we will estimate the number of extrema of the poly in the gap,
    # as we do not want a log of "kinks" in the predited polynomial (within the gap).
    #  These are the (analytical) derivatives at the gap x-values:
    p1gap=p1(xvals[gapidx_loc_first:gapidx_loc_last+1])
    #  This is then how many sign-changes in the derivative, ie extrema in the poly itself.
    #   To get the actual values would require a little extra work
    nextrs=np.sum(p1gap[0:-1]*p1gap[1:]<0)
    #
    # Compute the abs-sum of the second to fourth derivative as "smoothness indicators":
    f2_sum=np.sum(np.abs(p2( xvals[gapidx_loc_first:gapidx_loc_last+1] )))
    f3_sum=np.sum(np.abs(p3( xvals[gapidx_loc_first:gapidx_loc_last+1] )))
    f4_sum=np.sum(np.abs(p4( xvals[gapidx_loc_first:gapidx_loc_last+1] )))
    return poly_gap,poly_predict,residual,nextrs,f2_sum,f3_sum,f4_sum



##
# Secondary fill method
def splinefill_gaps(datevals,varvals,
                    gapmaxwidth=None,
                    gapmaxnpts=None,
                    polyorder=None,
                    fitsidewidth=None,fitnsidepts=None,
                    splineorder=None,
                    verb=None):
    fnam="splinefill_gaps" # Just for log/debug
    verbloc=ts._verbloc(verb)
    parsed_opts=join_args_and_opts(datevals,varvals,
                                   gapmaxwidth=gapmaxwidth,
                                   gapmaxnpts=gapmaxnpts,
                                   polyorder=polyorder,
                                   fitsidewidth=fitsidewidth,fitnsidepts=fitnsidepts,
                                   splineorder=splineorder,
                                   verb=verb)
    #
    # Extract my options:
    gapmaxnpts    = parsed_opts['gapmaxnpts']
    gapmaxwidth   = parsed_opts['gapmaxwidth']
    polyorder     = parsed_opts['polyorder']
    fitnsidepts  = parsed_opts['fitnsidepts']
    fitwidth     = parsed_opts['fitwidth']
    minimizer     = parsed_opts['minimizer']
    splineorder   = parsed_opts['splineorder']
    #
    # HARDCODING: How many time steps to use simple extrapolation into gaps:
    extrapnpts=2 # Could be related to fitnsidepts?
    gapminnpts=2*extrapnpts+2 # Too small will spell troubles
    # TODO: Make #training points and #knots a function of gap size. 
    #   something related to (say) one per 6-12 hours (tidal related).
    nknotsmidmin=1      # How many "internal knots" to use. 1 or 2 should be OK?
    hours_per_knot = 8 #12 # 1 extra internal knot per this many hours of gap. (6-12 range probably OKih)
    # Normally, avg before and after are taken over "gap" length (pre- and post).
    # This setting indicates a max avg-length to apply - even if the gap is longer.
    sideavg_maxsecs = 48*3600.0 # Set to negative to disable
    #
    # Preample (much the same as for polyfill_gaps:
    datevals,varvals=ts.piecewise_to_fully_equidistant(datevals=datevals,varvals=varvals)
    neles = varvals.size
    datevals_filled= datevals
    varvals_filled = varvals.copy()
    # The last two will start out by nan-values
    varvals_poly=np.empty(shape=varvals.shape,dtype=varvals.dtype) # Will actually contain SPLINE here
    varvals_poly[:]=np.nan
    # This one will have JUST the gap values - not the adjacent fitting/training values
    varvals_gap=varvals_poly.copy() # TODO: May be unnecessary
    #
    # First locate all the gaps in the time series, by
    # getting first and last index for each gap in series
    gaps_idxfirst,gaps_idxlast=get_gaps_startstopidx(datevals,varvals)
    num_gaps_tot = gaps_idxfirst.size
    #
    if verbloc:
        print(fnam+": Going over %d gaps in time series"%gaps_idxfirst.shape[0])
    #
    ##
    # We need access to extrapolation and B-splines. We do NOT need the command-line options, though.
    import tsExtrapolate as tsex
    #
    # Counters for simple statistics/logging. Conters will be modified per loop execution
    num_gaps_filled = 0 # Actual #gaps we process and fill
    num_nans_tot    = 0 # Total number of missing-values in series.
    num_nans_filled = 0 # Number of these, which we end filling.
    # Loop over the gaps, and deal with each in turn:
    for ig in range(num_gaps_tot):
        ifirst=gaps_idxfirst[ig]
        ilast =gaps_idxlast[ig]
        num_nans_tot += 1+ilast-ifirst
        if ifirst <= 0:
            if verbloc>1:
                print(fnam+":  skipping gap #%d: not filling nan-segment at start-of-series"%ig)
            continue
        if ilast >= neles-1:
            if verbloc>1:
                print(fnam+":  skipping gap #%d: not filling nan-segment at end-of-series"%ig)
            continue
        nptsgap=ilast-ifirst+1
        if verbloc>1:
            print(fnam+":  examining gap #%d at [%d:%d] (%d pts in gap), ie [%s : %s]"%(ig,ifirst,ilast,nptsgap,str(datevals[ifirst]),str(datevals[ilast])))
        # Gap width - in terms of datetime.timedelta: time between last value before gap and first after gap.
        dtgap_delta=datevals[ilast+1]-datevals[ifirst-1]
        # Gap width in seconds:
        dtgap_secs=dtgap_delta.total_seconds()
        # Center of gap - in indices. May be used later
        imidt=(ifirst+ilast)/2.
        if verbloc>2:
            print(fnam+":    gap covers %s seconds between sampled values"%str(dtgap_secs))
        if dtgap_secs>gapmaxwidth:
            if verbloc:
                print(fnam+" skipping gap #%d: too large - covering %s s > specified maxwidth"%(ig,str(dtgap_secs),))
            continue
        # Sanity check:
        #  Make sure that the little two-sided extrapolation does not actually close the gap!
        if nptsgap < gapminnpts:
            print(fnam+" skipping gap #%d: too small (need at least %d pnts in gap to avoid numerical troubles)"%(ig,gapminnpts))
            continue
        #
        # Let the gap size effect the number of control points:
        nknotsmid=nknotsmidmin + int(np.floor(dtgap_secs/(hours_per_knot*3600.)))
        nfitpsmid=2*nknotsmid+1
        print( " Plan to use %d internal knots and %d internal evaluation points for this gap"%(nknotsmid,nfitpsmid))
        #
        # For spline, we start using fitnsidepts. We will extract this many points
        # from each side of the gap. If there are missing values in those, then 
        # the method will fail.
        if ifirst<fitnsidepts:
            if verbloc:
                print(fnam+":  skipping gap #%d: too close to start of series"%ig)
            continue
        if ilast+1+fitnsidepts>=neles:
            if verbloc:
                print(fnam+":  skipping gap #%d: too close to start of series"%ig)
            continue
        # Points just before the gap - preseries
        datevals_pre = datevals[ifirst-fitnsidepts:ifirst]
        varvals_pre  = varvals[ifirst-fitnsidepts:ifirst]
        # Points just after the gap - postseries
        datevals_pst = datevals[ilast+1:ilast+1+fitnsidepts]
        varvals_pst  = varvals[ilast+1:ilast+1+fitnsidepts]
        # Extrapolate pre- and post-series "into" the gap:
        datevals_pre_extr,varvals_pre_extr=tsex.do_extrapolate(datevals=datevals_pre,varvals=varvals_pre,
                                                               extrapnpts=extrapnpts, 
                                                               polyorder=polyorder,
                                                               fitnpts=fitnsidepts,
                                                               noextrapbackward=True,
                                                               minimizer=minimizer,
                                                               verb=max(0,verbloc-1))
        datevals_pst_extr,varvals_pst_extr=tsex.do_extrapolate(datevals=datevals_pst,varvals=varvals_pst,
                                                               extrapnpts=extrapnpts, 
                                                               polyorder=polyorder,
                                                               fitnpts=fitnsidepts,
                                                               noextrapforward=True,
                                                               minimizer=minimizer,
                                                               verb=max(0,verbloc-1))
        # Create spline weights to go with these:
        wght_pre=10*np.ones(shape=(fitnsidepts+extrapnpts),dtype=float)
        wght_pst=10*np.ones(shape=(fitnsidepts+extrapnpts),dtype=float)
        ww=10.0
        for ii in range(extrapnpts):
            ww=ww*0.7
            wght_pre[fitnsidepts+ii  ]=ww
            wght_pst[extrapnpts  -ii-1]=ww
        #
        # To control - especially long - splines we want some internal control points, 
        # which are related to average values "near" the gap.
        # We go out something like "gap size" on either side to estimate avg
        #  Need to pick out the right indices first
        #  TODO: We could look further back/forward, and/or use some kind of ramping (before averaging)
        if (sideavg_maxsecs>0):
            dt4avg_secs=min(sideavg_maxsecs,dtgap_secs)
        else:
            dt4avg_secs=dtgap_secs
        dt4avg=datetime.timedelta(seconds=dt4avg_secs)
        datelimpre=datevals[ifirst]-dt4avg # datetime.timedelta(hours=48)
        datelimpst=datevals[ilast] +dt4avg # datetime.timedelta(hours=48)
        #
        idx_avg_pre=np.where(np.all( (datevals>=datelimpre , datevals<datevals[ifirst]),axis=0))
        idx_avg_pst=np.where(np.all( (datevals<=datelimpst , datevals>datevals[ilast]), axis=0))
        #
        avgpre=ts.mean(datevals[idx_avg_pre],varvals[idx_avg_pre],verb=max(0,verbloc-1))
        avgpst=ts.mean(datevals[idx_avg_pst],varvals[idx_avg_pst],verb=max(0,verbloc-1))
        avgavg=0.5*(avgpre+avgpst)
        #
        # Setup knots and control points
        # We will normalize time by the entire spline-period - including the 2*fitnsidepts areas:
        t0=datevals_pre_extr[0]
        tN=datevals_pst_extr[-1]
        #print "   __Spline should cover {} - {}".format(t0,tN)
        # "Hard" knots at points just before/after gap:
        k0=datevals_pre[-1]
        kN=datevals_pst[0]
        #print "   __Knots  should cover {} - {}".format(k0,kN)
        xtrain_pre=ts.timesince_totalseconds(datevals_pre_extr,t0)
        xtrain_pst=ts.timesince_totalseconds(datevals_pst_extr,t0)
        # Normalize by end of 
        timnorm=1./xtrain_pst[-1]
        xtrain_pre *= timnorm
        xtrain_pst *= timnorm
        #
        # Figure out internal training points:
        dx_int=(xtrain_pst[0]-xtrain_pre[-1])/(nfitpsmid+3)
        xtrain_mid=np.arange(xtrain_pre[-1]+2*dx_int,xtrain_pst[0]-1.5*dx_int,dx_int)
        wght_mid  =np.ones(shape=xtrain_mid.shape,dtype=float)
        #ytrain_mid=np.ones(shape=xtrain_mid.shape,dtype=float) * avgavg
        #ytrain_mid[0] =avgpre
        #ytrain_mid[-1]=avgpst
        davg=(avgpst-avgpre)/(nfitpsmid-1)
        ytrain_mid=np.arange(avgpre,avgpst+0.5*davg,davg)
        xtrain=np.concatenate((  xtrain_pre,     xtrain_mid, xtrain_pst),      axis=0)
        wtrain=np.concatenate((    wght_pre,       wght_mid,   wght_pst),      axis=0)
        ytrain=np.concatenate(( varvals_pre_extr,ytrain_mid,varvals_pst_extr), axis=0)
        #
        # Define knots in normalized time space (x-space)
        #  Beware _number_ of internal knots - be that 1 or 2
        xk0=(k0-t0).total_seconds() * timnorm
        xkN=(kN-t0).total_seconds() * timnorm
        dk=(xkN-xk0)/(nknotsmid+1)
        xknots=np.arange(xk0,xkN+0.5*dk,dk)
        #
        # Finally, we need the evaluation points - which are the gap dates:
        datevals_eval=datevals[ifirst:ilast+1]
        xeval=ts.timesince_totalseconds(datevals_eval,since=t0) * timnorm
        #
        # So, we can train and use the spline:
        #  Train/fit:
        t,c,k = interpolate.splrep(x=xtrain, y=ytrain, w=wtrain, xb=xtrain[0],xe=xtrain[-1],k=splineorder, t=xknots, task=-1 ) 
        #  Use/evaluate
        gapvals=interpolate.splev(xeval,[t,c,k],der=0)
        #
        #Now we need to stick it back to time series:
        varvals_filled[ifirst:ilast+1]=gapvals[:]
        varvals_poly[ifirst:ilast+1]=gapvals[:] # Not really necessary?
        varvals_gap[ifirst:ilast+1]=gapvals[:]
        #
        # Update statistical counters:
        num_gaps_filled += 1
        num_nans_filled += 1+ilast-ifirst
    #
    # Write simple statistics:
    if verbloc:
        print(fnam+": Filled %d of %d gaps (%d of %d total nan values) in series"%(num_gaps_filled,num_gaps_tot,num_nans_filled,num_nans_tot))
    #
    # Time series all updated.
    # The selector poly_optoutput selects between varvals_{poly,gap}
    global poly_optoutput
    if   poly_optoutput == 'full':
        varvals_opt = varvals_poly
    elif poly_optoutput == 'gap':
        varvals_opt = varvals_gap
    else:
        raise AssertionError("Unexpected value for optout='%s'. Go debug."%str(poly_optoutput))
    #
    return datevals_filled,varvals_filled,varvals_opt




##
# Tertiary fill method
#
# Helper function for computing hamonic based on (normalized) time series and some parameters.
#  tS (t*, is a vector of normalized time, where the gap ends are, respectively,  0 and 1.
#  The rest are scalar paramters, to be found by curve fitting.
#                         time >|< amplitude   >|<  BIAS/surge >|< phase >|< frequency
def _harmonic_normalized(tS=None,A0=None,A1=None,C0=None,C1=None,PHIS=None,domega=None):
    global omegaS
    useomega=omegaS+domega
    ftgt=( C0*(1-tS) + C1 * tS ) +  np.sin(useomega *tS + PHIS ) * ( A0*(1-tS) + A1 * tS )
    return ftgt
#   Same, but use 2nd-order poly variation for amplitude:
#                          time >|<   amplitude         >|<  BIAS/surge >|< phase >|< frequency
def _harmonic_normalized2(tS=None,A0=None,A1=None,A2=None,C0=None,C1=None,PHIS=None,domega=None):
    global omegaS
    useomega=omegaS+domega
    ftgt=( C0*(1-tS) + C1 * tS ) +  np.sin(useomega *tS + PHIS ) * ( A0*(1-tS) + A1 * tS + A2 * tS * (1-tS) )
    return ftgt
#
def harmonicfill_gaps(datevals,varvals,
                      gapmaxwidth=None,
                      gapmaxnpts=None,
                      fitsidewidth=None,fitnsidepts=None,
                      polyorder=None,   # Not necessary
                      splineorder=None, # Not necessary
                      verb=None):
    # Consider if we need to specify the primary period, or just use M2.
    # 
    fnam="splinefill_gaps" # Just for log/debug
    verbloc=ts._verbloc(verb)
    parsed_opts=join_args_and_opts(datevals,varvals,
                                   gapmaxwidth=gapmaxwidth,
                                   gapmaxnpts=gapmaxnpts,
                                   polyorder=polyorder,
                                   fitsidewidth=fitsidewidth,fitnsidepts=fitnsidepts,
                                   splineorder=splineorder,
                                   verb=verb)
    #
    # Extract my options:
    gapmaxnpts    = parsed_opts['gapmaxnpts']
    gapmaxwidth   = parsed_opts['gapmaxwidth']
    fitnsidepts  = parsed_opts['fitnsidepts']
    fitsidewidth = parsed_opts['fitsidewidth']
    fitwidth     = parsed_opts['fitwidth']
    #
    # Specify stuff Harmonic:
    TM2=44714.16 # This is the M2 period
    omega=2*np.pi/TM2
    #
    # In order to make the observations near the gap have higher weight than 
    # "away from the gap", we will tell the curve_fit algorithm, that the 
    # the expected error increases away from the gap. 
    # This is done so that the (normalized) expected error is unity at the 
    # gap, and then increases linearly to by some factor per time unit(TM2 period) we 
    # move away from the gap:
    stdscale_per_TM2 = 5.0 # * (fitsidewidth / TM2)
    #
    # Preample (much the same as for polyfill_gaps:
    datevals,varvals=ts.piecewise_to_fully_equidistant(datevals=datevals,varvals=varvals)
    dt=ts.get_timestep(datevals,verb=0)
    #
    neles = varvals.size
    datevals_filled= datevals
    varvals_filled = varvals.copy()
    # The last two will start out by nan-values
    varvals_fit=np.empty(shape=varvals.shape,dtype=varvals.dtype) # Will actually contain SPLINE here
    varvals_fit[:]=np.nan
    # This one will have JUST the gap values - not the adjacent fitting/training values
    varvals_gap=varvals_fit.copy() # TODO: May be unnecessary
    #
    # First locate all the gaps in the time series, by
    # getting first and last index for each gap in series
    gaps_idxfirst,gaps_idxlast=get_gaps_startstopidx(datevals,varvals)
    num_gaps_tot = gaps_idxfirst.size
    #
    if verbloc:
        print(fnam+": Going over %d gaps in time series"%gaps_idxfirst.shape[0])
    #
    ##
    #
    # Counters for simple statistics/logging. Conters will be modified per loop execution
    num_gaps_filled = 0 # Actual #gaps we process and fill
    num_nans_tot    = 0 # Total number of missing-values in series.
    num_nans_filled = 0 # Number of these, which we end filling.
    # Loop over the gaps, and deal with each in turn:
    for ig in range(num_gaps_tot):
        ifirst=gaps_idxfirst[ig]
        ilast =gaps_idxlast[ig]
        num_nans_tot += 1+ilast-ifirst
        if ifirst <= 0:
            if verbloc>1:
                print(fnam+":  skipping gap #%d: not filling nan-segment at start-of-series"%ig)
            continue
        if ilast >= neles-1:
            if verbloc>1:
                print(fnam+":  skipping gap #%d: not filling nan-segment at end-of-series"%ig)
            continue
        nptsgap=ilast-ifirst+1
        if verbloc>1:
            print(fnam+":  examining gap #%d at [%d:%d] (%d pts in gap), ie [%s : %s]"%(ig,ifirst,ilast,nptsgap,str(datevals[ifirst]),str(datevals[ilast])))
        # Gap width - in terms of datetime.timedelta: time between last value before gap and first after gap.
        dtgap_delta=datevals[ilast+1]-datevals[ifirst-1]
        # Gap width in seconds:
        dtgap_secs=dtgap_delta.total_seconds()
        # Gap ends - for use with normalization:
        time0=datevals[ifirst-1]
        time1=datevals[ilast+1]
        #
        if verbloc>2:
            print(fnam+":    gap covers %s seconds between sampled values"%str(dtgap_secs))
        if dtgap_secs>gapmaxwidth:
            if verbloc:
                print(fnam+" skipping gap #%d: too large - covering %s s > specified maxwidth"%(ig,str(dtgap_secs),))
            continue
        #
        # For harmonic, we start using fitnsidepts. We will extract this many points
        # from each side of the gap. If there are missing values in those, then 
        # the method may fail - or at least be less effective (TODO: We will see...?)
        if ifirst<fitnsidepts:
            if verbloc:
                print(fnam+":  skipping gap #%d: too close to start of series"%ig)
            continue
        if ilast+1+fitnsidepts>=neles:
            if verbloc:
                print(fnam+":  skipping gap #%d: too close to start of series"%ig)
            continue
        #
        # Points just before the gap - preseries
        datevals_pre = datevals[ifirst-fitnsidepts:ifirst]
        varvals_pre  = varvals[ifirst-fitnsidepts:ifirst]
        # Points just after the gap - postseries
        datevals_pst = datevals[ilast+1:ilast+1+fitnsidepts]
        varvals_pst  = varvals[ilast+1:ilast+1+fitnsidepts]
        #
        # Remove possible NaNs in windows:
        nnans_pre=ts.count_missing(varvals_pre, verb=0)
        nnans_pst=ts.count_missing(varvals_pst, verb=0)
        if nnans_pre>0 or nnans_pst>0:
            if verbloc:
                print(fnam+":    Removing {} NaN-value(s) from curvefit sidewindows".format(nnans_pre+nnans_pst))
            if nnans_pre>0:
                datevals_pre,varvals_pre = ts.remove_nans(datevals_pre,varvals_pre,verb=0)
            if nnans_pst>0:
                datevals_pst,varvals_pst = ts.remove_nans(datevals_pst,varvals_pst,verb=0)
        # 
        # These are estimates/guesses for some of the fit-params:
        A0 = 0.5*(varvals_pre.max()-varvals_pre.min())
        A1 = 0.5*(varvals_pst.max()-varvals_pst.min())
        C0 = varvals_pre.mean()
        C1 = varvals_pst.mean()
        datevals_curvefit=np.concatenate((datevals_pre,datevals_pst,),axis=0)
        varvals_curvefit =np.concatenate(( varvals_pre, varvals_pst,),axis=0)
        # Convert time (and PHI?) to normalized t*:
        t1t0=(time1-time0).total_seconds()
        tS=ts.timesince_totalseconds(datevals_curvefit,since=time0) / t1t0
        #
        # omegaS must be accessible by the function-to-fit (_harmonic_normalized2)
        global omegaS
        omegaS=omega*t1t0
        # This block is to give more weight to the near-gap points, by "increasing the uncertainty" 
        # of the varvals "away from the gap". Basically, assume unit accuracy at the gap and 
        # increase to stdscale_per_TM2 over a TM2 period (and increasing linearly even beyond that).
        tnorm_pre = np.abs(- ts.timesince_totalseconds(datevals_pre,since=time0) / TM2 )
        tnorm_pst = np.abs(  ts.timesince_totalseconds(datevals_pst,since=time1) / TM2 )
        xpre = 1.0 + (stdscale_per_TM2-1.0) * tnorm_pre
        xpst = 1.0 + (stdscale_per_TM2-1.0) * tnorm_pst
        sigmas_curvefit=np.concatenate((xpre,xpst,),axis=0) # These are then sigmas (error estimators) for curve_fit()
        #
        # Set bounds to limit the search/optimization zone.
        bounds_lo=[0.5*A0,0.5*A1,-1.,  C0-2*A0,C1-2*A1, -1.*np.pi,  -0.03*omegaS]
        bounds_hi=[1.5*A0,1.5*A1, 1.,  C0+2*A0,C1+2*A1,  3.*np.pi,   0.03*omegaS]
        #
        # Once in a while, a curve_fit goes all wrong, and then a better start guess may be the thing.
        # In particular, the start guess for PHI seems crititcal, so we will make FOUR curvefits, 
        # starting with PHI at each of the four quadrants, and then using the fit with best fit
        # (scaled by inverse sigmas!)
        #
        best_rms=None
        best_popt=None
        sigma_iweights=1./sigmas_curvefit
        wsum=np.sum(sigma_iweights)
        for iquadrant in range(4):
            # Add a start guess, which is (hopefully) better than all zeros.
            guess0=[A0,A1,0.0, C0,C1, iquadrant*0.5*np.pi, 0]
            #
            # Let scipy do the heavy lifting:
            from scipy.optimize import curve_fit
            popt, pcov = curve_fit(_harmonic_normalized2, tS, varvals_curvefit,sigma=sigmas_curvefit,p0=guess0,bounds=(bounds_lo,bounds_hi))
            varvals_harm=_harmonic_normalized2(tS,*popt)
            # Weighted errors (near-gap weights higher):
            varvals_werr = (varvals_harm - varvals_curvefit) * sigma_iweights
            # Compute weighted RMS:
            wrms = np.sqrt( np.sum(np.square(varvals_werr))/wsum)
            # If better than what we have, then store solution:
            if best_rms is None or wrms<best_rms:
                best_rms=wrms
                best_popt = popt
        # Stick with the best:
        popt=best_popt
        wrms=best_rms
        if verbloc>1:
            domega=popt[-1]/t1t0
            print(fnam+":    Curvefit params: A0={0:.2f} A1={1:.2f} A2={2:.3f}  C0={3:.2f} C1={4:.2f} phi={5:.3f} domega={6:.3f}".format(*popt))
            ampl_est=0.5*(popt[0]+popt[1])
            ampl_obs=0.5*(A0+A1)
            print(fnam+":    Estimated wRMS/A = {}".format(wrms/ampl_obs))
            # Finally a little on the Period adjustment relative to T_M2:
            omega_adj=(omegaS+popt[-1])/t1t0
            TM2_adj=2.0*np.pi/omega_adj
            print(fnam+":    Adjusted T_M2={} -> {}. Relative change: {}".format(TM2,TM2_adj,np.abs(TM2-TM2_adj)/TM2))
        #
        # Finally, we need the evaluation points - which are the gap dates:
        datevals_eval=datevals[ifirst:ilast+1]
        # Normalize to t* (these will be strictly 0<tS<1:
        tSeval=ts.timesince_totalseconds(datevals_eval,since=time0) / t1t0
        #datevals_brutto=ts.create_time_range(timestart=datevals_curvefit[0],timestop=datevals_curvefit[-1],dt=ts.get_timestep(datevals,verb=0))
        #tSbrutto=ts.timesince_totalseconds(datevals_brutto,since=time0) / t1t0
        gapvals=_harmonic_normalized2(tSeval,*popt)
        # 
        #Now we need to stick it back to time series:
        varvals_filled[ifirst:ilast+1]=gapvals[:]
        varvals_fit[ifirst:ilast+1]=gapvals[:] # Not really necessary?
        varvals_gap[ifirst:ilast+1]=gapvals[:]
        #
        # Update statistical counters:
        num_gaps_filled += 1
        num_nans_filled += 1+ilast-ifirst
    #
    # Write simple statistics:
    if verbloc:
        print(fnam+": Filled %d of %d gaps (%d of %d total nan values) in series"%(num_gaps_filled,num_gaps_tot,num_nans_filled,num_nans_tot))
    #
    # Time series all updated.
    # The selector poly_optoutput selects between varvals_{poly,gap}
    global poly_optoutput
    if   poly_optoutput == 'full':
        varvals_opt = varvals_fit
    elif poly_optoutput == 'gap':
        varvals_opt = varvals_gap
    else:
        raise AssertionError("Unexpected value for optout='%s'. Go debug."%str(poly_optoutput))
    #
    return datevals_filled,varvals_filled,varvals_opt

# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser=args_define_options()
    parser=args_define_positional(parser)
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Read data:
    datevals,varvals,longname    = tsnc.ncfil_load(filename=tsnc.ncsrc)
    # 
    # Modify data (fill gaps):
    global gap_method
    if   gap_method == 'polynomial':
        datevals_filled,varvals_filled,varvals_fit = polyfill_gaps(datevals,varvals)
    elif gap_method == 'spline':
        datevals_filled,varvals_filled,varvals_fit = splinefill_gaps(datevals,varvals)
    elif gap_method == 'harmonic':
        datevals_filled,varvals_filled,varvals_fit = harmonicfill_gaps(datevals,varvals)
    else:
        raise AssertionError("Somehow missing gap_method={}".format(gap_method))
    #
    # Save data:
    tsnc.ncfil_save(datevals=datevals_filled,varvals=varvals_filled,
                    filename=tsnc.nctgt,longname=longname)
    # Save optional output:
    global ncfit
    if not ncfit is None:
        tsnc.ncfil_save(datevals=datevals_filled,varvals=varvals_fit,
                        filename=ncfit,longname=longname)
    #
    # All done
    return

if __name__ == '__main__':
    main()
