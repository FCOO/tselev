#!/usr/bin/python
#
"""
Helper module/rutines for interfacing with netcdf based elevation time series.
This module is intended as base for FCOO work with elevation time series 
for eg observations, hindcast/model, validation and tuning.
"""

#
# Module history:
#  2019-08-19 bjb 0.0.0: Initial version. Read, write, round time, time.unit conversion.
#  2019-08-20 bjb 0.0.1: Implement various positional args possibilities:
#                         <INOUT>, <IN> <OUT>, <IN> [<OUT>], <IN> <IN2> <OUT>
#  2019-08-20 bjb 0.0.2: Added routines for checking time series for dt, monotone and equidistance
#  2019-08-21 bjb 0.0.3: Added append-mode to modify an existing file
#                        Print updated to Python3 compatibility
#                        Improve comments for python help()
#  2019-08-21 bjb 0.0.4: Add history option to _save. Fix roundto for get_timestep.
#  2019-08-26 bjb 0.0.5: Deal with missing values. Move timeseries functions to tsSimple.
#  2019-08-27 bjb 0.0.6: Add roundvals, move unit-change to tsSimple
#  2019-08-29 bjb 0.0.7: Bugfix for saving missing_value entries
#  2019-09-03 bjb 0.0.8: Allow saving files in single precision float, see --floatprecision
#  2019-09-18 bjb 0.0.9: Change name of module to tsNetcdf
#  2019-09-18 bjb 0.0.10: Add option on what to do with NaNs on output: savenan
#  2019-10-02 bjb 0.0.11: Always apply ts.apply_options() upon file load
#  2019-11-07 bjb 0.0.12: Adding possibility to round elevations to 10um and 100um
#  2020-01-21 bjb 0.0.13: In script mode accept time series with length < 2.
#  2020-01-28 bjb 0.0.14: Deal with zero-length time series on output (only nan values available)
#  2021-09-10 bjb 0.0.15: More info (log) with metadata on high verbose settings
#  2022-02-22 bjb 0.0.16: Updated arg-parsing to conform with new modules
#  2023-03-28 bjb 0.0.17: Allow degenerate (len=1) extra dimensions in input
#
version_info = (0, 0, 17)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

#
# TODO LIST:
#
# Ideas (maybe bad ideas)
#  * Add "collapse" option to savenan - to just keep the first of a range of nan-values.
#     This will require an extra routine in tsSimple to do the heavy lifting.
#  
#  * --requires option: Just test that script can load (has required modules) and exit.
#         Presumably, --help option can be used for this.
#
# This is neat to get the stack to find who called what (debug to decrease total output/log)
#import inspect
#for ii in range(2):
#  print " stack%d: %s"%(ii,inspect.stack()[ii][3])

import tsSimple as ts

import shutil  # Eg copy file input->output
import os      # Eg check if file exists + remove file
import sys     # For printing partial lines

import argparse

import netCDF4 as netCDF
import numpy   as np
import datetime

# This is to test if an argument is a string - to work in both py2 and py3:
if sys.version_info[0] < 3:
    strtype=basestring # python2
else:
    strtype=str        # python3

########################################################################
#                                                                      #
#      MODULE-WIDE VARIABLES                                           #
#                                                                      #
########################################################################
# The following block contains variables, which can be needed in 
# different parts of the module.
# Many of them will contain data from  parsed arguments.
#
# Allow this module to be called from command line to test things out.
# Global variables for this module/method, including defaults
ncscr =None
ncscr2=None
ncscrs=None
nctgt =None
##
# Variables holding information from optional arguments:
verbose=0
overwrite   = False   # If it already exists tehn delete it and create a new one.
append_file = False   # Modify existing file rather than writing a new one.
convert_missing='set2nan' # Setting missing to NaN on input. Alt: Throw away missing values.
round_vals  = None    # Optional rounding of read elev to ['m','cm','mm','um']
save_nan    = 'keep'  # Method of what to do with NaNs on save
savenan_choices = ['keep', 'delete', 'expand', ]
##
# These have to do with the layout of target file
time_units     = None  # Should be stuff like: "seconds since 2018-01-01"
var_units      = None  # Likely going to be "m"
var_longname   = None  # Could contain info on station or whatever.
append_history = True  # Append to netCDF history when writing to file?
missing_val    = None  # Set specific missing_value to use (on output)
var_precision  = None; # Set specific floating-point accuracy on output

##
# These are for options/metadata, which we read from netcdf file.
# They provide a basis for output metadata - unless we define something else.
time_units_loaded   = None
var_units_loaded    = None
history_loaded      = None
var_longname_loaded = None
var_missing_loaded  = None
var_fill_loaded     = None
var_precision_loaded= None

##
# HARDCODINGS: - some of these could be set by options if we want:
#
ncvarname='elev' # Should this be configurable?
nctimname='time' #
ncmissing  = np.float64(-9999) # Just a default
ncfill     = np.float64(-9999) # Just a default

# Make a companion variable, which actually contains the numpy float precision:
#   Use as eg nc2py_prec[ncprec]
nc2py_prec={ 'f4': np.float32, 
             'f8': np.float64 }

##
# This is to keep track of what positional arguments are defined.
_do_parse_positional=None

argparse_description="Check/validate/convert methods for ncfiles with elevation time series"

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False
# This is whether to load options from upstream/inherited eg tsSimple:
_opts_recurse = True

########################################################################
#                                                                      #
#      OPTIONS AND ARGUMENTS                                           #
#                                                                      #
########################################################################
# The following block concerns:
#   * Definitions of arguments
#   * Parsing of arguments

# Define command line options
def args_define_options(parser=None,recurse=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    # Set whether we should include "upstream" options:
    global _opts_recurse
    if not recurse is None:
        _opts_recurse = recurse
    #
    if parser == None:
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    #
    # If nobody defined verbose, then I'll do it:
    try:
        parser.add_argument("-v", "--verbose", help="increase output verbosity (allow multiple)",
                            action='count', default=0)
    except:
        # Dont worry. Probably somebody else defined it.
        pass
    #
    outgroup = parser.add_argument_group(title='NetCDF output options')
    outgroup.add_argument("-O", "--overwrite", dest="overwrite",
                        help="overwrite output, i.e. create new file",
                        action='store_true')
    outgroup.add_argument("-A", "--append", dest="append_file",
                        help="Modify existing file rather than writing a new one.",
                        action='store_true')
    outgroup.add_argument("-H", "--nohistory", dest="append_history",
                        help="Do not append to history",
                        action='store_false')
    tugroup=outgroup.add_mutually_exclusive_group(required=False)
    tugroup.add_argument("--timeunits",dest="timeunits", metavar="<TIMEUNITS>",
                        type=_checktimeunits,
                        help='set (output) time units. See netCDF4 doc for acceptable values')
    tugroup.add_argument('--sinceepoch',dest="sinceepoch", 
                        type=ts._checkepochstr, default=None, metavar="<EPOCHNN>",
                        help="Alternative to timeunits: Use this epoch for time.units as 'seconds since .. ..'")
    #
    parser.add_argument("--varunits",dest="varunits", metavar="<UNIT>",
                        default=None,
                        choices=['m', 'cm', 'mm'], # type=_checkvarunits,
                        help='used when reading data, and allows conversion to %s units: m, cm, mm. Unit will then be carried over to output. This is not the unit in the input file: if necessary, then unit conversion will be made'%ncvarname)
    outgroup.add_argument("--missingvalue",dest="missingvalue",metavar="<VAL>",
                        action='store',type=np.float64,
                        help='Explicit value to use for missing_value (%s attribute) to use on output. Will also be used for _FillValue'%ncvarname)
    outgroup.add_argument("--convertmissing",dest="convertmissing",metavar="<ACTION>",
                        choices=['set2nan', 'delete', 'keep', 'ignore'],
                        default='set2nan',
                        help='Decide what to do with missing values on input. Possible values are "set2nan", "delete", and "keep"')
    #
    global savenan_choices
    global save_nan
    outgroup.add_argument("--savenan",dest="savenan", 
                        metavar="<METHOD>",
                        choices=savenan_choices, #type=str, 
                        help='what to do with NaN-values on output. '+
                        'Allowed values are: %s (default %s)'%(', '.join(savenan_choices),str(save_nan),)  )
    #
    outgroup.add_argument('--fprec','--floatprecision',dest="float_precision",metavar="<f8|f4>",
                        action='store',choices=['f8','64bit','8byte','double','f4','32bit','4byte','single'],
                        help='Accuracy to use for time series values (%s) on output. Default is f8'%ncvarname)
    outgroup.add_argument("--roundvals",dest="roundvals",metavar="<UNIT>",
                        choices=['m', 'cm', 'mm', '100um', '10um', 'um'],
                        help='Optionally round read %s-values to specified accuracy. This is useful only if it is known a-priori that the measurements are of a specified accuracy. Possible values are "cm", "mm", "um" (micro meters) and "m" (dont use meters)'%ncvarname)
    #
    # Recurse to next level - unless disabled
    if _opts_recurse:
        parser=ts.args_define_options(parser)
    ## DEBUG:
    #print "ncts PARSER"
    #for item in vars(parser):
    #    print ": ",item.keys()
    _have_defined_options=True
    return parser

# Parse command line options
def args_parse_options(parsed_args):
    global verbose
    verbose=parsed_args.verbose
    if verbose:
        print(" setting verbose=%d"%verbose)
    global overwrite
    overwrite=parsed_args.overwrite
    if verbose and overwrite:
        print(" setting overwrite=ON")
    global append_history
    append_history=parsed_args.append_history
    if verbose and not append_history:
        print(" disable history append")
    global time_units
    if parsed_args.timeunits:
        time_units=parsed_args.timeunits
    elif parsed_args.sinceepoch:
        t0=ts.epochstr2datetime(parsed_args.sinceepoch)
        time_units=t0.strftime("seconds since %Y-%m-%d %H:%M:%S")
    if verbose and not time_units is None:
        print(" setting output %s.units='%s'"%(nctimname,time_units))
    global var_units
    var_units=parsed_args.varunits
    if verbose and not var_units is None:
        print(" setting output %s.units='%s'"%(ncvarname,time_units))
    global append_file
    append_file=parsed_args.append_file
    if verbose and append_file:
        print(" will modify existing file rather than creating a new one")
    global missing_val
    missing_val=parsed_args.missingvalue
    if verbose and not missing_val is None:
        print(" setting output %s.missing_value=%s"%(ncvarname,str(missing_val)))
    global convert_missing
    convert_missing=parsed_args.convertmissing
    if verbose:
        print(" read missing_values will be treated as %s"%convert_missing)
    #
    if not parsed_args.savenan is None:
        global save_nan
        save_nan=parsed_args.savenan
        if verbose:
            print(" output savenan method is '%s'"%save_nan)
    #
    if not parsed_args.float_precision is None:
        global var_precision
        if   parsed_args.float_precision in ['f8', '64bit', '8byte', 'double']:
            var_precision='f8'
        elif parsed_args.float_precision in ['f4', '32bit', '4byte', 'single']:
            var_precision='f4'
        else:
            raise AssertionError("Unexpected value for floatprecision=%s"%str(parsed_args.float_precision))
        if verbose:
            print(" float output will be stored as netcdf type '%s'"%var_precision)
    global round_vals
    round_vals=parsed_args.roundvals
    if verbose and not round_vals is None:
        print(" will round read %s-values to nearest %s"%(ncvarname,round_vals))
    # Let the tsSimple module get its hand on whatever options it needs:
    global _opts_recurse
    if _opts_recurse:
        ts.args_parse_options(parsed_args)
    return


# Internal method to check if passed option (string) is acceptable as an elev.units
def _checkvarunits(unit):
    ok_units=('m', 'cm', 'mm')
    if not isinstance( unit , str):
        raise argparse.ArgumentTypeError("%s is not a valid %s unit (not a string)" % (ncvarname,str(unit),))
    if not unit in ok_units:
        raise argparse.ArgumentTypeError("%s is not a valid %s unit (unknown unit)" % (ncvarname,unit,))
    return unit

# Internal method to check if passed option (string) is acceptable as time.units.
# For convenience, we will accept underscores in option in stead of spaces.
# This is ONLY to be able to avoid spaces in options (which then need to be escaped).
# So method will replace underscores to spaces, such that timeunits on command line 
# can be specified as eg --timeunits=seconds_since_1971-01-01_00:00:00
def _checktimeunits(units):
    units = units.replace("_"," ")
    try:
        dummy = netCDF.date2num(datetime.datetime.utcnow(),units)
    except:
        raise AssertionError("ERROR: Something may be wrong with %s.units='%s'"%(nctimname,units))
    return units


def _check_file(filename):
    if not os.path.isfile(filename):
        raise argparse.ArgumentTypeError("No such file: {}".format(filename))
    return filename

# Internal method to check if passed option (string) is acceptable as 
# method to deal with missing_values on input.
# Here are the possibilities in preferred order:
#  'set2nan': to set values to NaN (not-a-number)
#  'delete':  eliminate the missing values from the time series
#             (deleting both datevals and varvals).
#  'keep'/'ignore': keep the values and leave it to downstream
#              to deal with them. (this is discouraged)
def _checkmissingread(method):
    ok_methods=('set2nan', 'delete', 'keep', 'ignore')
    if not isinstance( method , str):
        raise argparse.ArgumentTypeError("%s is not a valid method to deal with missing_values (not a string)" % (str(method),))
    if not method in ok_methods:
        raise argparse.ArgumentTypeError("%s is not a valid method to deal with missing_values (unknown method)" % (method,))
    return method

### PREDEFINE POSITIONAL ARGUMENTS
# There are a few different definitions to choose from here:
#  *_one:     A single mandatory argument: <IN> (or inout)
#  *_tgt:     A single mandatory argument: <OUT> (defining nctgt only!)
#  *_two:     Two mandatory arguments:     <IN> <OUT>
#  *_oneplus: Two args, where the last is optional <IN> [<OUT>]
#  *_oneplus2: Three args, where the last two are optional <IN> [<IN2>] [<OUT>]
#  *_three:   Three mandatory arguments:   <IN1> <IN2> <OUT>

# Define postional arguments as: <NCSRC>
def args_define_positional_one(parser=None):
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments:
    parser.add_argument('ncsrc', action="store",metavar="<SRCNC>",
                        help='netcdf file to work on')
    global _do_parse_positional
    _do_parse_positional = args_parse_positional_one
    return parser
# Companion parser for args_define_positional_one
def args_parse_positional_one(parsed_args):
    global ncsrc
    ncsrc  = parsed_args.ncsrc
    return

# Define postional arguments as: <NCTGT>
def args_define_positional_tgt(parser=None):
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments:
    parser.add_argument('nctgt', action="store",metavar="<TGTNC>",
                        help='netcdf file to create')
    global _do_parse_positional
    _do_parse_positional = args_parse_positional_tgt
    return parser
# Companion parser for args_define_positional_tgt
def args_parse_positional_tgt(parsed_args):
    global nctgt
    nctgt  = parsed_args.nctgt
    return

# Define postional arguments as: <NCSRC> <NCTGT>
def args_define_positional_two(parser=None):
    # The argparse is really only needed for the command line interface.
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments:
    parser.add_argument('ncsrc', action="store",metavar="<SRCNC>",
                        help='input netcdf file')
    parser.add_argument('nctgt', action="store",metavar="<TGTNC>",
                        help='output netcdf file')
    global _do_parse_positional
    _do_parse_positional = args_parse_positional_two
    return parser
# Companion parser for args_define_positional_two
def args_parse_positional_two(parsed_args):
    global ncsrc
    global nctgt
    ncsrc  = parsed_args.ncsrc
    nctgt  = parsed_args.nctgt
    return

# Define postional arguments as: <NCSRC> [<NCTGT>]
def args_define_positional_oneplus(parser=None):
    # The argparse is really only needed for the command line interface.
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments:
    parser.add_argument('ncsrc', action="store",metavar="<SRCNC>",
                        help='input netcdf file')
    parser.add_argument('nctgt', action="store",metavar="<TGTNC>",
                        help='output netcdf file',nargs='?')
    global _do_parse_positional
    _do_parse_positional = args_parse_positional_oneplus
    return parser
# Companion parser for args_define_positional_oneplus
def args_parse_positional_oneplus(parsed_args):
    global ncsrc
    global nctgt
    ncsrc  = parsed_args.ncsrc
    nctgt  = parsed_args.nctgt
    return

# Define postional arguments as: <NCSRC> <NCSRC2> <NCTGT>
def args_define_positional_three(parser=None):
    # The argparse is really only needed for the command line interface.
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments:
    parser.add_argument('ncsrc', action="store",metavar="<SRCNC>",
                        help='input netcdf file')
    parser.add_argument('ncsrc2', action="store",metavar="<SRCNC2>",
                        help='second input netcdf file')
    parser.add_argument('nctgt', action="store",metavar="<TGTNC>",
                        help='output netcdf file (not always needed)')
    global _do_parse_positional
    _do_parse_positional = args_parse_positional_three
    return parser
# Companion parser for args_define_positional_three
def args_parse_positional_three(parsed_args):
    global ncsrc
    global ncsrc2
    global nctgt
    ncsrc  = parsed_args.ncsrc
    ncsrc2 = parsed_args.ncsrc2
    nctgt  = parsed_args.nctgt
    return
# Define postional arguments as: <NCSRC> [<NCTGT>]
def args_define_positional_Nplus1(parser=None):
    # The argparse is really only needed for the command line interface.
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments:
    parser.add_argument('ncsrc', metavar="<SRCNC>", default=[],
                        help='input netcdf file(s)',nargs='*')
    parser.add_argument('nctgt', action="store",metavar="<TGTNC>",
                        help='output netcdf file')
    global _do_parse_positional
    _do_parse_positional = args_parse_positional_Nplus1
    return parser
# Companion parser for args_define_positional_Nplus1
def args_parse_positional_Nplus1(parsed_args):
    global ncsrcs
    global nctgt
    ncsrcs = parsed_args.ncsrc # Note that here it is a list
    nctgt  = parsed_args.nctgt
    return

# This is a wrapper for whatever parser-routine is defined by  
#  the used args_define_positional_* 
def args_parse_positional(parsed_args):
    try:
        _do_parse_positional(parsed_args)
    except:
        print("Something is wrong with definition and parsing of positionals")
        raise AssertionError("Position arg issue - go debug")
    return

#                                                                      #
# END      OPTIONS AND ARGUMENTS                                       #
########################################################################

########################################################################
#                                                                      #
#      TIME SERIES OPERATIONS                                          #
#                                                                      #
########################################################################
#
# Time series routines have been moved to tsSimple.py.
#

#                                                                      #
# END     TIME SERIES OPERATIONS                                       #
########################################################################


########################################################################
#                                                                      #
#      FILE OPERATIONS                                                 #
#                                                                      #
########################################################################
# The following block concerns:
#   * Check if files are here and valid
#   * Read/load/parse (input) netcdf file - and pass data to caller
#   * Create/save (output) file - with given data and options
#

# Check if a file exists. 
# If filename is not passed, then ncsrc will be tried.
def file_is_present(filename=None,verb=None):
    verbloc=verb
    if verb is None:
        verbloc=verbose
    #
    if filename is None:
        filename=ncsrc
    if filename is None:
        raise AssertionError("Missing file name: Cannot determine if file is present")
    if os.path.isfile(filename):
        if verbloc>1:
            print(" file exists: %s"%filename)
        return True
    if verbloc:
        print(" no such file: %s"%filename)
    return False

# Carp unless file exists. 
# If filename is not passed, then ncsrc will be tried.
def assert_file_is_present(filename=None):
    if filename is None:
        filename=ncsrc
    if not file_is_present(filename,verb=0):
        raise AssertionError("No such file: %s"%filename)
    return;

# Check if an (existing) file conforms to whatever specifications we 
# have for netcdf elevation timeseries, such that it can at least be read.
# If filename is not passed, then ncsrc will be tried.
def file_is_ncts(filename=None,verb=None):
    verbloc=verb
    if verb is None:
        verbloc=verbose
    if filename is None:
        filename=ncsrc
    if filename is None:
        raise AssertionError("Missing file name: Cannot determine if file conforms")
    if not file_is_present(filename=filename,verb=0):
        return None
    if verbloc>1:
        print(" checking file netcdf validity: %s"%filename)
    try:
        ncobj=netCDF.Dataset(filename)
    except:
        print(" File does not appear to be a netCDF data set: %s"%filename)
        return False
    #
    # Keep track of any degenerate (len=1, fixed len) dimensions
    dims_degen=[]
    #
    # We should have just a single unlimited dimension, and that must be named 'time':
    num_unlimdims = 0 # These will be counted as we go over the dimensions
    num_dims      = len(ncobj.dimensions)
    if verbloc>1:
        print("  checking dimension metadata")
    for ncdim in ncobj.dimensions.values():
        dimnam=ncdim.name
        if not ncdim.isunlimited():
            if ncdim.size==1:
                # This is degenerate
                print("   dimension '%s' degenerate (len=1) fixed-size, (not unlimited)"%(dimnam,))
                dims_degen.append(dimnam)
            else:
                print("   dimension '%s' fixed-size (not unlimited) len=%d"%(dimnam,ncdim.size))
            # Skip right to next dimension - this one is not interesting for us.
            continue
        # If we get here, then it is an unlimited dimension
        num_unlimdims += 1
        if dimnam != nctimname:
            print("   dimension '%s' is unlimited [size=%d] with unexpected name: expected '%s'"%(dimnam,ncdim.size,nctimname))
            return False
        # It is unlimited with the right name
        if verbloc:
            print("   dimension '%s' is unlimited [size=%d] with expected name"%(dimnam,ncdim.size))
    #
    # Check that we have the required variables:
    for varnam in (nctimname, ncvarname):
        if not varnam in ncobj.variables:
            print("   missing variable '%s' in the dataset"%varnam)
            return False
        # Continue with this (existing) variable:
        ncvar=ncobj.variables[varnam]
        dimnams_wdegen=ncvar.dimensions
        if verbloc>1:
            dimstr="["
            for idim in range(len(dimnams_wdegen)):
                if idim>0:
                    dimstr+=','
                dimstr+=dimnams_wdegen[idim];
            dimstr+="]"
            print("   variable '%s%s' is in the dataset"%(varnam,dimstr))
        # Make a list of all *non*degenerate* dimensions in this var.
        dimnams=[]
        for dnam in dimnams_wdegen:
            if not dnam in dims_degen:
                dimnams.append(dnam)
        if len(dimnams)!=1:
            print("   variable '%s' has unexpected number of dimensions. Expected 1, but got %d"%(varnam,len(dimnams)))
            return False
        # Check  name of this (single) dimension - it should be just time
        if dimnams[0] != nctimname:
            print("   variable '%s[%s]' has unexpected dimension. Expected [%s]"%(varnam,dimnams[0],nctimname))
            return False
        # Check that this variable has a 'units' attribute:
        attrnam='units'
        if not attrnam in ncvar.ncattrs():
            print("   variable '%s' is missing the %s attribute"%(varnam,attrnam,))
            return False
        # Todo: We *could* check that the units are standard, eg "time since.." and "m"
        # Todo: We could check that elev (var) has attribute long_name...?
    ncobj.close()
    return True

# Carp unless file conforms to agreed time-series netCDF format.
# If filename is not passed, then ncsrc will be tried.
def assert_file_is_ncts(filename=None):
    if filename is None:
        filename=ncsrc
    if not file_is_ncts(filename,verb=0):
        raise AssertionError("Not compliant ncts file: %s"%filename)
    return;

# Copy a file. It will be tested that src file conforms to 
# agreed netCDF time series format.
def ncfil_copy(fnamsrc=None,fnamtgt=None):
    if fnamsrc is None:
        fnamsrc=ncsrc
    if fnamtgt is None:
        fnamtgt=nctgt
    if not file_is_ncts(fnamsrc):
        raise AssertionError("Not compliant ncts file: %s"%fnamsrc)
    if fnamtgt is None:
        raise AssertionError("Missing target file name")
    global overwrite
    if os.path.isfile(fnamtgt) and not overwrite:
        raise AssertionError("Refusing to overwrite target file %s"%fnamtgt)
    if verbose:
        print(" creating file %s"%fnamtgt)
    shutil.copy(fnamsrc,fnamtgt)
    return;

# Read netCDF time series.
# Some metadata will be stored in module and may be used for subsequent write/save.
# If filename is not given, then ncsrc (command line positional argument) will be used.
# There is a number fo optional arguments, which can pass information as an alternative 
# to (or overriding) command line options.
#  TODO: MISSING VALUE TREATMENT
def ncfil_load(filename=None,
               roundtime=None,
               varunits=None,
               precision=None,
               convertmissing=None,
               roundvals=None):
    if filename is None:
        global ncsrc
        filename=ncsrc
    assert_file_is_ncts(filename)
    ncobj   =  netCDF.Dataset(filename,'r')
    if verbose:
        print("Reading data from %s"%filename)
    nctime      = ncobj.variables[nctimname] # Presumably 'time'
    ncvar       = ncobj.variables[ncvarname] # Presumably 'elev'
    # We already checked, that unit attributes are here:
    global time_units_loaded
    time_units_loaded = nctime.getncattr('units')
    global var_units_loaded
    var_units_loaded = ncvar.getncattr('units')
    #
    # Store as possible defaults:
    global time_units
    if time_units is None:
        time_units=time_units_loaded
        if verbose:
            print("  proceding with time units '%s'"%time_units)
    global var_units
    if var_units is None:
        # Use function argument if available:
        if not varunits is None:
            var_units=varunits
        else:
            # Otherwise use from file:
            var_units=var_units_loaded
        if verbose:
            print("  proceding with series units '%s'"%var_units)
    # From here, we will use varunits (local var) for what to  do with units:
    if varunits is None:
        varunits=var_units
    # 
    if 'long_name' in ncvar.ncattrs():
        longname  = ncvar.getncattr('long_name')
        global var_longname_loaded
        var_longname_loaded = longname
        global var_longname
        if var_longname is None:
            var_longname=longname
            if verbose:
                print("  proceding with long_name='%s'"%var_longname)
    global var_fill_loaded
    if '_FillValue' in ncvar.ncattrs():
        var_fill_loaded = ncvar.getncattr('_FillValue')
        if verbose:
            print("  proceding with _FillValue='%s'"%str(var_fill_loaded) ) 
    global var_missing_loaded
    if 'missing_value' in ncvar.ncattrs():
        var_missing_loaded = ncvar.getncattr('missing_value')
        if verbose:
            print("  proceding with missing_value='%s'"%str(var_missing_loaded) )
    if var_missing_loaded is None:
        var_missing_loaded = var_fill_loaded # Could still be none, but not much else to be done
    #
    ntims=len(ncobj.dimensions[nctimname])
    if verbose:
        print("  reading %d time steps of %s and %s"%(ntims,nctimname,ncvarname))
    # 
    # Store/keep the input precision, so that we can save at the 
    # same precision if nothing else is given at that time:
    global var_precision_loaded
    if   ncvar.datatype == np.float32:
        var_precision_loaded='f4'
    elif ncvar.datatype == np.float64 or ncvar.datatype == np.float:
        var_precision_loaded='f8'
    else:
        raise AssertionError("Unexpected netcdf float-type (%s) for '%s' in file %s"%(str(ncvar.datatype),ncvarname,filename))
    #
    # But internally, we will always store time and elev as double precision floating point arrays:
    timvals=np.empty((ntims,),dtype=np.float64)
    varvals=np.empty((ntims,),dtype=np.float64)
    #
    # Actually copy data to local numpy arrays
    timvals[:] = nctime[:]
    varvals[:] = np.reshape(ncvar[...],newshape=(ntims,)) # May contain degenerate dimensions
    #
    # Convert time values (netcdf variable + units) to dateval 
    # (numpy ndarray of datetime.datetime objects):
    #
    #   Since around netCDF4 version 1.5.4, or rather precisely in cftime version 1.1.0, 
    #   it seems they migrated away from python datetime library
    #      https://stackoverflow.com/questions/60958754/typeerror-class-cftime-cftime-datetimegregorian-is-not-convertible-to-date
    #      https://github.com/Unidata/cftime
    #      https://github.com/Unidata/netcdf4-python
    datevals=netCDF.num2date(timvals,time_units_loaded)
    datevals=np.asarray([datetime.datetime.fromisoformat(dt.isoformat()) for dt in datevals])
    #
    # Round to, say, nearest micro-second, second or minute
    # (or round to micro-second, which is not to round at all).
    # If roundtime is None here (it normally is), then command line option 
    # parsed by the tsSimple module will determine the rounding.
    ts.round_dates(datevals,roundto=roundtime)
    # In verbose mode, write time of first and last time step.
    if verbose:
        print(" file time range covers %s to %s"%(datevals[0].strftime("%Y-%m-%d %H:%M:%S"),
                                                  datevals[-1].strftime("%Y-%m-%d %H:%M:%S"),))
    #
    # Keep stuff like history around if we need to re-use on output.
    if 'history' in ncobj.ncattrs():
        if verbose>1:
            print("  reading history attribute")
        global history_loaded
        history_loaded=ncobj.getncattr('history')
        if verbose>2:
            print(history_loaded)
    # Regarding missing_values:
    # If we do nothing special, then missing_value elements (in varvals)
    # are just read as standard floats, with the missing_value value.
    # That is normally pretty bad, so we will deal with it here
    if convertmissing is None:
        convertmissing = convert_missing
    
    if convertmissing == 'keep' or convertmissing == 'ignore':
        # In this case there is nothing to do, and we will keep the values.
        pass
    elif var_missing_loaded is None:
        print(" Warning: Netcdf variable %s does not have a missing_value attribute. Cannot dispose of missing values")
    else:
        # If we shoud process the missing_values, then we first convert them to NaN:
        varvals[varvals==var_missing_loaded]=np.float64("NaN") # np.nan
        if convertmissing == 'delete':
            datevals,varvals=ts.remove_nans(datevals,varvals)
    #
    # Possibly round read variable data
    if roundvals is None:
        global round_vals
        roundvals = round_vals
    if not roundvals is None:
        varvals = ts.roundto_unit(varvals,srcunit=var_units_loaded,roundto=roundvals)
    #
    # Possibly scale read variable data if units have to change.
    if varunits != var_units_loaded:
        # The read units and the "what-we-want" units do not agree.
        ts.change_unit(varvals,srcunit=var_units_loaded,tgtunit=varunits)
    #
    # Deal with options from loaded module tsSimple
    datevals,varvals=ts.apply_options(datevals,varvals)
    # All done:
    return datevals,varvals,longname

# Save/write a time series elev[datevals] to netCDF.
# Stored metadata - from previous load and/or command line options - may be used 
# for those arguments, which are not passed.
# If filename is not given, then nctgt (command line positional argument) will be used.
#
# datevals:  datetime vector. Mandatory unless append=True or --append is specified.
# elev:      elevation time series (same length as datevals).
#            Mandatory unless append=True or --append is specified.
# filename:  Target file name, or use whatever is specified to ncout
# longname:  Elevation longname attribute, eg 'Observed elevation at <STATION>'
# timeunits: Target time units, such as "seconds since 2010-01-01 00:00:00"
# precision: Accuracy on output: 'f4', 'f8', or a numpy.float{64,32,} type
# missingval: Use this value as missing_values and _FillValue attribute.
# history:   Add this to history attribute
# append:    If true, then assume that file already exists, and just overwrite with 
#             updated time and/or variable data.
# TODO: ADD missing/fill attr values
def ncfil_save(datevals=None,varvals=None,filename=None,
               longname=None,varunits=None,timeunits=None,
               precision=None,missingval=None,
               savenan=None,
               history=None,appendhistory=None,
               append=None): # optional history!
    #
    # Sanity checks:
    if append is None:
        append=append_file
    if not append:
        if datevals is None or varvals is None:
            raise AssertionError("ncfil_save requires both datetime vector and valvals vector except in append mode")
    if varvals  is None:
        nvars = 0
    else:
        nvars =len(varvals)
    if datevals is None:
        ndates = 0
        ts.assert_varvals(varvals)
    else:
        ndates = len(datevals)
        ts.assert_consistent(datevals,varvals)
    #
    # Figure out what precision to use on output of varvals.
    ncprec='fail'
    if not varvals is None:
        ncprec=_get_ncprec(varvals,precision=precision)
    #
    # Figure out what to do with NaNs
    if savenan is None:
        savenan = save_nan
    if not varvals is None:
        if savenan == 'keep':
            # Just keep whatever we have
            pass
        elif savenan == 'delete':
            # Remove NaNs (if there are any).
            datevals,varvals=ts.remove_missing(datevals=datevals,varvals=varvals)
        elif savenan == 'expand':
            # Expand series to equidistant, adding NaNs in all the holes:
            datevals,varvals=ts.piecewise_to_fully_equidistant(datevals=datevals,varvals=varvals)
        # TODO/maybe: Add option collapse to make consequtive nan-values a single value.
    #
    if filename is None:
        global nctgt
        filename=nctgt
    if filename is None:
        print(" Cannot deduce target file name")
        raise AssertionError("Cannot create file: filename missing")
    #
    # Note: use fall-back values if no values were loaded!
    if missingval is None:
        missingval=missing_val
    if missingval is None:
        missingval=var_missing_loaded
    if missingval is None:
        missingval=ncmissing
    # Ensure that missingval has the right precision to be stored as attribute
    if type(missingval) != nc2py_prec[ncprec]:
        missingval=nc2py_prec[ncprec](missingval) # eg np.float64(missingval)
    #
    if append:
        # In append mode we already have the file, but we need to check that it is valid:
        if not file_is_present(filename):
            raise AssertionError("File %s does not exist. Refusing to append."%filename)
        if not file_is_ncts(filename):
            raise AssertionError("File %s does not look like a valid time-series file. Refusing to append."%filename)
        if verbose:
            print(" Will overwrite (append mode) data in existing file %s"%filename)
        # Force-read the file. This makes sure that we have access to stuff 
        # like timeunits, longname etc as we move forward
        datevecOld,elevsOld,lnamOld=ncfil_load(filename=filename)
        #
        # Check that dimensionality is OK with new data - if we have new data
        nOld=len(datevecOld)
        if not datevals is None and nOld != ndates:
            raise AssertionError("Number of time steps differ between vector [%d] and file %s[%d]. Cannot append."%(ndates,filename,nOld))
        if not varvals is None and nOld != nvars:
            raise AssertionError("Number of var-values differ between vector [%d] and file %s[%d]. Cannot append."%(ndates,filename,nOld))
        ncobj=netCDF.Dataset(filename,'a')
        nctim = ncobj.variables[nctimname]
        ncvar = ncobj.variables[ncvarname]
    else:
        # Not append, so define a new netcdf file:
        if os.path.isfile(filename) and not overwrite:
            raise AssertionError("Refusing to overwrite target file %s"%filename)
        if verbose:
            print(" Creating new netcdf file %s"%filename)
        ncobj=netCDF.Dataset(filename,'w',format='NETCDF3_64BIT_OFFSET')
        ncobj.createDimension(nctimname,None) # unlimited dimension
        #   TODO: Should we ever save in less that 64bit float?
        nctim = ncobj.createVariable(nctimname,'f8',  (nctimname,))
        ncvar = ncobj.createVariable(ncvarname,ncprec,(nctimname,),fill_value=missingval) 
    #
    # longname for elev:
    if longname is None:
        # Try specified by option (or read)
        longname=var_longname
    if longname is None:
        # Try from load:
        longname=var_longname_loaded
    if longname is None:
        print(" Cannot deduce %s.long_name attribute"%ncvarname)
    #
    if timeunits is None:
        timeunits=time_units
    if timeunits is None:
        timeunits=time_units_loaded
    if timeunits is None:
        print(" Cannot deduce %s.units attribute"%nctimname)
        raise AssertionError("Cannot create file: %s.units missing"%nctimname)
    #
    # Validate that it seems like a unit which works:
    timeunits=_checktimeunits(timeunits)
    #
    # Check variable units - for input-output it should be already done,
    # but sometimes we want to specify on input here:
    if varunits is None:
        varunits=var_units
    if varunits is None:
        varunits=var_units_loaded
    if varunits is None:
        print(" Cannot deduce %s.units attribute"%ncvarname)
        raise AssertionError("Cannot create file: %s.units missing"%ncvarname)
    #
    # What to write/add to history attribute:
    if history == None:
        history="%s: %s"%(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC")," ".join(sys.argv[:]),)
    if appendhistory is None:
        global append_history
        appendhistory = append_history
    global history_loaded
    if appendhistory and (history != "") and (not history_loaded is None):
        history += "\n"+history_loaded
    #
    # Attributes:
    nctim.setncattr('units',timeunits)
    ncvar.setncattr('units',varunits)
    if not longname is None:
        ncvar.setncattr('long_name',longname)
    ncvar.setncattr('missing_value',missingval)
    # _FillValue must be set when variable is created (using fill_value keyword to createVariable)
    #ncvar.setncattr('_FillValue',missingval)
    if history != "":
        ncobj.setncattr('history',history)
    #
    # Convert datetime to floats for storage
    if not datevals is None and datevals.shape[0]>0:
        ts.round_dates(datevals)
        timvars=netCDF.date2num(datevals,timeunits)
        nctim[:]=timvars[:]
    if not varvals is None and varvals.shape[0]>0:
        # For some reason, the following crashes, as if all elements of varvals 
        # are not np.float (they are). 
        #    np.isnan(varvals) # This may fail.
        # So we force-convert the array to all elements of specific type.
        # As we do this, we may as well convert to target precision
        # (in principle, we could avoid this except when the varvals and target precision differ
        #varvals=np.asarray(varvals,dtype=type(varvals[0]))
        #varvals=np.asarray(varvals,dtype=nc2py_prec[ncprec])
        varvals=varvals.astype(dtype=nc2py_prec[ncprec],casting='same_kind')
        # Then we can convert NaNs to target missingval:
        varvals[np.isnan(varvals)]=missingval
        # Store in netcdf:
        ncvar[:]=varvals[:]
    # Sync and close file:
    ncobj.close()
    return True


# Helper routine - just return the requested accuracy ('f4' or 'f8') based 
# on argument, command-line option or varvals type.
def _get_ncprec(varvals,precision=None):
    global var_precision
    global var_precision_loaded
    ncprec='wrong'
    if not precision is None:
        # Get from argument to this sub
        if isinstance( precision , strtype):
            if   precision == 'f8' or precision == 'f4':
                ncprec = precision
            elif isinstance( precision, type ):
                if precision == np.float32:
                    ncprec='f4'
                elif precision == np.float or precision == np.float64 or precision == float:
                    ncprec='f8'
            if ncprec == 'wrong':
                raise AssertionError("unexpected value for argument precision='%s'"%str(precision))
    elif not var_precision is None:
        # Get from command-line option:
        ncprec=var_precision
    elif not var_precision_loaded is None:
        ncprec=var_precision_loaded
    else:
        # Get it from the varvals (numpy array).
        # (Normally, the array would be float64 unless the caller does something crazy)
        if   varvals.dtype == np.float32:
            ncprec='f4'
        elif varvals.dtype == np.float64:
            ncprec='f8'
    return ncprec

#                                                                      #
# END      FILE OPERATIONS                                             #
########################################################################


##
# Default behaviour is to check a file - and copy it if there are two arguments:
def main():
    parser=args_define_options()
    parser=args_define_positional_oneplus(parser)
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # We could run only parts of these depending on arguments:
    global ncsrc
    assert_file_is_present(ncsrc)
    assert_file_is_ncts(ncsrc)
    global nctgt
    #
    # This was just to check copy.
    if False and not nctgt is None:
        ncfil_copy(ncsrc,nctgt)
        assert_file_is_present(nctgt)
        assert_file_is_ncts(nctgt)
        if verbose:
            print(" Copy OK - deleting target file again")
        os.remove(nctgt)
    #
    # Test getting time series and data from file,
    # Note that ncil_load also does round etc.
    datevals,varvals,lnam=ncfil_load(filename=ncsrc)
    #
    # These just write some info:
    numvals=varvals.shape[0]
    dt=None
    if numvals == 0:
        if verbose:
            print(" WARNING: No data read (empty array).")
    elif numvals == 1:
        if verbose:
            print(" Time series consists of a single element. No dt can be computed")
    else:
        #datevals.shape[0]>1:
        dt = ts.get_timestep(datevals,roundto=False)
        ts.is_monotone(datevals)
        isequi=ts.is_equidistant(datevals)
    #
    # Possibly remove all nans from time series before save
    if convert_missing and convert_missing == 'delete':
        datevals,varvals=ts.remove_missing(datevals,varvals)
        
    #
    # OUTPUT
    if not nctgt is None:
        # plus creating new file based on time series and data.
        ncfil_save(datevals,varvals, )
    elif append_file:
        # nctgt is none, but we can append to ncsrc
        ncfil_save(datevals,varvals,filename=ncsrc)
    else:
        # No netcdf output, but if we are really verbose, then get more info on the time series:
        if verbose>1:
            nmiss=ts.count_missing(varvals)
            dateval_start= datevals[0]
            dateval_stop = datevals[-1]
            timelength   = np.float64((dateval_stop-dateval_start).total_seconds())
            if dt is None:
                nmax = varvals.shape[0]
                nact = nmax
                pmiss= 0.0 # No missing, but nothing interesting either
            else:
                nmax = 1+int(np.round(timelength/dt))
                nact = numvals-nmiss
                pmiss= float(nmax-nact)/float(nmax)
            print(" Time series contains real data for %d of %d possible values (%.2f%% missing)"%(nact,nmax,100*pmiss))
            if nmiss==0 and isequi:
                print(" Time series is really equidistant (equidistant and no missing values) covering '%s' to '%s'"%(str(datevals[0]),str(datevals[-1])))
            else:
                # So, there are pieces. Find out how many:
                datevals_nonan,varvals_nonan=ts.remove_missing(datevals,varvals)
                # 
                npieces=ts.get_separate_piece(datevals=datevals_nonan,varvals=varvals_nonan)
                for ip in range(npieces):
                    # This is only to generate log/output.
                    # We dont actually need the pieces
                    ts.get_separate_piece(datevals=datevals_nonan,varvals=varvals_nonan,ipiece=ip)

if __name__ == '__main__':
    main()
