#!/usr/bin/python
#
"""
Perform quality control validation on time series (of observational)
elevation data from a netcdf file.
"""

# 
# Module history:
#  2022-02-07 bjb 0.0.1: Initial work
#  2022-02-11 bjb 0.0.2: Use tsSpline for despike part
#  2022-02-25 bjb 0.0.3: Add more methods for QC
#  2022-03-11 bjb 0.0.4: Remove bug - ensure skip-to-next subseries when deleting uniq or flapping
#  2022-05-10 bjb 0.0.5: Work on sparse, preDegap. New structure for lonelies.
#  2022-05-11 bjb 0.0.6: Remove singleton lonlies from ts-piece ends.
#  2022-05-17 bjb 0.0.7: Deal with times series, which are degenerate (empty) after QC
#  2022-06-14 bjb 0.0.8: Bugfix for keep_ranges. Add extra timeseries-assertions during QC.
#  2022-08-03 bjb 0.0.9: Bugfix for removing pre-gapped values at/near/beyond end-of-series
#  2022-08-04 bjb 0.0.10: Let keep_ranges apply for all QC, not just simple_QC

version_info = (0, 0, 10)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

# TODO/WORK:
#  * Do we need/want sparse-implementation during the iterative QC-process along-side lonelies?
#  * Are there errors in the lonelies implementation???
#     -> Test a one-off loop with lonelies 1,1 + sparse 2,3 (or something)
#  * Try to pre-add/-fill small holes (single or 2-wide) in places where there 
#      are eg 2 or 3 data-points on each side.
#      Maybe only after initially removing really crap stuff, and possibly an initial spike-removal.
#    -> prefill 1,3 2,4. Need to return/append to list of prefilled data points, so we can remove them again

#
# FOR NOW, only use as a script, not include as a module.
# At some point, it may be feasible to use as module to use with other scripts/modules,
# but for now that has not been considered.
# Implementation of the present code is made 
# Some parts have been prepared to be used as a module, but really it should not be included.
# This is just a safe-guard against that. If the scripts is ever turned into a 
# module with other methods than "main()", then remove the following block:
if __name__ != '__main__':
    raise AssertionError("tsObsValidate.py should not be imported - only run as script")

#
# Locally implemented modules
import tsSimple as ts
import tsNetcdf as tsnc
import tsSpline as tssp
#import tsObsDespike as tsspk
#

# Other imports
import numpy   as np
import datetime

# Global variables for this module/method, including defaults
#  Simple QC:
keepPeriods=[]
delPeriods=[]
hardminlimit=-5.
hardmaxlimit=5.
delVals=[]
preDegap=[]
delLonelies=[]
delSpike=None
ndelrep=None
stuckval=[]
delflap=[]
delsparse=[]
roundtime='10sec'
#
#  Spline-based despiking [PUT IN SEPARATE MODULE FOR REUSE!]


# Defining our own parser should not really be necessary for this module.
argparse_description="""Perform simple Quality Control [QC] and spline-based despiking in an iterative process. 
Input (source) data and output (target) data in local tsElev-compliant NetCDF files.
Compared to the tsSpline module, the present module will force defaults: minwidth=2*knotwidth and maxgap=(2/3)*knotwidth.
Feel free to use other values, but no/zero values work really bad for observed elevation.
"""

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False


# Define possible named command line options
def args_define_options(parser=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    # This module cannot be called as script (yet), but we will add the 
    # magic needed if someone opts to implement it later:
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    # ============= parameters for simple QC of observations:
    qcgroup = parser.add_argument_group(title='Simple QC',description="Screen observations by range and/or delete specific value(s) ,and 'stuck' or 'flapping' sensor subseries. Time-values will be roundet to 10s or whatever is set by --roundtime")
    #
    qcgroup.add_argument("--keep_period","--keepperiod",dest="keepPeriods", 
                         metavar="<EPOCHNN>-<EPOCHNN>", type=ts._check_epstr_dash_epstr,
                         action="append", default=[], 
                         help="""Keep observations from this period back from 
                         other simple QC tests and deletions. 
                         Data from this period will be returned to the timeseries  
                         after other QC has finished. 
                         In particular keep_period is executed before delete_period, 
                         so delete_period can contain a keep_period. 
                         Option can be given more than once. Periods should, however, 
                         not overlap. If they do, then bad things may happen.""")
    #
    qcgroup.add_argument("--delete_period","--delperiod",dest="delPeriods", 
                         metavar="<EPOCHNN>-<EPOCHNN>", type=ts._check_epstr_dash_epstr,
                         action="append", default=[], 
                         help="""Delete all observations in a range defined by two epochs.
                         Option can be given more than once.""")
    #
    qcgroup.add_argument("--delhardminlimit",dest="hardminlimit", 
                         metavar="<FLOAT>",type=float,
                         action="store", default=hardminlimit, 
                         help="Delete all values lower than this hard minimum limit "+\
                         "(default {}[m]).".format(hardminlimit))
    #
    qcgroup.add_argument("--delhardmaxlimit",dest="hardmaxlimit", 
                         metavar="<FLOAT>",type=float,
                         action="store", default=hardmaxlimit, 
                         help="delete values greater than this hard maximum limit "+\
                         "(default {}[m]).".format(hardmaxlimit))
    #
    qcgroup.add_argument("--delval",dest="delVals", 
                         metavar="<FLOAT>", type=float, 
                         action="append", default=[], 
                         help="""Delete all observations with this exact value. 
                         Option can be given more than once.""")
    #
    qcgroup.add_argument("--preDegap","--predegap",dest="preDegap", 
                         metavar="<GROUPSIZE,NNEIGH>", type=ts._check_int_comma_int,
                         action="append", default=[], 
                         help="""Pre-fill data into small group (GROUPSIZE) of missing-values 
                         surrounded by at least NNEIGHT OK dat points on each side.
                         The pre-fill will be applied before each simple-QC loop, and the 
                         values will be removed again by the end of the loop.
                         These values are applied only to make other tests behave reasonably.
                         Normal values are eg "1,3", "2,3", but other values are possible.
                         Option can be given more than once. Default OFF.""")
    #
    qcgroup.add_argument("--delete_lonelies","--del_lones","--deleteLonelies",dest="delLonelies", 
                         metavar="<GROUPSIZE,NNEIGH>", type=ts._check_int_comma_int,
                         action="append", default=[], 
                         help="""Delete group of points (sub-series) of GROUPSIZE points 
                         separated from the rest of the time series by (at least) NNEIGH missing values on both sides.
                         Both GROUPSIZE and NNEIGH must be integers.
                         Normal values are eg "1,1", "2,1", and "3,2", but other 
                         values are possible.
                         Note that --dellonelies=1,1 is roughly equivalent to --delete_lonely 
                         in tsSimple (dellonelies is repeated iteratively, while delete_lonely 
                         only is in effect during data read).
                         This argument will also trigger removal of singleton lonelies at end of subseries,
                         ie. where distance to neighbor in one direction is at least --timestep_large.
                         Option can be given more than once. Default OFF.""")
    #
    qcgroup.add_argument("--ndelrep",dest="ndelrep", 
                         metavar="<NREP>",type=ts._check_nonnegative_int,
                         action="store", default=None, 
                         help="""Delete sub-series if N or more repetitions 
                         of the same (any) value is found. Default: OFF.""")
    #
    qcgroup.add_argument("--nstuckval",dest="stuckval", 
                         metavar="<NREP,FLOAT>", type=ts._check_int_comma_float,
                         action="append", default=[], 
                         help="""Delete sub-series with NREP or more repetitions 
                         of a particular specified value (float). 
                         Option can be given more than once. Default OFF.""")
    #
    qcgroup.add_argument("--delflapping",dest="delflap", 
                         metavar="<NREP,NUNIQ>", type=ts._check_int_comma_int,
                         action="append", default=[], 
                         help="""Delete sub-series with NREP or more repetitions 
                         with only NUNIQ unique values. 
                         Option can be given more than once. Default OFF.""")
    #
    qcgroup.add_argument("--delete_spikes","--delSpikes","--delspikes",dest="delSpikes", 
                         metavar="<FLOAT>",type=float,
                         action="store", default=None, 
                         help="""Delete points, where local difference-based estimation of curvature
                         corresponds to a spike of FLOAT [m] in an otherwise smooth series.
                         This option effectively eliminates spikes in an equidistant part of the 
                         time series.
                         Default: OFF.""")
    #
    qcgroup.add_argument("--delete_sparse","--delSparse","--delsparse",dest="delsparse", 
                         metavar="<NMISS,NLENGTH>", type=ts._check_int_comma_int,
                         action="append", default=[], 
                         help="""Look for NMISS or more missing values (observations) in any window of NLENGTH or smaller.
                         This option is good for taking out an entire period, which seems suspect.
                         Option can be given more than once. Default OFF.""")
    #
    #
    _have_defined_options=True
    # Let other used modules define their options if we are in MAIN/SCRIPT mode.
    # In the present case, we do not want any such options??
    if __name__ == '__main__':
        # Include tsnc options, but not any further options.
        parser = tsnc.args_define_options(parser,recurse=True)
        # Define spline, but not a required!
        parser = tssp.args_define_options(parser,withwidth=True, withorder=True, withoutput=False, withdespike=True, withreinsert=False,required_options=False)
        pass
    return parser
# Companion parser for args_define_options
# This just copies arguments to module-scoped variables
def args_parse_options(parsed_args):
    global verbose 
    verbose       = parsed_args.verbose # tsnc.verbose
    # Now deal with our own options:
    global keepPeriods
    keepPeriods=list(parsed_args.keepPeriods)
    global delPeriods
    delPeriods=list(parsed_args.delPeriods)
    global hardminlimit
    hardminlimit=parsed_args.hardminlimit
    global hardmaxlimit
    hardmaxlimit=parsed_args.hardmaxlimit
    assert hardmaxlimit>hardminlimit,"hardMAXlimit must be larger than hardMINlimit"
    global delVals
    delVals=list(parsed_args.delVals)
    global preDegap
    preDegap=list(parsed_args.preDegap)
    global delLonelies
    delLonelies=list(parsed_args.delLonelies)
    global delSpike
    delSpike=np.float64(parsed_args.delSpikes)
    global ndelrep
    ndelrep=parsed_args.ndelrep
    global stuckval
    stuckval=list(parsed_args.stuckval)
    global delflap
    delflap=list(parsed_args.delflap)
    global delsparse
    delsparse=list(parsed_args.delsparse)
    global roundtime
    if parsed_args.roundtime:
        roundtime=parsed_args.roundtime
    # Sanity check. We *MUST* have splinewidth if we use QC based on splines
    if parsed_args.splineorder:
        assert not parsed_args.splinewidth is None and parsed_args.splinewidth>0,\
            "Spline definition must be based on --splinewidth for validate to work"
        # FORCE defaults for minwidth and maxgapwidth for spline-fitting stuff.
        knotwidth=np.float64(parsed_args.splinewidth)
        if parsed_args.splineminwidth is None or parsed_args.splineminwidth<=0:
            minwidth=2*knotwidth
            if verbose:
                print(" subseriesminwidth: Defaulting value to 2xknot width={}".format(minwidth))
            parsed_args.splineminwidth=minwidth
        #
        if parsed_args.timestep_large is None or parsed_args.timestep_large<=0:
            dtsep=3*knotwidth/2
            if verbose:
                print(" timestep_large: Defaulting value to (2/3)xknot width={}".format(dtsep))
            parsed_args.timestep_large=dtsep
    if verbose:
        print(" roundtime: Rounding time to {}".format(roundtime))
        for kp in keepPeriods:
            print(" keep_period (no simple QC) for {}".format(kp))
        for dp in delPeriods:
            print(" del_period (flat deletion) for {}".format(dp))
        print(" deleting values outside range [{}:{}]".format(hardminlimit,hardmaxlimit))
        for dv in delVals:
            print(" delval: Will delete all values = {}".format(dv))
        for pdg in preDegap:
            ng,nn=pdg.split(',')
            ng=int(ng)
            nn=int(nn)
            print(" preDegap: Will pre-fill nan-groups of {} points as long as there are at least {} good value(s) on each side".format(ng,nn))
        for dl in delLonelies:
            ng,nn=dl.split(',')
            ng=int(ng)
            nn=int(nn)
            print(" delLone: Del groups of {} points separated from rest of series by at least {} bad values on each side".format(ng,nn))
        if not ndelrep is None:
            print(" ndelrep: Delete subseries with {} or more repetitions".format(ndelrep))
        for sv in stuckval:
            nn,vv=sv.split(',')
            print(" nstuckval: Delete subseries with {} or more repetitions of value = {}".format(nn,vv))
        for fl in delflap:
            nn,nuniq=fl.split(',')
            print(" delflap: Delete flapping subseries with {} or more repetitions of {} or fewer unique values".format(nn,nuniq))
        for sp in delsparse:
            nmiss,nlen=sp.split(',')
            print(" delsparse: Delete subseries where {} or more of {} or less is missing".format(nmiss,nlen))
        if not parsed_args.splineorder:
            print("NOTE: All spline-based QC disabled (order not given)")
    #
    if __name__ == '__main__':
        tsnc.args_parse_options(parsed_args)
        #tsspk.args_parse_options(parsed_args)
        if parsed_args.splineorder:
            tssp.args_parse_options(parsed_args)
    #
    return

# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC>  <NCTGT>
def args_define_positional(parser=None):
    if __name__ != '__main__':
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    # Define a two mandatory arguments here: IN OUT
    parser = tsnc.args_define_positional_two(parser)
    return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
    if __name__ != '__main__':
        return
    tsnc.args_parse_positional(parsed_args)
    return


def sanitize_series(datevals,varvals,roundto=None,
                    verb=None):
    verbloc=ts._verbloc(verb)
    if verbloc:
        print('Sanitizing time series')
    #
    # Round timesteps to nearest 10sec (or whatever) to get rid of 
    # small "jitter" on the time-part
    if roundto is None:
        global roundtime
        roundto=roundtime
    #
    datevals=ts.round_dates(datevals,roundto=roundto,verb=verb)
    #
    # Get rid of duplicate values.
    #  Time steps with several differing values are deleted totally
    datevals,varvals=ts.remove_duplicates(datevals,varvals, epsivar=0.0001, duplicates='delete-differs',verb=verb)
    #  For duplicated time steps, we simply average. It is more robustly implemented than taking first/last.
    datevals,varvals=ts.remove_duplicates(datevals,varvals, epsivar=0.0001, duplicates='average',verb=verb)
    #  Check timestep. If there are major gaps, then we should probably not continue(?)
    if verbloc:
        print('Find timestep')
    #
    #  This just writes whether the series is equidistant:
    ts.is_equidistant(datevals,verb=1)
    return datevals,varvals

# del_range applications
# Delete parts of the time series given by list of "ep-ep" descriptors.
def del_ranges(datevals,varvals,deleteperiods=None,dt=None,verb=None):
    verbloc=ts._verbloc(verb)
    verbnxt=max(0,verbloc-1)
    dt=ts._dtloc(datevals,dt=dt,verb=verbnxt)
    # 
    if deleteperiods is None:
        global delPeriods
        deleteperiods=delPeriods
    if deleteperiods is None:
        deleteperiods=[]
    #
    for rangestr in deleteperiods:
        print(" Must del {}".format(rangestr))
        # Option is pre-validated, so we can just split-by-dash:
        epstr1,epstr2=rangestr.split('-')
        datevals,varvals=ts.delete_time_range(datevals=datevals,varvals=varvals,fromtime=epstr1,totime=epstr2,verb=max(verbloc,1))
        ts.assert_monotone(datevals=datevals)
        ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
    #
    # End of del_ranges:
    return datevals,varvals


# Keep-ranges are defined by list of "ep-ep" descriptors.
# keep_ranges_strip goes over the list, and removes each section from the time series,
# storing it instead in a dict, so that it can be fetched again later.
# The dict is returned alongside the (remainder of the) timeseries, so it 
# can be used in keep_ranges_reinsert.
def keep_ranges_strip(datevals,varvals,keepperiods=None,dt=None,verb=None):
    verbloc=ts._verbloc(verb)
    verbnxt=max(0,verbloc-1)
    dt=ts._dtloc(datevals,dt=dt,verb=verbnxt)
    #
    # Deal with options/defaults
    if keepperiods is None:
        global keepPeriods
        keepperiods=keepPeriods
    if keepperiods is None:
        keepperiods=[]
    #
    # We will keep the ranges [datevals,varvals] in a dict, where the range start datetime is key,
    # so that we can sort them easily.
    # We do *not* want to make a full re-sort of the entire time series by dateval, 
    # so we need to be a little bit clever.
    # Also, we cannot rely upon keepperiods being sorted by time, so we cannot 
    # just append to a list.
    keep_ranges={}
    for rangestr in keepperiods:
        print(" Must keep {}".format(rangestr))
        # Option is pre-validated, so we can just split-by-dash:
        epstr1,epstr2=rangestr.split('-')
        di,vi=ts.get_time_range(datevals=datevals,varvals=varvals,fromtime=epstr1,totime=epstr2,verb=verbloc)
        if di.size==0:
            # In this case there are no actual data in the keep_period (range), 
            # so nothing to do. Thus, dont add to list of ranges to keep, but complain a little.
            print(" No actual values in {}".format(rangestr))
        else:
            keep_ranges[di[0]]=[di,vi]
            # Delete from original series:
            datevals,varvals=ts.delete_time_range(datevals=datevals,varvals=varvals,fromtime=epstr1,totime=epstr2,verb=0)
            #ts.assert_monotone(datevals=datevals)
            #ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
    return datevals,varvals,keep_ranges

def keep_ranges_reinsert(datevals,varvals,keep_ranges,dt=None,verb=None):
    verbloc=ts._verbloc(verb)
    verbnxt=max(0,verbloc-1)
    dt=ts._dtloc(datevals,dt=dt,verb=verbnxt)
    # Now, if any there are any keep-periods, then put them back into the time series.
    # TODO_BJB KEEP_RANGES SEEMS BROKEN!!
    if len(keep_ranges):
        # Ie keep_ranges dict is not empty.
        # Initialization.
        #  Fill these lists chronologically with timeseries-parts and interspacing keep_periods
        datparts=[]
        varparts=[]
        #  Here, datevals and varvals will represent the remaining timeseries parts not yet put into lists,
        #  Beware that because keep_ranges is a dict, the order is not guaranteed!! So sorting is needed,
        #  so we get the lowest key-values first.
        for date0 in sorted(keep_ranges):
            # Get (shorthands to) the dates and values for this keep_range part:
            keepdat=keep_ranges[date0][0]
            keepvar=keep_ranges[date0][1]
            # These are start and stop for the present keep_range:
            d0=keepdat[0]
            dN=keepdat[-1]
            assert d0==date0,'Internal error: d0!=date0'
            # Split the QC'ed part into a pre- and rest.
            dat0pre,var0pre=ts.get_time_range(datevals=datevals,varvals=varvals,fromtime=None,totime=d0,include_ends=0,verb=0)
            # Update for datevals,varvals to be everything *after* the split point:
            datevals,varvals=ts.get_time_range(datevals=datevals,varvals=varvals,fromtime=dN,totime=None,include_ends=0,verb=0)
            # Stuff the pre-part onto list
            datparts.append(dat0pre)
            varparts.append(var0pre)
            # Then append this keep_range:
            datparts.append(keepdat)
            varparts.append(keepvar)
        # Finally, add any remainder of the time series:
        datparts.append(datevals)
        varparts.append(varvals)
        # And catenate it all:
        datevals=np.concatenate(datparts)
        varvals =np.concatenate(varparts)
        ts.assert_monotone(datevals=datevals)
        ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
    return datevals,varvals


# Simple Quality Control:
#   * Keep specified periods out of QC ("quarantine")
#   * Delete specified periods
#   * Delete out-of-range [hardmin:hardmax] values
#   * Delete elements with particular specified values
#   * REPEAT ITERATIVELY (maxiters or until "no furhter changes").
#    - Loop over time series pieces and skip-to-next as soon as there are changes in 
#     * Delete repeat-values (ndelrepeat) aka "stuck sensor"
#     * Delete repeated specified values (ie often stuck at this value)
#     * TODO: Delete "lonely" points, ie with several time steps to nearest other point
#     * TODO: Delete "spike" points, ie with very high local "curvature" but with close-by other points
#             For now, only for a: points iwht dt-near neightbors, b: 2*dt-near neighbors.
#         THINK: How to find distance to nearest neighbor (in time) in N*dt for each point. Quick.
#     * Delete elements flapping between a few (2-3) values for nstuck or longer
#  
#   * Re-insert "quarantined" periods
#
def simple_QC(datevals,varvals,keepperiods=None,deleteperiods=None,
              hardmin=None,hardmax=None,deletevalues=None,
              predegap=None,deletelonlies=None,deletespikes=None,
              ndelrepeat=None,nstuckvalues=None,deleteflaps=None, 
              timestep_large=None,minserieslen=None,
              maxiters=None,
              # The following are mostly passed on to other routines:
              dt=None,
              dtsep=None, # Just 
              verb=None):
    #
    verbloc=ts._verbloc(verb)
    verbnxt=max(0,verbloc-1)
    #
    args=None
    # Deal with options/defaults
    if hardmin is None:
        hardmin=hardminlimit
    if hardmax is None:
        hardmax=hardmaxlimit
    if deletevalues is None:
        global delVals
        deletevalues=delVals
    if deletevalues is None:
        deletevalues=[]
    if predegap is None:
        global preDegap
        predegap=preDegap
    if deletelonlies is None:
        global delLonelies
        deletelonlies=delLonelies
    if deletespikes is None:
        global delSpike
        deletespikes=delSpike
    if ndelrepeat is None:
        global ndelrep
        ndelrepeat=ndelrep
    if nstuckvalues is None:
        global stuckval
        nstuckvalues=stuckval
    if nstuckvalues is None:
        nstuckvalues=[]
    if deleteflaps is None:
        global delflap
        deleteflaps=delflap
    if deleteflaps is None:
        deleteflaps=[]
    if maxiters is None:
        maxitrs=np.iinfo(np.int16).max
    else:
        maxitrs=maxiters
    #
    dt=ts._dtloc(datevals,dt=dt,verb=verbnxt)
    #
    

    #
    # Eliminate obvious outliers based on simple limits.
    print('Delete observations outside limits [{}:{}]'.format(hardmin,hardmax))
    datevals,varvals=ts.outliers_to_nan(datevals,varvals,varmin=hardmin,varmax=hardmax,verb=verbloc)
    datevals,varvals=ts.remove_nans(datevals,varvals,verb=0)
    #
    # Eliminate any specific value specified
    for delval in deletevalues:
        print('Delete observations matching {}'.format(delval))
        datevals,varvals=ts.value_to_nan(datevals,varvals,delvalue=delval,verb=verbloc)
    #
    # BJB TEST 2022-05-31: Start and stop by deleting lonelies.
    #       The interaction between pre-degap and lonelies is critical.
    ts.assert_monotone(datevals=datevals)
    ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
    # BJB TEST 2022-05-10:
    # Temporarily fill small gaps in series, eg 1-2 nans with at least 3 "OK" observations on each side.
    # (How do we find those?)
    #  The series right now is 
    datesprefilled=[]
    for pdg in predegap:
        ngap,nneigh= pdg.split(',')
        ngap=int(ngap)
        datevals,varvals,dpf=ts.fill_gaps_simple(datevals,varvals,ngap=ngap,nneigh=nneigh,dt=dt,verb=verbnxt)
        datesprefilled+=dpf
    datesprefilled=np.sort(np.asarray(datesprefilled))
    #
    if verbloc:
        print("Prefilled {} elements (simple degapping)".format(datesprefilled.size))
    #
    iloop=0
    nlen0=-1
    nlenN=datevals.size
    while iloop<maxitrs and nlen0!=nlenN and nlenN>0:
        nlen0=nlenN
        iloop += 1
        #
        # Get the start of each piece for later use. dtsep must match do_spline_pieces below
        idxbreaks=ts.get_separate_piece(datevals=datevals,varvals=varvals,dt=dt,dtsep=dtsep,want_breaks=True,verb=0)
        enddates =ts.idxbreaks2enddates(idxbreaks,datevals)
        npieces  =idxbreaks.size - 1
        #
        if verbloc:
            print('QC-iteration loop no {}, treating {} pieces of time series'.format(iloop,npieces))
            # Maybe write some statistics on the series, avg, spikiness etc?
            spk=(0.5*dt*dt)*np.abs(ts.curvi(datevals=datevals,varvals=varvals,verb=0))
            print(" Overall spikiness mean={} max={}".format("%.4f"%np.mean(spk),np.max(spk)))
        ts.assert_monotone(datevals=datevals)
        ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
        #
        # Update sub-intervals, then go over them one at a time
        datevalsSubs=ts.idxbreaks2subseries(idxbreaks,datevals)
        varvalsSubs =ts.idxbreaks2subseries(idxbreaks,varvals)
        #
        # For each piece, we will at most do *one* type of change, and then skip to next.
        # This is to ensure that delete values will effect imediately bot division into pieces and "earlier" tests
        for ip in range(npieces):
            i0=idxbreaks[ip]      # First element in subseries
            iN=idxbreaks[ip+1]-1  # Last element in subseries
            #sublensecs=(datevalsSubs[ip][-1]-datevalsSubs[ip][0]).total_seconds()
            #
            nsub=datevalsSubs[ip].size
            #
            # TODO: Delete too short subseries fully here?
            #  If there are less than 3 points, then curvature and spike-detection etc is meaningless, 
            #  and such small sub-series should simply be deleted.
            if nsub < 3: # And something else too?
                if verbloc:
                    print(' Delete degenerate sub-series #{} (length {}, period {} to {})'.format(ip,nsub,enddates[ip][0],enddates[ip][1]))
                    idxdel=range(nsub)
                    for idx in idxdel:
                        print(" Eliminating elev[%d]=elev[%s]=%.3f [subseries too short]"%(idx+i0,datevalsSubs[ip][idx].strftime("%Y-%m-%d %H:%M"),varvalsSubs[ip][idx]))
                varvalsSubs[ip]  = np.delete(varvalsSubs[ip] ,idxdel)
                datevalsSubs[ip] = np.delete(datevalsSubs[ip],idxdel)
                # Really, this deletes the entire subseries, so we can just replace by a new empty one(?)
                # (I tried it out, and for some reason it seems to lead to problems(?), bjb 2022-05-12)
                #varvalsSubs[ip]  = np.empty(shape=(0,),dtype=datetime.datetime)
                #datevalsSubs[ip] = np.empty(shape=(0,),dtype=float)
                # Skip to next subseries.
                continue
            #
            # Before we do anything else, we remove "lonely" points (or small groups of points).
            # The very first is to remove singleton lonelies at end-of-pieces.
            # The easiest way to test for this is to compute the time between the end point and its
            # in-series neighbor, eg dvals[1]-dvals[0] and dvals[-2]-dvals[-1]
            # TODO/BJB:CREATE OPTION FOR THIS
            if len(deletelonlies)>0:
                idxdel=[]
                dtend0=(datevalsSubs[ip][1]-datevalsSubs[ip][0]).total_seconds()
                dtendN=(datevalsSubs[ip][-1]-datevalsSubs[ip][-2]).total_seconds()
                #print(" TESTBJB[{}]-0 {} to {} = {}".format(ip,datevalsSubs[ip][0],datevalsSubs[ip][1],dtend0))
                if dtend0>1.5*dt:
                    idxdel.append(0)
                if dtendN>1.5*dt:
                    idxdel.append(datevalsSubs[ip].size -1)
                if len(idxdel)>0:
                    if verbloc:
                        print(' Delete lonely end-point(s) of sub-series #{} (period {} to {})'.format(ip,enddates[ip][0],enddates[ip][1]))
                        for idx in idxdel:
                            print(" Eliminating elev[%d]=elev[%s]=%.3f [lonely end point]"%(idx+i0,datevalsSubs[ip][idx].strftime("%Y-%m-%d %H:%M"),varvalsSubs[ip][idx]))
                    # Actually do it:
                    varvalsSubs[ip]  = np.delete(varvalsSubs[ip] ,idxdel)
                    datevalsSubs[ip] = np.delete(datevalsSubs[ip],idxdel)
                    # Skip to next subseries.
                    continue
            # Continue with other kinds of lonelies:
            # This actually does not require to look at the data, but is only to 
            # eliminate periods with fragmented/unreliable data.
            # The idea is to further divide this subseries into subseries to check for "lonelies":
            # Go over possible lonely-combos specified on CLI-options
            # As soon as we find/hit any, then we should skip to next sub-series to avoid problems
            nloneip=0
            # Loneopt: groupsize:adddistance
            # TODO: Implement lonelies option
            npre=datevalsSubs[ip].size
            for loneopt in deletelonlies: # [ '1,1', '2,2', '3,3' ]:
                nngroupsize,nndistmax=loneopt.split(',')
                nngroupsize=int(nngroupsize)
                nndistmax  =int(nndistmax)
                # Try the new lonelies test:
                datevalsSubs[ip],varvalsSubs[ip],lonedates=ts.lonelies_to_nan(datevalsSubs[ip],varvalsSubs[ip],nlonely=nngroupsize,nneigh=nndistmax,dt=dt,verb=verbloc)
                nloneip=len(lonedates)
                if nloneip>0:
                    # Skip to next subseries
                    # First break out of loneopt
                    break
                # Otherwise, continue to next loneopt case
                continue
            # If we modified this subseries:
            if nloneip>0:
                # Then skip to next subseries:
                continue
            #
            if not ndelrepeat is None:
                if verbloc>1:
                    print(' Delete observations repeated {} or more times'.format(ndelrepeat))
                datevalsSubs[ip],varvalsSubs[ip]=ts.repeatvalues_to_nan(datevalsSubs[ip],varvalsSubs[ip],nrepeats=ndelrepeat,verb=1)
                datevalsSubs[ip],varvalsSubs[ip]=ts.remove_nans(datevalsSubs[ip],varvalsSubs[ip],verb=0)
                if datevalsSubs[ip].size!=nsub:
                    continue
            #
            for stuckopt in nstuckvalues: # [ '10,0.0', '20,4.0' ]:
                nstuck,vstuck=stuckopt.split(',')
                nstuck=int(nstuck)
                vstuck=np.float64(vstuck)
                if verbloc>1:
                    print(' Delete observations repeated {} or more times with value {}'.format(nstuck,vstuck))
                datevalsSubs[ip],varvalsSubs[ip]=ts.repeatvalues_to_nan(datevalsSubs[ip],varvalsSubs[ip],nrepeats=nstuck,repval=vstuck,verb=verb)
                datevalsSubs[ip],varvalsSubs[ip]=ts.remove_nans(datevalsSubs[ip],varvalsSubs[ip],verb=0)
                # If we modify, then break out and continue immediately to next subseries.
                # We will return here later.
                if datevalsSubs[ip].size!=nsub:
                    break
            if datevalsSubs[ip].size!=nsub:
                continue
            #
            # * Spike-detection
            # For the "spikiness", use (absolute) curvature, normalized by dt(**2)
            # ie on "dt-scale". Units will be units of varvals, ie [m].
            # A value of eg unity may correspond to a spike of 0.5m in an otherwise smooth signal.
            # Normalize also by the factor of two, so we can compare directly with a "critical spike" level
            # Note that the value decreases fast with distance to neighbour points, due to
            # the (1/dt^2)-factor (even with non-equidistant cases).
            # The plan here is to find the maximum spikiness, and compare to set limit.
            # If the max-value is above the limit, then eliminate *ALL* point sharing the max-value.
            if not deletespikes is None:
                spklim=deletespikes
                spk=(0.5*dt*dt)*np.abs(ts.curvi_ip(datevals=datevalsSubs[ip],varvals=varvalsSubs[ip]))
                spkmax=np.max(spk)
                if spkmax>spklim:
                    # Q/Maybe: Find all larger than limit - not just the maxval?? 
                    #  A:No. Removing the maxvals may significantly change things for the remainders
                    # Find all occurances of the maxval
                    idxsdel=np.flatnonzero(spk == spkmax).tolist()
                    if verbloc:
                        for idx in idxsdel:
                            print(" Eliminating elev[{}]=elev[{}]={} [spikyness {}>{}]".format(idx+i0,datevalsSubs[ip][idx].strftime("%Y-%m-%d %H:%M"),"%.3f"%varvalsSubs[ip][idx],spkmax,spklim))
                    for idx in idxsdel:
                        varvalsSubs[ip][idx]=np.float64('NaN')
                    datevalsSubs[ip],varvalsSubs[ip]=ts.remove_nans(datevalsSubs[ip],varvalsSubs[ip],verb=0)
                    # Skip to next subseries
                    continue
            #
            #
            for flapopt in deleteflaps: # ['20,2', '30,3', ]
                nrep,nuniq=flapopt.split(',')
                nrep =int(nrep)
                nuniq=int(nuniq)
                if verbloc>1:
                    print(' Delete flapping observations ({} repetitions with {} or fewer unique values)'.format(nrep,nuniq))
                datevalsSubs[ip],varvalsSubs[ip]=ts.flapvalues_to_nan(datevalsSubs[ip],varvalsSubs[ip],nrepeats=nrep,nunique=nuniq,verb=verb)
                datevalsSubs[ip],varvalsSubs[ip]=ts.remove_nans(datevalsSubs[ip],varvalsSubs[ip],verb=0)
                # If we modify, then break out and continue immediately to next subseries.
                # We will return here later.
                if datevalsSubs[ip].size!=nsub:
                    break
            if datevalsSubs[ip].size!=nsub:
                continue
            #
            # TODO: MORE STUFF HERE!!
            #       Do "spikes" after lonelies, but *before* flapping(?)
            # 
            #
        #
        # Put sub-series back together to single series
        datevals=np.concatenate(datevalsSubs)
        varvals =np.concatenate(varvalsSubs)
        datevals,varvals=ts.remove_nans(datevals,varvals,verb=0) # This aught not be necessary

        nlenN=datevals.size
        # Go over the 
        #if datevals.size==nprev:
        #
        #    break
    ts.assert_monotone(datevals=datevals)
    ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
    if verbloc:
        print('Out of QC loop.')
    #
    # If we prefilled small gaps, then remove them again here:
    if datesprefilled.size>0:
        if verbloc:
            print(" Removing prefilled/pre-degapped values")
        # dateidx are indixes into datevals,varvals where datevals matches datesprefilled. 
        # Both date-arrays are pre-sorted, so we can use a numpy routine.
        # Really, some dates might not be in datevals, as even prefilled values *could* be removed by QC. 
        # Thus we make a ==-test to only change when necessary.
        # Further, some of the prefilled dates may be removed from ends-of-series, so not even in the 
        # present range, thus the dateidx[idx]<datevals.size.
        dateidx=np.searchsorted(datevals,datesprefilled)
        ndefilled=0
        for idx in range(dateidx.size):
            if dateidx[idx]<datevals.size and datesprefilled[idx] == datevals[dateidx[idx]]:
                # This is the normal
                varvals[dateidx[idx]]=np.float64('NaN')
                ndefilled+=1
                if verbloc>3:
                    print(" Removing prefilled value at {}".format(datesprefilled[idx].strftime("%Y-%m-%d %H:%M")))
        # If we removed some prefilled valies, then remove the re-nan values:
        if ndefilled>0:
            datevals,varvals=ts.remove_nans(datevals,varvals,verb=0)
        ts.assert_monotone(datevals=datevals)
        ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
    #
    # Remove lonelies:
    #
    # 
    # All done for simple_QC:
    return datevals,varvals

# 

#
# Delete periods (windows/subseries), where many observations are missing (elements are missing/nan).
# In such periods, the quality of the remaining observations are often questionable.
# It is probably a good idea to run this particular routine *after* other QC.
# If the present routine is run iteratively, then there could be unexpected side effects,
# and we could end up with too many values deleted (YMMV).
#
def delete_sparse_periods(datevals,varvals,deletesparse=None,
              # The following are mostly passed on to other routines:
              dt=None,
              verb=None):
    verbloc=ts._verbloc(verb)
    #
    if deletesparse is None:
        global delsparse
        deletesparse=delsparse
    if deletesparse is None:
        deletesparse=[]
    #
    # NOTE: This should only be done after all other QC-validation
    for sparseopt in deletesparse: # ['10,30', '20,60', ]
        nmiss,nlen=sparseopt.split(',')
        nmiss=int(nmiss)
        nlen=int(nlen)
        datevals,varvals=ts.sparsevalues_to_nan(datevals,varvals,nmissing=nmiss,nlength=nlen,verb=verbloc)
    return datevals,varvals

# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser=args_define_options()
    parser=args_define_positional(parser)
    global args
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Sanitizing of arguments is primarily done by the argparser.
    #
    verbose=ts._verbloc()
    # This loads the data, and performs whatever tsSimple stuff 
    # has been set.
    datevals,varvals,longname=tsnc.ncfil_load()
    #
    # Rounding for time, remove duplicates
    datevals,varvals=sanitize_series(datevals,varvals)
    # Get dt for later uses, also make sure we log some info about the actual timestep distribution
    dt,dum1,dum2=ts.get_timestep(datevals=datevals,wantvector=True,verb=2*verbose)
    #
    # TEST: Temporarily fill minor gaps in data to avoid that they have negative impact on QC.
    
    #
    # If there are particular periods, which are to be deleted pre-QC (known-to-be-bad), then do so now
    datevals,varvals=del_ranges(datevals,varvals,dt=dt,verb=verbose)
    #
    # If there are particular periods, which are to be kept back from QC (known-to-be-good), 
    # then do so now.
    datevals,varvals,keepers=keep_ranges_strip(datevals,varvals,dt=dt,verb=verbose)
    #  Perform del_ranges and keep_ranges *HERE* and re-insert stuff from keep_ranges 
    #  at end of loop rather than in simple_QC.
    #
    # TODO: Add option to disable the entire QC-part, and go straight to 
    #     various output options. Or just modify/update the tsSpline module to do it?
    nsrc=datevals.size
    nlen0=-1
    nlenN=datevals.size
    iloop=0
    while nlen0!=nlenN and nlenN>0:
        nlen0=nlenN
        ts.assert_monotone(datevals=datevals)
        ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
        #
        # TODO: Perform pre-gap-filling *HERE*


        # Simple QC - fixed limits, stuck sensor etc.
        # Beware that since this is a process over several different tests,
        # it may be necessary to repeat a few times. This is because eliminating some 
        # values may create new subseries with "stuck" values.
        datevals,varvals=simple_QC(datevals,varvals,dt=dt,verb=verbose)
        #
        # Sometimes we end up with no data left:
        if datevals.size==0:
            print("Left with empty timeseries")
            break
        ts.assert_monotone(datevals=datevals)
        ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
        #
        # Despiking by spline-fitting. This also may remove stuff, 
        # so we may want to go over the entire block simple_QC+despike
        # a few times
        if tssp.spline_order:
            datevals,varvals=tssp.despike_series(datevals,varvals,dt=dt,maxiters=3,verb=verbose) # Max iters per ip / nloops
            ts.assert_monotone(datevals=datevals)
            ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
        else:
            print("Skipping spline QC")
        #
        if datevals.size==0:
            print("Left with empty timeseries")
            break
        #
        #raise AssertionError("Kont coding")
        datevals,varvals=delete_sparse_periods(datevals,varvals,dt=dt,verb=verbose)
        ts.assert_monotone(datevals=datevals)
        ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
        # 
        nlenN=datevals.size
        iloop+=1
        #if iloop>1:
        #    print("DEBUG QUICK BREAK")
        #    break
    # Re-insert data which were kept back from QC:
    datevals,varvals=keep_ranges_reinsert(datevals,varvals,keepers,dt=dt,verb=verbose)
    #
    if datevals.size==0:
        print("No data left after quality control. Refusing to save empty file")
        # This is not *really* an error, there is just no good data.
        exit(1)
    #
    #ts.lonelies_to_nan(datevals,varvals,nlonely=4,nneigh=2,dt=dt,verb=42)
    #
    # Output:
    ntgt=datevals.size
    if verbose:
        print("Totally eliminated {} points in QC. (From {} to {} points in series.)".format(nsrc-ntgt,nsrc,ntgt))
    # Save data:
    tsnc.ncfil_save(datevals=datevals,varvals=varvals)
    return

if __name__ == '__main__':
    main()
