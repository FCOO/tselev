#!/usr/bin/python
#
"""
Extrapolate a time series (slightly), adding new points to the ends.
"""

# 
# Module history:
#  2020-01-21 bjb 0.0.1: Initial work
#  2020-01-24 bjb 0.0.2: Options to disable extrapolation in either end.
#  2020-01-29 bjb: Bugfix on noextrap{for,back}ward

version_info = (0, 0, 3)
version      = '.'.join(str(c) for c in version_info)
__version__  = version


# Much of this work is (very) similar to what goes on in tsDegap, but 
# for new we will keep the two structures separated.
#
# For now, this only extrapolates the ends of the full series, and the 
# ends must be equidistant and without NaNs.
#
# "Internal extrapolation" could be considered, but really should be done by tsDegap.
#

# This file could be used as a module, but presently, there is only a single 
# work routine, YMMV.

import numpy as np
import datetime
# Locally implemented modules
import tsSimple as ts

if __name__ == '__main__':
    import tsNetcdf as tsnc



# Possible file to store just the interpolating polynomial values 
# This may be feasible for debugging, and for trying out various methods (orders, window sizes etc):
ncpoly = None

# Global variables for this module/method, including defaults
extrap_width   = None # Length to extrapolate - in seconds
extrap_npts    = None # Alternative to width - specify length in #points to add
#extrap_pieces  = None # Extrapolate each equidistant piece or fail if non-equidistant?
#
noextrap_forward  = False # Dont disable forward (at time series end) extrapolation
noextrap_backward = False # Dont disable backward (at time series begining) extrapolation
#
# What to use for fitting (training):
poly_order   = None # Order of poly to fit with
fit_width    = None # How wide a window to use for poly fit
fit_npts     = None # Alternative to _width: Give number of points

# 
# Option on which higher-order derivative to use for smoothness estimation:
poly_minimizer = 'f2'
poly_minimizer_choices=['f2','f3','f4']
#
# 
argparse_description="Extrapolates a time series using polyfit of values at the ends of the series."


# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

# Define possible named command line options
def args_define_options(parser=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    # This module cannot be called as script (yet), but we will add the 
    # magic needed if someone opts to implement it later:
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    #
    # Optional arguments (aka "options"):
    # If we are main, then we for sure need to set polynomial options.
    needpolyopts=False
    if __name__ == '__main__':
        needpolyopts=True
    #
    extrapwidthgroup = parser.add_mutually_exclusive_group(required=needpolyopts)
    global extrap_width
    extrapwidthgroup.add_argument("--extrapwidth",dest="extrapwidth", 
                               type=ts._check_positive_float, metavar="<WIDTH>",
                               default=extrap_width,
                               help='width to extrapolate outside series.'+
                               'Width is computed as number of delta-t times number of steps to extrapolate. '+
                               'Thus, smallest sensible width is 1*dt')
    global extrap_npts
    extrapwidthgroup.add_argument("--extrapnpts",dest="extrapnpts", 
                               type=ts._check_positive_int, metavar="<N>",
                               default=extrap_npts,
                               help='number of points to extrapolate. '+
                               'Only points outside the original time seroes are. '+
                               'Thus, smallest sensible value is 1 (one) point.')
    #
    #parser.add_argument("--extrappieces", dest="extrappieces", 
    #                    default=None, action='store_true',
    #                    help='extrapolate for each equidistant piece of series in turn')
    #parser.add_argument("--noextrappieces", dest="extrappieces", 
    #                    default=None, action='store_false',
    #                    help='disable extrapolation of each equidistant piece of series')
    #
    global poly_order
    parser.add_argument("--extrappolyorder",dest="extrappolyorder", required=needpolyopts,
                        type=ts._check_positive_int, metavar="<ORDER>",
                        default=poly_order,
                        help='(max) order of polynomials to use for fitting. Lower-order polys may also be tried for properties')
    #
    widthgroup = parser.add_mutually_exclusive_group(required=needpolyopts)
    global fit_width
    widthgroup.add_argument("--extrapfitwidth",dest="extrapfitwidth",
                            type=ts._check_positive_float, metavar="<WIDTH>",
                            default=fit_width,
                            help='width of polynomial fitting window (in seconds) for extrapolation. ')
    global fit_npts
    widthgroup.add_argument("--extrapfitnpts",dest="extrapfitnpts",
                            type=ts._check_positive_int, metavar="<n>",
                            default=fit_npts,
                            help='number of points to use for polynomial fitting at time series end.')
    #
    parser.add_argument("--noextrapforward",dest="noextrapforward",
                        help="Disable forward extrapolation (at time series end)",
                        default=False,action='store_true')
    parser.add_argument("--noextrapbackward",dest="noextrapbackward",
                        help="Disable backward extrapolation (at time series begining)",
                        default=False,action='store_true')
    #
    global poly_minimizer_choices
    global poly_minimizer
    parser.add_argument("--extrappolyminimizer",dest="extrappolyminimizer", metavar="<NAME>",
                        choices=poly_minimizer_choices, #type=_checkroundunit,
                        help='select which higher-order derivative will be used to estimate '+
                        'the smoothnes of the extrapolation polynomial. '+
                        'Default is %s. '%poly_minimizer +
                        'Allowed values are %s'%', '.join(poly_minimizer_choices))
    #
    _have_defined_options=True
    # Let other used modules define their options if we are in MAIN/SCRIPT mode:
    if __name__ == '__main__':
        parser = ts.args_define_options(parser)
        parser = tsnc.args_define_options(parser)
    return parser
# Companion parser for args_define_options
def args_parse_options(parsed_args):
    global verbose 
    verbose       = parsed_args.verbose # tsnc.verbose
    # Now deal with our own options:
    #
    global extrap_width
    extrap_width = parsed_args.extrapwidth
    if verbose and not extrap_width is None:
        print(" setting extrapwidth=%s"%extrap_width)
    #
    global extrap_npts
    extrap_npts = parsed_args.extrapnpts
    if verbose and not extrap_npts is None:
        print(" setting extrapnpts=%s"%extrap_npts)
    #
    global noextrap_forward
    noextrap_forward=parsed_args.noextrapforward
    if verbose and noextrap_forward:
        print(" disable forward extrapolation")
    #
    global noextrap_backward
    noextrap_backward=parsed_args.noextrapbackward
    if verbose and noextrap_backward:
        print(" disable backward extrapolation")
    #
    global poly_order
    poly_order = parsed_args.extrappolyorder
    if verbose and not poly_order is None:
        print(" setting extrappolyorder=%s"%poly_order)
    #
    global fit_width
    fit_width = parsed_args.extrapfitwidth
    if verbose and not fit_width is None:
        print(" setting extrapfitwidth=%s"%fit_width)
    global fit_npts
    fit_npts = parsed_args.extrapfitnpts
    if verbose and not fit_npts is None:
        print(" setting extrapfitnpts=%s"%fit_npts)
    #
    global poly_minimizer
    if not parsed_args.extrappolyminimizer is None:
        poly_minimizer = parsed_args.extrappolyminimizer
    if verbose>2 or (verbose and not parsed_args.extrappolyminimizer is None):
        print(" setting extrappolyminimizer=%s"%poly_minimizer)
    #
    # Let imported modules deal with their own options:
    if __name__ == '__main__':
        ts.args_parse_options(parsed_args)
        tsnc.args_parse_options(parsed_args)
    return

# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC> <NCTGT>
def args_define_positional(parser=None):
    if __name__ != '__main__':
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    # Define two mandatory arguments here. IN OUT:
    parser = tsnc.args_define_positional_two(parser)
    return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
    if __name__ != '__main__':
        return
    tsnc.args_parse_positional(parsed_args)
    return


#
# Actually do the extrapolation. 
# Note that (eventuall) this routine could be called several times (ones for each 
# piece) to extrapolate each "equidistant piece" of a time series.
# Within this routine, each *end* of the time series must be equidistant 
# and with no nan values.
def do_extrapolate(datevals,varvals,
                   extrapwidth=None,extrapnpts=None,
                   polyorder=None,
                   fitwidth=None,fitnpts=None,
                   minimizer=None,
                   noextrapforward=None,noextrapbackward=None,
                   verb=None):
    fnam="do_extrapolate" # Just for log/debug
    verbloc=ts._verbloc(verb)
    ts.assert_consistent(datevals,varvals)
    #
    #<argument parsing>
    # Deal with optional arguments (these are more or less the same as the command line options).
    # First figure out if width is determined by functional arguments or command line options.
    # Default will be to use functional arguments.
    if (extrapwidth is None) and (extrapnpts is None):
        # No functional width arguments. Use globals / command line options:
        global extrap_width
        extrapwidth=extrap_width
        global extrap_npts
        extrapnpts = extrap_npts
    if (extrapwidth is None) and (extrapnpts is None):
        raise AssertionError("Missing width (or npts) to extrapolate. "+
                             "Either as functional argument or command line option")
    #
    if polyorder is None:
        global poly_order
        polyorder=poly_order
    if polyorder is None:
        raise AssertionError("Missing order for extrapolating polynomial. "+
                             "Either as functional argument or command line option")
    #
    if noextrapforward is None:
        global noextrap_forward
        noextrapforward=noextrap_forward
    if noextrapbackward is None:
        global noextrap_backward
        noextrapbackward=noextrap_backward
    # Local shorthands:
    extrapforward  = not noextrapforward
    extrapbackward = not noextrapbackward
    #
    # For definition of the "window width" for the interpolating polynomium there are a few
    # possibilities, but we will either base on functional arguments OR command line options. 
    # Not a mix.
    num_wargs=0
    for parg in [fitwidth, fitnpts]:
        if not parg is None:
            num_wargs += 1
    # Deal with the case of no functional arguments (of this kind).
    # In that case we rely fully on command line options:
    if num_wargs == 0:
        global fit_width
        fitwidth=fit_width
        global fit_npts
        fitnpts=fit_npts
    #
    # In either case, we can have one and only one specification of window width:
    num_wspecs=0
    for parg in [fitwidth, fitnpts]:
        if not parg is None:
            num_wspecs += 1
    if num_wspecs != 1:
        raise AssertionError("Need one and only one specification of width-parameter: width, npts")
    #
    #
    # What minimizer f^(2), f^(3), f^(4) to use
    if minimizer is None:
        global poly_minimizer
        minimizer=poly_minimizer
    if minimizer is None:
        raise AssertionError("Somehow is missing poly minimizer (even though there should be a default). Go debug.")
    #</argument parsing>
    # 
    # We will need the time step size on order to set relative to poly window widths
    dt=ts.get_timestep(datevals=datevals,verb=min(1,verbloc))
    #
    #<compute npts>
    # If we do not (yet) have npts data, then compute from "width"
    #
    #  Extrapolation "width" is npts*dt., so npts = width/dt (round down!), but add a second for unwanted rounding effects:
    if extrapnpts is None:
        extrapnpts = np.int32( np.floor((extrapwidth+1.0)/dt) )
        if verbloc>1:
            print(fnam+": Converting extrapwidth=%.1f to extrapnpts=%d (dt=%.1fs)"%(extrapwidth,extrapnpts,dt))
    #
    # Do the same for poly-window, so that we have both a window width and npts for the fit:
    # Poly "width" is (npts-1)*dt. 
    if fitnpts is None:
        fitnpts = np.int32( np.floor((fitwidth+1.0)/dt) ) + 1
        if verbloc>1:
            print(fnam+": Converting fitwidth=%.1f to fitnpts=%d (dt=%.1fs)"%(fitwidth,fitnpts,dt))
    #</compute npts>
    #
    # Sanity check:
    if varvals.shape[0] < fitnpts:
        raise AssertionError("Time series too short. Cannot use %d vals for fitting - time series length is only %d"%(fitnpts,varvals.shape[0]))
    #
    # Go over _start_ [*0] and _end_ [*N] of time series
    #  (Note that these are only windows into the original arrays [so dont modify the data!])
    datevals_train0 = datevals[0:fitnpts] # for _start_
    datevals_trainN = datevals[-fitnpts:] # for _stop_
    varvals_train0  = varvals[0:fitnpts]
    varvals_trainN  = varvals[-fitnpts:]
    #
    # For the poly-fit, we will scale the time-axis, such that the 
    # first (or last) time (x) will be zero, and increment is unity per time step,
    # ie x is t normalized by dt (and shifted to end)
    # 
    # Start/prepend extrapolation:
    xvals_train0 = np.arange(np.float64(fitnpts))          #  [0,1, ... ,npts-1]
    xvals_trainN = np.arange(np.float64(1-fitnpts),0.5)    #  [-npts+1, ... ,-1, 0]
    xvals_eval0  = np.arange(np.float64(-extrapnpts),-0.5) #  [-extrapnpts, ..., -2, -1]
    xvals_evalN  = np.arange(np.float64(1),extrapnpts+0.5) #  [1, 2, .. ,extrapnpts]
    #
    # Sanity check:
    for va in datevals_train0, datevals_trainN, varvals_train0, varvals_trainN, xvals_train0, xvals_trainN:
        if va.size != fitnpts:
            raise AssertionError("Internal error: Something is wrong with the array sizes for fitting. All should be size-%d but they are not. Go debug."%fitnpts)
    #
    # Make sure that the start and stop are equidistant sets
    if verbloc>1:
        print(fnam+": Testing end(s) of time series (%d fit pts) are equidistant without NaN values"%(fitnpts,))
    if extrapbackward:
        ts.assert_equidistant(datevals_train0,varvals_train0)
        if np.sum( np.isnan(varvals_train0) )!=0:
            raise AssertionError("Time series start contains NaN-values. Cannot extrapolate. Do deGap first?")
    if extrapforward:
        ts.assert_equidistant(datevals_trainN,varvals_trainN)
        if np.sum( np.isnan(varvals_trainN) )!=0:
            raise AssertionError("Time series stop contains NaN-values. Cannot extrapolate. Do deGap first?")
    #
    # Actually do the fit
    if verbloc:
        print(fnam+": Creating ifitting polynomial(s) - order %d spanning %d points"%(polyorder,fitnpts))
    #
    if extrapbackward:
        poly_coeffs0 = np.polyfit(xvals_train0,varvals_train0, polyorder)
        poly_model0  = np.poly1d(poly_coeffs0)
        residual0    = np.std(poly_model0(xvals_train0)-varvals_train0)
    if extrapforward:
        poly_coeffsN = np.polyfit(xvals_trainN,varvals_trainN, polyorder)
        poly_modelN  = np.poly1d(poly_coeffsN)
        residualN    = np.std(poly_modelN(xvals_trainN)-varvals_trainN)
    #
    # Compute residual (for logging only?)
    if verbloc>2:
        str0=""
        if extrapbackward:
            str0="start=%f"%residual0
        strN=""
        if extrapforward:
            strN="stop=%f"%residualN
        print(fnam+": Polynomial fitted with residual %s %s"%(str0,strN))
    #
    # Evaluate poly at extrapolation points:
    if verbloc:
        print(fnam+": Doing extrapolation (evaluating polynomials)")
    if extrapbackward:
        varvals_eval0 = poly_model0(xvals_eval0)
    if extrapforward:
        varvals_evalN = poly_modelN(xvals_evalN)
    #
    # Create datetime arrays for start and stop (this is not necessary for the extrapolation,
    # but we need it to send back an extrapolated datetime array
    if extrapbackward:
        datevals_eval0 = np.arange(datevals[0]-datetime.timedelta(seconds=extrapnpts*dt),
                                   datevals[0]-datetime.timedelta(seconds=0.5*dt), 
                                   datetime.timedelta(seconds=dt)).astype(datetime.datetime)
    else:
        datevals_eval0  = np.empty(shape=(0,),dtype=datetime.datetime)
        varvals_eval0   = np.empty(shape=(0,),dtype=np.float64)
    if extrapforward:
        datevals_evalN = np.arange(datevals[-1]+datetime.timedelta(seconds=dt),
                                   datevals[-1]+datetime.timedelta(seconds=(extrapnpts+0.5)*dt), 
                                   datetime.timedelta(seconds=dt)).astype(datetime.datetime)
    else:
        datevals_evalN  = np.empty(shape=(0,),dtype=datetime.datetime)
        varvals_evalN   = np.empty(shape=(0,),dtype=np.float64)
    #
    # Send back concatenated arrays:
    return np.concatenate((datevals_eval0,datevals,datevals_evalN),axis=0),\
           np.concatenate((varvals_eval0, varvals, varvals_evalN), axis=0)

#
# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser=args_define_options()
    parser=args_define_positional(parser)
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Read data:
    datevals,varvals,longname    = tsnc.ncfil_load(filename=tsnc.ncsrc)
    #
    # Modify data
    # Just do a single extrapolation at each end
    datevals_extrap,varvals_extrap=do_extrapolate(datevals=datevals,varvals=varvals)
    #
    # Save data:
    tsnc.ncfil_save(datevals=datevals_extrap,varvals=varvals_extrap,
                    filename=tsnc.nctgt,longname=longname)
    return

if __name__ == '__main__':
    main()
