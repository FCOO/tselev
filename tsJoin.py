#!/usr/bin/python
#
"""
Join two time series, copying data from the second into the first.
Presently, there are two modes, defined by --join_method:
 1: 'joinnans', 'nanunion': Copy all NaN-values from secondary to primary series.
     This mode is meant to explicitly invalidate some periods in the primary series, 
     based on info from the secondary series.
 2: 'fixnans', 'nanintersection': Replace NaN values in the primary series with 
    (identical-time) data from the secondary series. This may be used as if the 
    secondary series represent a "secondary gauge", ie. data which are less trustworthy 
    than the primary, but still better than no data.
 3: 'diffnans': Output time series from second series, but only those elements that are NaN-values 
    in the first series (and non-Nans in the second).
 4: 'differ': Make series of all those elements from the second series, which 
    differ from the same-time value in the first series.
"""

# 
# Module history:
#  2019-10-02 bjb 0.0.1: Initial work
#  2022-05-25 bjb 0.0.2: Work on alternative join methods 'diffnans', 'differ'

version_info = (0, 0, 2)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

#
# In principle, the content of this file could be implemented inside tsNetcdf, 
# as it requires a different set of positional arguments, it has been decided to 
# put in a separate file.

#
# This file should not be used as a module.
# Some parts have been prepared to be used as a module, but really it should not be included.
# This is just a safe-guard against that. If the scripts is ever turned into a 
# module with other methods than "main()", then remove the following block:
if __name__ != '__main__':
    raise AssertionError("tsJoin.py should not be imported - only run as script")

import numpy as np
# Locally implemented modules
import tsSimple as ts
import tsNetcdf as tsnc

# Global variables for this module/method, including defaults
join_method=None
#join_method_choices=['joinnans', 'nanunion', 'fixnans', 'nanintersection']
join_method_choices=ts.join_method_choices # ['joinnans', 'nanunion', 'fixnans', 'nanintersection']

# Defining our own parser should not really be necessary for this module.
argparse_description="Combine data values from two time series, inserting values frm the second into the first."

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

# Define possible named command line options
def args_define_options(parser=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    # This module cannot be called as script (yet), but we will add the 
    # magic needed if someone opts to implement it later:
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    #
    # Optional arguments (aka "options"):
    need_joinmethod=False
    if __name__ == '__main__':
        need_joinmethod=True
    #
    global join_method
    global join_method_choices
    parser.add_argument("--joinmethod",dest="joinmethod", required=need_joinmethod,
                        default=join_method, metavar="<METHOD>",
                        choices=join_method_choices,
                        help='name of method for joining data. '+
                        'Allowed values are: %s. Default: %s'%
                        (', '.join(join_method_choices),str(join_method)) )
    _have_defined_options=True
    # Let other used modules define their options if we are in MAIN/SCRIPT mode:
    if __name__ == '__main__':
        parser = ts.args_define_options(parser)
        parser = tsnc.args_define_options(parser)
    return parser
# Companion parser for args_define_options
def args_parse_options(parsed_args):
    global verbose 
    verbose       = parsed_args.verbose # tsnc.verbose
    # Now deal with our own options:
    global join_method
    join_method= parsed_args.joinmethod
    if verbose and not join_method is None:
        print(" setting join method=%s"%join_method)
    #
    if __name__ == '__main__':
        ts.args_parse_options(parsed_args)
        tsnc.args_parse_options(parsed_args)
    return

# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC> <NCSRC2> <NCTGT>
def args_define_positional(parser=None):
    if __name__ != '__main__':
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    # Define three mandatory arguments here. IN IN OUT:
    parser = tsnc.args_define_positional_three(parser)
    return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
    if __name__ != '__main__':
        return
    tsnc.args_parse_positional(parsed_args)
    return



# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser=args_define_options()
    parser=args_define_positional(parser)
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Get data:
    datevals,varvals,longname    = tsnc.ncfil_load(filename=tsnc.ncsrc)
    datevals2,varvals2,longname2 = tsnc.ncfil_load(filename=tsnc.ncsrc2)
    if longname != longname2:
        print("Warning: The :long_name atribute differs between the files '%s' vs '%s'"%(str(longname),str(longname2)))
    # 
    # Join the series:
    global join_method
    datevals_joined,varvals_joined=ts.join_series(datevals=datevals, varvals=varvals,
                                                  datevals2=datevals2,varvals2=varvals2,
                                                  method=join_method)
    #
    # Save data:
    tsnc.ncfil_save(datevals=datevals_joined,varvals=varvals_joined,
                    filename=tsnc.nctgt,longname=longname)
    return

if __name__ == '__main__':
    main()
