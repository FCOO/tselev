#!/usr/bin/python3
#
"""
Simple script to convert a time-series based netcdf to equidistant time axis.
This can be helpful for instance to compute statistics (or average) over 
files with slightly different time axes.
Also, the script can change the start/stop times and convert to new time units
(as long as the units of both source and target conform to standards).
"""

#
# TODO:
#  * Start with options
#  * Open src file, parse important info, maybe leave file open
#  * Figure out target time range - in source units(!)
#  * Create target file
#      - dimensions
#      - define variables
#
# -> Probably move "change units" to separate script, as that is 
#     essentially a totally different thing to do.

#
# Module history:
#  2021-11-05 bjb: Initial version. Snip methods from filtwrapper.py and nctsValidate.py

version_info = (0, 0, 1)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

import argparse

import os      # Eg check if file exists + remove file
import sys     # Just to get command line for history attribute

import netCDF4 as netCDF
import numpy   as np
import datetime

##
# HARDCODINGS: - some of these could be set by options if we want:
#
ncvarname='elev' # Should this be configurable?
nctimname='time' #
ncmissing  = np.float64(-9999) # Just a default
ncfill     = np.float64(-9999) # Just a default

# Make a companion variable, which actually contains the numpy float precision:
#   Use as eg nc2py_prec[ncprec]
nc2py_prec={ 'i4': np.int32, 
             'i8': np.int64,
             'f4': np.float32,
             'f8': np.float64 }


##
# This is to keep track of what positional arguments are defined.
# We must have "ncsrc nctgt"
_do_parse_positional=None;

argparse_description="Create netcdf file with equidistant time values"

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

verbose=0

########################################################################
#                                                                      #
#      GLOBAL VARIABLES                                                #
#                                                                      #
########################################################################
# Added to simplify flow structure and calls
#
#  File names:
ncsrc=None    # input file
nctgt=None    # output file
#
ncobjsrc =None # Input netcdf object
ncobjtgt =None # Input netcdf object
ncformat =None # Data model, eg NETCDF4 or NETCDF3_CLASSICAL
#
# Source time axis info:
ttypesrc=None # Time var dtype (int32/[int64]/floats) in src
tvalssrc=None # As read from file [:] as ndarray
ntimesrc=None # Number of time slices - input
time0src=None # First time step
timeNsrc=None # Last  time step
dt_src=None   # DT, likely the median of t_{i+1}-t_i from src

#
#  Define time axis of nctgt (target file):
time0tgt=None # First time step
timeNtgt=None # Last  time step
dt_tgt=None   # DT, ie (timeNtgt-time0tgt)/(len(t)-1)
ntimesrc=None # Number of time slices - output
tvalstgt=None # Values to write to file (ndarray)

time_units=None   # Time units defined as the target. Defaults to same as src time.units

########################################################################
#                                                                      #
#      OPTIONS AND ARGUMENTS                                           #
#                                                                      #
########################################################################
# The following block concerns:
#   * Definitions of arguments
#   * Parsing of arguments

# Define command line options.
#  Much of this was nicked from tsElev (bjb, FCOO).
def args_define_options(parser=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    #
    # If nobody defined verbose, then I'll do it:
    try:
        parser.add_argument("-v", "--verbose", help="increase output verbosity (allow multiple)",
                            action='count', default=0)
    except:
        # Dont worry. Probably somebody else defined it.
        pass
    #
    parser.add_argument("-O", "--overwrite", dest="overwrite",
                        help="overwrite output, i.e. create new file",
                        action='store_true')
    parser.add_argument("-H", "--nohistory", dest="append_history",
                        help="Do not append to history",
                        action='store_false')
    parser.add_argument("--timeunits",dest="timeunits", metavar="<TIMEUNITS>",
                        type=_checktimeunits,
                        help='set (output) time units. See netCDF4 doc for acceptable values')
    parser.add_argument("--missingvalue",dest="missingvalue",metavar="<VAL>",
                        action='store',type=np.float64,
                        help='Explicit value to use for missing_value (%s attribute) to use on output for variables, which do not already have a missing_value. Will also be used for _FillValue'%ncvarname)
    #
    parser.add_argument('--tprec','--timeprecision',dest="time_precision",metavar="<float|long|int32>",
                        action='store',choices=['float','double','f','int','int32'],
                        help='Accuracy to use for time values on output. Time values can be saved as standard 4-byte (32-bit) integers (as in unix time) with the risk of overflow (specify "int32"), or as floating-point numbers ("float", "double", "f"), which then must be 8-byte/64-bit "double precision" due to rounding issues. Default is to use the same as for the source file. Note that 8-byte (long) integers are not supported.')
    #
    parser.add_argument('--start','--t0','--timestart',dest="timestart",
                        metavar="<DATETIME>",action='store',
                        type=_checktimestamp,
                        help='set (output) start time. Give string datetime, which can be parsed (in UTC time), eg "2005-01-01" or "1970-01-01 00:00:00". Default is to use start time from source netcdf file.')
    parser.add_argument('--end','--tN','--timeend',dest="timeend",
                        metavar="<DATETIME>",action='store',
                        type=_checktimestamp,
                        help='set (output) end time. Give string datetime, which can be parsed (in UTC time). Default is to use end time from source netcdf file.')
    #
    ## DEBUG:
    #print "PARSER"
    #for item in vars(parser):
    #    print ": ",item.keys()
    _have_defined_options=True
    return parser

# Parse command line options
def args_parse_options(parsed_args):
    global verbose
    verbose=parsed_args.verbose
    if verbose:
        print(" setting verbose=%d"%verbose)
    global overwrite
    overwrite=parsed_args.overwrite
    if verbose and overwrite:
        print(" setting overwrite=ON")
    global append_history
    append_history=parsed_args.append_history
    if verbose and not append_history:
        print(" disable history append")
    global time_units
    time_units=parsed_args.timeunits
    if verbose and not time_units is None:
        print(" setting output %s.units='%s'"%(nctimname,time_units))
    global missing_val
    missing_val=parsed_args.missingvalue
    if verbose and not missing_val is None:
        print(" setting output %s.missing_value=%s"%(ncvarname,str(missing_val)))
    #
    global tvar_precision
    tvar_precision=None
    if not parsed_args.time_precision is None:
        global time_precision
        if   parsed_args.time_precision in ['float', 'double', 'f', 'f8']:
            tvar_precision='f8'
        #elif parsed_args.time_precision in ['long', 'i8', 'int8', 'int64']:
        #    tvar_precision='i8'
        elif parsed_args.time_precision in ['int', 'i4', 'int4', 'int32']:
            tvar_precision='i4'
        else:
            raise AssertionError("Unexpected value for floatprecision=%s"%str(parsed_args.float_precision))
        if verbose:
            print(" float output will be stored as netcdf type '%s'"%tvar_precision)
    #
    # timestart and timeend will be datetime objects (or None if not set)
    global timestart
    timestart=None
    if not parsed_args.timestart is None:
        timestart=parsed_args.timestart
        print(f" setting explicit timestart={timestart}")
    global timeend
    timeend=None
    if not parsed_args.timeend is None:
        timeend=parsed_args.timeend
        print(f" setting explicit timeend={timeend}")

    return


# Internal method to check if passed option (string) is acceptable as time.units.
# For convenience, we will accept underscores in option in stead of spaces.
# This is ONLY to be able to avoid spaces in options (which then need to be escaped).
# So method will replace underscores to spaces, such that timeunits on command line 
# can be specified as eg --timeunits=seconds_since_1971-01-01_00:00:00
def _checktimeunits(units):
    units = units.replace("_"," ")
    try:
        dummy = netCDF.date2num(datetime.datetime.utcnow(),units)
    except:
        raise AssertionError("ERROR: Something may be wrong with time units='%s'"%(units,))
    return units

# Internal method to check if passed option (string) is acceptable as datetime.
# For convenience, we will accept underscores in option in stead of spaces.
# This is ONLY to be able to avoid spaces in options (which then need to be escaped).
# So method will replace underscores to spaces, such that timeunits on command line 
# can be specified as eg --timestart=1971-01-01_00:00:00
def _checktimestamp(tidstr):
    tidstr = tidstr.replace("_"," ")
    strpfmt=None
    print(f"GOT {tidstr}")
    if   len(tidstr)==10:
        # eg "2005-01-01"
        strpfmt="%Y-%m-%d"
    elif len(tidstr)==16:
        # eg "2005-01-01 00:00"
        strpfmt="%Y-%m-%d %H:%M"
    elif len(tidstr)==19:
        # eg "2005-01-01 00:00:00"
        strpfmt="%Y-%m-%d %H:%M:%S"
    else:
        raise AssertionError("ERROR: Unknown time format '%s'"%(tidstr,))
    print(f"GOT {tidstr} - assume time format: {strpfmt}")
    dtim=None
    try:
        dtim = datetime.datetime.strptime(tidstr,strpfmt)
    except:
        raise AssertionError("ERROR: Something may be wrong with time='%s'"%(tidstr,))
    return dtim


# Define postional arguments as: <NCSRC> <NCTGT>
def args_define_positional_two(parser=None):
    # The argparse is really only needed for the command line interface.
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments:
    parser.add_argument('ncsrc', action="store",metavar="<SRCNC>",
                        help='input netcdf file')
    parser.add_argument('nctgt', action="store",metavar="<TGTNC>",
                        help='output netcdf file')
    global _do_parse_positional
    _do_parse_positional = args_parse_positional_two
    return parser
# Companion parser for args_define_positional_two
def args_parse_positional_two(parsed_args):
    global ncsrc
    global nctgt
    ncsrc  = parsed_args.ncsrc
    nctgt  = parsed_args.nctgt
    return

# This is a wrapper for whatever parser-routine is defined by  
#  the used args_define_positional_* 
def args_parse_positional(parsed_args):
    try:
        _do_parse_positional(parsed_args)
    except:
        print("Something is wrong with definition and parsing of positionals")
        raise AssertionError("Position arg issue - go debug")
    return


# Check if a file exists. 
# If filename is not passed, then ncsrc will be tried.
def file_is_present(filename=None,verb=None):
    verbloc=verb
    if verb is None:
        verbloc=verbose
    #
    if filename is None:
        filename=ncsrc
    if filename is None:
        raise AssertionError("Missing file name: Cannot determine if file is present")
    if os.path.isfile(filename):
        if verbloc>1:
            print(" file exists: %s"%filename)
        return True
    if verbloc:
        print(" no such file: %s"%filename)
    return False

# Carp unless file exists. 
# If filename is not passed, then ncsrc will be tried.
def assert_file_is_present(filename=None):
    if filename is None:
        filename=ncsrc
    if not file_is_present(filename,verb=0):
        raise AssertionError("No such file: %s"%filename)
    return;



######
# Actual work routines

##
# Open and test source file
def ncsrc_open(verbloc=None):
    if verbloc is None:
        verbloc=verbose
    global ncsrc
    assert_file_is_present(ncsrc)
    #
    if verbloc>1:
        print(f"Reading source file {ncsrc}")
    global ncobjsrc
    ncobjsrc=netCDF.Dataset(ncsrc,'r')
    global ncformat
    ncformat=ncobjsrc.data_model
    if verbloc>2:
        print(f"   source file is in {ncformat} format")
    #
    # We must have a time axis.
    if not 'time' in ncobjsrc.dimensions:
        raise AssertionError(f"Dimension 'time' missing in source netcdf file. File has dimensions: {ncobjsrc.dimensions.keys()}")
    if not 'time' in ncobjsrc.variables:
        raise AssertionError(f"Coordinate-variable 'time' missing in source netcdf file. File has variables: {ncobjsrc.variables.keys()}")
    # Time variable must exist (above), and be spanned by just 'time' dimension.
    tvar=ncobjsrc.variables['time']
    tvardims=tvar.dimensions
    if len(tvardims)!=1:
        raise AssertionError(f"Coordinate-variable 'time' is not spanned by just one dimension. Expected ('time',) but got: {tvardims}")
    if tvardims[0]!='time':
        raise AssertionError(f"Coordinate-variable 'time' is not spanned by 'time'. Expected ('time',) but got: {tvardims}")
    # Check that the time units attribute exists and pans out as understandable
    if not 'units' in tvar.ncattrs():
        raise AssertionError(f"Coordinate-variable 'time' is missing 'units' attribute. Existing attributes: {tvar.ncattrs()}")
    global tunits
    tunits=tvar.getncattr('units')
    _checktimeunits(tunits)
    #
    # Read the time values - start, stop, units and find (median) dt
    global ttypesrc
    ttypesrc=tvar.dtype
    if verbloc>2:
        print(f"Reading src time axis as {ttypesrc}[{len(tvar)}]")
    global tvalssrc
    global ntimesrc
    ntimesrc=len(tvar)
    #
    # If there is only one time step in the src file, then there is simply 
    # no point in continuing as we cannot reasonably compute dt etc.
    # In principle we could allow dt from cmdline, but it hardly does any good.
    #assert ntimesrc>1,"Refusing to equidistify file with only one time step. Even with two time steps it would seem like a redundant operation"
    if ntimesrc<2:
        print(f"File has only {ntimesrc} time step: {ncsrc}")
        print("Refusing to equidistify file with only one time step. Even with two time steps it would seem like a redundant operation",file=sys.stderr)
        exit(1)
    #
    tvalssrc=np.empty(shape=(ntimesrc,),dtype=ttypesrc)
    # Read into array:
    tvalssrc[:]=tvar[:]
    #
    # If time.units are specified explicitly, then convert immediately to target uints
    if not time_units is None and time_units != tunits:
        if verbloc:
           print(f" Converting read times to target units (from '{tunits}' to '{time_units}')")
        if verbloc>1:
           print(f"  from [{tvalssrc[0]} .. {tvalssrc[-1]}]  {tunits}") 
        datevalssrc=netCDF.num2date(tvalssrc,units=tunits)
        if verbloc>4:
            print(f"   tids: {datevalssrc}")
        tvalssrc=netCDF.date2num(datevalssrc,units=time_units)
        if verbloc>1:
           print(f"  to   [{tvalssrc[0]} .. {tvalssrc[-1]}]  {time_units}") 
    #
    global time0src
    global timeNsrc
    time0src=tvalssrc[0]
    timeNsrc=tvalssrc[-1]
    #
    # input time delta is simply:
    dtsrc=tvalssrc[1:]-tvalssrc[:-1]
    #
    # Numpy can find the median directly.
    # It will, however, be cast to at least float64, 
    # so we cast directly to the read type:
    global dt_src
    dt_src=np.median(dtsrc).astype(ttypesrc)
    if verbloc>2:
        print(f" input time values range [{time0src} : {timeNsrc}] (in {tunits})")
        print(f"  dt={dtsrc}. min={dtsrc.min()}  max={dtsrc.max()}")
    if verbloc:
        print(f" input dt_src={dt_src} (based on median value)")
    #
    # We could do more stuff with the srcfile here, but for now that should be enough.
    return


##
# Create target file with metadata
def nctgt_define(verbloc=None):
    if verbloc is None:
        verbloc=verbose
    global nctgt
    global overwrite
    if file_is_present(nctgt,verb=0) and not overwrite:
        raise AssertionError(f"Already exists (refusing to overwrite without -O): {nctgt}")
    #
    global ncformat
    if verbloc:
        print(f"Creating dataset {nctgt} in format {ncformat}")
    global ncobjtgt
    ncobjtgt=netCDF.Dataset(nctgt,mode='w',format=ncformat)
    #
    # Copy global attributes at once via dictionary:
    if verbloc>2:
        print(f"  Copy global attributes {ncobjsrc.__dict__}")
    ncobjtgt.setncatts(ncobjsrc.__dict__)
    # 
    # Possibly set/update history attribute.
    global append_history
    if append_history:
        if verbloc>1:
            print("Updating history attribute (UTC time)")
        nowstr=f'{datetime.datetime.utcnow().strftime("%a %b %d %H:%M:%S %Y")}'
        cmdlin=' '.join(sys.argv)
        newhist=nowstr+": "+cmdlin
        if 'history' in ncobjsrc.__dict__:
            histold=ncobjsrc.getncattr('history')
            #print(f"hist typ: {type(histold)}")
            #print(f"hist dat: {histold}")
            newhist += "\n"+histold
        # Define/update history attribute
        ncobjtgt.setncattr('history',newhist)
    #
    # Define dimensions - it should be all the same as for the source:
    for dimnam in ncobjsrc.dimensions:
        if verbloc>1:
            print(f" Defining dimension {dimnam}")
        srcdim=ncobjsrc.dimensions[dimnam]
        dimsiz=len(srcdim)
        unlim=srcdim.isunlimited()
        if unlim:
            dimsiz=None
        ncobjtgt.createDimension(dimnam, size=dimsiz)
    # Define variables. 
    # Each should be exactly the same as for source in terms of
    # dimensions, type and attributes. All must be copied/defined:
    for varnam in ncobjsrc.variables:
        vsrc=ncobjsrc.variables[varnam]
        vdims=vsrc.dimensions
        if verbloc>1:
            print(f" Defining variable {varnam}{vdims}")
        if len(vdims)>4:
            raise AssertionError(f"Hardcoding only OK for variables up to 4 dimensions")
        # Start with default value - then possibly overwrite per-variable:
        global missing_val
        vfill= missing_val # May still be None
        for attrnm in ('_FillValue', 'missing_value'):
            if attrnm in vsrc.ncattrs():
                vfill=vsrc.getncattr(attrnm)
                if verbloc>3:
                    print(f"   getting _FillValue={vfill} from src {attrnm}")
                break
        dtyp=vsrc.dtype
        global tvar_precision
        if varnam=='time' and not tvar_precision is None:
            dtyp=tvar_precision
            if verbloc>1:
                print(f"  defining time with datatype={dtyp}")
        vtgt=ncobjtgt.createVariable(varnam, datatype=dtyp, dimensions=vsrc.dimensions, fill_value=vfill)
        # Copy over attributes (except _FillValue!),
        #  see eg https://stackoverflow.com/questions/15141563/python-netcdf-making-a-copy-of-all-variables-and-attributes-but-one#49592545
        vtgt.setncatts(vsrc.__dict__)
        #
        # For time, we *MAY* have a different time unit. BEWARE!
        global time_units
        if varnam=='time' and not time_units is None:
            if verbloc>1:
                print(f"  defining time.units={time_units}")
            # They are already checked, so just write to attribute:
            vtgt.setncattr('units',time_units)
            
    # We sync the ncobj now so we can see the structure (without time slices)
    # This is a debug feature, and it should be really quick:
    ncobjtgt.sync()
    exit(83)
    return



##
# Create time axis. 
# This also "inflates" the target netcdf file to "full size".
def nctgt_settimeaxis(verbloc=None):
    if verbloc is None:
        verbloc=verbose
    #
    global ncobjtgt
    #
    # Initially, we just take the same units and start/stop as the source-file(?)
    global dt_tgt
    #
    if verbloc>1:
        print(f" Computing target time range")
    #
    # Shorthands:
    tvar=ncobjtgt.variables['time']
    tunits=tvar.getncattr('units')
    #
    # Default is to use source time start/end
    # but overwrite with command-line options
    global time0tgt
    time0tgt = time0src
    global timestart
    if not timestart is None:
        time0tgt = netCDF.date2num(timestart,tunits)
        if verbloc>1:
            print(f" timestart: {timestart} = {time0tgt} {tunits}")
    #
    global timeNtgt
    timeNtgt = timeNsrc
    global timeend
    if not timeend is None:
        timeNtgt = netCDF.date2num(timeend,tunits)
        if verbloc>1:
            print(f" timeend: {timeend} = {timeNtgt} {tunits}")
    # 
    # Time step is conserved (otherwise things would get truly complicated)
    global dt_tgt
    dt_tgt   = dt_src
    # Select netcdf datatype:
    #  Use src type
    ttyp=ttypesrc
    #  Or overwrite with option ['i4' 'f8']->[np.int32, np.float64] :
    if not tvar_precision is None:
        ttyp=nc2py_prec[tvar_precision]
    global tvalstgt
    tvalstgt=np.arange(time0tgt,timeNtgt+1,dt_tgt, dtype=ttyp)
    global ntimetgt
    ntimetgt=tvalstgt.size # Same as .shape[0]
    if verbloc>1:
        print(f" tvalstgt={tvalstgt}  (len={ntimetgt})")
    #
    # Define just the last time value.
    # This should allocate the entire structure.
    vtid=ncobjtgt.variables['time']
    if verbloc:
        print(f" Allocating target time slices [this may take a little while]")
    vtid[ntimetgt-1]=tvalstgt[-1]
    ncobjtgt.sync()
    if verbloc>1:
        print(f" Copying time axis values to file")
    vtid[:]=tvalstgt[:]
    # 
    return

def nc_close(verbloc=None):
    if verbloc is None:
        verbloc=verbose
    #
    if verbloc>1:
        print(f"Closing datasets {ncsrc} and {nctgt}")
    ncobjsrc.close()
    ncobjtgt.sync()
    ncobjtgt.close()
    if verbloc>1:
        print(f"  ... closed")


##
# Copy data from source to target file
#
# If the numer of time steps is really large, then copy one time 
# step at a time turns out to be really slow, and thus we will try 
# to copy one *variable* at a time.
def nctgt_copydata_loopovervars(verbloc=None):
    if verbloc is None:
        verbloc=verbose
    global ncobjtgt
    #
    if verbloc>1:
        print(f" Copy data from source to target: var-by-var")
    # For each source time index, find the target time index.
    if verbloc>2:
        print(f" Setup time[isrc] to time[itgt] mapping")
    global tvalssrc
    global tvalstgt
    #
    # First we need to find any time values "hanging out"
    isrc0=-1
    isrcN=-1
    itidsrc=-1
    for tidsrc in tvalssrc:
        itidsrc+=1
        if tidsrc<tvalstgt[0] or tidsrc>tvalstgt[-1]:
            continue
        if isrc0<0:
            isrc0=itidsrc
        isrcN=itidsrc
    # Now iscr0 is the first source time, which maps onto the target range,
    # and iscrN is the ditto last one.
    #
    # The actual index-to-index can be done by numpy.
    # See eg https://stackoverflow.com/questions/42373654/find-index-mapping-between-two-numpy-arrays#42373694
    idxsrc2tgt=np.searchsorted(tvalstgt,tvalssrc[isrc0:isrcN+1])
    # Now also drop the "hanging out" part on the target array
    itgt0=idxsrc2tgt[0]
    itgtN=idxsrc2tgt[-1]
    NSRCTIM=isrcN+1-isrc0
    NTGTTIM=itgtN+1-itgt0
    idxsrc2tgt=idxsrc2tgt-itgt0
    # When stepping through the idx (source-style), then we must start at/add isrc0.
    # Similar, for tgt, we just skip right to itgt0, and write from there.
    #
    for varnam in ncobjsrc.variables:
        if varnam=='time':
            # Time is already created - equidistified.
            continue
        vsrc=ncobjsrc.variables[varnam]
        vtgt=ncobjtgt.variables[varnam]
        ndims=len(vsrc.dimensions)
        if ndims==0:
            # Such a thing exists(!)
            if verbloc>1:
                print(f" copy data for constant (no-dimension) variable {varnam}")
            vtgt.assignValue(vsrc.getValue())
            continue
        #
        # If time is not in the dimensions, then we just copy without modifications.
        if not 'time' in vsrc.dimensions:
            if verbloc>1:
                print(f" copy data for no-time variable {varnam}")
            vtgt[:]=vsrc[:]
            continue
        #
        vfill=None
        for attrnm in ('_FillValue', 'missing_value'):
            if attrnm in vsrc.ncattrs():
                vfill=vsrc.getncattr(attrnm)
                break
        
        if verbloc>1:
            print(f" copy data for {ndims}-dimension variable {varnam} (fill={vfill})")
        #
        if ndims==1:
            # Set array:
            vdatasrc=np.empty(shape=(NSRCTIM,),dtype=vsrc.dtype)
            vdatatgt=np.full(shape=(NTGTTIM,),fill_value=vfill,dtype=vsrc.dtype)
            # Read data - these are contiguous:
            vdatasrc[:]=vsrc[isrc0:isrcN+1]
            # Write data - these may be non-contiguous:
            # THIS IS SLOW: >2min for ML-series
            #for ilsrc in range(NSRCTIM):
            #    idxtgt=idxsrc2tgt[ilsrc]
            #    vtgt[idxtgt]=np.asarray(vdatasrc[ilsrc])
            # The following is *MUCH* faster (several orders of magnitude)
            # writing in 1-2 seconds (after the file has been allocated).
            # Copy data to alread-vfilled target array (wo/ hanging out parts)
            for ilsrc in range(NSRCTIM):
                idxtgt=idxsrc2tgt[ilsrc]
                vdatatgt[idxtgt]=vdatasrc[ilsrc]
            # Dump to target (contiguous, even if disk is non-contiguous data).
            vtgt[itgt0:itgtN+1]=vdatatgt[:]
            continue
        #
        # NOTE: Adding more dimensions should be relatively trivial,
        #  although we have to read the extra dimension sizes per-variable
        #  to get the np.empty/full shapes right.
        #  All read, copy and write then "just" uses first index
        #  for time, and a single collective colon for the remaining 
        #  dimensions (one or more of those)
        srcshape=list(vsrc.shape) ; srcshape[0]=NSRCTIM
        tgtshape=list(vtgt.shape) ; tgtshape[0]=NTGTTIM
        vdatasrc=np.empty(shape=srcshape,dtype=vsrc.dtype)
        vdatatgt=np.full(shape=tgtshape,fill_value=vfill,dtype=vsrc.dtype)
        # Read data - these are contiguous:
        vdatasrc[:]=vsrc[isrc0:isrcN+1,:]
        # Copy data to alread-vfilled target array (wo/ hanging out parts)
        for ilsrc in range(NSRCTIM):
            idxtgt=idxsrc2tgt[ilsrc]
            vdatatgt[idxtgt,:]=vdatasrc[ilsrc,:]
        # Dump to target (contiguous, even if disk is non-contiguous data).
        vtgt[itgt0:itgtN+1,:]=vdatatgt[0:NTGTTIM,:]
    #ncobjtgt.sync()
    return


def nctgt_copydata_loopovertime(verbloc=None):
    if verbloc is None:
        verbloc=verbose
    global ncobjtgt
    #
    if verbloc>1:
        print(f" Copy data from source to target")
    #
    # For efficiency reasons the outer loop here is over time rather than variables.
    # The point is that each time slice should be compactly stored on disk,
    # so data for variables at the same time should be stored "closely".
    # Further, we will just loop over the *SOURCE* times, which are data to 
    # actually be copied, and then for each of those locate the target time slice.
    varnames2copy=[]
    varnamndims={}
    for varnam in ncobjsrc.variables:
        if varnam=='time':
            continue
        ndims=len(ncobjsrc.variables[varnam].dimensions)
        if ndims<1:
            continue
        varnames2copy.append(varnam)
        varnamndims[varnam]=ndims
    global tvalssrc
    global tvalstgt
    itidtgt=0
    itidsrc=-1
    for tidsrc in tvalssrc:
        itidsrc+=1
        if tidsrc<tvalstgt[0] or tidsrc>tvalstgt[-1]:
            # Skip time step out of target range
            if verbloc>3:
                print(f" skip data for time={tidsrc} (out of target range)")
        # Get index of this time in target array:
        while tvalstgt[itidtgt]<tidsrc:
            itidtgt+=1
        tidtgt=tvalstgt[itidtgt] # Not really used except for log/check
        if verbloc>3 and itidsrc%1000==0:
            print(f" copy data for source time[{itidsrc}]={tidsrc} to outout time[{itidtgt}]={tidtgt}")
        for varnam in varnames2copy:
            vsrc=ncobjsrc.variables[varnam]
            vtgt=ncobjtgt.variables[varnam]
            ndims=varnamndims[varnam]
            #print(f"   .. copy {varnam} [{ndims} dimensions]")
            #print(f"DEBUG: src({type(vsrc)})={vsrc}")
            #print(f"DEBUG: tgt({type(vtgt)})={vtgt}")
            #print(f"DEBUG: idx {itidsrc} -> {itidtgt}")
            #print(f"DEBUG: val src {vsrc[itidsrc]}")
            #print(f"DEBUG: val tgt {vtgt[itidtgt]}")
            #print(f" COPY {varnam} with {varnamndims[varnam]} dimension(s)")
            # I dont really know how to compact this in general:
            if   ndims==1:
                vtgt[itidtgt]=np.asarray(vsrc[itidsrc])
            elif ndims==2:
                vtgt[itidtgt,:]=np.asarray(vsrc[itidsrc,:])
            elif ndims==3:
                vtgt[itidtgt,:,:]=np.asarray(vsrc[itidsrc,:,:])
            elif ndims==4:
                vtgt[itidtgt,:,:,:]=np.asarray(vsrc[itidsrc,:,:,:])
            else:
                raise AssertionError(f"Hardcoding only OK for up to 4 dimensions")

nctgt_copydata=nctgt_copydata_loopovervars

##
# Default behaviour is to check a file - and copy it if there are two arguments:
def main():
    parser=args_define_options()
    # We need two positional arguments ncsrc nctgt:
    parser=args_define_positional_two(parser)
    global args
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Get metadata from input file:
    ncsrc_open()
    #
    # Define output dataset (this will create/scratch nctgt):
    nctgt_define()
    nctgt_settimeaxis()
    nctgt_copydata()
    # Sync and close in an orderly way:
    nc_close()
    global ncobjtgt
    
    exit(42)
    global nctgt
    #
    # This was just to check copy.
    if False and not nctgt is None:
        ncfil_copy(ncsrc,nctgt)
        assert_file_is_present(nctgt)
        assert_file_is_ncts(nctgt)
        if verbose:
            print(" Copy OK - deleting target file again")
        os.remove(nctgt)
    #
    # Test getting time series and data from file,
    # Note that ncil_load also does round etc.
    datevals,varvals,lnam=ncfil_load(filename=ncsrc)
    #
    # These just write some info:
    numvals=varvals.shape[0]
    dt=None
    if numvals == 0:
        if verbose:
            print(" WARNING: No data read (empty array).")
    elif numvals == 1:
        if verbose:
            print(" Time series consists of a single element. No dt can be computed")
    else:
        #datevals.shape[0]>1:
        dt = ts.get_timestep(datevals,roundto=False)
        ts.is_monotone(datevals)
        isequi=ts.is_equidistant(datevals)
    #
    # OUTPUT
    if not nctgt is None:
        # plus creating new file based on time series and data.
        ncfil_save(datevals,varvals, )
    elif append_file:
        # nctgt is none, but we can append to ncsrc
        ncfil_save(datevals,varvals,filename=ncsrc)
    else:
        # No netcdf output, but if we are really verbose, then get more info on the time series:
        if verbose>1:
            nmiss=ts.count_missing(varvals)
            dateval_start= datevals[0]
            dateval_stop = datevals[-1]
            timelength   = np.float64((dateval_stop-dateval_start).total_seconds())
            if dt is None:
                nmax = varvals.shape[0]
                nact = nmax
                pmiss= 0.0 # No missing, but nothing interesting either
            else:
                nmax = 1+int(np.round(timelength/dt))
                nact = numvals-nmiss
                pmiss= float(nmax-nact)/float(nmax)
            print(" Time series contains real data for %d of %d possible values (%.2f%% missing)"%(nact,nmax,100*pmiss))
            if nmiss==0 and isequi:
                print(" Time series is really equidistant (equidistant and no missing values) covering '%s' to '%s'"%(str(datevals[0]),str(datevals[-1])))
            else:
                # So, there are pieces. Find out how many:
                datevals_nonan,varvals_nonan=ts.remove_missing(datevals,varvals)
                # 
                npieces=ts.get_separate_piece(datevals=datevals_nonan,varvals=varvals_nonan)
                for ip in range(npieces):
                    # This is only to generate log/output.
                    # We dont actually need the pieces
                    ts.get_separate_piece(datevals=datevals_nonan,varvals=varvals_nonan,ipiece=ip)

if __name__ == '__main__':
    main()
