#!/usr/bin/python
#
"""
Helper module/routines for filtering tsSimple-based time series.
The present modules hould contain various methods/filters appropriate 
primarily for ocean elevation time series, and wrapped in a structure 
which make them easy to use in oceanographic applications.

Be aware that the different parameters, such as filter width, might 
have different interpretations in the various filters. Thus, the 
same width may result in very different cut-off frequencies in the 
various filters.
"""

#
# Module history:
#  2019-08-30 bjb 0.0.1: Initial version - first filter(s). Work on decent interface to use.
#  2019-09-10 bjb 0.0.2: Add debias() and detrend() methods
#  2019-09-17 bjb 0.0.3: Add option to select highpass/lowpass even when filter is the opposite.
#  2019-09-17 bjb 0.0.4: Add window-type filters: hanning/hann, hamming, bartlett, blackman, kaiser.
#  2019-09-18 bjb 0.0.5: Update calls to tsNetcdf
#  2019-09-19 bjb 0.0.6: Bug fix for detrend in windows-based filters
#  2019-10-01 bjb 0.0.7: Allow filtering of partwise-equidistant series
#  2019-10-08 bjb 0.0.8: Add piecewise polynomial + bspline fitting as (low-pass) filters
#  2019-10-11 bjb 0.0.9: Add bspline: fit a spline to the points - as a (low-pass) filter
#  2019-11-07 bjb 0.0.10: Significant updates to bspline - allow recursing and disqualification of non-fitting points
#  2019-11-08 bjb 0.0.11: B-spline improvements
#  2019-11-11 bjb 0.0.12: B-spline moved to separate file: tsSpline
#  2020-01-17 bjb 0.0.13: Bugfix for filter-window slightly longer than data series
#  2022-01-18 bjb 0.0.14: Add gap-detection: "piecewise B-spline"
#  2022-01-25 bjb 0.0.15: Small update related to interaction between tsFilter and tsSpline
version_info = (0, 0, 15)

# TODO:
#   * Add filter design using cut-off frequency (and possibly damping at that frequency) 
#     rather than explicit window length. This may be necessary for treating data where 
#     the dt is not known (dt={600,900,3600}s).
#
#
#
# Note:
#  Tidal analysis has been tried, but it may not give us a lot of extra 
#  info given the relative short time series. For now (bjb 2019-09-18) we will 
#  stick to low-pass filtering, and compare the remainder.
#

version      = '.'.join(str(c) for c in version_info)
__version__  = version

#
# TODO:
#   * Fit with a number of tidal constituents (to be used high-passed signal, or on
#       remainder after low-pass).

import numpy as np
from scipy import signal
from scipy import optimize
from scipy import interpolate
#import scipy.interpolate as interpolate

# Locally implemented modules
import tsSimple as ts
import tsSpline as tssp
if __name__ == '__main__':
   import tsNetcdf as tsnc

# Global variables for this module/method, including defaults
filt_method   = None    # Text name for method to use
#
filt_width    = None    # Width in seconds, eg 2 days=172800
filt_npts     = None    # Width in number of points, so width:=dt*(npts-1), npts=M=round(width/dt)+1
#filt_strength = None    # Strength - whatever that may be
filt_detrend  = None    # Option to detrend - normally not used - or depends on filter
filt_btype    = None    # low, high, band.
filt_beta     = None    # Necessary extra parameter for filters like Kaiser.
filt_order    = None    # Necessary extra parameter for filters like polynomial.
filt_ends     = None    # What to do with convolution ends - nan/delete or keep
filt_pieces   = None    # Filter each equidistant piece or fail if non-equidistant?
verbose=0


## Hardcodings - update for eg new filter implementations:
filter_windowed=['hanning', 'hann', 'hamming', 'bartlett', 'blackman', 'kaiser']
filter_polynoms=['polynomial', 'weighted_polynomial'] # These are really bad
filter_splined=['bspline',]
filter_choices=filter_windowed + filter_polynoms + filter_splined + ['PL33TN', 'debias', 'detrend', ]
# Each filter will typically be eg lowpass. Setting "highpass" will 
# then return the complement (ie original minus filtered)
btype_choices = ['low', 'high',] # 'band']
#
convend_choices=['keep', 'nan',]

# Optional file to write output of "original minus filtered" data:
ncrest = None



# Defining our own parser should not really be necessary for this module.
argparse_description="Filter time series with some specified method. "

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False


# Define possible named command line options
def args_define_options(parser=None):
   global _have_defined_options
   if _have_defined_options:
      return parser
   if parser == None:
      # The argparse is really only needed for the command line interface.
      import argparse
      global argparse_description
      parser = argparse.ArgumentParser(description=argparse_description)
   #
   # This module cannot be called as script (yet), but we will add the 
   # magic needed if someone opts to implement it later:
   if __name__ == '__main__':
      parser.add_argument('--version', action='version',
                          version='%(prog)s {}'.format(version),
                          help='show the version number and exit')
   #
   # Optional arguments (aka "options"):
   # TODO: Add required=True where applicable
   need_filtername=False
   if __name__ == '__main__':
      need_filtername=True
   global filt_method
   global filter_choices
   parser.add_argument("--filtername",dest="filtername", required=need_filtername,
                       default=filt_method, metavar="<FILTERNAME>",
                       choices=filter_choices, #type=str, 
                       help='name of filter to apply, no default. '+
                       'Allowed values are: %s'%', '.join(filter_choices))
   # Specify filter width with either width (seconds) or npts (#pnts).
   # These must be mutually exclusive.
   widthgroup = parser.add_mutually_exclusive_group(required=False)
   global filt_width
   widthgroup.add_argument("--filterwidth",dest="filterwidth", 
                           type=ts._check_positive_float, metavar="<FILTERWIDTH>",
                           default=filt_width,
                           help='width of filter in seconds. '+
                           'Some filters have their own defaults - other not.')
   global filt_npts
   widthgroup.add_argument("--filternpts",dest="filternpts", 
                           type=ts._check_positive_int, metavar="<NWIDTH>",
                           default=filt_npts,
                           help='width of filter in number of points (integer). '+
                           'Some filters have their own defaults - other not.')
   #
#    global filt_strength
#    parser.add_argument("--filterstrength",dest="filterstrength", type=float, 
#                        metavar="<FILTERSTRENGTH>", default=filt_strength,
#                        help='strength of filter. This may be required for some filters and not for others.')
   #
   global filt_btype
   global btype_choices
   parser.add_argument("--filterbandtype",dest="filterbtype",
                       choices=btype_choices, #type=float, 
                       metavar="<PASSTYPE>", default=None,
                       help='select highpass/lowpass options. '+
                       'Each filter has its own default, but it is possible to '+
                       'select the opposite to get the complement returned. '+
                        'Allowed values are: %s'%', '.join(btype_choices))
   #
   parser.add_argument("--detrend", dest="detrend", 
                       default=True, action='store_true',
                       help='enforce detrend in filters, which support this.')
   parser.add_argument("--nodetrend", dest="detrend", 
                       default=True, action='store_false',
                       help='disable detrend in filters, which support this.')
   #
   global filt_beta
   parser.add_argument("--filterbeta",dest="filterbeta", type=float, 
                       metavar="<BETA>", default=filt_beta,
                       help='additional filter parameter needed for eg Kaiser filter.')
   # 
   global filt_order
   parser.add_argument("--filterorder",dest="filterorder", type=ts._check_positive_int, 
                       metavar="<N>", default=filt_order,
                       help='additional filter parameter needed for eg piecewise polynomial filter.')
   #
   global filt_ends
   parser.add_argument("--filterends",dest="filterends", 
                       choices=convend_choices,
                       metavar="<keep|nan>", default=filt_ends,
                       help='what to do with time series ends for convolution-type filters (eg windows). '+
                       'Allowed values are: %s'%', '.join(convend_choices))
   #
   parser.add_argument("--filterpieces", dest="filterpieces", 
                       default=None, action='store_true',
                       help='filter each equidistant piece of series in turn')
   parser.add_argument("--nofilterpieces", dest="filterpieces", 
                       default=None, action='store_false',
                       help='disable filtering of each equidistant piece of series')
   #
   _have_defined_options=True
   # Let other used modules define their options if we are in MAIN/SCRIPT mode:
   if __name__ == '__main__':
      parser = ts.args_define_options(parser)
      parser = tsnc.args_define_options(parser)
      # Do NOT let spline module define all its options - we will use our own *filter* options for width instead!
      parser = tssp.args_define_options(parser,withwidth=False,withorder=False,withoutput=False,withdespike=True,withreinsert=False)
   return parser
# Companion parser for args_define_options
def args_parse_options(parsed_args):
   # verbose option is defined in tsnc, but we can get it if we want.
   global verbose 
   verbose       = parsed_args.verbose # tsnc.verbose
   # Now deal with our own options:
   global filt_method
   filt_method   = parsed_args.filtername
   if verbose:
      print(" setting filter method=%s"%filt_method)
   #
   if not parsed_args.filterwidth is None:
      global filt_width
      filt_width    = np.float64(parsed_args.filterwidth)
      if verbose:
         print(" setting filter width=%f s"%filt_width)
   #
   if not parsed_args.filternpts is None:
      global filt_npts
      filt_npts = parsed_args.filternpts
      if verbose:
         print(" setting filter npts=%d"%filt_npts)
#
#    if not parsed_args.filterstrength is None:
#        global filt_strength
#        filt_strength = np.float64(parsed_args.filterstrength)
#        if verbose:
#            print(" setting filter strength=%f"%filt_strength)
   global filt_detrend
   filt_detrend = parsed_args.detrend
   if verbose and not filt_detrend:
      print(" disabling detrending of data")
   #
   global filt_btype
   filt_btype=parsed_args.filterbtype
   if verbose and filt_btype:
      print(" explicitly selecting for %s-pass filtering"%filt_btype)
   #
   global filt_beta
   filt_beta=parsed_args.filterbeta
   if verbose and not filt_beta is None :
      print(" setting filter beta=%s"%str(filt_beta))
   #
   global filt_order
   filt_order=parsed_args.filterorder
   if verbose and not filt_order is None :
      print(" setting filter order=%d"%filt_order)
   #
   global filt_ends
   filt_ends=parsed_args.filterends
   if verbose and not filt_ends is None :
      print(" setting filter ends to '%s'"%filt_ends)
   #
   if __name__ == '__main__':
      ts.args_parse_options(parsed_args)
      tsnc.args_parse_options(parsed_args)
      tssp.args_parse_options(parsed_args) # Not all spline options will be read
   return

# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC> <NCTGT>
def args_define_positional(parser=None):
   if __name__ != '__main__':
      return parser
   if parser == None:
      # The argparse is really only needed for the command line interface.
      import argparse
      global argparse_description
      parser = argparse.ArgumentParser(description=argparse_description)
   # Define two mandatory arguments here. Beware difference between -O and -A,
   parser = tsnc.args_define_positional_two(parser)
   # Add a third optional (output) argument just for this module.
   # If set, then we will interpret it as file to seve "rest" of time series,
   # ie "original minus filtered".
   parser.add_argument('ncrest', action="store",metavar="<RESTNC>",
                       help='optional output netcdf file to store "original minus filtered" series',
                       nargs='?')

   return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
   if __name__ != '__main__':
      return
   tsnc.args_parse_positional(parsed_args)
   global ncrest
   ncrest = parsed_args.ncrest
   return

#
# This routine is a wrapper for the implemented filters in the present module.
# It will look at the command line arguments and determine what filter to actually use.
# It is possible to call each "expert routine" diredctly for a better control of the 
# various options.
# Note that this routine may be called several times (ones for each piece) to filter
# each "equidistant piece" of a time series.
# Within this routine, the (piece of) time series must be equidistant and with no nans.
def do_filter(datevals,varvals,verb=None):
   verbloc=ts._verbloc(verb)
   # Sanity check:
   if varvals.size<2:
      # No way we can filter this
      vvals=np.empty(shape=varvals.shape[0],dtype=varvals.dtype)
      vvals[:]=np.nan
      return datevals,vvals
   #
   # Ensure that we have a consistent time series.
   ts.assert_consistent(datevals=datevals,varvals=varvals)
   # All filters (so far) requires that the time series is
   # monotone and equidistant. 
   ts.assert_monotone(datevals=datevals,varvals=varvals)
   ts.assert_no_missing(varvals=varvals)
   #
   # Go over the possible implemented filters:
   used_passmethod=None
   global filt_method
   global filt_width
   global filt_strength
   if not filt_method:
      raise AssertionError("ERROR: No filter specified on command line. Cannot select what filter to use.")
   ###
   # The datevals array is normally returned unchanged, but in certain  cases, the filtering may 
   # change the datevals (such as when b-splines are evaluated at different points).
   # If datevals_filt is None at the end-of-routine, then we will replace with the input datevals.
   datevals_filt = None
   ########
   if filt_method == 'PL33TN':
      if not filt_beta is None:
         raise AssertionError("ERROR: Filter %s does not support beta set in command line options"%filt_method)
      ts.assert_equidistant(datevals=datevals,varvals=varvals)
      Thours=None
      if not filt_width is None:
         Thours = filt_width/3600.
      if verbloc:
         print(" Will apply %s-filter to time series"%filt_method)
      varvals_filt=pl33tn(varvals=varvals,datevals=datevals,dt=None,Thours=Thours,detrend=None)
      used_btype='low'
      ########
   elif filt_method in filter_windowed:
      if verbloc:
         print(" Will apply %s-windowing to time series"%filt_method)
      ts.assert_equidistant(datevals=datevals,varvals=varvals)
      varvals_filt=window(varvals=varvals,datevals=datevals)
      used_btype='low'
      ########
   elif filt_method in filter_polynoms:
      if verbloc:
         print(" Will apply %s-filter to time series"%filt_method)
      ts.assert_equidistant(datevals=datevals,varvals=varvals)
      varvals_filt=polynomial(varvals=varvals,datevals=datevals)
      used_btype='low'
      ########
   elif filt_method in filter_splined:
      if verbloc:
         print(" Will apply %s-filter to time series"%filt_method)
      # A spline "filter" do not necessarily need equidistant input.
      # But the input must not have "too large" separations (relative to spline knots).
      datevals_filt,varvals_filt=spline(datevals=datevals,varvals=varvals)
      used_btype='low'
      ########
   elif filt_method in ('debias', 'detrend'):
      # These are highpass filters, only removing the most lowly of the low "frequencies"
      used_btype='high'
      if not filt_strength is None:
         raise AssertionError("ERROR: Filter %s does not support strength set in command line options"%filt_method)
      if not filt_width is None:
         raise AssertionError("ERROR: Filter %s does not support width set in command line options"%filt_method)
      if filt_method == 'debias':
         varvals_filt=debias(varvals=varvals,datevals=datevals)
      if filt_method == 'detrend':
         varvals_filt=detrend(varvals=varvals,datevals=datevals)
      ########
   else:
      # Catch-all regarding un-implemented filters:
      raise AssertionError("ERROR: Unexpected filter specified on command line %s. Use --help for help"%filt_method)
   # If the passtype (highpass/lowpass) is explicitly specified, then we may 
   # have to return the complement of the original values.
   global filt_btype
   if filt_btype and filt_btype != used_btype:
      # NOTE: This requires that the datevals and datevals_filt are identical.
      if verbloc:
         print(" Filter %s is %s but command line option asked for %s. Returning complement."%(filt_method,used_passmethod,filt_btype,))
      varvals_filt = varvals - varvals_filt
   #
   # Normally, the filtering does NOT touch the datetime values, but there are exceptions
   # (eg specific cases of the b-spline stuff, where we may evaluate/interpolate at 
   # other times - like equidistant, where the input is not equidistant).
   if datevals_filt is None:
      datevals_filt = datevals
   #
   # Return the filtered values:
   return datevals_filt,varvals_filt

#
# Wrapper for do_filter (which is itself a wrapper), to filter each 
# equidistant piece of a time series in turn.
def do_filter_pieces(datevals,varvals,npieces=None,verb=None):
   verbloc=ts._verbloc(verb)
   # Remove NaNs - this may make the series non-equidistant:
   datevals_nonans,varvals_nonans=ts.remove_nans(datevals,varvals)
   if npieces is None:
      npieces=ts.get_separate_piece(datevals_nonans,varvals_nonans)
   # This is just a test/debug counter to make sure we go over all the elements:
   ngot=0
   # Placeholder for filtered varvals - each of the npieces:
   l = [None] * npieces # varvals
   d = [None] * npieces # datevals
   # Now gover each piece in turn:
   for ip in range(npieces):
      # Get the piece:
      datevals_i,varvals_i=ts.get_separate_piece(datevals_nonans,varvals_nonans,ipiece=ip,verb=verb)
      # Filter it
      datevals_i_filt,varvals_i_filt=do_filter(datevals=datevals_i,varvals=varvals_i)
      import datetime
      if not isinstance(datevals_i_filt[0] ,datetime.datetime):
         raise AssertionError(" Unexpected return type for datevals_i_filt: %s"%type(datevals_i_filt[0]))
      if not isinstance(varvals_i_filt[0] , float):
         raise AssertionError(" Unexpected return type for varvals_i_filt: %s"%type(varvals_i_filt[0]))
      # Store it - first temporarily
      l[ip]=varvals_i_filt
      d[ip]=datevals_i_filt
      # Increment counter of total processed time series elelemts:
      ngot += datevals_i.size
   # Join all the little pieces:
   #print "LL",type(l),l
   #print "DD",type(d),d
   varvals_filt  = np.concatenate(l)
   datevals_filt = np.concatenate(d)
   return datevals_filt,varvals_filt

# Remove constant non-zero component from time series.
# Just compute average, and return varvals-varvals.average(), but as a copy 
# and without changing the original (input) varvals
def debias(varvals,datevals=None,fillval=None,verb=None):
   verbloc=ts._verbloc(verb)
   if not datevals is None:
      ts.assert_consistent(datevals=datevals,varvals=varvals)
   else:
      ts.assert_varvals(varvals=varvals)
   #
   avg=ts.mean(datevals=datevals,varvals=varvals,fillval=fillval)
   # 
   # If avg is zero, then we are already don and we can return the (pointer to) array 
   # without actually making a copy.
   # The only reason to taking care of this case separately is that this method 
   # could be called recursively and it may save a little mem/speed on secondary calls:
   if avg == 0:
      return varvals
   #
   # We are going to modify the values, so make a copy of varvals to return:
   varvals_debiased = varvals.copy()
   # We need to modify only the variables which are NOT MISSING.
   notmissing = ~ts.is_missing(varvals,datevals=datevals,fillval=fillval,verb=verb)
   varvals_debiased[notmissing] -= avg
   return varvals_debiased

# Super-simple method to just remove a linear component from a time series.
# This is just slightly more than a de-BIAS.
#
# IFF the time series is monotone and equidistant, and does not have 
# any missing values, then we can use scipy.signal.detrend, which requires 
# only the varvals (it silently assumes the above things - testing only for nans).
# Otherwise we will have to be more careful, carefully making a linear regression, 
# computing the linear value at each step and subtracting the linear value
# from the original.
#
# method refers to the signal.detrend key word "type" for "linear" or "constant".
# Using "constant" is really the same as debias.
def detrend(varvals,datevals=None,method=None,fillval=None,polyfit=False,verb=None):
    verbloc=ts._verbloc(verb)
    if not datevals is None:
        ts.assert_consistent(datevals=datevals,varvals=varvals)
    else:
        ts.assert_varvals(varvals=varvals)
    detrend_choices=['constant', 'linear']
    if method is None:
        method = 'linear'
    if not method in detrend_choices:
        raise AssertionError("detrend: unexpected type=%s. Allowed values are %s"%(str(method),", ".join(detrend_choices)))
    #
    use_scipy = True
    if datevals is None:
        # In this case we must assume that the datevals are equidistant
        use_scipy = True
    elif polyfit:
        use_scipy = False
    elif not ts.is_equidistant(datevals=datevals,verb=0):
        use_scipy = False
    elif ts.count_missing(datevals=datevals,varvals=varvals,verb=0) > 0:
        use_scipy = False
    #
    if use_scipy:
        varvals_detrended=signal.detrend(varvals,type=method)
    elif method == 'constant':
        # For 'constant' we have our own method:
        varvals_detrended=debias(datevals=datevals,varvals=varvals,fillval=fillval,verb=verb)
    else:
        # In these cases we cannot rely on signal.detrend nor the simpler local debias method.
        # Present solution is to make an actual linear fit over the range/time series.
        # This involves converting datetime objects to actual seconds.
        # 
        # First get rid of all nans for the computation:
        datevals_nonans,varvals_nonans=ts.remove_nans(datevals=datevals,varvals=varvals)
        #
        # Convert to seconds:
        timevals_nonans=ts.timesince_totalseconds(datevals=datevals_nonans)
        p=np.polyfit(timevals_nonans,varvals_nonans,deg=1)
        if verbloc:
            print(" detrending with poly %s*time+%s"%(str(p[0]),str(p[1]),))
        # Make a copy of the original series (possibly including the nans) to operate
        # on and return, thus not destroying the varvals argument
        varvals_detrended=varvals.copy()
        # Convert the *original* timevals to true seconds to compute the polygon:
        timevals=ts.timesince_totalseconds(datevals=datevals)
        # Actually detrend - subtracting linear estimate
        varvals_detrended -= timevals*p[0]+p[1]
    return varvals_detrended

   

# Old-school PLN33TN method originally a Matlab function from 
# https://github.com/sea-mat/bobstuff/blob/master/pl33tn.m
#
# This version is adapted to the datevals/varvals setup of tsSimple.
# Further, to avoid problems with non-equidistant time series, 
# missing values are NOT accepted in the present version.
#
# PL33TN: pl33t with variable dt and T
# varvals_filtered=PL33TN(varvals,[datevals|dt],[T]) computes
# low-passed series varvals_filtered from varvals
# using pl33 filter, with optional interval dt (seconds)
# and filter half-amplitude period T (hrs) as input for
# non-hourly series.
#
# INPUT:  varvals=time series
#         dt=sample interval [seconds] No default.
#         datevals=time values for series, as alternative to dt.
#                    If datevals (and not dt) is given, then it will be 
#                    checked whether the time series is equidistant
#         T=filter half-amplitude period [hrs] (Default: T=33)
#
# OUTPUT: varvals_filtered=filtered series
#
# The PL33 filter is described on p. 21, Rosenfeld (1983), WHOI
# Technical Report 85-35. Filter half amplitude period = 33 hrs.,
# half power period = 38 hrs. The time series varvals is folded over
# and cosine tapered at each end to return a filtered time series
# varvals_filtered of the same length. 
# Assumes length of varvals greater than 2*T+1.
#
# The filtered values normally seem to have the same legth as the input values
# and with no missing values, nans or Nones.
#
# NOTE: Not detrending does not really work for this method.
def pl33tn(varvals,datevals=None,dt=None,Thours=None,detrend=None,verb=None):
#################################################################
# 10/30/00
#################################################################
    # Treat optional arguments:
    verbloc=ts._verbloc(verb)
    if dt is None and datevals is None:
        raise AssertionError("ERROR: pl33tn need dt or datevals to glean time step size")
    if Thours is None:
        # Try to get from argument list (we may not have it, though)
        if not filt_width is None:
            T=int(np.round(filt_width/3600.))
        else:
            T=33
    else:
        T=Thours
    #
    # Sanity checks:
    ts.assert_varvals(varvals)
    ts.assert_no_missing(varvals)
    # 
    # Make sure that we have dt - so that we can compute ratio Thours/dt
    if dt is None:
        ts.assert_datevals(datevals)
        dt = ts.get_timestep(datevals,verb=0)
    # In this routine, dt will be in hours:
    dt = dt/3600.
    #
    # Sanity check: make sure that the filtering width (Thours) is larger than the dt,
    # as in some cases user may try to specify hours for filter on the command line.
    if T<dt:
        raise AssertionError("ERROR: pl33tn cannot use filt_width<dt. Thours=%s<%s=dt [all in hours]"%(str(T),str(dt)))
    #
    # Figure out id we should detrend the series:
    if detrend is None:
        detrend=filt_detrend
    #
    # To keep some of the original code/infrastructure, let x be a copy of varvals,
    # but ensure that we use double precision arithmetic:
    if varvals.dtype == np.float64:
        x = varvals # Just an extra reference/pointer/name.
    else:
        # Make a copy - using 64bit float (double precision):
        x = varvals.astype(dtype=np.float64,casting='same_kind')
    # Make yet another copy - this is to hold the output values, and 
    # it is a real copy of the data - not just a ref:
    xf = np.copy(x) # Make a real copy useful for output.
    #
    cutoff = float(T)/float(dt)
    fq     = float(1.)/float(cutoff)
    #nw     = int(np.ceil(float(T)/float(dt)))
    # Same thing:
    nw     = int(np.floor(cutoff))
    nw2    = 2*nw
    npts   = len(x)
    #
    # generate filter weights
    j             = np.r_[1:nw+1]   # j=1:nw
    t             = np.pi*j
    den           = fq*fq*t**3 # assumes f1=f2/2
    wts           = (2.*np.sin(2.*fq*t)-np.sin(fq*t)-np.sin(3.*fq*t))/den
    # make symmetric filter weights
    array1        = np.array([2.*fq])   #Not used. To be removed?
    len1          = len(wts)
    tmp           = np.zeros((2*len1+1))
    tmp[0:len1]   = wts[::-1]
    tmp[len1]     = 2.*fq
    tmp[len1+1::] = wts
    wts2          = tmp
    wts2          = wts2/sum(wts2)# normalize to exactly one
    # fold tapered time series on each end
    cs            = np.cos(t/float(nw2))  #cs=cos(t'./nw2)
    jm            = np.r_[nw:0:-1]
    csm           = cs[ ::-1]    #jos 
    jgd           = np.where(x[:] > -9999.)[0] #range(len(x))  #bad values already filtered out
    npts          = len(jgd)
    if (npts>nw2):
        # detrend time series, then add trend back after filtering
        xdt                 = signal.detrend(x[jgd], axis=-1, type='linear', bp=0)
        trnd                = x[jgd]-xdt
        y                   = np.zeros(len(xdt)+len(jm)+len(j))
        xdt_start           = xdt[0:nw]
        y[0:nw]             = csm*xdt_start[::-1]
        y[nw:len(xdt)+nw]   = xdt
        xdt_end             = xdt[-nw:]
        y[-nw:]             = cs*xdt_end[::-1]
        #yf                  = signal.lfilter(wts2, 1., y, axis=-1, zi=None)
        yf                  = signal.lfilter(wts2, 1., y, axis=-1, zi=None)
        xf[jgd]  = yf[int(nw2):int(npts+nw2)]
        # add back trend
        xf[jgd]             = xf[jgd]+trnd
    else:
        xf[:]=np.nan
        if verbloc:
           print("pl33tn warning: Time series is too short - low-pass is all NaN")
        #raise AssertionError('ERROR: No low pass filter; time series is too short')
    #
    return xf

#
##
# Window-type filter, eg Hanning (hann), Hamming, ...
# These are all defined in numpy, and requires that the window width 
# is known relative to the sample period (frequency) dt, something like
#   M=round(width/dt)+1
# It is possible to specify the parameter M directly, thus circumventing 
# the need to compute it. Alternatively, the filter width can be given 
# (either as command line option or as argument) can be given together 
# with either the known dt or the time series (to give dt).
def window(varvals,datevals=None,filtername=None,npts=None,width=None,beta=None,dt=None,ends=None,verb=None):
    verbloc=ts._verbloc(verb)
    # Sanity checks on input:
    if not datevals is None:
        ts.assert_consistent(datevals=datevals,varvals=varvals)
        ts.assert_equidistant(datevals=datevals)
    else:
        ts.assert_varvals(varvals=varvals)
    #ts.assert_no_missing(varvals)
    #
    # Compute M unless we already have it:
    dt,npts,width= _get_filter_size(datevals=datevals,npts=npts,width=width,dt=dt,verb=verb)
    #
    if filtername is None:
        global filt_method
        filtername = filt_method
    if filtername is None:
        raise AssertionError("ERROR: Window method missing filter name - cannot compute")
    if filtername == 'kaiser':
        if beta is None:
            beta = filt_beta
        if beta is None:
            raise AssertionError("ERROR: Window method missing filter beta - cannot use %s filter"%filtername)
    #
    if ends is None:
        ends=filt_ends
    if ends is None:
        # Default for ends is to NaN them to avoid wrapping/end features
        ends='nan'
    # Dont try this with times series shorter than the window length?
    # From here on, the filter methods should be (more or less) the same:
    if   filtername == 'hanning' or filtername == 'hann':
        win=np.hanning(M=npts)
    elif filtername == 'hamming':
        win=np.hamming(M=npts)
    elif filtername == 'bartlett':
        win=np.bartlett(M=npts)
    elif filtername == 'blackman':
        win=np.blackman(M=npts)
    elif filtername == 'kaiser':
        win=np.kaiser(M=npts,beta=beta)
    else:
        raise AssertionError("ERROR: Unknown/unimplemented window name '%s'"%filtername)
    #
    N = varvals.shape[0]
    if verbloc:
        print(" %s-window is M=%d time steps"%(filt_method,npts))
        if npts>=N:
           print("  beware that window (%d time steps) is longer than data series (%d time steps)"%(npts,N))
    #
    # Possibly detrend the series before filtering.
    # For the windows-based filters, detrending should not really make *any* difference,
    # as the trend (a linear function) will be perfectly taken care of by the 
    # symmetric window coeficients.
    # It is implemented, but only used if actively enabled.
    global filt_detrend
    if filt_detrend:
        if verbloc:
            print("  applying detrend before actual windows-filtering")
        varvals2=detrend(varvals,datevals=datevals,method='linear',polyfit=True)
        trend=varvals-varvals2
    else:
        varvals2=varvals
    #
    # Apply filter.
    # The 'same' mode will keep the output length as max(M,N), 
    # where N is the length of the time series.
    varvals_filt = np.convolve(varvals2,win,mode='same')
    #
    # Normalize result:
    varvals_filt = (float(1)/np.sum(win)) * varvals_filt
    if filt_detrend:
        if verbloc:
            print("  adding trend back to filtered values")
        #varvals_filt=varvals_filt+trend
        varvals_filt[:] += trend[:]
    #
    # If the data series is shorter than the window, then the return vector
    # will be window-length. In this case, the filter is not going to give 
    if N<npts:
        # This is likely going to be (more or less) all crap.
        varvals_filt = np.asarray(varvals_filt[:N])
        if ends == 'nan':
           varvals_filt[:]=np.nan
        else:
           if verbloc:
              print("  beware that data series is shorter than window")
    elif ends == 'nan':
        # Mark the ends as "BAD" in the general case (data series longer than window)
        varvals_filt[:(npts-1)//2]=np.nan
        varvals_filt[-(npts-1)//2:]=np.nan
    #
    return varvals_filt

##
# Polynomial type filter, polynomial or weighted_polynomial.
# Note that each point is "filtered" by fitting a local polynomial just around the point. 
# The result is NOT SMOOTH, but it may give an indication of how well each (local)
# point fits the series as a whole.
# In general, these filters do not seem particularly useful. YMMV. BJB 2019-10-11.
def polynomial(varvals,datevals=None,filtername=None,order=None,dt=None,width=None,npts=None,ends=None,verb=None):
   verbloc=ts._verbloc(verb)
   # Sanity checks on input:
   if not datevals is None:
      ts.assert_consistent(datevals=datevals,varvals=varvals)
      ts.assert_equidistant(datevals=datevals)
   else:
      ts.assert_varvals(varvals=varvals)
   #
   # Compute M unless we already have it:
   dt,npts,width = _get_filter_size(datevals=datevals,npts=npts,width=width,dt=dt,verb=verb)
   #
   if filtername is None:
      global filt_method
      filtername = filt_method
   if filtername is None:
      raise AssertionError("ERROR: polynomial method missing filter name - cannot compute")
   if order is None:
      global filt_order
      order=filt_order
   if order is None:
      raise AssertionError("ERROR: polynomial method missing filter order - cannot compute")
   if ends is None:
      ends=filt_ends
   if ends is None:
      # Default for ends here is to keep them
      ends='keep'
   #
   varvals_filt=varvals.copy()
   varvals_filt[:]=np.nan
   neles=varvals.size
   #
   # This is how many points we will use before and after the central point
   # (which is the one we will estimate/filter)
   # Integer division, rounds down.
   # Note that for even values of npts, the effective filter will be npts+1 wide (including the central point)
   nside=npts/2
   #
   weights=None
   if filtername == 'weighted_polynomial':
      weights=np.zeros(shape=(2*nside+1,),dtype=float)
      weights[nside]=0.75 # The central point
      for isid in range(nside):
         ww=1./(float(isid)+2.) # 1/2, 1/3, ...
         weights[nside-1-isid] = ww
         weights[nside+1+isid] = ww
   #
   # Go over each point in the time series, and create a local interpolating polynomial.
   # The ends 0:npts etc will be treated last.
   #
   # Pick x-values covering range [-1:1] - for deccent conditioning of polyfit:
   xvals_train=(1./nside)*(np.arange(2*nside+1)-nside)
   for ic in range(nside,neles-nside):
      # Here, the central point is always at x=0, which is xvals_train[nside]
      vvals_train=varvals[ic-nside:ic+nside+1]
      # Find fitting polynomial:
      poly_coeffs = np.polyfit(xvals_train,vvals_train, order, w=weights)
      # As we - in this loop - always evaluate the poly at x=0 (the central point), 
      # the following three are equivalent/identical (we just use the constant at the end * x^0)
      #   np.polyval(poly_coeffs,xvals_train[nside])
      #   np.polyval(poly_coeffs,0.0)
      #   poly_coeffs[-1]
      varvals_filt[ic]=poly_coeffs[-1]
   #
   # If necessary, we will treat the ends as well.
   # This is more tedious than the main part of the series, but the compute time should be small.
   # We will not treat the ends, if they are specifeid to stay at NaN.
   if ends == 'keep':
      # Using poly-fit for ends. This is a slightly different process.
      # We will keep  the xvals_train values, and the vvals_train will also be constant 
      # (in each loop), but the central value (and possibly the weights) will vary.
      #
      # In principle, we only need to refit for each ic if weights are used.
      # However, for ease of implementation, we will use one single implementation
      #
      # Start-of-timeseries
      vvals_train_start=varvals[0:2*nside+1]
      vvals_train_stop =varvals[-2*nside-1:]
      weights_start=None
      weights_stop=None
      if filtername == 'weighted_polynomial':
         weights_start=np.zeros(shape=(2*nside+1,),dtype=float)
         weights_stop=np.zeros(shape=(2*nside+1,),dtype=float)
      #
      # Go over the start/stop - beginning at the point(s) at the very end(s) [ic=0]
      for ic in range(nside):
         if filtername == 'weighted_polynomial':
            for ii in range(2*nside+1):
               weights_start[ii]=1./(abs(ii-ic)+1) # 1/2, 1/3,...
            weights_start[ic]=0.75
            weights_stop[:]=weights_start[::-1] # Create a view, but copy data over.
         #
         # Find fitting polynomial at start:
         poly_coeffs = np.polyfit(xvals_train,vvals_train_start, order, w=weights)
         varvals_filt[ic]=np.polyval(poly_coeffs,xvals_train[ic])
         #
         poly_coeffs = np.polyfit(xvals_train,vvals_train_stop, order, w=weights)
         varvals_filt[-ic-1]=np.polyval(poly_coeffs,xvals_train[-ic-1])
   # All done. Return the filtered values.
   return varvals_filt


##
# Wrapper to tsSpline to use B-spline as "filter".
def spline(datevals,varvals,filtername=None,order=None,npts=None,width=None,dt=None,ends=None,
           want_derivative=False,output_datevals=None,
           verb=None):
   verbloc=ts._verbloc(verb)
   #
   # Make sure that we have the filter width:
   dt,npts,width = _get_filter_size(datevals=datevals,npts=npts,width=width,dt=dt,verb=verb)
   #
   # We do not really need the filter name in this case. We already know that it should be 'bspline'
   # so just set it to bspline unless we already have it.
   if filtername is None:
      global filt_method
      filtername = filt_method
   if filtername is None:
      filtername = 'bspline'
   global filter_splined
   if not filtername in filter_splined:
      raise AssertionError("ERROR: spline method no good for filtername='%s' - cannot compute"%filtername)
   #
   # Other stuff we need to pass to compute the spline:
   if order is None:
      global filt_order
      order=filt_order
   if order is None:
      raise AssertionError("ERROR: polynomial method missing filter order - cannot compute")
   #
   if output_datevals is None:
      output_datevals=datevals.copy()
   #
   # Let the spline evaluation do the heavy lifting
   #datevals_spline,varvals_spline=tssp.do_spline(datevals=datevals,varvals=varvals, order=order,  width=width, output_datevals=datevals.copy(), want_derivative=want_derivative,verb=verb)
   return tssp.do_spline(datevals=datevals,varvals=varvals, order=order,  width=width, dt=dt,output_datevals=output_datevals, want_derivative=want_derivative,verb=verb)


##
# Internal work method to find window width (M=npts) based on
# fct arguments or command line options.
def _get_filter_size(datevals=None,npts=None,width=None,dt=None,verb=None):
   verbloc=ts._verbloc(verb)
   #
   # First we will see if we have functional arguments to use for width:
   if (npts is None) and (width is None):
      # No functional arguments, so we rely on globals / command line options:
      global filt_npts
      npts = filt_npts
      global filt_width
      width= filt_width
   #
   # Sanity check: We need some kind of width: either "width" or "npts" (*dt)
   if (npts is None) and (width is None):
      raise AssertionError("Missing filter width (or npts) - cannot specify filter")
   #
   #  ... and dt
   dt=_get_dt(datevals=datevals,dt=dt)
   # If we do not already have dt, width and npts, then find what we need:
   if npts is None:
      #   we will have to use width to compute the filter width:
      npts = int( np.floor(width/dt) +1 )
   #
   if width is None:
      width = dt * npts
   #
   return dt,npts,width

# Unify getting dt. If we do not already have dt, then get it from datevals.
def _get_dt(datevals=None,dt=None,verb=None):
   verbloc=ts._verbloc(verb)
   if dt is None:
      if datevals is None:
         raise AssertionError("Missing datevals - cannot compute dt")
      dt = ts.get_timestep(datevals,verb=0)
   return dt



# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser=args_define_options()
    parser=args_define_positional(parser)
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Verify that the src netcdf file actually exists and has the expected properties.
    tsnc.assert_file_is_present()
    tsnc.assert_file_is_ncts()
    #
    # Read data from input file:
    # Filename will default to <NCSRC>
    datevals,varvals,longname=tsnc.ncfil_load() #(filename=args.ncsrc)
    datevals,varvals=ts.remove_missing(datevals,varvals)
    #
    # Do the filtering (whatever that may be):
    if args.filterpieces and not ts.is_equidistant(datevals=datevals,varvals=varvals,verb=0):
       # Filter each equidistant piece in turn:
       datevals_filt,varvals_filt=do_filter_pieces(datevals=datevals,varvals=varvals)
    else:
       # Just do a single filter
       datevals_filt,varvals_filt=do_filter(datevals=datevals,varvals=varvals)
    # Note that filter may result in new/more NaN values - in particular at 
    # end of (each piece of) the time series
    # The save-method options may toggle if these are kept, deleted, or if time series
    # is expanded/padded with nans to yield equidistant time values.
    tsnc.ncfil_save(datevals=datevals_filt,varvals=varvals_filt)
    #
    # Possibly write difference between filtered and on-filtered data:
    if not ncrest is None:
        if verbose:
            print("Computing and storing remainder after filtering")
        # 
        # This works ONLY in cases where the filtering does not change the datetime values(!)
        #  Sanity checks:
        if varvals.size != varvals_filt.size or datevals.size != datevals_filt.size:
           raise AssertionError("ERROR: This filter (type) has changed (number of) time step values. Cannot compute remainder")
        #
        # So the size is OK. Compute the difference, and count for how many elements that difference is NOT zero
        import datetime
        ndiffers=np.sum((datevals-datevals_filt)!=datetime.timedelta(0))
        if ndiffers != 0:
           raise AssertionError("ERROR: This filter (type) has changed step values. Cannot compute remainder.")
        #
        # Compute difference:
        varvals_remain = varvals-varvals_filt
        # Make sure we write slightly different history:
        import datetime
        import sys
        history="%s: %s"%(datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S UTC remainder after ")," ".join(sys.argv[:]),)
        # Actually save:
        tsnc.ncfil_save(datevals=datevals,varvals=varvals_remain,
                        filename=ncrest,history=history)


if __name__ == '__main__':
    main()
