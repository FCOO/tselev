#!/usr/bin/python
#
"""
Helper module/rutines for constructing a B-spline from a tsSimple-based time series.
The present module contain methods necessary to contruct the spline appropriate 
primarily for ocean elevation time series, and wrapped in a structure 
which make them easy to use in oceanographic applications.
The present module may also be used for resampling of a time series to a different 
time step (with "--splineoutputdt=<DT> --splinereinsertdata").
"""

#
# Module history:
#  2019-11-11 bjb v0.01: Move spline() method and command line options from tsFilter
#  2019-11-12 bjb v0.02: Bugfix for output_datevals in do_spline_pieces
#  2020-01-23 bjb v0.03: Add option to re-insert original data whereever the source
#                        and target time series have identical times
#  2020-01-23 bjb v0.04: Bugfix regarding --splineillfitlimit with units (not *RMS) specification
#  2022-02-09 bjb v0.05: Move all "illfit" stuff out of do_spline and into do_spline_pieces: will use new implementation
#  2022-02-28 bjb v0.06: Bugfix for reinsert-data
#  2022-03-21 bjb v0.07: Align splineoutputdt time steps to unix epoch (ie midnights)
#  2022-03-22 bjb v0.08: Move logistics of dt-alignment up to tsSimple
#  2022-03-25 bjb v0.09: Small update related to interaction between tsFilter and tsSpline
#  2022-05-11 bjb v0.10: Fixed py3 module-wide ("global") scoping issue
#  2022-06-02 bjb v0.11: Spike/noise fixing with rolling averaging
#  2022-06-14 bjb v0.12: Add a few extra time-series sanity checks
#

version_info = (0, 0, 12)
version      = '.'.join(str(c) for c in version_info)
__version__  = version


import numpy as np
import math        # For ceil()
from scipy import interpolate

import argparse
import datetime

# Locally implemented modules
import tsSimple as ts
if __name__ == '__main__':
   import tsNetcdf as tsnc


# Global variables for this module/method, including defaults
spline_order    = None    # Necessary parameter for polynomial splines
spline_width    = None    # Distance between spline knots (in seconds), eg 1h = 3600 [s]
spline_smooth   = None    # "Smoother" used in splines - as alternative to knots

spline_outputdt        = None # Select DT for output spline (evaluation points)
spline_outputroundtime = None # Only useful in combination with spline_outputdt(!)
spline_reinsertdata    = None # Reinsert original data where possible. Only useful in combination with spline_outputdt(!)

spline_despikelimitabs    = np.float64(0)     # eg 0.10 [m] - will be in the unit of varvals.
spline_despikelimitrms    = np.float64(0)     # eg 10.0
spline_despikelimit_endfact=np.float64(1.0)   # Default, typical use 0.5-1.0. 1.0 means "no special treatment of end spike limit.
spline_despikemaxendslope = np.float64(0)     # Factor over mean, eg 15.
spline_despikeminwidth    = np.float64(-1)    # Negative means "use default" any other values means "set"
spline_despikerolling     = False             # Despike also based on rolling (windowed) errors
spline_despikerolling_fact= np.float64(0.7)   # Default value for f*(err_rolling+err_local) for comparison to crit val
spline_despikerolling_nneigh=2                # Default value for number of neighbors used in rolling-type updates. At least 1.
spline_despikeend_fact    = np.float64(1)     # Option to require lower spike/error for the end points in a spline subseries

#
verbose=0

##
# A few simple routines to get the above globals.
# They are made to allow unified interface for calls using valune vs None for each.
def _get_width(width=None):
    if not width is None:
        return np.float64(width)
    global spline_width
    return spline_width
#
def _get_smooth(smooth=None):
    if not smooth is None:
        return smooth
    global spline_smooth
    return spline_smooth
#
def _get_order(order=None):
    if not order is None:
        return int(order)
    global spline_order
    return spline_order
#
def _get_outputdt(outputdt=None):
    if not outputdt is None:
        return np.float64(outputdt)
    global spline_outputdt
    return spline_outputdt
#
def _get_outputroundtime(outputroundtime=None):
    if not outputroundtime is None:
        return outputroundtime
    global spline_outputroundtime
    return spline_outputroundtime
#
def _get_reinsertdata(reinsertdata=None):
    if not reinsertdata is None:
        return bool(reinsertdata)
    global spline_reinsertdata
    return spline_reinsertdata
#
def _get_despikelimitrms(despikelimitrms=None):
    if not despikelimitrms is None:
        return np.float64(despikelimitrms)
    global spline_despikelimitrms
    return spline_despikelimitrms
def _get_despikelimitabs(despikelimitabs=None):
    if not despikelimitabs is None:
        return np.float64(despikelimitabs)
    global spline_despikelimitabs
    return spline_despikelimitabs
def _get_despikemaxdev_endfactor(despikemaxdev_endfactor=None):
    if not despikemaxdev_endfactor is None:
        return np.float64(despikemaxdev_endfactor)
    global spline_despikelimit_endfact
    return spline_despikelimit_endfact

def _get_despikerolling(despikerolling=None):
    if not despikerolling is None:
        return bool(despikerolling)
    global spline_despikerolling
    return spline_despikerolling
def _get_despikerolling_factor(despikerolling_factor=None):
    if not despikerolling_factor is None:
        return np.float64(despikerolling_factor)
    global spline_despikerolling_fact
    return spline_despikerolling_fact
def _get_despikerolling_nneighs(despikerolling_nneighs=None):
    if not despikerolling_nneighs is None:
        return int(despikerolling_nneighs)
    global spline_despikerolling_nneigh
    return spline_despikerolling_nneigh

def _get_despikemaxendslope(despikemaxendslope=None):
    if not despikemaxendslope is None:
        return np.float64(despikemaxendslope)
    global spline_despikemaxendslope
    return spline_despikemaxendslope
def _get_despikeminwidth(despikeminwidth=None):
    if not despikeminwidth is None:
        return np.float64(despikeminwidth)
    global spline_despikeminwidth
    return spline_despikeminwidth
#

# Defining our own parser should not really be necessary for this module.
argparse_description="Create spline-approximation of time series. "

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

#
# Define possible named command line options
#  We will divide the definitions into several parts related to width, output and recurse (now "despike"), so that 
# tsFilter can use just the despike-part (and use width from tsFilter, and NOT use the output).
_have_defined_width    = False
_have_defined_output   = False
_have_defined_despike  = False
_have_defined_order    = False
_have_defined_reinsert = False
#
def args_define_options(parser=None, withwidth=True, withorder=True, withoutput=True, withdespike=True, withreinsert=False, required_options=True):
   global _have_defined_options
   if _have_defined_options:
      return parser
   if parser == None:
      # The argparse is really only needed for the command line interface.
      global argparse_description
      parser = argparse.ArgumentParser(description=argparse_description)
   #
   # This module cannot be called as script (yet), but we will add the 
   # magic needed if someone opts to implement it later:
   if __name__ == '__main__':
      parser.add_argument('--version', action='version',
                          version='%(prog)s {}'.format(version),
                          help='show the version number and exit')
   #
   spltitl='Spline-fitting options'
   spldesc=\
        """Fit B-spline to input. 
        Actual spline-fitting is done by scipy.interpolate.splrep 
        called by way of tsSpline.
        Note that --timestep_large is used to break timeseries into 
        'subseries' - if not given, it will be set to 1.5 times splinewidth."""
   splgroup = parser.add_argument_group(title=spltitl,
                                        description=spldesc)
   if withorder:
      global _have_defined_order
      _have_defined_order   = True
      splgroup.add_argument("--splineorder",dest="splineorder", type=_check_spline_order, 
                            required=required_options, metavar="<N>", 
                            help='Order of piecewise polynomial spline.')
   if withwidth:
      global _have_defined_width
      _have_defined_width   = True
      # We should not have both width AND smooth:
      widthgroup = splgroup.add_mutually_exclusive_group(required=False)
      # Define spline "width" as distance between consequitive knots.
      # This is similar to filter "width" - larger width removes more high-frequency content.
      widthgroup.add_argument("--splinewidth","--splineknotwidth",dest="splinewidth", 
                              type=ts._check_positive_float, metavar="<WIDTH>",
                              help='distance between spline knots (seconds). ')
      # For B-splines we may use a smoother [0.0:inf] rather than a width.
      #  Caveat: I have not seen this actually work in practice [BJB]
      widthgroup.add_argument("--splinesmooth",dest="splinesmooth", 
                              type=ts._check_nonnegative_float, metavar="<S>",
                              help="""smooth-condition as alternative to defining knots by width. 
                                  The condition is passed through to scipi.interpolate.splrep, 
                                  but in practice it seems that the use of spline knots is far 
                                  superior (the present smoother is not recommended).""")
   #
   # Spline may be evaluated (for output) either at the same points as given on input, 
   # or on a equidistant time line [within the data range]
   if withoutput:
      global _have_defined_output
      _have_defined_output  = True
      splgroup.add_argument("--splineoutputdt", dest="splineoutputdt", type=ts._check_positive_float,
                          metavar="<DT>",
                          help="select DT for spline output. Alternative to using input-datevals for evaluation.")
      splgroup.add_argument("--splineoutputroundtime", dest="splineoutputroundtime", 
                          metavar="<TIMEUNIT>", choices=ts.roundtime_choices,
                          help="round (first) time step - to be used with splineoutputdt")
   #
   # If some (a few / several) data points fit the interpolating spline really bad, 
   # then it is a possibility to throw them away and re-create the spline. This may be 
   # done several times in a recursive manner specified by:
   if withdespike:
      global _have_defined_despike
      _have_defined_despike = True
      spiktitl='Spline-fitting de-spiking quality control (data selection for spline)'
      spikdesc=\
        """Screen observations by fitting a B-spline and removing outliers
        and end-points with de-normal slope tendencies. 
        The data selection is done in an iterative process, in a way so that 
        points are discarded and spline updated in a per-subseries manner.
        Several points per subseries may be discarded in a single iteration if 
        the points a relatively far away from each other and thus do not influence 
        the spline at each other's location."""
      #spikgroup = splgroup.add_argument_group(title=spiktitl,description=spikdesc) # This does not seem to work
      spikgroup = parser.add_argument_group(title=spiktitl,description=spikdesc)
      global spline_despikelimitabs
      spikgroup.add_argument("--despikemaxabs","--despikelimitabs",dest="splinedespikelimitabs", 
                             type=ts._check_nonnegative_float,
                             metavar="<FLOAT>", default=np.float64(0),
                             help='At each recusing throw away values deviating more than FLOAT [m] the interpolating spline.'+
                             ' Specify zero to turn OFF removing points based on ABS distance to spline, or any positive value to enable criterion.'+
                             ' Default is 0 [OFF]')
      global spline_despikelimitrms
      spikgroup.add_argument("--despikemaxrms","--despikelimitrms",dest="splinedespikelimitrms", 
                             type=ts._check_nonnegative_float,
                             metavar="<FLOAT>",default=spline_despikelimitrms,
                             help='At each recusing throw away values deviating more than FLOAT times RMS from the interpolating spline,'+
                             ' where RMS is the measure between data and interpolating spline in the data points.'+
                             ' Specify zero to turn OFF removing points based on RMS distance to spline, or any positive value to enable criterion.'+
                             ' Default is 0 [OFF]')
      global spline_despikelimit_endfact
      spikgroup.add_argument("--despikemax_endfactor",dest="splinedespikelimit_endfact", 
                             type=ts._check_nonnegative_float,
                             metavar="<FLOAT>",default=spline_despikelimit_endfact,
                             help='For sub-series (spline segment) ends use a smaller max[abs,rms] than the rest of the series.'+
                             ' A value smaller than unity will help throw away dubious points at end-of-segments,'+
                             ' which are typically next to periods with dubious/problematic data.'+
                             ' A value of 0.5-0.75 is recommended, while a value of unity effectively turns of special treatment.'+
                             ' A value close to zero will probably result in the entire time series recursively being discarded as bad.'+
                             ' Default is 1, which means no special treatment of spikes at end-of-segment [OFF]')
      #
      global spline_despikerolling
      spikgroup.add_argument("--despikerolling",dest="splinedespikerolling", 
                             default=False,action='store_true',
                             help='Enable "spike-detection" with emphasis on multiple points with high error. '+
                             'A rolling/windowed/smoothed error is computed over the near neighbors, '+
                             'and this is used in conjunction with the localized error for a measure of goodness. '+
                             'Err=f*(Eroll+Eloc). f and the number of neighbors may be set separately. '+
                             'Note that rolling averaging disregards NaN/missing values, but is limited to each split (sub-series). '+
                             'Default is OFF.')
      global spline_despikerolling_fact
      spikgroup.add_argument("--despikerolling_factor",dest="splinedespikerolling_fact", 
                             type=ts._check_nonnegative_float,
                             metavar="<FLOAT>",default=spline_despikerolling_fact,
                             help='For rolling-type spike detection, use this factor in f(Eroll+Eloc). '+
                             'Normally a value between 0.5 and 1.0 is advisible. '+
                             'Default is %.2f (but only in effect when --despikerolling is enabled)'%(spline_despikerolling_fact,))
      global spline_despikerolling_nneigh
      spikgroup.add_argument("--despikerolling_nneighs",dest="splinedespikerolling_nneigh", 
                             type=ts._check_positive_int,
                             metavar="<N>",default=spline_despikerolling_nneigh,
                             help='For rolling-type spike detection, use this many neighbors on each '+
                             'side of the point to compute windowed/smoothed error. '+
                             'Normally a value in the range of 1-4 is advisible. '+
                             'Default is %d (but only in effect when --despikerolling is enabled)'%(spline_despikerolling_nneigh,))
      #
      global spline_despikemaxendslope
      spikgroup.add_argument("--despikemaxendslopefactor",dest="splinemaxslopefactor", 
                             metavar="<FACTOR>", type=ts._check_positive_float,
                             action="store", default=spline_despikemaxendslope,
                             help="""Define maximum absolute end slope (relative to 
                             mean absolute slope) of end points of (sub)series. 
                             End points leading to higher slope will be eliminated. 
                             Default is to not elimiate end-points based on slope.""")
      global spline_despikeminwidth
      spikgroup.add_argument("--subseriesminwidth","--despikeminwidth",dest="splineminwidth", 
                             metavar="<SECONDS>", type=ts._check_nonnegative_float,
                             action="store", default=spline_despikeminwidth, 
                             help="""Define minimum length (seconds) of a (sub)series 
                             in order to select data for a spline fitting. 
                             Default is 2 times spline-width, but only active with spline-despiking.
                             Set to 0 to disable minwidth in despiking.""")
   #
   # Define options for copy-along original data to spline time line (where times coinside)
   if withreinsert:
      global _have_defined_reinsert
      _have_defined_reinsert = True
      global spline_reinsertdata
      splgroup.add_argument("--splinereinsertdata",dest="splinereinsertdata", 
                          action='store_true',default=False,
                          help='Reinsert original data into spline time series, where times coinside.'+
                          ' Typically only useful with --splineoutputdt.' )
   #
   _have_defined_options=True
   # Let other used modules define their options if we are in MAIN/SCRIPT mode:
   if __name__ == '__main__':
      parser = ts.args_define_options(parser)
      parser = tsnc.args_define_options(parser)
   return parser

##
# Companion parser for args_define_options
#  Really, this should be able to do without the with* specifications
def args_parse_options(parsed_args):
   # verbose option is defined in tsnc, but we can get it if we want.
   global verbose
   try: 
      verbose = parsed_args.verbose # tsnc.verbose
   except:
      verbose = 0
   #
   global _have_defined_order
   global _have_defined_width
   global _have_defined_output
   global _have_defined_despike
   global _have_defined_width
   global _have_defined_reinsert
   global spline_width
   global spline_order
   global spline_smooth
   global spline_outputdt
   global spline_outputroundtime
   global spline_despikelimitabs
   global spline_despikelimitrms
   global spline_despikelimit_endfact
   global spline_despikemaxendslope
   global spline_despikeminwidth
   global spline_despikerolling
   global spline_despikerolling_fact
   global spline_despikerolling_nneigh
   global spline_despikeend_fact
   global spline_despikeminwidth
   global spline_reinsertdata
   #
   if _have_defined_order:
      if not parsed_args.splineorder is None:
         spline_order = parsed_args.splineorder
         if verbose:
            print(" setting spline order=%s"%str(spline_order))
   #
   if _have_defined_width:
      if not parsed_args.splinewidth is None:
         spline_width    = np.float64(parsed_args.splinewidth)
         if verbose:
            print(" setting spline width=%f s"%spline_width)
      #
      if not parsed_args.splinesmooth is None:
         spline_smooth = parsed_args.splinesmooth
         if verbose:
            print(" setting spline smoother=%s"%str(spline_smooth))
   #
   if _have_defined_output:
      if not parsed_args.splineoutputdt is None:
         spline_outputdt = parsed_args.splineoutputdt
         if verbose:
            print(" setting spline output dt = %s"%str(spline_outputdt))
      #
      if not parsed_args.splineoutputroundtime is None:
         spline_outputroundtime = parsed_args.splineoutputroundtime
         if verbose:
            print(" will round output times to nearest '%s'"%spline_outputroundtime)
   #
   if _have_defined_despike:
      # These options all have defaults, so we can just grab them:
      spline_despikelimitabs      = parsed_args.splinedespikelimitabs
      spline_despikelimitrms      = parsed_args.splinedespikelimitrms
      spline_despikelimit_endfact = parsed_args.splinedespikelimit_endfact
      spline_despikemaxendslope   = parsed_args.splinemaxslopefactor
      spline_despikerolling       = parsed_args.splinedespikerolling
      spline_despikerolling_fact  = parsed_args.splinedespikerolling_fact
      spline_despikerolling_nneigh= parsed_args.splinedespikerolling_nneigh
      spline_despikeminwidth      = parsed_args.splineminwidth
      # Default of minwidth depends on width.
      if _have_defined_width and spline_despikeminwidth<=0:
         assert not spline_width is None ,"Despike data quality control requires --splinewidth (not splinesmooth)"
         assert spline_width>0,"--splinewidth must be positive"
         spline_despikeminwidth=2*spline_width
   if _have_defined_width and spline_width:
      if parsed_args.timestep_large is None or parsed_args.timestep_large<=0:
        dtsep=3*spline_width/2
        if verbose:
            print(" timestep_large: Defaulting value to (2/3)xknot width={}".format(dtsep))
        parsed_args.timestep_large=dtsep
   #
   if _have_defined_reinsert:
      if parsed_args.splinereinsertdata:
         spline_reinsertdata = parsed_args.splinereinsertdata
         if verbose:
            print(" will re-insert original data into spline time series (whereever times coinside)")
   #
   if __name__ == '__main__':
      ts.args_parse_options(parsed_args)
      tsnc.args_parse_options(parsed_args)
   return
#
# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC> [<NCTGT>]
def args_define_positional(parser=None):
   if __name__ != '__main__':
      return parser
   if parser == None:
      # The argparse is really only needed for the command line interface.
      import argparse
      global argparse_description
      parser = argparse.ArgumentParser(description=argparse_description)
   # Define one mandatory and one optional argument here.
   parser = tsnc.args_define_positional_oneplus(parser)
   return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
   if __name__ != '__main__':
      return
   tsnc.args_parse_positional(parsed_args)
   return

### Helper routines
#
def _check_spline_order(k):
    k=ts._check_positive_int(k)
    if k>5:
        raise argparse.ArgumentTypeError("Order must be integer in range 1-5, not {}".format(k))
    return k


##
# Fit a B-spline to the points.
# It is recommended to give width (or npts), which will be interpreted as 
# (an approximate, there may be rounding effects) distance between spline knots.
# Alternatively, a "smoothness parameter" (argument "smooth", command line option spline_smooth) 
# can be given, which will make the spline contruction in charge of positioning knots 
# in order to fulfill the requirement. Actually choosing an appropriate smoothness parameter
# is not simple. Smooth=0 corresponds to an interpolating spline, ie a function which goes 
# through all the varvals. In the present scenario, this will end in no smoothing (so it 
# happens to be appropriately named).
#
# Optionally, this routine may call itself recursively, throwing out one (or a few) 
# ill-fitted points at each call. Beware that this could take a long time.
#
# Argument description:
#  datevals,varvals: Numpy arrays with time series (dtype: datetime.datetime and float)
#  order:            Order of the piecewise polynomial spline. Acceptable values 1-5, splrep( ... , k=order , ...)
#  width             Time between spline knots, ie time-scale of time series variability.
#                    For tidal elevations, order=5, and width somewhere in the range 1800 - 7200 may be appropriate.
#                    Note that width impacts the degrees of freedom of the spline, so low width will require 
#                    a relatively high sampling frequency.
#  smooth            Smoothing condition for spline. Given as alternative to "width".
#  dt                Prescribed (or already computed) dt for time series. If given, then it may save some time.
#                    Only used for time-normalization, and only with "smooth" (not width).
#  output_datevals:  ndarray of datetime.datetime objects which should be used for evaluation, 
#                    ie the spline will be evaulated at these points. This is optional, if not given 
#                    it will default to either datevals (first arg), or (if output_dt is given) 
#                    a equidistant time series will be constructed.
#                    When spline is evaluated, output_datevals outside the "trained" spline will 
#                    get NaN for function values.
#  output_dt:        In the abscence of output_datevals, it is possible to specify a dt used for 
#                    output/evaluation. output_dt will specify the equidistant dt for datevals on 
#                    output. In this case, the first datetime element on output will be datevals[0],
#                    possibly rounded as per output_roundtime.
#                    If neither output_datevals or output_dt are given, then the input
#                    datevals (first arg) will be used also for output.
#  output_roundtime: Used with output_dt to ensure that output datevals coinside with "nice" 
#                    time-on-the-clock values. 
# TODO: THE despike* STUFF SHOULD BE MOVED TO A DIFFERENT SUB:
#  despike_maxrecurse Limit for how many times this routine may recurse. Default is 10.
#  despike_limit{rms,abs} Limits to determine if a data point (time series element) is a good 
#                    or bad fit to the spline. Badly (ill) fitted points may be eliminated in
#                    subsequent recursing. 
#                    *abs defines limit in absolute (varvals) units, likely [m], while 
#                    *rms defines relative to RMS of spline fit.
#                    Both relates to dynamically cahnging spline, which adjusts as the 
#                    worst-fitting points is eliminated.
#  want_derivative   Set to true to return time,spline,derivate rather than just time,spline
#
# See also 
#   https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splrep.html
#   https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splev.html
#
# TODO bjb 2019-11-07:
#   * Add default/command-line-option values for new arguments
#   * Remove/null more than one value at a time
#   * Give up if RMS>limit?
#
def do_spline(datevals,varvals,order=None,width=None,smooth=None,dt=None,
              output_datevals=None,output_dt=None,output_roundtime=None,
              want_derivative=False, 
              verb=None):
   verbloc=ts._verbloc(verb)
   # Sanity checks on input:
   ts.assert_consistent(datevals=datevals,varvals=varvals)
   #
# TODO: All illfit in separarate routine:
#   if illfit_maxrecurse is None:
#      global spline_illfitmaxrecurse
#      illfit_maxrecurse = spline_illfitmaxrecurse
#   if illfit_ndelete is None:
#      global spline_illfitndelete
#      illfit_ndelete = spline_illfitndelete
#   if despike_limitrms is None:
#      global spline_despikelimitrms
#      despike_limitrms = spline_despikelimitrms
#   if not isinstance( despike_ndelete, int ) or despike_ndelete<1:
#      raise AssertionError(" spline argument 'despike_ndelete' can only be set to positive integer, not '%s' (%s)"%(str(despike_ndelete),type(despike_ndelete)))
#   if not isinstance( despike_maxrecurse, int ) or despike_maxrecurse<0:
#      raise AssertionError(" spline argument 'despike_maxrecurse' can only be set to natural number, not '%s' (%s)"%(str(despike_maxrecurse),type(despike_maxrecurse)))
   #
   if (smooth is None) and (width is None):
      # Length scale (or smoother) not given as functional arguments. 
      # Maybe we can get them from command line?
      global spline_smooth
      smooth=spline_smooth
      global spline_width
      width =spline_width
   if (    smooth is None) and (    width is None):
      raise AssertionError("Spline fitting need smooth-criterion or width")
   if (not smooth is None) and (not width is None):
      raise AssertionError("Spline fitting cannot use both smooth-criterion AND width")
   #
   if verbloc:
      print(" spline() called for '%s' - '%s' (%d-element time series)"%(str(datevals[0]),str(datevals[-1]),varvals.size,))
   #
   if not smooth is None:
      specify_knots=False
      # We will use a smoother-condition, rather than specifying knots
      if verbloc>1:
         print(" Using smoother condition smooth=%s to define spline"%str(smooth))
   else:
      # Otherwise we will have to define knots. Width must then already be set.
      specify_knots=True
   #
   if order is None:
      global spline_order
      order = spline_order
   if order is None:
      raise AssertionError("ERROR: spline method missing order - cannot compute")
   #
   # Get rid of NaN-values (missing data points) on input.
   if ts.has_missing(varvals=varvals,verb=0):
      datevals_nonan,varvals_nonan=ts.remove_nans(datevals=datevals,varvals=varvals,verb=0)
      if verbloc>1 and varvals_nonan.size != varvals.size:
         print("  spline() removed %d nan-data points before fitting spline (from %d to %d)"%(varvals.size-varvals_nonan.size,varvals.size,varvals_nonan.size))


   # This needs to be done only one primary call, if we ensure to not pass NaN-values recursively.
   datevals_nonan,varvals_nonan=ts.remove_nans(datevals=datevals,varvals=varvals,verb=0)
   if verbloc>1 and varvals_nonan.size != varvals.size:
      print("  spline() removed %d nan-data points before fitting spline (from %d to %d)"%(varvals.size-varvals_nonan.size,varvals.size,varvals_nonan.size))
   #
   # For now, just test/ensure that we actually got rid of the NaNs
   ts.assert_no_missing(varvals=varvals_nonan)
   #
   # Figure out what datevals should be used for evaluation.
   # If not already given, then define HERE:
   if output_datevals is None:
      #
      # Define a set of target evaluation points to use. We need them *before* the 
      # spline "traning", as the first and last evaluation point will be needed to
      # specify the necessary valid x-range of the spline.
      # Use one of:
      #   A: Input datevals - converted to xvals. This is *exactly* xvals_train(!)
      #       Note that points with nan-vals might have been removed.
      #   B: As input evals, but with "for sure" constant dt
      #   C: Define new evaluation points based on (whatnot?) eg finer (or  coarser) resolution
      # Note: B+C are essentially the same, just with modified tstart and dt(?)
      #
      # If output_dt (and output_roundtime) are not given, then check for command line option values:
      if output_dt is None and output_roundtime is None:
         global spline_outputdt
         global spline_outputroundtime
         output_dt        = spline_outputdt
         output_roundtime = spline_outputroundtime
      #
      if output_dt is None:
         # This is case A.
         output_datevals=datevals.copy()
         if not output_roundtime is None:
            output_datevals = ts.round_dates(output_datevals)
      else:
         # This is case B+C:
         if output_dt<=0:
            raise AssertionError(" spline argument 'output_dt' must be a positive number, not '{}'".format(output_dt))
         # We want to make sure that the target time values are "nicely aligned" to the "wall clock",
         # in (normally) that all "on-the-hour" values are included.
         # The logistics of it has been moved to tsSimple:
         # Create output time series with aligned dt:
         output_datevals=ts.create_time_range(timestart=datevals_nonan[0],timestop=datevals_nonan[-1],dt=output_dt,alignto=output_dt,verb=verb)
   #
   #
   # The x-axis (time, really) will be defined starting with zero and increasing with 
   # unity per time_scale [some number of seconds]. If we have width (eg if we ourselves 
   # define the knots), then that will be used as scale. Otherwise we use dt.
   # As the series is (first try) equidistant, this means that xvals_train=np.arange(neles)
   # The scaling is to improve the conditioning of the final system. 
   # Monomials (x^n) "behave" best in the interval x=[0:1], where the value is also [0:1]
   if width is None:
      if dt is None:
         dt = ts.get_timestep(datevals,verb=0)
      time_scale = dt
   else:
      # We always end here, if we are to make the knots
      time_scale = width # dt # width
   #
   # These are the input (spline "training") data - with time scaled by the selected time_scale.
   # Training data will not be modified, so it is OK, that vvals_train might still just be 
   # a ref to the input varvals.
   datevals_train = datevals_nonan
   varvals_train  = varvals_nonan
   xvals_train    = (1.0/time_scale) * ts.timesince_totalseconds(datevals=datevals_train,since=datevals_train[0])
   vvals_train    = varvals_nonan
   #
   # Make a quick return, if there are *obviously* not enough data points:
   #   * less than 3 pnts
   #   * less than one "width" of data
   neles=varvals.shape[0]
   # TODO - CONSOLIDATE QUICK-RETURNS, but take into account possible output_datevals!
   ts_width=(datevals_train[-1]-datevals_train[0]).total_seconds()
   if varvals.size<3 or (not width is None and width>ts_width):
      # No way we can filter less than 3 points with a spline
      # And if we have less than one width of data, then it also do not work.
      if verbloc:
         print("  Too short series or too few data points (%d) to contruct meaningful B-spline"%varvals.size)
      vvals=np.empty(shape=output_datevals.shape,dtype=varvals.dtype)
      vvals.fill(np.nan)
      if want_derivative:
         dvals=np.empty(shape=output_datevals.shape,dtype=varvals.dtype)
         dvals.fill(np.nan)
         return output_datevals,vvals,dvals
      return output_datevals,vvals
   ###
   # Create weights:
   # Use uniform weights - we do NOT want an interpolating spline (through all the points) -
   # this is pretty much the standard of splrep:
   ###
   # 
   if specify_knots==False:
      # Rely on smoothing condition
      #
      # Not sure if this should be w[:]=1. or 1/n:
      w=np.ones(shape=xvals_train.shape,dtype=varvals.dtype)*(1./xvals_train.size)
      # TODO: Use w[:] = 1/N??
      #
      # In certain cases, the training set will not be enough to compute the spline.
      # In particular, the number of training points may not match the degrees of 
      # freedom in the spline (~ order + #knots). (see below for more info):
      if order+2>=xvals_train.size:
         print("  spline interval seems to have too few data points; %d points only in ['%s' to '%s']"%(vvals_train.size,str(datevals_train[0]),str(datevals_train[-1])))
      try:
         t, c, k = interpolate.splrep(xvals_train, vvals_train, w=w, k=order,s=smooth) #s=1.0, k=3)
         gotspline=True
      except:
         print("  failed to create smoothed-spline in ['%s' to '%s']"%(str(datevals_train[0]),str(datevals_train[-1])))
         gotspline=False
   else:
      # Forcing knots
      #https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splrep.html
      #https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splev.html
      #  Note that specified knots "should be interior knots as knots on the ends will be added automatically".
      # We will rely on end knots being at same spacing as interior knots:
      knot1  = xvals_train[0]  + width/time_scale # aka unit
      knotN  = xvals_train[-1] - width/time_scale # end of line - unit
      dknot  = width/time_scale  # Target normalized distance between knots
      if knotN > knot1:
         nknots = 1+int(np.ceil((knotN-knot1)/dknot))
      else:
         # In this case the total time range is less than 2x width, so we will
         # just add a single interior knot.
         # (Zero interior knots do not seem to play well with splrep):
         nknots = 1
         knot1  = 0.5*(xvals_train[0]+xvals_train[-1])
         knotN  = knot1
      knots  = np.linspace(start=knot1,stop=knotN,num=nknots)
      #
      # In certain cases, the training set will not be enough to compute the spline.
      # In particular, the number of training points may not match the degrees of 
      # freedom in the spline (~ order + #knots). There are other special cases too,
      # but for now we can issue a warning, if it seems like there are not enough 
      # training points.
      # Subsequently, we will create the spline-rep in a TRY: block, so that we can
      # catch the *actual* errors.
      if order+nknots>=xvals_train.size:
         print("  spline interval seems to have too few data points; %d points only in ['%s' to '%s']"%(vvals_train.size,str(datevals_train[0]),str(datevals_train[-1])))
      try:
         # This did not work with w=1/N, so we use  uniform unity weights.
         w=np.ones(shape=xvals_train.shape,dtype=varvals.dtype) # *(1./xvals_train.size)
         # Optional todo: go for tck, fp, ier, msg output, so we can test/print fp. 
         #  Then t,c,p=tcp (or something) [Also fix the smothed-thing above)
         #  https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splrep.html
         #  But note that RMS error is computed below anyway to be able to eliminate outliers.
         t,c,k = interpolate.splrep(x=xvals_train, y=vvals_train, w=w, xb=xvals_train[0],xe=xvals_train[-1],k=order, t=knots, task=-1 ) 
         #s=1.0, k=3) # xb=xvals_eval[0],xe=xvals_eval[-1],
         #xb=xvals_train[0],xe=xvals_train[-1]
         gotspline=True
      except:
         print("  failed to create knotted-spline in ['%s' to '%s']"%(str(datevals_train[0]),str(datevals_train[-1])))
         gotspline=False
   #
   ###
   # SPLINE EVALUATION
   if not gotspline:
      # No spline evaluation, so we will just have to give up.
      varvals_eval=np.empty(shape=output_datevals.shape,dtype=varvals.dtype)
      varvals_eval.fill(np.nan)
      if want_derivative:
         dervals_eval=np.empty(shape=output_datevals.shape,dtype=varvals.dtype)
         dervals_eval.fill(np.nan)
         return output_datevals,varvals_eval,dervals_eval
      return output_datevals,varvals_eval
   #
   # If we continue, then we do have a valid spline, and we can evaluate it at the wanted points.
   # We have a spline representation.
   #
   # Find evaluation point (output_datevals) in spline coordinates,
   # which is either the spline know width (if given), or the timestep.
   xvals_eval=(1.0/time_scale) * ts.timesince_totalseconds(datevals=output_datevals,since=datevals_train[0])
   #
   # Some points may actually be outside the "training interval". For these points want to return NaN, 
   # but in order to perform the computations, we will replace by end-of-train xvals (just for temporary computations):
   # First make a quick test of the ends, to see ifthere are *any* matches
   if output_datevals[0]<datevals_train[0] or output_datevals[-1]>datevals_train[-1]:
      # If we get here, then there are output-points outside the training interval.
      numnans=0 # Just count for output/logging - if we are verbose enough
      if output_datevals[0]<datevals_train[0]:
         xvals_eval[output_datevals<datevals_train[0] ] = xvals_train[0]
         if verbloc>1:
            numnans += np.sum(output_datevals<datevals_train[0])
      if output_datevals[-1]>datevals_train[-1]:
         xvals_eval[output_datevals>datevals_train[-1]] = xvals_train[-1]
         if verbloc>1:
            numnans += np.sum(output_datevals>datevals_train[-1])
      if verbloc>1:
         print("  will return nan-values for %d output point(s) outside training interval"%numnans)
   #
   # Now evaluate spline in eval points.
   varvals_eval    = np.empty(shape=xvals_eval.shape, dtype=varvals.dtype)
   varvals_eval[:] = interpolate.splev(xvals_eval,[t,c,k],der=0)
   if want_derivative:
      dervals_eval    = np.empty(shape=xvals_eval.shape, dtype=varvals.dtype)
      dervals_eval[:] = interpolate.splev(xvals_eval,[t,c,k],der=1)
   # If we evaluated for points, which are really outside the interval:
   if output_datevals[0]<datevals_train[0]:
      varvals_eval[output_datevals<datevals_train[0] ] = np.float64('NaN')
      if want_derivative:
         dervals_eval[output_datevals<datevals_train[0] ] = np.float64('NaN')
   if output_datevals[-1]>datevals_train[-1]:
      varvals_eval[output_datevals>datevals_train[-1]] = np.float64('NaN')
      if want_derivative:
         dervals_eval[output_datevals>datevals_train[-1]] = np.float64('NaN')
   # Sanity check:
   if ( output_datevals.size != varvals_eval.size ):
      print(" Problematic spline data about to be returned: %d datevals and %d varvals"%(datevals_eval.size,varvals_eval.size))
      raise AssertionError(" spline return size ERROR")
   if want_derivative:
      return output_datevals,varvals_eval,dervals_eval
   return output_datevals,varvals_eval

# Loop over all time-series "pieces" and do a spline-fit for each.
# NOTE: It will require a bit of work for this to use explicitly set output_datevals.
# With want_pieces: Return lists of subseries/pieces rather than catenated arrays.
def do_spline_pieces(datevals,varvals,order=None,width=None,smooth=None,dt=None,
                     output_datevals=None,output_dt=None,output_roundtime=None,dtsep=None,
                     want_derivative=False,want_pieces=False,
                     verb=None):
   verbloc=ts._verbloc(verb)
   #
   if dtsep is None:
      dtsep=ts.timestep_large
   # If timestep_large is *not* set, then just call do_spline on the entire time series:
   if dtsep is None:
      return do_spline(datevals=datevals,varvals=varvals,order=order,width=width,smooth=smooth,dt=dt,
                       output_datevals=output_datevals,output_dt=output_dt,output_roundtime=output_roundtime,
                       want_derivative=False,
                       verb=verb)
   #
   # Get dt based on entire timeseries (unless we have it already):
   dt=ts.get_timestep(datevals=datevals,wantvector=False,dt=dt,verb=verb)
   #
   # Remove NaNs - this is to find the "pieces"
   datevals_nonans,varvals_nonans=ts.remove_nans(datevals,varvals)
   # 
   idxbreaks = ts.get_separate_piece(datevals_nonans,varvals_nonans,dt=dt,dtsep=dtsep,want_breaks=True)
   npieces   = idxbreaks.size - 1
   enddates  = ts.idxbreaks2enddates(idxbreaks,datevals_nonans)
   # Create new sub-intervals
   datevalsSubs=ts.idxbreaks2subseries(idxbreaks,datevals_nonans)
   varvalsSubs =ts.idxbreaks2subseries(idxbreaks,varvals_nonans)
   #
   # This is just a test/debug counter to make sure we go over all the elements:
   ngot=0
   # Placeholder for filtered varvals - each of the npieces:
   l = [None] * npieces # varvals
   d = [None] * npieces # datevals
   if want_derivative:
      der = [None] * npieces # dervals, spline slope [derivative]
   # Now gover each piece in turn:
   for ip in range(npieces):
      # Get the piece:
      datevals_i=datevalsSubs[ip]
      varvals_i =varvalsSubs[ip]
      # If output_datevals is set, then we need to limit to this part of the given datevals
      if output_datevals is None:
         # Keeping None will just use the input datetime for output
         output_datevals_i=None
      else:
         # If given output_datevals, then we need to keep track of which datetimes we already evaluated.
         # Note that this procedure may lead to returning NaN for those evaluation point (output_datevals) 
         # which end up in the "gaps".
         # TODO/Alternative: Find those evaluation points which are inside the present subseries
         #   and evaluate just those.
         if ip == 0:
            idxdate_start=0
         else:
            idxdate_start=idxdate_stop
         if ip == npieces-1 or output_datevals[-1] <= datevals_i[-1]:
            idxdate_stop=-1
         else:
            # We need to find the last element in output_datevals, which fall in the datevals_i regime:
            for idx in range(idxdate_start+1,output_datevals.shape[0]):
               if output_datevals[idx]>datevals_i[-1]:
                  idxdate_stop=idx
                  break
         if idxdate_stop==-1:
            output_datevals_i=np.copy(output_datevals[idxdate_start:])
         else:
            output_datevals_i=np.copy(output_datevals[idxdate_start:idxdate_stop])
      # Actually make a spline fit of this part
      splout=do_spline(datevals=datevals_i,varvals=varvals_i,order=order,width=width,smooth=smooth,dt=dt,
                       output_datevals=output_datevals_i,output_dt=output_dt,output_roundtime=output_roundtime,
                       want_derivative=want_derivative,
                       verb=verb)
      datevals_i_eval=splout[0]
      varvals_i_eval=splout[1]
      if want_derivative:
         dervals_i_eval=splout[2]
      #
      # Sanity check:
      import datetime
      if not isinstance(datevals_i_eval[0] ,datetime.datetime):
         raise AssertionError(" Unexpected return type for datevals_i_eval: %s"%type(datevals_i_eval[0]))
      if not isinstance(varvals_i_eval[0] , float):
         raise AssertionError(" Unexpected return type for varvals_i_eval: %s"%type(varvals_i_eval[0]))
      # Store it - first temporarily
      d[ip]=datevals_i_eval
      l[ip]=varvals_i_eval
      if want_derivative:
         der[ip]=dervals_i_eval
      # Increment counter of total processed time series elements.
      # Note that some of thse might be nan-values (in-between pieces)
      ngot += datevals_i.size
   if want_pieces:
      # Dont catenate:
      datevals_eval = d
      varvals_eval  = l
      if want_derivative:
         dervals_eval = der
   else:
      # Join all the little pieces:
      datevals_eval = np.concatenate(d)
      varvals_eval  = np.concatenate(l)
      if want_derivative:
         dervals_eval = np.concatenate(der)
   if want_derivative:
      return datevals_eval,varvals_eval,dervals_eval
   return datevals_eval,varvals_eval


#
# Despike by spline-fitting and removing outliers based on how far the
# time-series data (elements/observations) are from the spline-function
# and possibly on very steep gradient/slope at end.
# Time-series is divided into subseries based on timestep_large
# (aka maxgapwidth), and each subseries is treated separately
# in an iterative process.
# The iterative process can be limited by setting maxiters. This may 
# be a good idea, if the despiking is performed iteratively together with 
# some other QA-process, see eg tsObsValidate.py.
# If there are changes to the end point(s) of any changes to subseries,
# then the subseries are re-evaluated. This also happens if a new "gap"
# is opening, such that the number of subseries changes.
#
# Dt is here just as an optional. If not given, then
# use global optionally set by args, or in worst case examine the 
# time series to guesstimate it.
#
# Note that the present function will NOT work with *smooth*
# (alternate to spline knots).
def despike_series(datevals,varvals,
                   order=None,width=None,dt=None,dtsep=None,
                   #output_datevals=None,output_dt=None,output_roundtime=None,
                   #
                   #splineorder=None,splinewidth=None,
                   #splinemaxgapwidth=None, splineminwidth=None,
                   despikemaxrmsdev=None,despikemaxabsdev=None,despikemaxdev_endfactor=None,
                   despikemaxendslope=None,despikeminwidth=None,
                   despikerolling=None,despikerolling_factor=None,despikerolling_nneighs=None,
                   maxiters=None,
                   verb=None):
   #
   # Defaults/globals:
   verbloc=ts._verbloc(verb)
   #
   #
   # Get dt based on entire timeseries (unless we have it already):
   dt=ts.get_timestep(datevals=datevals,wantvector=False,dt=dt,verb=verb)
   # Similar for "large step" (separation between sub-series)
   if dtsep is None:
      dtsep=ts.timestep_large
   #
   # Ensure that other stuff is set (as much as possible)
   order    = _get_order(order=order)
   width    = _get_width(width=width)
   #smooth   = _get_smooth(smooth=smooth)        # We cannot use smooth here, must require width
   #output_dt= _get_outputdt(outputdt=output_dt) # No special output, we will return data not spline
   despikemaxrmsdev       = _get_despikelimitrms(despikelimitrms=despikemaxrmsdev)
   despikemaxabsdev       = _get_despikelimitabs(despikelimitabs=despikemaxabsdev)
   despikemaxdev_endfactor= _get_despikemaxdev_endfactor(despikemaxdev_endfactor=despikemaxdev_endfactor)
   despikemaxendslope     = _get_despikemaxendslope(despikemaxendslope=despikemaxendslope)
   despikeminwidth        = _get_despikeminwidth(despikeminwidth=despikeminwidth)
   despikerolling         = _get_despikerolling(despikerolling=despikerolling)
   despikerolling_factor  = _get_despikerolling_factor(despikerolling_factor=despikerolling_factor)
   despikerolling_nneighs = _get_despikerolling_nneighs(despikerolling_nneighs=despikerolling_nneighs)
   #
   # Check that spline order and knot width are set
   if order==0:
      if verbloc:
         print('Spline-based despiking de-selected (order={})'.format(order))
      return datevals,varvals
   assert not width is None and width>0,"Spline-based despiking requires knot width (option --splinewidth or argument width)"
   #
   # Actual work start (order>0)
   if verbloc:
      print("Starting spline-fitting despike process")
   #
   # Eliminate bad-/ill-fitting data points one or a few at a time.
   # This is a little like do_spline with illfit-recursing.
   #
   # BEWARE: This is *at least* an O(N^2) process, so for long time series,  
   #  it will take long^2 time. (Some components may actually be O(N^3), and kick in for N>1000).
   # TODO: For very long series, we might subdivide [with overlap] and then 
   #   need to "stich together" afterwards. If we go that way, then start 
   #   by "natural subdivision" by gaps, and then look at each subseries to 
   #   see if it is "very long" or not.
   dt=ts.get_timestep(datevals=datevals,wantvector=False,dt=dt,verb=verb)
   #
   # Sanity check on time series:
   ts.assert_monotone(datevals=datevals)
   ts.assert_piecewise_equidistant(datevals=datevals,dt=dt)
   # 
   # This array is to check if the sub-series structure of the data has changed from
   # previous loop iteration. If it has not, then we *may* be able to reuse at least 
   # parts of the previous-loop spline fitting.
   # All subseries where data was modified (esentially, thown out) will be recomputed 
   # in either case.
   # Initialize to something, which cannot match a real "breaks" array.
   # (Breaks are the start-indices of each subinterval, ending with nlen, so 0, ... , nlen)
   idxbreaks       = np.zeros(shape=(2,),dtype=np.int64)
   idxbreaks[1]    = datevals.size
   datevalsSubs=[datevals,]
   varvalsSubs =[varvals,]
   #
   # To see how/if subseries needs full update, lets keep track of the datetime start and stop
   # for each subseries.
   # If *ANY* end points change, then we will trigger a full update of all subseries.
   # Lets just start with a single
   enddates=ts.idxbreaks2enddates(idxbreaks,datevals)
   # This array is to flag whether a particular subseries has been changed 
   # during the latest loop iteration, and thus will need an update.
   # If a subseries has been changed by one despiking-operation, then
   # subsequent operations will skip that particular subseries. 
   # But the subseries will be flagged for a spline-update in the 
   # following loop iteration.
   subseries_changed = np.ones(shape=(1,),dtype=np.bool_)
   # 
   # This flag will signal to recompute for all subseries, first catenating everything, and resetting
   reset_subseries=True
   #
   # Simple loop counter for logging,
   iloop=0
   while True:
      # Increment loop counter. This is solely for logging. - implement maxloop also.
      iloop += 1
      # Test for maxloop
      if not maxiters is None and iloop>maxiters:
         # If we get here, then we should not take any more iterations at the moment
         print(" Maximum iterations ({}) met.".format(maxiters))
         break
      #
      idxbreaks_prev=idxbreaks.copy()
      enddates_prev=enddates.copy()
      #   # Update datevals/varvals by catenation of existing subseries:
      # Update remaining obs time,value removing any NaNs created in the previous loop iteration:
      datevals=np.concatenate(datevalsSubs)
      varvals =np.concatenate(varvalsSubs)
      datevals,varvals=ts.remove_nans(datevals,varvals)
      #
      # Get the start of each piece for later use. dtsep must match do_spline_pieces below
      idxbreaks=ts.get_separate_piece(datevals=datevals,varvals=varvals,dt=dt,dtsep=dtsep,want_breaks=True)
      enddates =ts.idxbreaks2enddates(idxbreaks,datevals)
      npieces  =idxbreaks.size - 1
      pieces_unchanged=ts._ndarrays_identical(enddates,enddates_prev)
      #
      if reset_subseries or not pieces_unchanged:
         # Create new sub-intervals
         datevalsSubs=ts.idxbreaks2subseries(idxbreaks,datevals)
         varvalsSubs =ts.idxbreaks2subseries(idxbreaks,varvals)
         #
         # Create subseries-lists for spline-fitting output:
         datevalsFiltSubs=[None]*npieces
         varvalsFiltSubs =[None]*npieces
         dervalsFiltSubs =[None]*npieces
         #
         # Mark all subseries for re-splining:
         subseries_changed=np.ones(shape=(npieces,),dtype=np.bool_)
         # Reset reset, so we dont go here every loop iteration
         reset_subseries=False
      #
      print("De-spike loop no %d. Working on %d time series pieces"%(iloop,npieces))
      #
      # Check if some pieces (subseries) are simply too short.
      if despikeminwidth>0:
         ndel_short=0
         for ip in range(npieces):
            i0=idxbreaks[ip]     # First element in subseries
            iN=idxbreaks[ip+1]-1  # Last element in subseries
            sublensecs=(datevalsSubs[ip][-1]-datevalsSubs[ip][0]).total_seconds()
            #
            # BUGFIX BJB 2022-06-10 particular case found negative range!
            assert sublensecs>=0,"Negative sub-series len in idxbreaks={}".format(idxbreaks)
            #
            if sublensecs<despikeminwidth:
               print(" Eliminating elev idx [{}] to [{}], time '{}' to '{}' [subseries too short {} < {}]"\
                    .format(i0,iN,datevalsSubs[ip][0].strftime("%Y-%m-%d %H:%M"),datevalsSubs[ip][-1].strftime("%Y-%m-%d %H:%M"),sublensecs,despikeminwidth))
               # 
               # Update counter:
               ndel_short+=varvalsSubs[ip].size
               # Set-to-nan to mark the subseries for deletion
               #varvalsSubs[ip][:]=np.float64('NaN')
               # In principle, we could simply toast the entire array, as long as we mark a full reset
               varvalsSubs[ip]=np.empty(shape=(0,),dtype=np.float64)
               datevalsSubs[ip]=np.empty(shape=(0,),dtype=datetime.datetime)
               reset_subseries=True
         #
         # If we deleted anything, then restart the loop, so we can recount the sub-series.
         if ndel_short>0:
            print("Eliminated {} points due to too short subs-series".format(ndel_short))
            # This definitely changed to subseries and breaks, so we will need a full recompute
            continue
      #
      # Make a piecewise spline representation. Divide in separate splines on wide (maxgapwidth) gaps.
      # No recursing - we will do it in the present loop to keep control.
      for ip in range(npieces):
         if subseries_changed[ip]:
            print(" .. updating spline for ip={}".format(ip+1))
            datevalsFiltSubs[ip],varvalsFiltSubs[ip],dervalsFiltSubs[ip] =\
                  do_spline(datevals=datevalsSubs[ip],varvals=varvalsSubs[ip],
                            order=order,width=width,smooth=None,
                            dt=dt,
                            want_derivative=True, verb=0)
            # We will only want abs-val for derivatives,
            # we change it here to avoid doing it multiple times later.
            dervalsFiltSubs[ip]=np.abs(dervalsFiltSubs[ip])
      # 
      # When we get here, then set all sub-series (pieces) to "not changed"
      # (they are cleanly created), so we can follow-up on status in next 
      # loop iteration
      subseries_changed=np.zeros(shape=(npieces,),dtype=np.bool_)
      #
      # If a piece is very small, then creating a spline will simply fail 
      # (for that piece), and the varvalsFilt will then be nan.
      # If so, then remove that piece and continue. '
      # In principle, we could still process the other pieces, but beware that 
      # computing statistics would fail if there are still nans in the series
      num_removed=0
      for ip in range(npieces):
         # When spline fails, then presumably all returned values are nan 
         # (not just a few?)
         # So presumably we could just test the first.
         # The present implementation is more advanced and explicitly only throws 
         # out the "bad eggs".
         idxnans=np.flatnonzero(np.isnan(varvalsFiltSubs[ip])).tolist()
         if len(idxnans)>0:
            for imax in idxnans:
               print(" Eliminating elev[%d]=elev[%s]=%.3f [bspline failed]"%(imax,datevalsSubs[ip][imax].strftime("%Y-%m-%d %H:%M"),varvalsSubs[ip][imax]))
            # When it happens, it is presumably the entire subseries, which fail.
            #print(" Eliminating subseries '%s' to '%s' [bspline failed]"%(inan,datevals[inan].strftime("%Y-%m-%d %H:%M"),varvals[inan]))
            varvalsSubs[ip]  = np.delete(varvalsSubs[ip] ,idxnans)
            datevalsSubs[ip] = np.delete(datevalsSubs[ip],idxnans)
            varvalsFiltSubs[ip]  = np.delete(varvalsFiltSubs[ip] ,idxnans)
            dervalsFiltSubs[ip]  = np.delete(dervalsFiltSubs[ip] ,idxnans)
            # Dont fiddle with this subseries again (this loop iteration)
            subseries_changed[ip]=True
            # Increment counter:
            num_removed+=len(idxnans)
            # Schedule a full reset on next loop iteration:
            reset_subseries=True
      #
      # In certain cases, the spline-fit will try to match a single or a few extreme 
      # values at end-of-interval. This can be detected by a very large slope 
      # *and* that the value is "extreme". The value-part being extreme seems rather 
      # difficult to quantify.
      # 
      # Compute std-errors now before we start eliminating (putting nans into the vector):
      epsis=np.abs(np.concatenate(varvalsSubs)-np.concatenate(varvalsFiltSubs))
      epsrms=np.sqrt(np.mean(np.square(epsis))) # Same as (varvals-varvalsFilt).std(), but NOT epsis.std() [!]
      #
      # For simplicity compute a critical value for the error (relative to spline) to be used in this loop:
      critepsi=np.float32('inf')
      if despikemaxrmsdev>0:
         critepsi=min(critepsi,despikemaxrmsdev*epsrms)
      if despikemaxabsdev>0:
         critepsi=min(critepsi,despikemaxabsdev)
      #
      # If the max-abs-derivative is at "an end", and at least Nx the average, then clip it
      # typical values may be mean~10 and max ~400 in case of problems
      dervalsFilt=np.concatenate(dervalsFiltSubs)
      deriv_absmean=np.mean(np.abs(dervalsFilt))
      deriv_maxlimit=despikemaxendslope*deriv_absmean
      print(" spline metadata: absmean(deriv)={}, RMS={}".format(deriv_absmean,epsrms))
      #
      # Whenever we remove (set to nan) a point as an outlier, we will create a "zone" (period) 
      # around the point, where we will not modify again before the spline is recomputed.
      # The idea here is that observation influence on spline is local.
      buffer_seconds=int(np.ceil(order*width))                 # In seconds, a bit conservative(?)
      buffer_datetime=datetime.timedelta(seconds=buffer_seconds)  # In datetime
      #
      # We assume taht we can prune all ends (two ends per subseries/piece) without interference
      # Keep track on how many points we remove in total during the sweep over subseries.
      for ip in range(npieces):
         if subseries_changed[ip]:
            # If this subseries was already modified, then skip to next
            continue
         #print(".check outliers for subseries no {}".format(ip+1))
         #
         # Length of this subseries:
         nn=varvalsFiltSubs[ip].size
         # TODO; If we look just at a subseries, then we are looking at
         # an entire array (albeit a smaller one)
         if despikemaxendslope>0:
            # Fix slope based on:
            #  a. end-point of local spline (subseries) is above critical value
            #  b. end-point of local spline (subseries) is max for the subseries
            # Early implementations were a&&b, but maybe a is enough by itself.
            #imaxloc=np.argmax(dervalsFiltSubs[ip])
            #maxderiv=dervalsFiltSubs[ip][imaxloc]
            maxderiv=np.max(dervalsFiltSubs[ip])
            imax   =np.argmax(dervalsFiltSubs[ip])
            if maxderiv>deriv_maxlimit:
               idxdel=[]
               for iend in (0, nn-1):
                  idx=iend+idxbreaks[ip]
                  thisval=dervalsFiltSubs[ip][iend]
                  #if thisval==maxderiv:
                  if thisval>deriv_maxlimit:
                     print(" Eliminating elev[{}]=elev[{}]={} [extreme slope at (sub)interval end={}, {} times avg]]".format(iend,datevals[iend].strftime("%Y-%m-%d %H:%M"),"%.3f"%varvalsSubs[ip][iend],"%.3f"%thisval,"%.1f"%(thisval/deriv_absmean)))
                     # Mark this end for deletion:
                     idxdel.append(iend)
                     # This should automatically trigger a reset as the end has changed.
                  # If the other end is just as bad (unlikely), then treat that too.
               if len(idxdel)>0:
                  # Actually delete:
                  varvalsSubs[ip] =np.delete(varvalsSubs[ip],idxdel)
                  datevalsSubs[ip]=np.delete(datevalsSubs[ip],idxdel)
                  # Increment counter:
                  num_removed+=len(idxdel)
                  # Mark this subseries as modified:
                  subseries_changed[ip]=True
         if subseries_changed[ip]:
            # If this subseries was already modified, then skip to next
            continue
         #
         # Difference between data and spline values, just as abs-values for now. Large is large.
         # If they are really big, then something may be wrong.
         if despikemaxrmsdev>0 or despikemaxabsdev>0:
            #
            # We may look at each sub-interval (piece) at a time, discarding up to one 
            # data value from each. This is to avoid un-necessary recomputation of unchanged 
            # sub-interval splines
            #
            # Find local (this subseries) "errors" (differences between spline and data)
            epsisi=np.abs(varvalsSubs[ip]-varvalsFiltSubs[ip])
            #
            if despikerolling:
               # Compute the rolling average of the errors to
               #  explicitly find regions with high errors (noisy stuff), which is indicative
               #  of problematic sections.
               #  Presently, we use a nroll=5-window average and will compare to 75% of 
               #  critical value for the single values. (Both may/should be introduced as options).
               nrollh=despikerolling_nneighs
               nroll=2*nrollh+1
               # Cumulative sum, and then find difference and average:
               epsisi_roll=np.cumsum(epsisi,dtype=epsisi.dtype)
               # This version averages over the previous nroll elements (including the element itself),
               #epsisi_roll[nroll:]=(1./nroll)*(epsisi_roll[nroll:]-epsisi_roll[:-nroll])
               # Really, what we want is a central version, where we average over the nrollh elements 
               # before, the element itself and the next nrollh elements. This means that nroll must be odd.
               epsisi_roll[nrollh+1:-nrollh]=(1./nroll)*(epsisi_roll[nroll:]-epsisi_roll[:-nroll])
               # Deal with ends, just copy back the first actual value across ends:
               epsisi_roll[:nrollh] =epsisi_roll[  nrollh+1 ]
               epsisi_roll[-nrollh:]=epsisi_roll[-(nrollh+1)]
               #  Now make an average of the rolling and instanteneous error.
               #  This metric is to be compared to the "standard" critical error measure.
               #  Note that the total weight here is > unity.
               epsisi_roll=despikerolling_factor*(epsisi_roll+epsisi)
            #
            idxdel=[]
            # These are the *relative* derivatives of the spline.
            # They will be used to limit the throw away values in regions with very
            # high derivatives. We often see values being discarded in 
            # The idea is thaty for problematic peridos, we often may see
            # values 
            #derivscaler=(1/deriv_absmean)*dervalsFiltSubs[ip].copy()
            while True:
               # Precompute a few details to ease the if-block below.
               # These are the maximum errors in the part of the (sub)time series, which 
               # has not already been treated (and thus errors zeroed out below)
               epsisi_max=np.max(epsisi)
               epsisi_roll_max=np.float64(0)
               if despikerolling:
                  epsisi_roll_max=np.max(epsisi_roll)
               # TODO 2022-05-31: First test the end points. These are the root for much grief.
               # So, if they are bad, remove them first, even if they are not the largest cases
               if epsisi[0]>despikemaxdev_endfactor*critepsi:
                  pnttyp = "start-part single-pnt"
                  imax   = 0
               elif epsisi[-1]>despikemaxdev_endfactor*critepsi:
                  pnttyp = "stop-part single-pnt"
                  imax   = epsisi.size-1
               elif epsisi_max>critepsi:
                  pnttyp = "single-pnt"
                  imax   = np.argmax(epsisi)
               elif epsisi_roll_max>critepsi:
                  pnttyp = "{}-pnt average".format(nroll)
                  imax   = np.argmax(epsisi_roll)
               else:
                  # If we get here, then there are no more bad eggs to find in
                  # this subseries at the moment
                  break
               realerr=epsisi[imax]
               #
               #DEBUG#print('[{}] {}: errs={}'.format(ip,enddates[ip,:],epsisi))
               # Find the N worst (highest error) cases. 
               # NOTE: There are several ways to code this, eg:
               #   arr.argsort()[::-1][:N]     # Requires sorting of entire array, which in general is expensive
               #   np.argpartition(a, -N)[-N:] # May sort/find only the N largest elements, but requires numpy 1.8+ 
               # For just getting the single max, no actual sorting may be needed.
               #print(" Testing elev[%d]=elev[%s]=%.3f [err=%.3f err/std=%.3f typ=%s]"%(imax,datevalsSubs[ip][imax].strftime("%Y-%m-%d %H:%M"),varvalsSubs[ip][imax],realerr,realerr/epsrms,pnttyp))
               print(" Eliminating elev[%s]=%.3f [large %s deviation %.5f]"%(datevalsSubs[ip][imax].strftime("%Y-%m-%d %H:%M"),varvalsSubs[ip][imax],pnttyp,realerr))
               #
               # Create a buffer-range plus/minus around the modified value.
               d0=datevalsSubs[ip][imax]-buffer_datetime
               d1=datevalsSubs[ip][imax]+buffer_datetime
               # Local (sub-series) indices of the points inside the buffer region:
               #  np.flatnonzero(np.logical_and(datevals[sli]>=d0,datevals[sli]<=d1))
               # end in global-array indixes:
               idxsbuffer=np.flatnonzero(np.logical_and(datevalsSubs[ip]>=d0,datevalsSubs[ip]<=d1))
               # Apply the buffer by zeroing out the buffer zone errors, so they cannot be new max:
               epsisi[idxsbuffer]=0
               if despikerolling:
                  epsisi_roll[idxsbuffer]=0
               # Sanity check - dont delete the same point twice:
               if imax in idxdel:
                  print('ERROR: index [{}][{}] found twice'.format(ip,imax))
                  raise AssertionError("Debug needed")
               # Actually mark point for deletion:
               idxdel.append(imax)
               #
               # Try again (this subseries/piece)
               #continue
            #
            if (len(idxdel)>0):
               # Actually remove from obs:
               datevalsSubs[ip]=np.delete(datevalsSubs[ip],idxdel)
               varvalsSubs[ip] =np.delete(varvalsSubs[ip],idxdel)
               # Mark subseries as modified
               subseries_changed[ip]=True
               # Increment counter:
               num_removed+=len(idxdel)
         if subseries_changed[ip]:
            continue
      #
      if num_removed:
         print(" This loop removed {} outliers based on local spline matching".format(num_removed))
         continue
      #
      # If we get here, then we are finished pruning the data.
      # We also have a spline-representation of the data, but only with values in
      # the (remaining/pruned) obsdata-points.
      print("No more obvious points to eliminate")
      break
   # Catenate subseries back to single series (with holes):
   #  These are QC-passed data:
   datevals=np.concatenate(datevalsSubs)
   varvals =np.concatenate(varvalsSubs)
   datevals,varvals=ts.remove_nans(datevals,varvals,verb=0)
   return datevals,varvals


##
#  spline_reinsertdata: After making the spline fit, reinsert the original data in points where 
#                    the input (src) and output (tgt, splined) times coinside. This only makes sense
#                    when the spline times are not exactly the same as the input, so typically 
#                    only useful with output_dt specified.
def reinsert_data(datevals_src,varvals_src,
                  datevals_tgt,varvals_tgt, 
                  verb=None):
   return ts.insert_data(datevals_src,varvals_src,
                         datevals_tgt,varvals_tgt, 
                         verb=verb)


##
# Default behaviour is read a file, perform the spline, and - if OUTFILE is given, store the result.
def main():
    parser=args_define_options(withwidth=True, withorder=True, withoutput=True, withdespike=True, withreinsert=True)
    parser=args_define_positional(parser)
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Verify that the src netcdf file actually exists and has the expected properties.
    tsnc.assert_file_is_present()
    tsnc.assert_file_is_ncts()
    #
    # Read data from input file:
    # Filename will default to <NCSRC>
    datevals_src,varvals_src,longname=tsnc.ncfil_load() #(filename=args.ncsrc)
    #
    # Possibly do despiking, if any of the necessary options are set:
    global spline_despikelimitabs
    global spline_despikelimitrms
    global spline_despikemaxendslope
    if spline_despikelimitabs>0 or spline_despikelimitrms>0 or spline_despikemaxendslope>0:
       datevals_fit,varvals_fit=despike_series(datevals=datevals_src,varvals=varvals_src)
    else:
       datevals_fit,varvals_fit=datevals_src,varvals_src
    #
    # Actually do spline interpolation:
    datevals_eval,varvals_eval=do_spline_pieces(datevals=datevals_fit,varvals=varvals_fit)
    datevals_out=datevals_eval
    varvals_out =varvals_eval
    #
    # Hook to copy originial data "back to" spline interpolation whereever appropriate:
    global spline_reinsertdata
    if spline_reinsertdata:
       datevals_rein,varvals_rein=reinsert_data(datevals_src=datevals_fit, varvals_src=varvals_fit,
                                                datevals_tgt=datevals_eval,varvals_tgt=varvals_eval)
       datevals_out=datevals_rein
       varvals_out =varvals_rein
    #
    # If defined, then reinsert original-data into the evaluate spline where possible;
    # ie. for elements where the times of the two time series coinside.
    #
    # OUTPUT
    if not tsnc.nctgt is None:
       # plus creating new file based on time series and data.
       tsnc.ncfil_save(datevals_out,varvals_out, )
    elif tsnc.append_file:
       # nctgt is none, but we can append to ncsrc. 
       # This may only work if datevals_eval[:] == datevals[:], otherwise things may break. YMMV
       tsnc.ncfil_save(datevals_out,varvals_out,filename=tsnc.ncsrc)

if __name__ == '__main__':
    main()
