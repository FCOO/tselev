#!/usr/bin/python
#
"""
Extract data from a (Mongo) DataBase and save as ts-compatible netcdf file,
possibly converting units to SI/[m].
Database interaction is done through FCOO 'observations-tools' 'observationData' module.
"""

# 
# Module history:
#  2022-02-07 bjb 0.0.1: Initial work - use code from PEB&BRS.
#  2022-02-09 bjb 0.0.2: Cleanup. Add validate=True on DB.getObservations call
#  2022-04-24 bjb 0.0.3: Accept unicode (non-ascii) characters in station names
#  2022-04-27 bjb 0.0.4: More flexibility in selection of stations
#  2022-05-02 bjb 0.0.5: Move from obs2ifm to dbGetData for database interface
#                        Use locale-based or ascii-based (default) name for netcdf long_name.

version_info = (0, 0, 5)
version      = '.'.join(str(c) for c in version_info)
__version__  = version


#
# FOR NOW, only use as a script, not include as a module.
# At some point, it may be feasible to use as module to use with other scripts/modules,
# but for now that has not been considered.
# Implementation of the present code is made 
# Some parts have been prepared to be used as a module, but really it should not be included.
# This is just a safe-guard against that. If the scripts is ever turned into a 
# module with other methods than "main()", then remove the following block:
if __name__ != '__main__':
    raise AssertionError("db2ts.py should not be imported - only run as script")

#
# Locally implemented modules
import tsSimple as ts
import tsNetcdf as tsnc
#
# The locally (GEOMETOC) implemented python repo observations-tools has a number of subdirs 
# and it seems cruxial to pick the right one eg "dbGetData" in PYTHONPATH
from getObservationData import observationData


# Other imports
import numpy   as np
import datetime



# Global variables for this module/method, including defaults

# Defining our own parser should not really be necessary for this module.
argparse_description="Extract observational elevation data from MongoDB database and save to local tsElev-compliant NetCDF file."

# This is for only defining options ONCE - even if module is use/included mutiple times
_have_defined_options=False

# Define possible named command line options
def args_define_options(parser=None):
    global _have_defined_options
    if _have_defined_options:
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    # This module cannot be called as script (yet), but we will add the 
    # magic needed if someone opts to implement it later:
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    #
    # Optional arguments (aka "options"):
    parser.add_argument('--dbcfg',dest="dbconfigfile", type=tsnc._check_file, required=True,
                        action='append', #action="store", type=list, #default='settings.cfg', 
                        help="Give path to one or more MongoDB config files, where each must include at least 'hosturl' and 'database' entries.")
    stgroup = parser.add_argument_group(title='Station selection options',
                                        description="For station selection enough information must be provided to uniquely select a single station.")
    stgroup.add_argument('--station-id','--stationId',dest="stationId", 
                        action="store", default=None, type=int,
                        help="Match this 'stationId' (Internal database ID) in database, eg 36")
    stgroup.add_argument('--station-name','--stationName',dest="stationName", 
                        action="store", default=None, 
                        help="Match this station 'stationName' entry in database, eg 'Hvide Sande Kyst'")
    stgroup.add_argument('--station-provider','--stationProvider',dest="stationProvider", 
                        action="store", default=None, 
                        help="Match this station 'provider' (data provider) entry in database, eg 'DMI'")
    stgroup.add_argument('--station-ProviderId','--station-provider-id','--stationProviderId',dest="stationProviderId", 
                        action="store", default=None, 
                        help="Match this station 'providerId' entry in database, eg '24342'")
    stgroup.add_argument('--station-owner',dest="stationOwner", 
                        action="store", default=None, 
                        help="Match this station 'owner' (name of owner of physical gauge station) entry in database, eg 'Kystdirektoratet'")
    stgroup.add_argument('--station-ownerId',dest="stationOwnerId", 
                        action="store", default=None, 
                        help="Match this station 'ownerId' (owner's station ID) entry in database, eg '24342'")
    stgroup.add_argument('--station-country',dest="stationCountry", 
                        action="store", default=None, 
                        help="Match this station 'country' (station physical location) entry in database, eg 'DK'")

    timgroup = parser.add_argument_group(title='Database time range selection',
                                        description="Select time start/stop for database interaction.")
    timgroup.add_argument("--start_time",dest="starttime", metavar="<EPOCHNN>", required=True,
                        type=ts._checkepochstr,
                          help='Set start time for database extraction')
    timgroup.add_argument("--stop_time",dest="stoptime", metavar="<EPOCHNN>", required=True,
                        type=ts._checkepochstr,
                        help='Set end/stop time for database extraction')

    parser.add_argument('--noSIunits','--DBunits',dest="obs2m", 
                        action="store_false", default=True, 
                        help="Disable conversion of observations from DataBase internal units to SI-unit [m]. "+\
                         "Note that setting --varunits will override this option.")

    parser.add_argument('--locale_longname',dest="use_asciiname", 
                        action="store_false", default=True, 
                        help="Use locale-based station name in netcdf elevation longname attribute. Default is to use the ascii-based database shortName attribute.")


    _have_defined_options=True
    # Let other used modules define their options if we are in MAIN/SCRIPT mode.
    # In the present case, we do not want any such options??
    if __name__ == '__main__':
        # Include tsnc options, but not any further options.
        parser = tsnc.args_define_options(parser,recurse=False)
        pass
    return parser
# Companion parser for args_define_options
def args_parse_options(parsed_args):
    global verbose 
    verbose       = parsed_args.verbose # tsnc.verbose
    # Now deal with our own options:
    #
    if __name__ == '__main__':
        #ts.args_parse_options(parsed_args,recurse=False)
        tsnc.args_parse_options(parsed_args)
        pass
    return

# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC> <NCSRC2> <NCTGT>
def args_define_positional(parser=None):
    if __name__ != '__main__':
        return parser
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    # Define a single mandatory arguments here: OUT
    parser = tsnc.args_define_positional_tgt(parser)
    return parser
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
    if __name__ != '__main__':
        return
    tsnc.args_parse_positional(parsed_args)
    return



def getObsWL( obs_DB, stationID, starttime, stoptime ):
    """
    Get water level data from DB.

    Parameters:
    ----------
    obs_DB       : DataBase API object
    stationID    : Internal DB station ID, e.g. 125 for 'Bogense Havn I'
    starttime    : datetime, begining of selection period. 
    stoptime     : datetime, end of selection period.
    
    CAVEAT: Presently hardcoded to DMI ID.
    """
    # Delta t for getObservations is in minutes, so convert to seconds, and then /60:
    #deltat   = ((stoptime-starttime).total_seconds())/60.
    #
    # Station ID (internal ..?
    #statid    = obs_DB.getStationId('DMI', str(stationID))
    #
    timeseries = obs_DB.getObservations(stationId=stationID, timestampFrom=starttime, timestampTo=stoptime, datatype="sealevel")
    #
    # When there are no data, then 'unit' is returned as '', and it is otherwise in a list
    # Here we just wrap it in a list to be consistent with otherwise output
    if timeseries['unit'] == '':
        timeseries['unit']=['']
    #
    # Sanity checks on output.
    # Check stuff that we will use in the present context.
    for kkey in ('unit', 'values', 'timestamp'):
        assert (kkey in timeseries),\
            "Return dict from obsDB.getObservations missing key='{}'".format(kkey)
        if kkey == 'unit':
            continue
        assert isinstance(timeseries[kkey],list),\
            "Unexpected type for obs['{}'] (obs_DB.getObservations). Expected 'list', but got type={} in {}.".format(kkey,type(timeseries[kkey]),timeseries[kkey])
    lv=len(timeseries['values'])
    lt=len(timeseries['timestamp'])
    #
    assert lv==lt,"obs_DB.getObservations lists lengths differ: len(values)={} != {}=len(timestamp)".format(lv,lt) 
    # Unit should be a single string (not packed in list or anything)
    #if lv>0:
    #    assert lu==1,"obs_DB.getObservations unit should be list of len one, but is len={}: {}".format(lu,timeseries['unit'])
    if lt>0:
        assert isinstance(timeseries['timestamp'][0],datetime.datetime),\
            "obs_DB.getObservations timestamps should be datetime.datetime, but are {}".format(type(timeseries['timestamp'][0]))
    #
    return timeseries




# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser=args_define_options()
    parser=args_define_positional(parser)
    args=parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
        #
    # Sanitizing of arguments is primarily done by the argparser.
    #
    starttime = ts.epochstr2datetime(args.starttime)
    stoptime  = ts.epochstr2datetime(args.stoptime)
    assert starttime<stoptime,"Error: start must be before stop"
    #print( 'Selected period start: {}'.format(starttime) )
    #print( 'Selected period end:   {}'.format(stoptime ) )
    #
    # Read DB-config file and set up the observation DB interaction object:
    print('Get database object, parsing {}'.format(args.dbconfigfile))
    obs_DB = observationData(args.dbconfigfile)
    #
    # Create a dict with all the option station-info, to uniquely identify station.
    findopts={}
    if not args.stationId is None:
        findopts['stationId']=args.stationId
    if not args.stationName is None:
        findopts['stationName']=args.stationName
    if not args.stationProvider is None:
        findopts['provider']=args.stationProvider
    if not args.stationProviderId is None:
        findopts['providerId']=args.stationProviderId
    if not args.stationOwner is None:
        findopts['owner']=args.stationOwner
    if not args.stationOwnerId is None:
        findopts['ownerId']=args.stationOwnerId
    if not args.stationCountry is None:
        findopts['country']=args.stationCountry
    # Find all matching stations from the database(s),
    # The return value is an iterator, so we must find the lngeth and assert that it is unity
    print(u'Look for station matching {}'.format(findopts))
    #sts=list(obs_DB.stations.find(findopts))
    ids=obs_DB.getStationIdsFromQuery(findopts)
    nsts=len(ids)
    assert nsts!=0,u"ERROR: Query {} resulted no matches (expected a single match)".format(findopts,nsts)
    assert nsts<=1,u"ERROR: Query {} resulted in {} matches (expected a single match)".format(findopts,nsts)
    #
    # If we get here, then there is just a single match.
    # And we should be able to get the actual internal database stationId, 
    # which is uniq and can be used for further processing
    stationId=ids[0]
    print(u"Verified stationID={}".format(stationId))
    print(u"Get station meta-data")
    stationdict=obs_DB.getStation(stationId)
    assert isinstance(stationdict,dict),"ERROR: Expected a dict from .getStation, but got a {} ({})".format(type(stationdict),stationdict)
    for kkey in ('stationName', 'shortName') :
        assert (kkey in stationdict),"ERROR: getStation returned dict with no {} entry".format(kkey)
    #
    stationName=stationdict['stationName']
    asciiName=stationdict['shortName']
    print(u"Found stationID={}, stationName='{}', shortName='{}'".format(stationId,stationName,asciiName))
    #
    print('Extract time series from database: stationID={}, from {} to {}'.format(stationId,starttime,stoptime))
    obswl = getObsWL(obs_DB, stationId,  starttime=starttime, stoptime=stoptime)
    #
    nlen=len(obswl['values'])
    print("Extracted {} values in time series".format(nlen))
    assert nlen>0,"ERROR: Refusing to create time series with no data"
    assert nlen>1,"ERROR: Need at least two (2) elements to create time series data. This is necessary to ts to compute dt."
    #
    # Convert observations to tsSimple format.
    datevals=np.asarray(obswl['timestamp'],dtype=datetime.datetime)
    varvals =np.asarray(obswl['values'],dtype=np.float64)
    # Beware that stationName may contain non-ascii characters
    if args.use_asciiname:
        uselongname=asciiName
    else:
        uselongname=stationName
    longname=u'Observed elevation at {}'.format(uselongname)
    #
    # Source/DB units is in 'unit' and packed in a list [length 1].
    if varvals.size > 0:
        srcunit=obswl['unit']
    else:
        srcunit='cm'
    #
    if args.varunits:
        tgtunit=args.varunits
    elif args.obs2m:
        tgtunit='m'
    else:
        tgtunit=srcunit
    if nlen>0 and tgtunit!=srcunit:
        print('Convert from DB-units [{}] to [{}]'.format(srcunit,tgtunit))
        varvals=ts.change_unit(varvals,srcunit,tgtunit)
    #
    print('Check time series consistency')
    ts.assert_consistent(datevals=datevals,varvals=varvals)
    #
    if args.timeunits:
        timeunits=args.timeunits
    elif args.sinceepoch:
        t0=ts.epochstr2datetime(args.sinceepoch)
        timeunits=t0.strftime("seconds since %Y-%m-%d %H:%M:%S")
    else:
        # Use start time as "time since"
        timeunits=starttime.strftime("seconds since %Y-%m-%d %H:%M:%S")
    #
    # Save data:
    tsnc.ncfil_save(datevals=datevals,varvals=varvals,
                    varunits=tgtunit,timeunits=timeunits,
                    filename=args.nctgt,longname=longname)
    return

if __name__ == '__main__':
    main()
