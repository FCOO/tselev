#!/usr/bin/python
"""
Wrapper for reading a netCDF file of obs-elevation and do
  spike/jump detection and removal
possibly followed by
  interpolation of small holes
and finally storing in a netCDF.

Call as:
  python tsDespike.py <options> INPUT.nc OUTPUT.nc

e.g.
  python tsDespike.py ${dir}/SEALEVEL-RAW/HORNBAEK_2017.nc hornbaek_out.nc --overwrite -vvv --roundtime=m --delstockedvalues=10 --delhardminlimit=-3.5 --delhardmaxlimit=17 --doplot   --smallholes=-1 --spikescale=2.2
"""

#
# Module history:
#  2019-09-09 peb 0.0.0: Initial version. Nothing really works yet.
#  2019-09-26 peb 0.0.1: (most) functionality now extensively tested on
#                        multiple time series.
#  2019-10-01 peb 0.0.2: Tool renamed from tsDepike.py, and committed to gitlab.
#  2019-10-02 peb 0.0.3: Option to keep a segment of the time series untouched.
#  2019-10-03 peb 0.0.4: Option to do higher order polynomial intitial interpolation
#                        of holes to get the SG filter started.

version_info = (0, 0, 4)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

#
# Libraries, which is needed in both command-line and functional modes:
import numpy   as np

import tsSimple as ts
import tsSpline as tsspl
import tsFilter as tsfilt
import tsNetcdf as tsnc


# Global variables for this module/method, including defaults
fixed_values  = -1
hard_limits   = [np.float64(-5.), np.float64(5.)]
spike_scale   = np.float64(3.)
spike_factor  = np.float64(0.3)
rec_fac       = np.float64(1.)
rep_val       = None
rep_steps     = 2
small_holes   = -1
do_plot       = False
do_smoothed   = False
# define SG filter:
do_bspline    = False
sg_windowsize = 25
sg_porder     = 3             # order of the polynomial used to fit the sample
sg_mode       = 'nearest'     # extend beyond beginning and end
intp_kind     = 'linear'      # Intitial interpolation of holes to get the SG filter started;
                              # 'cubic' might be better than 'linear' to fill small holes in
                              # smooth timeseries (tidal dominated)
keep_from     = None
keep_to       = None

argparse_description = "Filter obs elev time series by deleting spikes and interpolating small holes. "+ \
                       "The tool operates on netCDF files."


########################################################################
#                                                                      #
#      OPTIONS AND ARGUMENTS                                           #
#                                                                      #
########################################################################
# The following block concerns:
#   * Definitions of arguments
#   * Parsing of arguments
# Helper methods for command-line interface:


# Define possible named command line options
def args_define_options(parser=None):
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    # We need to define our own version option, but ONLY for "called as a script":
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    parser=ts.args_define_options(parser)
    parser=tsspl.args_define_options(parser)
    parser=tsfilt.args_define_options(parser)
    parser=tsnc.args_define_options(parser)
    #
    # This filter's own optional arguments (aka "options"):
    global fixed_values, hard_limits, small_holes
    global do_plot
    global do_smoothed
    global sg_windowsize, sg_porder, sg_mode, intp_kind, do_bspline
    global spike_scale, spike_factor, rec_fac, rep_val, rep_steps
    global keep_from, keep_to
    parser.add_argument("--delstockedvalues",dest="delstockedvalues", type=int,
                        default=fixed_values,
                        help='delete (set to NaN) this number or more stucked values (<=1 means no action), default: '+str(fixed_values))
    parser.add_argument("--delhardminlimit",dest="delhardminlimit", type=float,
                        default=hard_limits[0],
                        help='delete (set to NaN) values lower than this hard minimum limit, default: '+str(hard_limits[0]))
    parser.add_argument("--delhardmaxlimit",dest="delhardmaxlimit", type=float,
                        default=hard_limits[1],
                        help='delete (set to NaN) values greater than this hard maximum limit, default: '+str(hard_limits[1]))
    parser.add_argument("--spikescale",dest="spikescale", type=float,
                        default=spike_scale,
                        help='spike scale factor such that spike=std/spikescale, default: '+str(spike_scale))
    parser.add_argument("--spikefactor",dest="spikefactor", type=float,
                        default=spike_factor,
                        help='factor such that ds=spike*spikefactor is the allowed deviation from the SG smoothed curve, default: '+str(spike_factor))
    parser.add_argument("--recfac",dest="recfac", type=float,
                        default=rec_fac,
                        help='recover scale factor, default: '+str(rec_fac))
    parser.add_argument("--repval",dest="repval", type=float,
                        default=rep_val,
                        help='repeated value to delete, default: '+str(rep_val))
    parser.add_argument("--repsteps",dest="repsteps", type=int,
                        default=rep_steps,
                        help='delete this many repeated values, default: '+str(rep_steps))
    parser.add_argument("--smallholes",dest="smallholes", type=int,
                        default=small_holes,
                        help='number of time steps to define small holes to be filled (no action if <=0), default: '+str(small_holes))
    parser.add_argument("--doplot",dest="doplot", 
                        default=do_plot, action='store_true',
                        help='make a plot of what went on during filtering, default: '+str(do_plot))
    parser.add_argument("--dosmoothed",dest="dosmoothed", 
                        default=do_smoothed, action='store_true',
                        help='output the smoothed (Savitzky-Golay filtered) series, default: '+str(do_smoothed))
    parser.add_argument("--dobspline",dest="dobspline", 
                        default=do_bspline, action='store_true',
                        help='apply b-splines instead of the Savitzky-Golay filter, default: '+str(do_bspline))
    parser.add_argument("--sgwindow",dest="sgwindow", type=int,
                        default=sg_windowsize,
                        help='Size of window used in Savitzky-Golay filter, default: '+str(sg_windowsize))
    parser.add_argument("--sgporder",dest="sgporder", type=int,
                        default=sg_porder,
                        help='Polynomial order used in Savitzky-Golay filter, default: '+str(sg_porder))
    parser.add_argument("--sgmode",dest="sgmode", type=str,
                        default=sg_mode,
                        help='Type of extension to use for the padded signal to which the S-G filter is applied, default: '+str(sg_mode))
    parser.add_argument("--intpkind",dest="intpkind", type=str,
                        default=intp_kind,
                        help='Kind of intitial interpolation of holes to get the SG filter started, default: '+str(intp_kind))
    parser.add_argument("--keepfrom",dest="keepfrom", type=str,
                        default=keep_from,
                        help='keep time series data untouched starting at this time')
    parser.add_argument("--keepto",dest="keepto", type=str,
                        default=keep_to,
                        help='keep time series data untouched ending at this time')
    return parser

# Companion parser for args_define_options
def args_parse_options(parsed_args):
    # Let tsnc get its own options:
    ts.args_parse_options(parsed_args)
    tsspl.args_parse_options(parsed_args)
    tsfilt.args_parse_options(parsed_args)
    tsnc.args_parse_options(parsed_args)
    #
    # This filter's own optional arguments
    global fixed_values, hard_limits, small_holes
    global do_plot
    global do_smoothed
    global sg_windowsize, sg_porder, sg_mode, intp_kind, do_bspline
    global spike_scale, spike_factor, rec_fac, rep_val, rep_steps
    global keep_from, keep_to
    spike_scale    = parsed_args.spikescale
    spike_factor   = parsed_args.spikefactor
    rec_fac        = parsed_args.recfac
    rep_val        = parsed_args.repval
    rep_steps      = parsed_args.repsteps
    fixed_values   = parsed_args.delstockedvalues
    hard_limits[0] = parsed_args.delhardminlimit
    hard_limits[1] = parsed_args.delhardmaxlimit
    small_holes    = parsed_args.smallholes
    do_plot        = parsed_args.doplot
    do_smoothed    = parsed_args.dosmoothed
    do_bspline     = parsed_args.dobspline
    sg_windowsize  = parsed_args.sgwindow
    sg_porder      = parsed_args.sgporder
    sg_mode        = parsed_args.sgmode
    intp_kind      = parsed_args.intpkind
    keep_from      = parsed_args.keepfrom
    keep_to        = parsed_args.keepto
    return

# Define positional arguments.
# In this case we simply use a template from tsnc, which then ensures that tsnc will
# read and hold the information to us. So basically, this routine is just a wrapper,
# which ensures that the correct help/description is added - and selects the 
# kind of positional arguments. In this case <NCSRC> <NCTGT>
def args_define_positional(parser=None):
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    # Define two mandatory arguments here. Beware difference between -O and -A,
    return tsnc.args_define_positional_two(parser)
# Companion parser for args_define_positional.
# This is also just a wrapper to let tsnc "do its thing".
def args_parse_positional(parsed_args):
    tsnc.args_parse_positional(parsed_args)
    return
#                                                                      #
# END      OPTIONS AND ARGUMENTS                                       #
########################################################################

########################################################################
#                                                                      #
#      TIME SERIES (FILTER) OPERATIONS                                 #
#                                                                      #
########################################################################

#  Seek and delete (set value to nan) values that are 
#  manually identified as outliers
def delete_outliers(vals, hard_min, hard_max):
    """
    Arguments:

    vals     :   parameter
    hard_min :   hard min value
    hard_max :   hard max value

    Performs the following filtering:

       Seek and delete (set value to nan) values manually identified
       as outliers.
    """

    rvals = vals.copy()

    # how many out-of-range values?
    idx1 = ~np.isnan(rvals)
    idx1[idx1] &= rvals[idx1] < hard_min
    imn = len(np.where(idx1)[0])

    idx2 = ~np.isnan(rvals)
    idx2[idx2] &= rvals[idx2] > hard_max
    imx = len(np.where(idx2)[0])

    # deleted outliers:
    if imn+imx > 0:
        if imn > 0:
            rvals[idx1] = np.nan
            print('   Deleted %d values lower than %f'%(imn,hard_min))
        if imx > 0:
            rvals[idx2] = np.nan
            print('   Deleted %d values larger than %f'%(imx,hard_max))
    else:
        print('   Deleted no outliers' )

    return rvals


# Seek and delete (set value to nan) fixed value over nst or more steps
def delete_fixed(vals,nst=4):
    """
    Arguments:
        
    vals: parameter
    nst : number of steps, delete when value is repeated for nst or more steps
          (default nst=4)

    Performs the following filtering:
      Seek and delete fixed value over nst or more steps
    """

    rvals = vals.copy()

    #  Seek and delete fixed value over nst or more steps
    itotal = 0
    if nst > 1:
        i1     = 0
        i2     = 1
        val1   = rvals[i1]
        for i in range(1,len(rvals)):
            if rvals[i] == val1:
                i2 = i2 + 1
            else:
                if i2 >= nst:
                    itotal = itotal + i2
                    rvals[i1:i1+i2] = np.nan
                i1 = i
                i2 = 1
                val1 = rvals[i1]

        # catch up if we ended the time series on fixed values:
        if i2 >= nst:
            itotal = itotal + i2
            rvals[i1:i1+i2] = np.nan

    if itotal > 0:
        print( '   Deleted %d fixed values'%itotal)
    else:
        print( '   Deleted no fixed values')

    return rvals


# Seek and delete (set value to nan) when a given value (repval) is repeated
def delete_repeated(vals,repval,nst=2):
    """
    Arguments:
        
    vals: parameter
    repval : value that must not be repeated

    Performs the following filtering:
      Seek and delete when a given value (repval) is repeated
    """

    rvals = vals.copy()

    #  Seek and delete repeated value over nst or more steps
    itotal = 0

    if nst > 1:
        i1     = 0
        i2     = 1
        val1   = rvals[i1]
        for i in range(0,len(rvals)):
            if rvals[i] == val1 and val1 == repval:
                i2 = i2 + 1
            else:
                if i2 >= nst:
                    itotal = itotal + i2
                    rvals[i1:i1+i2] = np.nan
                i1 = i
                i2 = 1
                val1 = rvals[i1]

        # catch up if we ended the time series on fixed values:
        if i2 >= nst:
            itotal = itotal + i2
            rvals[i1:i1+i2] = np.nan

        if itotal > 0:
            print( '   Deleted %d repeated values'%itotal)
        else:
            print( '   Deleted no repeated values')
    elif nst == 1:
        rvals[np.where(rvals == repval)[0]] = np.nan

    return rvals


# calculate STD (possibly containing fillvalue(s)) 
# and get spike size from STD
def getSTD_Spike ( arr, nr=None, spkfac=3., fillvalue=None ):

    if fillvalue==None:
        std = np.std( arr[np.where(~np.isnan(arr))[0]] )
    else:
        std = np.std( arr[np.where(arr != fillvalue)[0]] )

    if nr==None:
        nrstr = ""
    else:
        nrstr = str(nr)

    print("     STD"+nrstr+" = %f"%std)

    spike = std/spkfac # def. of a spike or jump, relative to std
    print("     Spike"+nrstr+" = %f"%spike)

    return std, spike


# Apply a Savitzky-Golay filter through the data (time,data)
# with holes filled by linear interpolation.
def sg_filter( time, data, window_size=41, porder=3, mode='nearest', kind='linear' ):
    """ Apply a Savitzky-Golay filter through the data (time,data)
        with holes filled by linear interpolation. """

    from scipy.signal      import savgol_filter
    from scipy.interpolate import interp1d

    # first, populate holes with linearly interpolated values:
    t  = time[np.where(~np.isnan(data))[0]].copy()
    x  = data[np.where(~np.isnan(data))[0]].copy()
    y1 = interp1d(t,x,kind=kind)

    # savgol_filter does not like going outside range, so define "inside" i2: 
    i2 = np.where(time[np.where(time <= np.max(t))[0]] >= np.min(t))[0]
    t2 = time[i2].copy()
    y2 = y1(t2)

    # now, the actual filter:
    ysg = savgol_filter(y2, window_size, porder, mode=mode)

    return ysg, i2


# Apply b-spline through the data (datevals,data)
def bspl(datevals, data):

    dateval_spline, varvals_spline = tsspl.do_spline_pieces(datevals, data,
                                            output_datevals=datevals.copy() )
    i2 = range(0,len(datevals))

    return varvals_spline, i2


# compare screened, bad, and quarantined series to the 
# Savitzky-Golay or b-spline smoothed series (ysg)
def compare_to_smoothed( adiff, ysg, i2, screened, bad, quarantined ):

    for i in range(0,len(ysg)):
        idx = i2[i]
        if ~np.isnan(screened[idx]):
            # test if screened is too far from smoothed:
            if np.abs( screened[idx]-ysg[i] ) >= 2.*adiff:
                bad[idx]      = screened[idx]
                screened[idx] = np.nan

        elif ~np.isnan(bad[idx]):
            # is "bad" value not bad after all?
            if np.abs( bad[idx]-ysg[i] ) <= adiff:
                screened[idx] = bad[idx]
                bad[idx]      = np.nan

        elif ~np.isnan(quarantined[idx]):
            # is quarantined OK ?
            if np.abs( quarantined[idx]-ysg[i] ) <= adiff:
                screened[idx]    = quarantined[idx]
                quarantined[idx] = np.nan

    return screened, bad, quarantined


# Seek and delete jumps/spikes
def delete_jumps(times, varvals, 
                 spikefactor=3., spikefactor2=0.3, rec=-1.0,
                 window_size=41, porder=3, mode='nearest', kind='linear',
                 doplot=False, datevals=None, dospl=False):
    """
    Arguments:
        
    times:      day-number
    varvals:    parameter
    spikefactor: optional
                spike factor such that spike=std/spikefactor
    rec:        optional
                Measure for recover period in [days] (default rec=-1.0)
                rec<=0 will use the dt from times[:].
                Ususally, rec should be the time sampling dt in [days].
    doplot:     boolean, optional
                do some plotting for test (default False)
    datevals:   datetime 
                to be used in plotting if doplot=True  (default=None)
    dospl:      boolean, optional
                do b-spline (default False), else do the SG filter
    
    Performs the following filtering:
        
       Seek and delete jumps including spikes
     
    """

    ########################################################################
    #  Seek and delete jumps

    valsi = varvals.copy()

    nnn = len(valsi)
    if nnn < 1:
        print( '     Too short series to do spike detection.')
        return valsi

    #  -- first, get STD of the input series and estimate spike size:
    std, spike = getSTD_Spike( valsi, nr=0, spkfac=spikefactor )

    #then, define some arrays:
    screened    = np.empty(nnn)
    screened.fill(np.nan)
    bad         = np.empty(nnn)
    quarantined = np.empty(nnn)
    bad.fill(np.nan)
    quarantined.fill(np.nan)

    # dt in minutes, assuming input in days:
    dt_min = np.int32((times[1]-times[0])*24.*60.)

    dti = 0.25       # deltat increment
    if rec <= 0. :
        recover = 12*60/8/dt_min    # recover period, approx 1/8 M2 period
    else:
        recover = np.int32(1./rec/2./8.)
    # half of that seems to be OK:
    recover = 0.5*recover

    lvalid = np.mean( valsi[np.where(~np.isnan(valsi))[0]] )  # valid level
    deltat = 1.0*dt_min/10.  # in 10min units
    ibad   = 0               # No. of consecutive bad values
    irec   = 0               # recover ?
    lbad0  = nnn+1           # "first" bad value

    # Initial screening, seeking spikes: ---------------------------------------
    for l in np.where(~np.isnan(valsi))[0]:
        if abs(lvalid-valsi[l]) <= spike*deltat:
            # a good value is found:
            lvalid = valsi[l]
            deltat = 1.0
            if irec < recover:
                screened[l]    = valsi[l]
                bad[l]         = np.nan
                quarantined[l] = np.nan

                # now, let's see if we can include some previously excluded points:
                if ibad > 0 and lbad0 < l:
                    ldeltat = 1.0
                    llvalid = valsi[l]
                    for ll in range(l-1,lbad0-1,-1):
                        if valsi[ll] != np.nan and abs(llvalid-valsi[ll]) <= spike*ldeltat:
                            llvalid         = valsi[ll]
                            ldeltat         = 1.0
                            screened[ll]    = valsi[ll]
                            bad[ll]         = np.nan
                            quarantined[ll] = np.nan
                        elif screened[ll] == np.nan:
                            ldeltat         = ldeltat + dti
                            screened[ll]    = np.nan
                            bad[ll]         = valsi[ll]
                            quarantined[ll] = np.nan

                lbad0 = nnn+1
                ibad  = 0
                irec  = 0
            else:
                # quarantined, might be OK:
                screened[l]    = np.nan
                bad[l]         = np.nan
                quarantined[l] = valsi[l]
                irec = irec - 1

        else:
            # flag this as a bad value:
            deltat         = deltat + dti
            screened[l]    = np.nan
            bad[l]         = valsi[l]
            quarantined[l] = np.nan

            if l < lbad0: lbad0 = l
            ibad = ibad + 1
            irec = irec + 1

            # try to establish a new valid level:
            if ibad > 64*recover:
                lvalid = valsi[l]
            else:
                lvalid = ((64.*recover-ibad+1.)*lvalid + (ibad-1.)*valsi[l])/(64.*recover)


    if doplot:
        import matplotlib.pyplot as plt
        plt.plot( datevals, valsi, 'ko')


    # Apply a Savitzky-Golay filter or b-spline --------------------------------
    if dospl:
        # do the b-spline stuff
        ysg, i2 = bspl(datevals, screened)
    else:
        if len(np.where(~np.isnan(screened))[0]) < window_size:
            # clean up and escape if too few screened values:
            print( '     Too few screened values in series to do SG filter.')
            if doplot:
                plt.show()
            valsi[np.where(np.isnan(screened))[0]] = np.nan
            return valsi

        # apply filter on screened data (spikes removed) 
        # and holes filled by linear interpolation:
        ysg, i2 = sg_filter(times, screened, window_size=window_size, porder=porder, mode=mode, kind=kind)


    # try to include bad+quarantined into screened pool if values
    # don't deviate too much from smoothed (Savitzky-Golay filtered)
    # and mark values that do deviate too much from smoothed as bad
    #
    #  -- get STD of the first-time screened series and estimate new spike size:
    std, spike = getSTD_Spike(screened, nr=1, spkfac=spikefactor)
    #
    # size of allowed diff 
    adiff = spike*spikefactor2
    #
    screened, bad, quarantined = compare_to_smoothed( adiff, ysg, i2, screened, bad, quarantined )
    #
    # did we get them all above?
    #   (This part most often does not do anything at all, but still ... )
    ldeltat = 1.0
    llvalid = 0.0
    for ll in np.where(~np.isnan(valsi))[0]:
        if np.isnan(screened[ll]):
            if ~np.isnan(screened[max(0,ll-1)]):
                llvalid = screened[max(0,ll-1)]
            if abs(llvalid-valsi[ll]) <= spike*ldeltat:
                screened[ll]    = valsi[ll]
                bad[ll]         = np.nan
                quarantined[ll] = np.nan
                llvalid         = valsi[ll]

    # One more time ...   ------------------------------------------------------
    if dospl:
        # do the b-spline stuff
        ysg, i2 = bspl(datevals, screened)
    else:
        # apply filter on screened data (spikes removed) 
        # and holes filled by linear interpolation:
        ysg, i2 = sg_filter(times, screened, window_size=window_size, porder=porder, mode=mode, kind=kind)

    if doplot:
        plt.plot( datevals[i2],ysg, 'ro')

    #
    # try to include bad+quarantined into screened pool if values
    # doesn't deviate too much from smoothed (Savitzky-Golay filtered)
    # and mark values that do deviate too much from smoothed as bad
    #
    #  -- get STD of the second-time screened series:
    std, spike = getSTD_Spike(screened, nr=2, spkfac=spikefactor)
    #
    # size of allowed diff 
    adiff = spike*0.1
    #
    screened, bad, quarantined = compare_to_smoothed( adiff, ysg, i2, screened, bad, quarantined )


    # Done screening. Now print, plot and return -------------------------------
    quarantined[np.where(~np.isnan(screened))[0]] = np.nan
    print( '     quarantined: %d'%len(np.where(~np.isnan(quarantined))[0]) )
    print( '     bad        : %d'%len(np.where(~np.isnan(bad))[0]) )
    #if len(np.where(~np.isnan(bad))[0]) > 0: print( datevals[np.where(~np.isnan(bad))[0]])


    if doplot:
        plt.plot( datevals, quarantined, 'go')
        plt.plot( datevals, screened, 'bo')
        plt.show()

    # prepare return:
    #  -- fill all holes with nan:
    valsi[np.where(np.isnan(screened))[0]] = np.nan

    return valsi


# smoothing with Savitzky-Golay filter
def smooth_with_SG_filter( times, data, window_size=41, porder=3, mode='nearest', kind='linear' ):
    """ Return the smoothed obtained from a Savitzky-Golay filter through 
        the data (time,data)  with holes filled by linear interpolation.
    """
    
    valsi = data.copy()

    ysg, i2 = sg_filter( times, valsi, window_size=window_size, porder=porder, mode=mode, kind=kind )
    for i in range(0,len(ysg)):
        valsi[i2[i]] = ysg[i]

    return valsi, ysg, i2


# Fill small holes 
def fill_small_holes(varvals, smallholes, ysg, i2):
    """
    Arguments:
        
    varvals:    parameter
    smallholes: size of small holes
    ysg:        array with fillings (e.g. from Savitzky-Golay filter)
    i2:         index of ysg
    """

    valsi = varvals.copy()

    totalholes = 0
    il = len(valsi) + smallholes*10  # just something large "enough"
    ih = 0
    for i in range(0,len(ysg)):
        if np.isnan(valsi[i2[i]]):
            ih = i
            il = min(il,i)
        else:
            if 0 < ih-il+1 <= smallholes:
                totalholes = totalholes + 1
                idxl = i2[il]
                idxh = i2[ih]
                valsi[idxl:idxh+1] = ysg[il:ih+1]
            # reset indices:
            il = len(valsi) + smallholes*10  # just something large "enough"
            ih = 0

    if totalholes > 0:
        print('   %d small holes filled.'%totalholes)
    else:
        print('   No small holes filled.')

    return valsi


#                                                                      #
#END      TIME SERIES (FILTER) OPERATIONS                              #
########################################################################


# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser = args_define_options()
    parser = args_define_positional(parser)
    args   = parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Verify that the src netcdf file actually exists and has the expected properties.
    tsnc.assert_file_is_present()
    tsnc.assert_file_is_ncts()
    #
    # Read data from input file:
    # Filename will default to <NCSRC>
    #  -- make sure that varvals are in meters ('m') which is expected in the 
    #     following, so we need to insist on 'm' here:
    datevals, varvals, longname = tsnc.ncfil_load(varunits='m')

    # do the screening filter step 1:   ========================================
    #  -- define a unique, equidistant and monotone time axis,
    #     possibly fill holes with nan
    #
    #     Assure that time series is monotone:
    ts.assert_monotone(datevals)
    #
    #     Delete (in their entirety) doubles, where the elevation is not consistent:
    #     For model time series, there should be none of these:
    datevals, varvals = ts.remove_duplicates(datevals,varvals,duplicates='delete-differs')
    #
    #     Then we keep only the first of those, where the doubles are consistent, ie where
    #     there are essentially really a copy of both time and elevation values.
    #     This one is great for model time series:
    datevals, varvals = ts.remove_duplicates(datevals,varvals,duplicates='keepfirst')
    #
    #     Obtain dt and delete when too close in time:
    use_dt = ts.get_timestep(datevals)   # dt is in seconds
    datevals, varvals = ts.remove_duplicates(datevals,varvals,epsitime=0.91*use_dt,duplicates='delete')
    use_dt, dt_min, dt_max = ts.get_timestep(datevals,wantvector=True)
    #
    #     Make sure that time axis is now fully equidistant
    if ts.is_piecewise_equidistant(datevals):
        if use_dt < 600.0:
            datevals          = ts.round_dates(datevals,roundto='10min')  # NOTE: hardcoded to 10min
            datevals, varvals = ts.remove_duplicates(datevals,varvals,duplicates='average')
        datevals,varvals  = ts.piecewise_to_fully_equidistant(datevals,varvals)
    else:
        # Consider doing something here if not piecewise equidistant;
        # for now, we just bail out
    #   raise AssertionError("Time series does not seem to be piecewise equidistant")

        # Attempt to create a equidistant time series.
        # First, do a screening for outliers and stocked and repeated values:
        varvals = delete_outliers( varvals, hard_min=hard_limits[0], hard_max=hard_limits[1] )
        varvals = delete_fixed( varvals, nst=fixed_values )
        if rep_val is not None:
            if isinstance( rep_val, float ):
                varvals = delete_repeated( varvals, repval=rep_val, nst=rep_steps )
        # Secondly, attempt to do a rough screening 
        seconds_pr_day    = np.float64(24*60*60)
        days_since        = ts.timesince_totalseconds(datevals)/seconds_pr_day
        recover_period    = (dt_min/seconds_pr_day)*rec_fac
        varvals           = delete_jumps( days_since, varvals,
                                spikefactor=spike_scale*0.5, spikefactor2=spike_factor*2.0, rec=recover_period*0.5,
                                doplot=False, datevals=datevals, dospl=True )
        datevals          = ts.round_dates(datevals,roundto='10min')  # NOTE: hardcoded to 10min
        datevals, varvals = ts.remove_duplicates(datevals,varvals,duplicates='average')
        # Let's try again...
        if ts.is_piecewise_equidistant(datevals):
            datevals,varvals = ts.piecewise_to_fully_equidistant(datevals,varvals)
        else:
            # I give up :(
            raise AssertionError("Can not make timeseries piecewise equidistant ... bailing out")
    #
    #     To do filter, we need to ensure that time axis is monotone and equidistant.
    ts.assert_monotone(datevals)
    ts.assert_equidistant(datevals)
    use_dt = ts.get_timestep(datevals)


    # plot "initial" series in black:
    if do_plot:
        import matplotlib.pyplot as plt
        plt.plot( datevals, varvals, 'ko')


    # prepare for the last step: ===============================================
    #  -- possibly keep a segment of the time series free of the screening process:
    nkeep = 0
    if (not keep_from is None) and (not keep_to is None):
        kstart = ts.epochstr2datetime(keep_from)
        kstop  = ts.epochstr2datetime(keep_to)
        print('   Will keep interval: %s to %s free from screening.'%(keep_from,keep_to))
        keepvals = varvals.copy()
        keepvals[ np.where(datevals > kstop)[0] ]  = np.nan
        keepvals[ np.where(datevals < kstart)[0] ] = np.nan
        nkeep = len(np.where(~np.isnan(keepvals))[0])
        print(' keep %d'%nkeep )


    # do the screening filter step 2:   ========================================
    #  -- delete values that are very high/low:
    varvals = delete_outliers( varvals, hard_min=hard_limits[0], hard_max=hard_limits[1] )


    # do the screening filter step 3a:  ========================================
    #  -- delete values that are fixed for fixed_values or more time steps:
    varvals = delete_fixed( varvals, nst=fixed_values )

    # do the screening filter step 3b:  ========================================
    #  -- delete repeated values:
    if rep_val is not None:
        if isinstance( rep_val, float ):
            varvals = delete_repeated( varvals, repval=rep_val, nst=rep_steps )


    # do the screening filter step 4:   ========================================
    #  -- detect and delete spikes/jumps, 
    #     and possibly smooth the data and interpolate small holes:
    print('   Spike detection:')
    print('     spike scale is %f'%spike_scale)
    print('     spike factor is %f'%spike_factor)
    seconds_pr_day = np.float64(24*60*60)
    days_since     = ts.timesince_totalseconds(datevals)/seconds_pr_day
    recover_period = use_dt/seconds_pr_day            # in unit days
    recover_period = recover_period*rec_fac
    window_size    = sg_windowsize
    if do_bspline==False:
        if window_size != (1+2*(window_size//2)): 
            print('     window_size changed from %d to %d'%(window_size,window_size+1))
            window_size = window_size + 1                 # window_size must be odd
        else:
            print('     window_size is %d'%window_size)
        print('     porder is %d'%sg_porder)
        print('     mode is %s'%sg_mode)
    #
    varvals = delete_jumps( days_since, varvals, 
                            spikefactor=spike_scale, spikefactor2=spike_factor, rec=recover_period, 
                            window_size=window_size, porder=sg_porder, mode=sg_mode,
                            doplot=False, datevals=datevals, dospl=do_bspline )


    # do the screening filter step 5:   ========================================
    #  -- possibly smooth the data before output:
    if do_smoothed==True:
        idxnan  = np.where(np.isnan(varvals))[0]
        if do_bspline:
            print( '   Smoothing by B-spline.')
            ysg, i2 = bspl(datevals, varvals)
            varvals = ysg.copy()
            #  -- make sure that all holes are filled with nan:
            varvals[idxnan] = np.nan
        else:
            if len(np.where(~np.isnan(varvals))[0]) < window_size:
                print( '   Too few non-nan values in series to do smoothing by SG filter.')
            else:
                print( '   Smoothing by SG filter.')
                varvals, ysg, i2 = smooth_with_SG_filter( days_since, varvals, window_size=window_size, porder=sg_porder, mode=sg_mode, kind=intp_kind )
                #
                #  -- make sure that all holes are filled with nan:
                varvals[idxnan] = np.nan


    # do the screening filter step 6:  =========================================
    #  -- possibly keep a segment of the time series free of the screening process:
    if nkeep > 0:
        varvals[np.where(~np.isnan(keepvals))[0]] = keepvals[np.where(~np.isnan(keepvals))[0]]


    # do the screening filter step 7:   ========================================
    #  -- fill small holes:
    if small_holes > 0:
        idxnotnan = np.where(~np.isnan(varvals))[0]
        if len(idxnotnan) < window_size:
            print( '   Too few non-nan values in series to fill holes by SG filter.')
        else:
            if do_smoothed==False:
                if do_bspline:
                    ysg, i2 = bspl(datevals, varvals)
                else:
                    # if not already done above, apply the filter:
                    ysg, i2 = sg_filter( days_since, varvals, window_size=window_size, porder=sg_porder, mode=sg_mode, kind=intp_kind )
            varvals = fill_small_holes(varvals, small_holes, ysg, i2)


    # plot final series in blue:
    if do_plot:
        plt.plot( datevals, varvals, 'bo')
        if small_holes > 0:
            varholes = varvals.copy()
            varholes[idxnotnan] = np.nan
            plt.plot( datevals, varholes, 'ro')
            if nkeep>0:
                plt.plot(datevals, keepvals, 'bo')
        plt.show()


    # Finally, we create a new file - or modify an existing one:  ==============
    nctgt = tsnc.nctgt
    tsnc.ncfil_save(datevals,varvals,filename=nctgt)


if __name__ == '__main__':
    main()
