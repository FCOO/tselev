#!/usr/bin/python
"""
Plot one or more netCDF files of elevation using matplotklib.
This is a super-simple way to visualize the data, for moire 
features etc use eg pyncview/multiplot or create a dedicated 
routine.

Call as:
  python tsPlot.py <options> INPUT.nc[:marker[:label]] ... [OUTPUT.png]

"""

#
# Module history:
#  2022-02-22 bjb 0.0.1: Initial version.
#  2022-03-01 bjb 0.0.2: Add tsSimple options - especially for start/stop usage
#                        Add nan between separate pieces for pretty-plotting

version_info = (0, 0, 2)
version      = '.'.join(str(c) for c in version_info)
__version__  = version

#
# Libraries, which is needed in both command-line and functional modes:
import numpy   as np

import argparse
import os      # Eg check if file exists + remove file


import tsSimple as ts
import tsNetcdf as tsnc


# Global variables for this module/method, including defaults
argparse_description = "Plot elev time series."

figsize=None
outputfile=None

########################################################################
#                                                                      #
#      OPTIONS AND ARGUMENTS                                           #
#                                                                      #
########################################################################
# The following block concerns:
#   * Definitions of arguments
#   * Parsing of arguments
# Helper methods for command-line interface:


# Define possible named command line options
def args_define_options(parser=None):
    if parser == None:
        # The argparse is really only needed for the command line interface.
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=argparse_description)
    #
    # We need to define our own version option, but ONLY for "called as a script":
    if __name__ == '__main__':
        parser.add_argument('--version', action='version',
                            version='%(prog)s {}'.format(version),
                            help='show the version number and exit')
    parser=ts.args_define_options(parser)
    #parser=tsnc.args_define_options(parser)
    #
    # This filter's own optional arguments (aka "options"):
    parser.add_argument("--figsize",dest="figsize", metavar="<XSIZE,YSIZE>",
                        type=ts._check_float_comma_float,
                        default=None, action='store',
                        help='Explicitly set size of plot (in inches).')
    parser.add_argument("--output",dest="output", metavar="<TGTFIL>",
                        default=None, action='store',
                        help='Write plot to file TGTFIL rather than interactive to screen')
    return parser

# Companion parser for args_define_options
def args_parse_options(parsed_args):
    # Let tsnc get its own options:
    ts.args_parse_options(parsed_args)
    #tsnc.args_parse_options(parsed_args)
    #
    global figsize
    if not parsed_args.figsize is None:
        # Convert string xval,yval to array of floats
        xval,yval=parsed_args.figsize.split(",")
        figsize=(np.float64(xval),np.float64(yval))
    # This module's own optional arguments
    return

# Define postional arguments as: <IN1> [<IN2>] .. [PNG]
def args_define_positional(parser=None):
    # The argparse is really only needed for the command line interface.
    if parser == None:
        import argparse
        global argparse_description
        parser = argparse.ArgumentParser(description=description)
    #
    # Positional arguments - one or more.
    parser.add_argument('input', nargs='+', metavar="<INPUT>", type=_check_input,
                        help='Input definition. One or more specifications as: NCFIL[:MARKER[:LABEL]]')
    return parser
def args_parse_positional(parsed_args):
    # WHAT TO DO? CHECK THEM?
    #tsnc.args_parse_positional(parsed_args)
    return

def _check_input(inpstr):
    # inp is a string, count number of colons, must be 0-2:
    ncol=inpstr.count(':')
    if ncol>2:
        raise argparse.ArgumentTypeError("%s is not a valid INPUT: Expected 0-2 colons, not {} as in '{}'".format(ncol,inpstr))
    # Add empty segments if we have too few, this is only to make inputstr consistent,
    # so we know it will split to three elements later:
    outstr=inpstr
    if ncol==0:
        outstr+="::"
    elif ncol==1:
        outstr+=":"
    # Test-split - we will really only use the filename:
    filename,markr,labl=outstr.split(':',3)
    if not os.path.isfile(filename):
        raise argparse.ArgumentTypeError("No such file: '{}' in argument '{}'".format(filename,inpstr))
    #
    return outstr

# END      OPTIONS AND ARGUMENTS                                       #
########################################################################


# Stuff to do only if in command-line mode:
def main():
    # Deal with options:
    parser = args_define_options()
    parser = args_define_positional(parser)
    args   = parser.parse_args()
    args_parse_options(args)
    args_parse_positional(args)
    #
    # Go over the inputs
    import matplotlib.pyplot as plt
    nlabls=0
    global figsize
    fig, ax = plt.subplots(figsize=figsize)
    plt.xticks(rotation=30) # Would like to operate directly on ax., but cant figure out how.
    for inpstr in args.input:
        filename,markr,labl=inpstr.split(':',3)
        print(" loading {}".format(filename))
        if markr=='':
            markr=None
        if labl=='':
            labl==None
        tsnc.ncsrc=filename
        tsnc.assert_file_is_present()
        tsnc.assert_file_is_ncts()
        #     Assure that time series is monotone:
        datevals,varvals,longname=tsnc.ncfil_load(varunits='m')
        ts.assert_monotone(datevals)
        # Deal with any tsSimple options:
        datevals,varvals=ts.apply_options(datevals,varvals)
        datevals,varvals=ts.remove_nans(datevals,varvals)
        #
        # String together non-eqidistant pieces using a single nan-value between each:
        datevals,varvals=ts.nanjoin_pieces(datevals,varvals)
        # Possibly add NaNs in "holes"?
        if markr:
            ax.plot(datevals,varvals,markr,label=labl)
        else:
            ax.plot(datevals,varvals,label=labl)
        if labl:
            nlabls+=1
    print("Assigning xylabels")
    ax.set_ylabel('elev [m]')
    ax.set_xlabel("Time")
    if nlabls>0:
        print("Adding legend")
        ax.legend()
    if args.output:
        print("Saving figure {}".format(args.output))
        fig.savefig(args.output)
        plt.close(fig)
    else:
        print("Showing figure")
        plt.show()
    exit(0)

if __name__ == '__main__':
    main()
