# tsElev

Python scripts/modules work with time series of elevations (or other variables) measured or modelled. 
 * Quality-assert/validate and extract parts of time series
 * Save to and read from standardized netcdf format elev[time]
 * Low/high-pass filtering of data

The module will process time series data of the form elev(time), 
with equidistant sampling, ie. it is often assumed that
```math
  \eta_i = \eta(t_i) \ , \quad \text{where} \ \ t_{i+1}=t_i + dt
```
Especially when data stems from observations (tide gauges), 
there may be "holes" or "missing values" in the time series, 
and in this case it is advisible to pre-process data to make data 
as consistent as possible.
There are methods to deal with data which are not completely 
equidistantly sampled in time.

Most/all python-files can be called directly from the command line 
or `#include`d into your own scripts to add functionality.
In script-mode, the `-h` (or `--help`) switch should show you the help/usage of 
that particular script. 

Additionally, `-v` (or `--verbose`) and 
`--version` should be generably understood. 
Multiple specification of verbose, as in `-vvv`, results in increased output levels.

## Installation

```
git clone https://gitlab.com/FCOO/tsElev <TARGETDIR>/tsElev
```

## tsSimple.py

This is the "back bone" of the project, and provide some simple 
structure and methods. 

A "time series" is defined as two vectors (numpy ndarrays): `timevals,varvals`. 
`timevals` will be an array of `datetime.datetime` objects, while 
`varvals` will consist of `numpy.float64`s aka `float`s. 

The module defines methods to:
 - round time to nearest second (or other unit)
 - deal with duplicate values
 - delete parts of the time series
 - convert partially equidistant time series to equidistant by adding NaNs
 - remove NaNs to create partially equidistant series
 - return the equisdistant pieces of a time series


## tsNetcdf.py

Write and read standardized NetCDF format.
This ensures that data from many provides can be processed in a unified way.

## tsFilter.py

Provides methods to (Finite Impulse Response)-filter data. 
This normally requires that the input data are equidistant in time. 
However, it is possible to filter each equidistant sub-series separately. 
Note, that filtering normally invalidates a number of samples at each end 
of the time series. Thus, "holes" may "grow larger" when filtering is applied.

A number of different filters are implemented. 
Most implemented filters are low-pass filter, but the remainder (the high-pass part) 
can be saved in addition to (or in place of) the low-pass part.

## tsDespike.py

Contains tool for detecting and removing spikes in time series (of observed elevation) data.

The tool can be used in the process of screening observation data by deleting spikes
There are routines to delete values that are stucked at a fixed value for a longer time, 
to delete outliers (values that are outside hard limits), 
and to interpolate small holes.
Also, there is the posibility to output the smoothed (Savitzky-Golay filtered) series.

A unique, monotonic time axis with equidistant time sampling is assumed,
and will be enforced in the `main` script.

### Usage
The python-file can be called directly from the command line or included into other scripts to add functionality.
The module should run with both python2 and python3.

Example of an application of the routines is found in the `main` which may be called like this from bash:

~~~~bash
tsopt="-O -vvv --roundtime=m"
opt="--doplot --smallholes=18 --sgwindow=25 --delhardminlimit=-3. --delhardmaxlimit=4.5 --spikescale=2.5"
python tsDespike.py infile.nc outfile.nc $tsopt $opt
~~~~

where `tsopt` is a list of options for the more basic tsElev methods, while `opt` is a list of options specific for this de-spike tool.

## tsJoin.py

Join two time series, copying data from the second into the first.

Presently, there are two modes, defined by `--join_method`:

 - `--join_method=joinnans`, `--join_method=nanunion`: Copy all NaN-values from secondary to primary series. This mode is meant to explicitly invalidate some periods in the primary series, based on info from the secondary series.

 - `--join_method=fixnans`, `--join_method=nanintersection`: Replace NaN values in the primary series with (identical-time) data from the secondary series. This may be used as if the secondary series represent a "secondary gauge", ie. data which are less trustworthy than the primary, but still better than no data.

### Usage

The python-file should be called directly from the command line, ie. is not to be included from other scripts.
This file does not define any new re-uasable routines: it provides and alternative interface to methods defined 
by tsSimple (and tsNetcdf).

~~~~bash
tsopt="-O -vv --roundtime=m"
opt="--joinmethod=fixnans"
python tsJoin.py $tsopt $opt primaryIn.nc secondaryIn.nc joined.nc 
~~~~

## tsDegap.py

Remove "gaps" (missing values) in a time series, by contructing a polynomial, which interpolates the points just before and after each gap.
The polynomium order is found adaptively for each gap - up to a maximum (`--gappolyorder`) specified on the command line. 
Both the maximum gap size (`--gapmaxwidth` in seconds) to interpolate - and the width of the interpolation window to use must be specified on the command line. 

The size of the interpolation window can be given as one of (mandatory, but mutually exclusive options):

 - `--gappolywidth`: specify full window in absolute terms (seconds)

 - `--gappolysize`: specify number of seconds before and after the gap

 - `--gappolynsidepts`: specify umber of points before and after the gap ( as integer)

In addition, it there is a "fuzz parameter" `--gappolynshift`, which it is strongly suggested to use - especially or highorder polynomials.
Specifyinge `nshift` will allow the use of (slightly) unsymmetric interpolation windows, and this may in some cases significantly improve the found solution.

For each gap, the script will examine different interpolating polynomials - varying the order and shifting the window - to lower the residual (between the interpolating polynomial and the values to interpolate), and maximizing the "smoothness" of the polynomial withing the gap.

The abs-sum of second, third or fourth polynomial derivative over the gap points is used as a proxy for the "non-smoothness". Normally, the second derivative is used, but a higher-order derivative can be selected by specifying `--gappolyminimizer`.

### Usage

The python-file should be called directly from the command line, or included from other scripts.
There are only a few new re-uasable routines.

~~~~bash
tsopt="-O -vv --roundtime=m"
opt="--gapmaxwidth=16200 --gappolyorder=9 --gappolynsidepts=7 --gappolynshift=2"
python tsDegap.py $tsopt $opt observations.nc filled.nc polynomials.nc 
~~~~

